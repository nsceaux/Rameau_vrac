\version "2.19.80"
\include "common.ily"
%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = " " }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #8
  \with-line-width-ratio#0.7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 1)
  \table-of-contents
}

\bookpart {
  \act "Dardanus"
  \pieceToc\markup\wordwrap {
    Chaconne
  }
  \includeScore "Dardanus/Chaconne"
}
\bookpart {
  \pieceToc\markup\wordwrap {
    Trois Songes, chœur : \italic { Par un sommeil agréable }
  }
  \includeScore "Dardanus/ParUnSommeilAgreable"
}
\bookpart {
  \pieceToc\markup\wordwrap {
    Anténor : \italic { Voici les tristes lieux } – \italic { Monstre affreux }
  }
  \includeScore "Dardanus/VoiciLesTristesLieux"
  \includeScore "Dardanus/MonstreAffreux"
}
\bookpart {
  \pieceToc\markup\wordwrap {
    Prologue – Tambourins
  }
  \includeScore "Dardanus/PrologueTambourinI" \noPageTurn
  \includeScore "Dardanus/PrologueTambourinII"
}
\bookpart {
  \act "Les Fêtes d’Hébé"
  \pieceToc\markup\wordwrap { Musette en Rondeau }
  \includeScore "LesFetesdHebe/MusetteEnRondeau"
}
\bookpart {
  \pieceToc\markup\wordwrap { Tambourin en Rondeau }
  \includeScore "LesFetesdHebe/TambourinEnRondeau"
}
\bookpart {
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Suivez les loix }
  }
  \includeScore "LesFetesdHebe/SuivezLesLoix"
}
\bookpart {
  \act "Naïs"
  \pieceToc "Ouverture"
  \includeScore "Nais/ouverture"
}
\bookpart {
  \act "Les Paladins"
  \pieceToc "Entrée très gaye de Troubadours"
  \includeScore "LesPaladins/EntreeTresGayeDeTroubadours"
}
\bookpart {
  \act "Platée"
  \pieceToc\markup\wordwrap {
    La Folie : \italic { Formons les plus brillants concerts }
    – \italic { Au langueurs d’Apollon }
  }
  \includeScore "Platee/FormonsLesPlusBrillantsConcerts"
}
\bookpart {
  \act "Le Temple de la Gloire"
  \pieceToc "Ouverture"
  \includeScore "LeTempleDeLaGloire/ouverture"
}
\bookpart {
  \pieceToc "Air tendre pour les Muses"
  \includeScore "LeTempleDeLaGloire/AirTendrePourLesMuses"
}
\bookpart {
  \act "Zaïs"
  \pieceToc "Ouverture"
  \includeScore "Zais/ouverture"
}
\bookpart {
  \act "Zoroastre"
  \pieceToc "Air tendre en rondeau"
  \includeScore "Zoroastre/AirTendreEnRondeau"
}
\bookpart {
  \pieceToc "Air grave pour les esprits infernaux"
  \includeScore "Zoroastre/AirGravePourLesEspritsInfernaux"
}
