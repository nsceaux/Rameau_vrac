\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Haute-contre" } <<
      \global \keepWithTag #'haute-contre \includeNotes "parties"
    >>
    \new Staff \with { instrumentName = "Taille" } <<
      \global \keepWithTag #'taille \includeNotes "parties"
    >>
  >>
  \layout { indent = \largeindent }
}
