\score {
  \new GrandStaff <<
    \new Staff <<
      \global \keepWithTag #'hautbois1 \includeNotes "hautbois"
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'hautbois2 \includeNotes "hautbois"
    >>
  >>
  \layout { }
}
