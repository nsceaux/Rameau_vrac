\score {
  \new GrandStaff <<
    \new Staff <<
      \global \keepWithTag #'violon1 \includeNotes "violon"
      $(or (*score-extra-music*) (make-music 'Music))
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'violon2 \includeNotes "violon"
    >>
  >>
  \layout { }
}
