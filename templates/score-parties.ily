\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Haute-contre" } <<
      \global \includeNotes "haute-contre"
    >>
    \new Staff \with { instrumentName = "Taille" } <<
      \global \includeNotes "taille"
    >>
  >>
  \layout { indent = \largeindent }
}
