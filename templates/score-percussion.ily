\score {
  \new StaffGroup <<
    \new Staff \with { \tinyStaff } <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #(*tag-notes*) \includeNotes #(*note-filename*)
    >>
    \new Staff \with { \tinyStaff } <<
       \keepWithTag #(*tag-global*) \global
      \includeNotes "basse"
    >>
    \new Staff \with {
      \remove "Clef_engraver"
      \remove "Key_engraver"
    } << \keepWithTag #(*tag-global*) \global >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
    ragged-last = #(*score-ragged*)
  }
}
