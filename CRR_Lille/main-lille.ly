\version "2.19.80"
\include "common.ily"
%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = \markup\wordwrap-center\fontsize#-4 {
      Extraits de \italic {
        Castor et Pollux,
        Dardanus,
        Les Fêtes d’Hébé,
        Le Temple de la Gloire,
        Zoroastre
      }
    }
  }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #8
  \with-line-width-ratio#0.7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 1)
  \table-of-contents
}

\bookpart {
  \act "Castor et Pollux"
  \pieceToc\markup\wordwrap { Tristes apprêts }
  \includeScore "CastorEtPollux/TristesApprets"
}
\bookpart {
  \pieceToc\markup\wordwrap { Ritournelle (Acte V) }
  \includeScore "CastorEtPollux/RitournelleActeV"
}

\bookpart {
  \act "Dardanus"
  \pieceToc\markup\wordwrap { Bruit de guerre }
  \includeScore "Dardanus/BruitDeGuerre"
}
\bookpart {
  \pieceToc\markup\wordwrap {
    Trois Songes, chœur : \italic { Par un sommeil agréable }
  }
  \includeScore "Dardanus/ParUnSommeilAgreable"
}
\bookpart {
  \pieceToc\markup\wordwrap {
    Prologue – Tambourins
  }
  \includeScore "Dardanus/PrologueTambourinI" \noPageTurn
  \includeScore "Dardanus/PrologueTambourinII"
}
\bookpart {
  \act "Les Fêtes d’Hébé"
  \pieceToc\markup\wordwrap { Musette en Rondeau }
  \includeScore "LesFetesdHebe/MusetteEnRondeau"
}
\bookpart {
  \pieceToc\markup\wordwrap { Tambourin en Rondeau }
  \includeScore "LesFetesdHebe/TambourinEnRondeau"
}
\bookpart {
  \act "Le Temple de la Gloire"
  \pieceToc "Ouverture"
  \includeScore "LeTempleDeLaGloire/ouverture"
}
\bookpart {
  \pieceToc "Air tendre pour les Muses"
  \includeScore "LeTempleDeLaGloire/AirTendrePourLesMuses"
}
\bookpart {
  \act "Zoroastre"
  \pieceToc "Air tendre en rondeau"
  \includeScore "Zoroastre/AirTendreEnRondeau"
}
