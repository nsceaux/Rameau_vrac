\version "2.19.80"
\include "common.ily"
%% Title page
\bookpart {
  \paper { #(define page-breaking ly:page-turn-breaking) }
  \header {
    title = \markup\wordwrap-center\fontsize#-4 {
      Extraits de \italic {
        Castor et Pollux,
        Dardanus,
        Les Fêtes d’Hébé,
        Le Temple de la Gloire,
        Zoroastre
      }
    }
  }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #8
  \with-line-width-ratio#0.7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 1)
  \table-of-contents
}

\act "Castor et Pollux"
\pieceToc\markup\wordwrap { Tristes apprêts }
\includeScore "CastorEtPollux/TristesApprets"

\pieceToc\markup\wordwrap { Ritournelle (Acte V) }
\includeScore "CastorEtPollux/RitournelleActeV"

\act "Dardanus"
\pieceToc\markup\wordwrap { Bruit de guerre }
\includeScore "Dardanus/BruitDeGuerre"

\pieceToc\markup\wordwrap {
  Trois Songes, chœur : \italic { Par un sommeil agréable }
}
\includeScore "Dardanus/ParUnSommeilAgreable"

\pieceToc\markup\wordwrap { Prologue – Tambourins }
\includeScore "Dardanus/PrologueTambourinI" \noPageTurn
\includeScore "Dardanus/PrologueTambourinII"

\act "Les Fêtes d’Hébé"
\pieceToc\markup\wordwrap { Musette en Rondeau }
\includeScore "LesFetesdHebe/MusetteEnRondeau"

\pieceToc\markup\wordwrap { Tambourin en Rondeau }
\includeScore "LesFetesdHebe/TambourinEnRondeau"

\act "Le Temple de la Gloire"
\pieceToc "Ouverture"
\includeScore "LeTempleDeLaGloire/ouverture"

\pieceToc "Air tendre pour les Muses"
\includeScore "LeTempleDeLaGloire/AirTendrePourLesMuses"

\act "Zoroastre"
\pieceToc "Air tendre en rondeau"
\includeScore "Zoroastre/AirTendreEnRondeau"
