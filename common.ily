\header {
  composer = "Jean-Philippe RAMEAU"
  copyrightYear = "2011"
}

\paper {
  %% de la place entre dernier système et copyright
  last-bottom-spacing.padding = #3
}

#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'print-footnotes (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
%% Staff size
#(set-global-staff-size
  (cond ((symbol? (ly:get-option 'part)) 18)
        ((eqv? #t (ly:get-option 'urtext)) 14)
        (else 16)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"

\setPath "."
\opusPartSpecs
#'((dessus "Violons" () (#:score-template "score-violons"))
   (violon1 "Violons I" () (#:notes "violon" #:tag-notes violon1))
   (violon2 "Violons II" () (#:notes "violon" #:tag-notes violon2))
   (flutes "Flûtes" () (#:notes "flute" #:tag-notes flutes))
   (hautbois "Hautbois" () (#:notes "hautbois" #:tag-notes hautbois))
   (bassons "Bassons" ((basses "Basses"))
            (#:notes "basson" #:tag-notes bassons #:clef "bass"))
   (cors "Cors" () (#:notes "cor" #:tag-notes cors #:tag-global ()))
   (trompette "Trompettes" () (#:notes "trompette" #:tag-global ()))
   (trombones "Trombones" () (#:score "score-trombones"))
   (percussions "Percussions" () (#:notes "timbales" #:clef "bass"))
   (parties "Parties" () (#:notes "parties" #:clef "alto"))
   (haute-contre "Hautes-contre" () (#:notes "parties" #:tag-notes haute-contre #:clef "alto"))
   (taille "Tailles" () (#:notes "parties" #:tag-notes taille #:clef "alto"))
   (basses "Basses" ()
           (#:notes "basses" #:tag-notes basse #:clef "bass")))

\opusTitle "Rameau"

\paper {
  bookTitleMarkup = \markup\fill-line\fontsize#1 {
    \on-the-fly #(lambda (layout props arg)
                   (if (*part*)
                       (interpret-markup layout props (markup (*part-name*)))
                       empty-stencil)) \null
    \override #'(baseline-skip . 8) \center-column {
      \fontsize#4 \bold\override #'(baseline-skip . 4) \fromproperty #'header:title
      \fromproperty #'header:subtitle
    }
    \fromproperty #'header:composer
  }
}

\layout {
  indent = #(if (symbol? (ly:get-option 'part))
                smallindent
                largeindent)
}
