\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*5 s2 \bar "" \break s4 s2.*7\break s2.*8\break
      }
    >>
  >>
  \layout { indent = \smallindent }
  \midi { }
}
