\clef "vhaute-contre" <>^\markup\character Antiochus sib8 do' |
do'2( re'8) mib' |
re'4 sol8 r do' re' |
do'4.( sib16[\trill la]) sib4 |
la2.\trill |
la8. sib16 \appoggiatura sib8 do'4. re'8 |
fad4\trill fad8 r re'4~ |
re'4. fad8 sol do' |
do'4~ do'4.( sib16\trill[ la]) |
sib4( la4.)\trill sol8 |
sol2. |
re'8. mi'16 mi'4( fa'8.) sol'16 |
fad'4\trill \appoggiatura mi'8 re' r sol'4~ |
sol'4. sib8 la re' |
re'4~ re'4.( do'16[\trill sib]) |
sib4( la4.)\trill sol8 |
sol4\fermata re'4. do'16[ sib] |
\appoggiatura sib8 do'2 fa'4 |
sol'2 la'8 sib' |
la'2\trill fa'8 sib' |
re'2. |
mi'8. sol'16 fa'4( mi'8)\trill mi' |
dod'4\trill dod'8 r la'4~ |
la'4. dod'8 re' sol' |
sol'4~ sol'4.( fa'16\trill[ mi']) |
fa'4( mi'4.\trill) re'8 |
re'2
