\clef "basse" sol8. la16 |
la2( sib8.) do'16 |
sib4 sib,8 r mib8. re16 |
sol2 sol,4 |
re2. |
fad,8. sol,16 \appoggiatura sol,8 la,4. sib,8 |
do2~ do8( sib,16\trill la,) |
\appoggiatura la,8 sib,4. do8 sib,8. fad,16 |
sol,4~ sol,4. do,8 |
re,2. |
sol, |
sib4 la4.\trill sol8 |
re'2 sib,4 |
mib4. re8 do8.\trill sib,16 |
do4~ do4.\trill( sib,16 do) |
re4 re,2 |
sol,4\fermata sib2~ |
sib la4 |
sol do'2 |
fa r8 fa |
sib2. |
sol8. sib16 la4 sol8\trill fa16 mi |
la2 r4 |
r8 mi? sol4 fa8. dod16 |
re4~ re4. sol,8 |
la,2. |
re,2
