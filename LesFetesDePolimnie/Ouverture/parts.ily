\piecePartSpecs
#`((flutes #:notes "flute")
   (hautbois #:notes "dessus")
   (trompette #:instrument "Trompette en ré")
   (bassons #:notes "basse" #:clef "tenor")
   (percussions #:score "score-timbales")
   
   (violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (parties)
   (haute-contre)
   (taille)
   (basses #:notes "basse"))
