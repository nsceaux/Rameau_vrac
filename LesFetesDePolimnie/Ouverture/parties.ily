\clef "haute-contre" r2 re'~ |
re' sol'~ |
sol'1~ |
sol'2 mi''~ |
mi''4 la' re'' re' |
re'2 mi'~ |
mi' fad' |
sol' fad' |
mi' r |
r la~ |
la re'~ |
re'1~ |
re'2 si'~ |
si'4 mi' la' la |
la2 si~ |
si dod' |
re' si4 mi'~ |
mi'1 |
r8 re'16 re' re' re' re' re' re'8 fad'16 fad' fad' fad' fad' fad' |
fad'8 la'16 la' la' la' la' la' la'8 la' re''4 |
dod''8\trill la16 la la la la la la8 dod'16 dod' dod' dod' dod' dod' |
dod'8 mi'16 mi' mi' mi' mi' mi' mi'8 mi' sol'4 |
fad'8\trill fad'16 fad' fad' fad' fad' fad' fad'8 la'16 la' la' la' la' la' |
la'8 fad''16 fad'' fad'' fad'' fad'' fad'' fad''8 fad'' la'4 |
re'2 r8 re'16 re' re' re' re' re' |
re'4 r r8 re'16 re' re' re' re' re' |
re'4 r r8 re'16 re' re' re' re' re' |
re'4 r r8 re''16 re'' re'' re'' re'' re'' |
la'2 la'4. la8 |
la2 la4 r |
la r la r |
la r r8 mi''16 mi'' mi'' mi'' mi'' mi'' |
la'4 r r8 la'16 la' la' la' la' la' |
la'4 r r8 fad''16 fad'' fad'' fad'' fad'' fad'' |
si'4 r r8 sold'16 sold' sold' sold' sold' si' |
mi'2 r4 mi' |
fad'2 si'~ |
si'4.( dod''16 si') la'8 r r la' |
la'4.( si'16 la') re''4 si' |
mi' dod''8 si' si' mi'' mi'4 |
mi'8 la16 la la la la la la8 dod'16 dod' dod' dod' dod' dod' |
dod'8 mi'16 mi' mi' mi' mi' mi' mi'8 mi' sol'4 |
fad'2\trill r8 la' la'4 |
la' r r8 fad' fad'4 |
fad' r r8 fad' fad'4 |
fad' r r2 |
R1*2 |
r2 r8 si16 si si si si si |
si8 red'16 red' red' red' red' red' red'8 fad' mi'\trill red' |
mi' red' mi' red' mi' red' mi' red' |
mi' r r4 r2 |
r2 r8 la16 la la la la la |
la8 dod'16 dod' dod' dod' dod' dod' dod'8 mi' re'\trill dod' |
re' dod' re' dod' re' dod' re' dod' |
re' r r4 r r8 la' |
re'2 r |
re' r |
re' r |
re' r4 r8 la' |
la'2 la' |
la8 la16 la la la la la la4 r |
R1*2 |
la'16 la' la' la' la' la' la' la' la' la' la' la' la' la' la' la' |
fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' |
re' re' re' re' re' re' re' re' re' re' re' re' re' re' re' re' |
re' re' re' re' re' re' re' re' la'4 mi' |
fad' la'8 re'' re''4 dod''\trill |
re''2 r |
r8 re'16 re' re' re' re' re' re'8 fad'16 fad' fad' fad' fad' fad' |
fad'8 la'16 la' la' la' la' la' la'8 re''16 re'' re'' re'' re'' re'' |
re''8 fad'16 fad' fad' fad' fad' fad' fad' fad' fad' fad' fad'8 la' |
la' re'16 re' re'8 re'16 re' re'8 la16 la la la la la |
la1\trill |
