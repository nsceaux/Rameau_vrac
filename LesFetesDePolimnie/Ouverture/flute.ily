\clef "dessus" R1*4 |
r2 re'''~ |
re'''4.*13/12 dod'''32 si'' la'' sol''4 fad'' |
mi'' la''2. |
r4 la''2. |
r4 la''2. |
R1*4 |
r2 la''~ |
la''4.*13/12 sol''32 fad'' mi'' re''4 dod'' |
si' mi''2 mi''4 |
fad''2 mi'' |
dod''1\trill |
%%
R1*4 |
re''2 fad'' |
la''~ la''8 la'' si''4 |
si''2 r8 re''' re'''4 |
re'''2 r8 re''' re'''4 |
re'''2 r8 re''' re'''4 |
re'''2 r4 r8 la'' |
la''2 la''4. re'''8 |
dod'''2\trill r8 dod''' re'''4 |
dod'''8\trill dod''' re'''4 dod'''8\trill dod''' re'''4 |
dod'''2\trill r8 dod''' dod'''4\trill |
re'''2 r8 re''' re'''4 |
red'''2\trill r8 red''' red'''4\trill |
mi'''2 r8 re''' re'''4 |
dod'''2\trill r4 mi''' |
mi'''4.( re'''8) re'''2~ |
re''' dod'''~ |
dod''' si''~ |
si''4 la''8 re'''~ re''' dod''' si'' mi''' |
dod'''2\trill r |
R1 |
r16 la'' re''' la'' re''' la'' re''' la'' re'''4 r |
r16 la'' re''' la'' re''' la'' re''' la'' dod'''4 r |
r16 fad'' dod''' fad'' dod''' fad'' dod''' fad'' si''4 r |
r16 fad'' si'' fad'' si'' fad'' si'' fad'' lad''4 r |
R1 |
r8 <<
  { dod'''8 \appoggiatura dod'''8 re'''4 dod'''8 } \\
  { lad''8 \appoggiatura lad''8 si''4 lad''8 }
>> r8 r4 |
R1 |
r2 r4 si'' |
si''2 si''~ |
si''4 si'' mi'''2 |
R1 |
r2 r4 la'' |
la''2 la''~ |
la''4 la'' re'''2 |
r4 r8 re''' re'''2 |
r4 r8 re''' re'''2 |
r4 r8 re''' re'''2 |
r4 r8 re''' re'''4. la''8 |
la''2 la''4. re'''8 |
dod'''2\trill r |
r r8 la'' re'''4 |
dod'''2\trill r |
<<
  { re'''2. dod'''4~ |
    dod''' si''2 la''4~ |
    la'' sol''2 fad''4 |
    si''2 mi''4 la''~ |
    la''8 si''16 dod''' re'''8 re'' fad''4 mi''\trill |
    re'' } \\
  { fad''2 mi'' |
    re'' dod'' |
    si' la'4 re'' |
    re''2 mi''4 la''~ |
    la''8 si''16 dod''' re'''8 re'' re''4 dod''\trill |
    re''4 }
>> r4 r2 |
R1*2 |
r8 re'''16 re''' re''' re''' re''' re''' re''' re''' re''' re''' re'''8 dod'''\trill |
re'''2 fad''4 fad'' |
fad''1\trill |
