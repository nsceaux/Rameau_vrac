\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Trompette en ré" } <<
      \keepWithTag #'() \global
      \keepWithTag #'trompettes \includeNotes "trompette"
    >>
    \new Staff \with { instrumentName = "Tymbales" } <<
      \keepWithTag #'() \global \includeNotes "timbales"
    >>
  >>
  \layout { indent = \largeindent }
}
