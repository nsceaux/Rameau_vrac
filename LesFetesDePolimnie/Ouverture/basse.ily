<<
  \tag #'bassons {
    \clef "petrucci-f/tenor" R1 |
    mi'1~ |
    mi'~ |
    mi'2. mi'4 |
    la2 fad'~ |
    fad' mi'4 re' |
    la2 r | \clef "bass"
  }
  \tag #'basse {
    \clef "basse" r2 re~ |
    re1~ |
    re |
    dod |
    re2 fad, |
    sol,1 |
    la,2 r |
  }
>>
la,2 r |
la, r |
<<
  \tag #'bassons {
    R1 |
    si1~ |
    si~ |
    si2. si4 |
    mi2 dod'~ |
    dod' si4 la |
    mi2 la |
    re mi |
  }
  \tag #'basse {
    r2 la,~ |
    la,1~ |
    la, |
    sold, |
    la,2 dod, |
    re,1 |
    mi,2 la, |
    re, mi, |
  }
>>
la,1 |
\tag #'bassons \clef "tenor"
R1*2 |
r8 la16 la la la la la la8 dod'16 dod' dod' dod' dod' dod' |
dod'8 mi'16 mi' mi' mi' mi' mi' mi'8 mi' sol'4 |
fad'2\trill r8 re16 re re re re re |
re4 r r8
<<
  \tag #'bassons {
    re'8 re'4~ |
    re'8 re' re' re' re'4 r |
    r8 re' re' re' re'4 r |
    r8 re' re' re' re'4 r |
    r8 re' re' re' re'4 \clef "bass" r8 re' |
  }
  \tag #'basse {
    re8 re4 |
    sol8 sol,16 sol, sol, sol, sol, sol, sol,8 sol\doux sol4 |
    re'8 re,16\fort re, re, re, re, re, re,8 re\doux re4 |
    sol8 sol,16\fort sol, sol, sol, sol, sol, sol,8 sol\doux sol4 |
    re'8 re,16\fort re, re, re, re, re, re,4 r8 re' |
  }
>>
dod'4 la re' re |
<<
  \tag #'bassons {
    la2 la4 r |
    la r la r |
    la r r8
  }
  \tag #'basse {
    la8 la,16 la, la, la, la, la, la,4 r |
    r8 la,16 la, la, la, la, la, la,4 r |
    r8 la,16 la, la, la, la, la, la,8
  }
>> la16 la la la la la |
re'4 r r8 re16 re re re re re |
si,4 r r8 si16 si si si si si |
mi'4 r r8 mi16 mi mi mi mi mi |
dod2 r4 dod |
re2 si, |
dod la, |
si,2 sold, |
la,8 si, dod re mi la, mi mi, |
<<
  \tag #'bassons {
    la,8 \clef "tenor" la16 la la la la la la8 dod'16 dod' dod' dod' dod' dod' |
    dod'8 mi'16 mi' mi' mi' mi' mi' mi'8 mi' sol'4 |
    fad'2\trill r8 fad' fad'4\trill |
    mi'4 r r8 mi' mi'4\trill |
    re' r r8 re' re'4\trill |
    dod' r r2 |
    R1*2 |
    r2 r8 si16 si si si si si |
    si8 red'16 red' red' red' red' red' red'8 fad' mi'\trill red' |
    mi' red' mi' red' mi' red' mi' red' |
    mi' r r4 r2 |
    r2 r8 la16 la la la la la |
    la8 dod'16 dod' dod' dod' dod' dod' dod'8 mi' re'\trill dod' |
    re' dod' re' dod' re' dod' re' dod' |
    re' r r4 r2 |
    \clef "bass" r8 re' re' re' r2 |
    r8 re' re' re' r2 |
    r8 re' re' re' r2 |
    r8 re' re' re' r4
  }
  \tag #'basse {
    la,4 r r8 la16 la la la la la |
    la8 dod'16 dod' dod' dod' dod' dod' dod'8 dod' dod'4\trill |
    re'2 r8 re re4 |
    la4 r r8 la lad4\trill |
    si r r8 si, si,4 |
    fad4 r r2 |
    R1*3 |
    r8 si,16 si, si, si, si, si, si,4 r8 si, |
    mi si, mi si, mi si, mi si, |
    mi r r4 r2 |
    R1 |
    r8 la,16 la, la, la, la, la, la,4 r8 la, |
    re la, re la, re la, re la, |
    re re,16 re, re, re, re, re, re,8 r r re |
    sol sol,16 sol, sol, sol, sol, sol, sol,4 r8 sol\doux |
    re' re,16\fort re, re, re, re, re, re,4 r8 re\doux |
    sol sol,16\fort sol, sol, sol, sol, sol, sol,4 r8 sol\doux |
    re'8 re,16\fort re, re, re, re, re, re,4
  }
>> r8 re' |
dod'4 la re' re |
la8 <<
  \tag #'bassons {
    la16 la la la la la la4 r |
    R1*2 |
  }
  \tag #'basse {
    la,16 la, la, la, la, la, la,4 r |
    r8 la,16 la, la, la, la, la, la,4 r |
    r8 la,16 la, la, la, la, la, la,4 r |
  }
>>
re16 re re re re re re re la la la la la la la la |
si si si si si si si si fad fad fad fad fad fad fad fad |
sol sol sol sol sol sol sol sol re re re re re re re re |
sol sol sol sol sol sol sol sol sol sol sol sol sol sol sol sol |
fad4. sol8 la4 la, |
re r r2 |
<<
  \tag #'bassons {
    \clef "tenor" r8 re'16 re' re' re' re' re' re'8 fad'16 fad' fad' fad' fad' fad' |
    fad'8 la16 la la la la la la8 re'16 re' re' re' re' re' |
    re'8 fad'16 fad' fad' fad' fad' fad' fad' fad' fad' fad' fad'8 la |
    \clef "bass" re8 re16 re re8 re16 re re8 la16 la la la la la |
    la1\trill |
  }
  \tag #'basse {
    r2 r8 re'16 re' re' re' re' re' |
    re'8 re16 re re re re re re8 re16 re re re re re |
    re8 re16 re re re re re re re re re re8 la, |
    re re,16 re, re,8 re,16 re, re,8 re,16 re, re, re, re, re, |
    re,1\trill |
  }
>>
