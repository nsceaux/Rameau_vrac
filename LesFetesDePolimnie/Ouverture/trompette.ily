\clef "dessus" \transposition re
R1*6 |
r2 mi''4 do'' |
re''2 mi''4 do'' |
re''2 r |
R1*9 |
%%
r8 do'16 do' do' do' do' do' do'8 mi'16 mi' mi' mi' mi' mi' |
mi'8 sol'16 sol' sol' sol' sol' sol' sol'8 sol' do''4 |
sol'4 r r2 |
r8 sol'16 sol' sol' sol' sol' sol' sol'8 sol' sol'4 |
mi'2\trill r2 |
R1*6 |
r2 r8 re''8 mi''4 |
re'' r4 r2 |
r2 r8 re'' fa''4 |
mi''2\trill r8 mi'' mi''4 |
mi''2\trill r8 mi'' sol''4 |
fad''2\trill r8 re'' re''4 |
sol''2 r |
R1*5 |
r2 r8 sol'16 sol' sol' sol' sol' sol' |
sol'4 r r2 |
R1*2 |
r2 r8 mi''16 mi'' mi'' mi'' mi'' mi'' |
mi''8 r r4 r8 mi''16 mi'' mi'' mi'' mi'' mi'' |
mi''8 r r4 r2 |
R1*7 |
r2 r4 r8 do'' |
do''2 r4 r8 do'' |
do''2 r4 r8 do'' |
do''2 r4 r8 do'' |
do''2 r4 r8 do'' |
re''4 sol' mi''4~ mi''8. sol''16 |
re''2\trill sol'4 r |
R1*7 |
r8 do'16 do' do' do' do' do' do'8 mi'16 mi' mi' mi' mi' mi' |
mi'8 sol'16 sol' sol' sol' sol' sol' sol'8 do''16 do'' do'' do'' do'' do'' |
do''8 mi''16 mi'' mi'' mi'' mi'' mi'' mi''8 sol''16 sol'' sol'' sol'' sol'' sol'' |
sol''2~ sol''4. sol''8 |
mi''\trill do'16 do' do'8 do'16 do' do'8 do'16 do' do' do' do' do' |
do'1\trill |
