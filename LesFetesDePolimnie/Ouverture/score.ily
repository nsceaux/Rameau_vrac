\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        instrumentName = "Flutes"
        shortInstrumentName = "Fl."
      } << \global \includeNotes "flute" >>
      \new Staff \with {
        instrumentName = "Hautbois"
        shortInstrumentName = "Htb."
      } << \global \keepWithTag #'hautbois \includeNotes "dessus" >>
      \new Staff \with {
        instrumentName = "Bassons"
        shortInstrumentName = "Bsn."
      } << \global \keepWithTag #'bassons \includeNotes "basse" >>
      \new Staff \with {
        instrumentName = "Trompette en ré"
        shortInstrumentName = "Tr."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'trompettes \includeNotes "trompette"
      >>
      \new Staff \with {
        instrumentName = "Tymbales"
        shortInstrumentName = "Tim."
      } << \keepWithTag #'() \global \includeNotes "timbales" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with {
        instrumentName = "Violons"
        shortInstrumentName = "Vln."
      } <<
        \new Staff << \global \keepWithTag #'violon1 \includeNotes "dessus" >>
        \new Staff << \global \keepWithTag #'violon2 \includeNotes "dessus" >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Hautes-contre Tailles }
        shortInstrumentName = \markup\center-column { H-c. T. }
      } << \global \keepWithTag #'parties \includeNotes "parties" >>
      \new Staff \with {
        instrumentName = "B.C"
        shortInstrumentName = "B."
      } <<
        \global \keepWithTag #'basse \includeNotes "basse"
        \origLayout {
          s1*10\break s1*8\pageBreak
          s1*6\break s1*4\pageBreak
          s1*5\break s1*5\pageBreak
          \grace s8 s1*5\break s1*6\pageBreak
          s1*6 s2 \bar "" \break s2 s1*4 s2 \bar "" \pageBreak
          s2 s1*4\break s1*4 s2 \bar "" \pageBreak
        }
      >>
    >>
  >>
  \layout { short-indent = 8\mm }
  \midi { }
}
