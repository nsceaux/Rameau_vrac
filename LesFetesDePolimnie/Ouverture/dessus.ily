\clef "dessus" <<
  \tag #'hautbois { R1*6 | r2 }
  \tag #'violon1 {
    R1*2 |
    si'1 |
    la'2 la''~ |
    la''4.*13/12 sol''32 fad'' mi'' re''4 dod'' |
    si'2. re''4 |
    dod''2\trill
  }
  \tag #'violon2 {
    R1*2 |
    r2 mi''~ |
    mi''4.*13/12 re''32 dod'' si' la'4 sol' |
    fad'2 la''~ |
    la''4 re'' sol''2~ |
    sol''
  }
>> re''4 si' |
dod''2 re''4 si' |
dod''2 r |
<<
  \tag #'hautbois { R1*9 }
  \tag #'violon1 {
    R1*2 |
    fad'1 |
    mi'2 mi''~ |
    mi''4.*13/12 re''32 dod'' si' la'4 sold' |
    fad'2. la'4 |
    sold'2 la'~ |
    la' sold'\trill |
    la'1 |
  }
  \tag #'violon2 {
    R1*2 |
    r2 si'~ |
    si'4.*13/12 la'32 sol' fad' mi'4 re' |
    dod'2 mi''~ |
    mi''4 la' re''2~ |
    re'' dod'' |
    si' si'\trill |
    la'1 |
  }
>>
%%
r8 re'16 re' re' re' re' re' re'8 fad'16 fad' fad' fad' fad' fad' |
fad'8 la'16 la' la' la' la' la' la'8 la' re''4 |
<<
  \tag #'hautbois { la'4 r r2 | r8 }
  \tag #'(violon1 violon2) {
    dod''16\trill si' dod'' re'' mi'' re'' dod'' si' la'8 sol'16 fad' mi' re' dod' si |
    la8
  }
>> la'16 la' la' la' la' la' la'8 la' la'4 |
fad'8\trill re''16 re'' re'' re'' re'' re'' re''8 fad''16 fad'' fad'' fad'' fad'' fad'' |
fad''8 la''16 la'' la'' la'' la'' la'' la''8 la'' fad''4\trill |
<<
  \tag #'hautbois {
    <<
      { si''2 \oneVoice r8 \voiceOne si''8 la''\trill sol'' |
        la''2 \oneVoice r8 \voiceOne la'' sol''\trill fad'' |
        si''2 \oneVoice r8 \voiceOne si'' la''\trill sol'' |
        la''2 \oneVoice r8 \voiceOne la'' sol''\trill fad'' |
        sol''2 fad''4\trill fad''8 la'' |
        mi''4\trill } \\
      { sol''2 s8 sol'' fad'' mi'' |
        fad''2 s8 fad'' mi'' re'' |
        sol''2 s8 sol'' fad'' mi'' |
        fad''2 s8 fad'' mi'' re'' |
        mi''2 re''4 re'' |
        la' }
    >> r4 r8 <<
      { mi''8 fad''4 | mi'' } \\ { la'8 re''4 | la'4 }
    >> r4 r8 <<
      { mi''8 fad''4 | mi'' } \\ { la'8\doux re''4 | la'4 }
    >> r4 r8 mi''8\fortSug sol''4 |
    fad''2\trill r8 fad'' fad''4 |
    si''2 r8 fad'' la''4 |
    sold''2\trill r8 mi'' mi''4 |
    la''2 r4 la'' |
    la''2~ la''4.( si''16 la'') |
    sold''8 si'' mi''2 la''4 |
    \appoggiatura sol''8 fad''4.( sold''16 la'') mi''4~ mi''16 re'' dod'' si' |
    dod''8 re'' mi'' fad'' sold'' la''4 sold''8\trill |
    la''2 r |
    R1 |
    r2 r8 fad' fad'4\trill |
    mi' r r8 mi' mi'4\trill |
    re' r r8 re' re'4\trill |
    dod' r r2 |
    r8 <<
      { dod''8 \appoggiatura dod'' re''4 dod''4 } \\
      { lad'8 \appoggiatura lad' si'4 lad' }
    >> r4 |
    R1*8 |
    r2 r4 r8 <<
      { la''8 |
        si''2 s8 si'' la''\trill sol'' |
        la''2 s8 la'' sol''\trill fad'' |
        si''2 s8 si'' la''\trill sol'' |
        la''2 s8 la'' sol''\trill fad'' |
        sol''2 } \\
      { fad''8 |
        sol''2 \oneVoice r8 \voiceTwo sol'' fad''\trill mi'' |
        fad''2 \oneVoice r8 \voiceTwo fad'' mi''\trill re'' |
        sol''2 \oneVoice r8 \voiceTwo sol'' fad''\trill mi'' |
        fad''2 \oneVoice r8 \voiceTwo fad'' mi''\trill re'' |
        mi''2 }
    >> re''4 si' |
    dod''8 dod''16 dod'' dod'' dod'' dod'' mi'' la'4 r |
    R1*2 |
    <<
      { re'''2. dod'''4~ |
        dod''' si''2 la''4~ |
        la'' sol''2 fad''4 |
        si''2 mi''4 la''~ |
        la''8 si''16 dod''' re'''8 re'' fad''4 mi''\trill | } \\
      { fad''2 mi'' |
        re'' dod'' |
        si' la'4 re'' |
        re''2 mi''4 la''~ |
        la''8 si''16 dod''' re'''8 re'' re''4 dod''\trill | }
    >>
    re''8 re'16 re' re' re' re' re' re'8 fad'16 fad' fad' fad' fad' fad' |
    fad'8 la'16 la' la' la' la' la' la'8 re''16 re'' re'' re'' re'' re'' |
    re''8 fad''16 fad'' fad'' fad'' fad'' fad'' fad''8 la''16 la'' la'' la'' la'' la'' |
    la''2~ la''4. la''8 |
    fad''8\trill re'16 re' re'8 re'16 re' re'8 re'16 re' re' re' re' re' |
    re'1\trill |
  }
  \tag #'(violon1 violon2) {
    \ru#4 { <si'' sol''>16 <re' sol> } <si'' sol''>8 r r4 |
    \ru#4 { <la'' fad''>16 re' } <la'' fad''>8 r r4 |
    \ru#4 { <si'' sol''>16 <re' sol> } <si'' sol''>8 r r4 |
    \ru#4 { <la'' fad''>16 re' } <la'' fad''>8 r r4 |
    \ru#4 { <sol'' mi''>16 la' } \ru#4 { <fad'' re''>16 la' } |
    <<
      \tag #'violon1 {
        mi''4 r r8 mi'' fad''4 |
        mi'' r r8 mi''\doux fad''4 |
        mi''8
      }
      \tag #'violon2 {
        la'4 r r8 la' re''4 |
        la' r r8 la'\doux re''4 |
        la'8
      }
    >> la''16\fort la'' mi'' mi'' dod'' dod'' la'4 r |
    r8 re''16 re'' la' la' fad' fad' re'4 r |
    r8 si''16 si'' fad'' fad'' red'' red'' si'4 r |
    r8 mi''16 mi'' si' si' sold' sold' mi'4 r |
    \ru#4 { la''16 mi'' la'' dod''' } |
    fad'' mi'' fad'' la'' fad'' mi'' fad'' la'' fad'' re'' fad'' si'' fad'' re'' fad'' si'' |
    mi'' re'' mi'' sold'' mi'' re'' mi'' sold'' mi'' dod'' mi'' la'' mi'' dod'' mi'' la'' |
    re'' dod'' re'' fad'' re'' dod'' re'' fad'' si'' la'' sold'' fad'' mi'' re'' dod'' si' |
    dod'' la' re'' la' mi'' la' fad'' la' sold'' mi'' la'' dod'' si'8.\trill la'16 |
    la'2 r |
    R1 |
    r16 la'' re''' la'' re''' la'' re''' la'' re'''4 r |
    r16 la'' re''' la'' re''' la'' re''' la'' dod'''4 r |
    r16 fad'' dod''' fad'' dod''' fad'' dod''' fad'' si''4 r |
    r16 fad'' si'' fad'' si'' fad'' si'' fad'' lad''4 r |
    R1*2 |
    r8 si16 si si si si si si8 red'16 red' red' red' red' red' |
    red'8 fad'16 fad' fad' fad' fad' fad' fad'8 la' sol'\trill fad' |
    sol' fad' sol' fad' sol' fad' sol' fad' |
    sol' r r4 r2 |
    r8 la16 la la la la la la8 dod'16 dod' dod' dod' dod' dod' |
    dod'8 mi'16 mi' mi' mi' mi' mi' mi'8 sol' fad'\trill mi' |
    fad' mi' fad' mi' fad' mi' fad' mi' |
    fad' r r4 r r8 <>^\markup\right-align "à 2 cordes" <fad'' la''>8 |
    \ru#4 { <sol'' si''>16 <re' sol> } <sol'' si''>8 r r4 |
    \ru#4 { <fad'' la''>16 re' } <fad'' la''>8 r r4 |
    \ru#4 { <sol'' si''>16 <re' sol> } <sol'' si''>8 r r4 |
    \ru#4 { <fad'' la''>16 re' } <fad'' la''>8 r r4 |
    \ru#4 { <mi'' sol''>16 la' } \ru#4 { <re'' fad''>16 la' } |
    <<
      \tag #'violon1 {
        mi''16 mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi''8 mi'' fad''4 |
        mi''4 r4 r8 mi''8 \appoggiatura mi'' fad''4 |
        mi''2\trill r |
        re'''16 re''' re''' re''' re''' re''' re''' re''' re''' re''' re''' re''' dod''' dod''' dod''' dod''' |
        dod''' dod''' dod''' dod''' si'' si'' si'' si'' si'' si'' si'' si'' la'' la'' la'' la'' |
        la'' la'' la'' la'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' fad'' fad'' fad'' fad'' |
      }
      \tag #'violon2 {
        la'16 la' la' la' la' la' la' la' la'8 la' re''4 |
        dod''4 r4 r8 dod''8 \appoggiatura dod'' re''4 |
        la'2 r |
        fad''16 fad'' fad'' fad'' fad'' fad'' fad'' fad'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
        re'' re'' re'' re'' re'' re'' re'' re'' dod'' dod'' dod'' dod'' dod'' dod'' dod'' dod'' |
        si' si' si' si' si' si' si' si' la' la' la' la' la' la' la' la' |
      }
    >>
    mi''16 fad'' sol'' fad'' mi'' fad'' re'' mi'' dod'' la' si' dod'' re'' mi'' fad'' sol'' |
    la''8 si''16 dod''' re'''8 re'' fad''4 mi''\trill |
    re''4 r r8 re'16 re' re' re' re' re' |
    re'8 fad'16 fad' fad' fad' fad' fad' fad'8 la'16 la' la' la' la' la' |
    la'8 re''16 re'' re'' re'' re'' re'' re''8 fad''16 fad'' fad'' fad'' fad'' fad'' |
    fad''8 re'''16 re''' re''' re''' re''' re''' re''' re''' re''' re''' re'''8 dod'''\trill |
    re'''8 re'16 re' re'8 re'16 re' re'8 re'16 re' re' re' re' re' |
    re'1\trill |
  }
>>
