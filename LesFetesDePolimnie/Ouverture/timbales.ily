\clef "basse" R1*7 |
la,2 r |
la, r |
R1*9 |
R1*3 |
r8 la,16 la, la, la, la, la, la,8 la, la,4 |
re r r8 re16 re re re re re |
re4 r r2 |
R1*5 |
r8 la,16 la, la, la, la, la, la,4 r |
r8 la,16 la, la, la, la, la, la,4 r |
r8 la,16 la, la, la, la, la, la,4 r |
R1*9 |
r8 la,16 la, la, la, la, la, la,8 la, la,4 |
re r r2 |
R1*10 |
r8 la,16 la, la, la, la, la, la,4 r8 la, |
re la, re la, re la, re la, |
re re16 re re re re re re8 r r4 |
R1*5 |
r8 la,16 la, la, la, la, la, la,4 r |
r8 la,16 la, la, la, la, la, la,4 r |
r8 la,16 la, la, la, la, la, la,4 r |
R1*7 |
r8 re16 re re re re re re8 re16 re re re re re |
re8 re16 re re re re re re re re re re8 la, |
re8 re16 re re8 re16 re re8 re16 re re re re re |
re1 |
