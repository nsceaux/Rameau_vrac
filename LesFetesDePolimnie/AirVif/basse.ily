\clef "basse" <>^"Tous" la2 la4 |
mi2 mi4 |
fa2 fa4 |
do2 do4 |
re2 re4 |
la,2. |
re4 mi fa |
mi2 r8 r16 la |
sold2\trill r8 r16 la |
mi2 r8 r16 mi |
la4. la8 la,4 |
mi2. |
do'2 do4 |
sol2 sol,4 |
re'2 re4 |
la2 la,4 |
mi'2 red'4\trill |
mi'2 sol4 |
la2 do'4 |
si2 r4 |
si,4 r r |
si,4 r si |
do'4 si8. do'16 si8. la16 |
sol4 fad\trill sol |
la si si, |
mi2 r8 r16 sol |
fa2 r8 r16 fa |
mi2 r8 r16 do |
re4 mi fa |
mi2 r8 r16 la |
sold2\trill r8 r16 la |
mi2 r8 r16 mi |
la8. re16 mi4 mi, |
la,2. |
r4 r r8 r16 la |
re2 do4 |
re mi mi, |
la,2. |
