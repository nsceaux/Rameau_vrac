\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes violon1)
   (violon2 #:notes "dessus" #:tag-notes violon2)
   (hautbois #:notes "dessus" #:tag-notes hautbois)
   (haute-contre #:notes "parties" #:tag-notes haute-contre)
   (taille #:notes "parties" #:tag-notes taille)
   (bassons #:notes "basse")
   (basses #:notes "basse"))
