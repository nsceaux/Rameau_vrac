\tag #'(haute-contre parties) \clef "haute-contre"
\tag #'taille \clef "taille"
do''2. |
si' |
la' |
sol' |
fa' |
mi'2 la'4 |
la'2. |
sold'2\trill r8 r16 mi' |
mi'2 r8 r16 la' |
sold'2\trill r8 r16 si' |
mi'4. mi'8 la'4 |
sold'2.\trill |
mi''2. |
re'' |
fa'' |
mi'' |
sol'2 fad'4 |
si do' mi' |
\twoVoices #'(haute-contre taille parties) <<
  { fad'2. | fad'2 }
  { mi'2. | red'2\trill }
>> r4 |
si4 r r |
r r8
\twoVoices #'(haute-contre taille parties) <<
  { fad'16 sol' la'8 sol' |
    fad'4 sol'8. la'16 sol'8. fad'16 |
    sol'4 la' si'~ |
    si'8 mi'' red''4.\trill mi''8 |
    mi''2 }
  { red'16 mi' fad'8 mi' |
    red'4\trill mi'8. red'16 mi'4 |
    mi'4 la' si' |
    do'' si' la' |
    sol'2 }
>> r8 r16 la' |
la'2 r8 r16 sol' |
sol'2 r8 r16 la' |
la'2. |
sold'2\trill r8 r16 mi' |
mi'2 r8 r16 la' |
sold'2\trill r8 r16 si' |
\twoVoices #'(haute-contre taille parties) <<
  { mi'8. la'16 sold'4.\trill la'8 |
    la'2.~ |
    la'8. si'16 do''8. re''16 si'8. do''16 |
    sold'4\trill si' do''~ |
    do''8 si' si'4.\trill la'8 |
    la'2. | }
  { mi'8. fa'16 mi'4 re' |
    do'2~ do'8. re'16 |
    mi'8. la'16 la'4 mi'8. mi'16 |
    fa'2 mi'4 |
    fa' mi' re' |
    do'2. | }
>>
