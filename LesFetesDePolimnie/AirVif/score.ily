\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Hautbois Violons }
    } << \global \keepWithTag #'tous \includeNotes "dessus" >>
    \new Staff \with {
      instrumentName = \markup\center-column { Hautes-contre Tailles }
    } << \global \keepWithTag #'parties \includeNotes "parties" >>
    \new Staff \with { instrumentName = "[Basses]" } <<
      \global \includeNotes "basse"
      \origLayout {
        s2.*8\break \grace s8 s2.*8\break s2.*7\pageBreak
        s2.*8\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
