\clef "dessus" la'8. si'16 do''8. re''16 mi''8. la''16 |
mi'4 la'' sol'' |
fa'8. sol'16 la'8. si'16 do''8. fa''16 |
do'4 fa'' mi'' |
re'8. mi'16 fa'8. sol'16 la'8. re''16 |
<<
  \tag #'(hautbois tous) \new Voice {
    \tag #'tous { \voiceOne  <>^\markup\small Htb } mi'4
  }
  \tag #'(violon1 violon2 tous) \new Voice {
    \tag #'tous { \voiceTwo <>_\markup\small Vln } la
  }
>> re''4 do'' |
fa''8. mi''16 re''8. do''16 si'8.\trill la'16 |
\appoggiatura la'8 si'4 mi' r8 r16 la' |
\appoggiatura la'8 si'4 mi'4.( fad'32*4/7 sold' la' si' do'' re'' mi'') |
si'4 mi' r8 r16 mi'' |
do''4.\trill si'8 do''4 |
si'2.\trill |
do''8. re''16 mi''8. fa''16 sol''8. do'''16 |
sol'4 do''' si'' |
re''8. mi''16 fa''8. sol''16 la''8. re'''16 |
la'4 re''' do''' |
r8 r16 do''' si''8. la''16 sol''8.\trill fad''16 |
sol''4 la''8.(\trill sol''32 la'') si''4~ |
si'' la''8 sol'' fad''\trill mi'' |
<<
  \tag #'(violon1 hautbois tous) \new Voice {
    \tag #'(hautbois tous) \voiceOne si'4. fad''16( sol'') la''8 sol'' |
    si'4. fad''16( sol'') la''8 sol'' |
    si'2. |
  }
  \tag #'(violon2 hautbois tous) \new Voice {
    \tag #'(hautbois tous) \voiceTwo si4. red''16( mi'') fad''!8 mi'' |
    si'4. red''16( mi'') fad''!8 mi'' |
    si'2. |
  }
>>
la''4 sol''8.\trill fad''16 sol''8. la''16 |
si''4 red''\trill mi''~ |
mi''8 la'' fad''4.\trill mi''8 |
mi''4~ \tuplet 3/2 { mi''8[ sol'' fa''] } \tuplet 3/2 { mi''[ re'' dod''] } |
\appoggiatura dod''8 re''4~ \tuplet 3/2 { re''8[ fa'' mi''] } \tuplet 3/2 { re''[ do'' si'] } |
\appoggiatura si'8 do''4~ \tuplet 3/2 { do''8[ mi'' re''] } \tuplet 3/2 { do''8[ si' la'] } |
fa''8. mi''16 re''8. do''16 si'8.\trill la'16 |
\appoggiatura la'8 si'4 mi' r8 r16 la' |
\appoggiatura la'8 si'4 mi'4.( fad'32*4/7 sold' la' si' do'' re'' mi'') |
si'4 mi' r8 r16 mi'' |
do''8.\trill si'16 si'4.\trill la'8 |
la'2~ la'8. si'16 |
do''8. re''16 mi''8. fad''16 sold''8.\trill la''16 |
si''4 mi'' la''~ |
la''8 si'' sold''4.\trill la''8 |
la''2. |
