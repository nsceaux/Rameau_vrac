\clef "dessus" sib'8.\ademi do''16 |
do''2( re''8.) mib''16 |
re''4 sol'8 r do''8. re''16 |
do''2~ do''16([ sib'\trill la']) sib' |
la'2.\trill |
re''8. mi''16 mi''4( fa''8.) sol''16 |
sol''8( fad''16\trill mib'') re''8.( do''16) sib'8.(\trill la'16) |
do''8. sib'16 sib'4( la'8.)\trill sol'16 |
sol'2. |
re''8. mi''16 mi''4( fad''8.) sol''16 |
re''4~ re''16( mi'' fad'' sol'') la''( sib'' sol'' la'') |
\appoggiatura la''8 sib''( la''16\trill sol'') fad''4.\trill sol''8 |
sol''2\fermata re''8 \appoggiatura do'' sib' |
\appoggiatura sib' do''2 fa''4 |
sol'' sol''( la''8.) sib''16 |
la''4.\trill r8 fa''8. la''16 |
re''4 mi''8.( fa''32 sol'') fa''8( mi''16.)\trill mi''32 |
dod''4\trill la''2~ |
la''4.( dod''8) re''8. sol''16 |
sol''4~ sol''4.( fa''16\trill mi'') |
fa''4( mi''4.)\trill re''8 |
re''2
