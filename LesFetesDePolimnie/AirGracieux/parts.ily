\piecePartSpecs
#`((violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (flutes #:notes "dessus")
   (haute-contre #:notes "haute-contre")
   (taille #:notes "taille")
   (bassons #:notes "basse")
   (basses #:notes "basse"))
