\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Flûtes Violons }
    } << \global \includeNotes "dessus" >>
    \new Staff \with { instrumentName = "Hautes-contre" } <<
      \global \includeNotes "haute-contre"
    >>
    \new Staff \with { instrumentName = "Tailles" } <<
      \global \includeNotes "taille"
    >>
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \origLayout { s4 s2.*9\break s2.*8\break }
    >>
  >>
  \layout { }
  \midi { }
}
