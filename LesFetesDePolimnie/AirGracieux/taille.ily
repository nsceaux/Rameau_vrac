\clef "taille" re'8.\doux mib'16 |
re'2~ re'8. la'16 |
\appoggiatura la'8 sib'4 \appoggiatura la'8 sol' r sol'4 |
sol'4~ sol'4.( fad'16 sol') |
fad'2.\trill |
la'8.( re'16) re'4~ re'8. sib'16 |
la'2 r8 la' |
re'2 re'8. la16 |
\appoggiatura la8 sib2. |
R2. |
r4 r r8 re'' |
re'' do''16\trill sib' la'4.\trill sol'8 |
sol'2\fermata fa'4 |
mib'2 do''4 |
sib' \appoggiatura la'16 sol'4. do''8 |
do''2 r8 fa' |
fa'4 sib'4. sol'8 |
mi'2\trill dod'8. re'16 |
mi'4.( sol'8) fa'8.( mi'16) |
la4~ la4. sib'8 |
la'2 sol'4 |
fad'2
