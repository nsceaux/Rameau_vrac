\clef "basse" sol8.\doux la16 |
la2( sib8.) do'16 |
sib4 sib,8 r mib8. re16 |
sol2 sol,4 |
re2. |
do'8. sib16 sib4( la8.)\trill sol16 |
re'2 r8 re' |
fad8. sol16 re4 re, |
sol,2. |
R2. |
r4 r r8 re' |
sol'8. do'16 re'4 re |
sol2\fermata sib4 |
sib2 la4 |
sol do'2 |
fa r8 fa |
sib4 sol4. mi8 |
la2 r4 |
r4 r8 mi fa8. dod16 |
re4~ re4. sol,8 |
la,2 la,4 |
re2
