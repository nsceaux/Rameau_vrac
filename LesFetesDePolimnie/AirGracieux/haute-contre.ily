\clef "haute-contre" sol'8.\doux fad'16 |
sol'2~ sol'8. fad'16 |
sol'4 re' la'8. sib'16 |
re'2. |
re' |
mib'8. fa'16 sol'4( la'8.) sib'16 |
\appoggiatura sib'8 do''4. r16 la' \appoggiatura la'8 sol'8. fad'16 |
la'8.( sol'16) sol'4( fad'8.)\trill sol'16 |
sol'2. |
R2. |
r4 r r8 fad'' |
sol''8. mib''16 re''4 do'' |
sib'2\fermata fa'8. sib'16 |
\appoggiatura la'8 sol'2 fa'4 |
fa''2 mi''4 |
\appoggiatura mi''8 fa''2 r8 la' |
la'8. sol'16 sol'4. sol'8 |
mi'2\trill mi'8. fa'16 |
sol'4.( sib'8) la'4~ |
la'~ la'4. re''8 |
re''4( dod''4.)\trill re''8 |
re''2
