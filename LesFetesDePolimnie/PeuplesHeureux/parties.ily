\tag #'(haute-contre parties) \clef "haute-contre"
\tag #'taille \clef "taille"
R2.*3 R1*22 |
r4 re'' re'' re'' |
re'' do''8. sib'16 la'4 la' |
re'2 r |
r r4 r8 sib' |
la'2 la'4 la'8 sib' |
sib'2( la'4)\trill si' |
do'' sol' fad'4. sol'8 |
fad'2\trill r8 \twoVoices #'(haute-contre taille parties) <<
  { sib'8 la' sol' |
    fad'4\trill fa' fa' fa' |
    fa'2. fa'4 |
    fa'2. sib'4 | }
  { re'8 do' sib |
    la4 sib do' re' |
    mib'2. mib'4 |
    fa2. sib'4 | }
>>
sib'4. sol'8 do''2~ |
do''4. do''8 do''4 sib' |
do''1 |
la'2 la'4. sib'8 |
sib'2( la'4.)\trill sol'8 |
sol'4 sib' la'\trill sib' |
fa'1 |
fa'2 sib'~ |
sib'4. sol'8 do''2~ |
do''4. do''8 do''4 sib' |
la'1\trill |
re'2 re'4. sol'8 |
sol'2( fad'4.)\trill sol'8 |
sol'2. \twoVoices #'(haute-contre taille parties) <<
  { re''4 |
    re''2. do''4 |
    do''2~ do''4. sib'8 |
    do''2 sib'4. do''8 |
    sib'2( la'4.)\trill sol'8 |
    sol'2 }
  { sib'4 |
    sib'2. la'4 |
    la'2~ la'4. la'8 |
    re'2 re'4. mib'8 |
    re'2 do' |
    sib }
>> r2 |
R1*10 | \allowPageTurn
r2 sib'4 sib'8 sib' |
\appoggiatura la'16 sol'2. fa'8 fa' |
fa'1 |
fa'4 sib' do'' sib' |
sib'( la'8)\trill r la'2 |
re'4 re' re' re' |
re'2 sol' |
sol4 sol' sol' re' |
sol'4 sol'8 sol' sol'4 re' |
sol'2 r |
r r4 re' |
sol'2 r |
r r4 re'\fort |
sol'4 fa'8 mib' re' do' sib la |
sol4 r r fa' |
fa'1 |
fa'2. sib'4 |
sib'4. sol'8 do''2~ |
do''4. do''8 do''4 sib' |
re''1 |
re'2 re'4. sol'8 |
sol'2 fad'4.\trill sol'8 |
sol'2. \twoVoices #'(haute-contre taille parties) <<
  { sol'4 |
    sol'2 fa'4 fa' |
    fa'2. re''4 |
    re''2. do''4 |
    do''2~ do''4. sib'8 |
    \appoggiatura sib'16 do''2( sib'4.)\trill la'8 |
    sib'2( la'4.)\trill sol'8 |
    sol'2 }
  { mib'4 |
    mib'2. do'4 |
    fa2. sib'4 |
    sib'2. la'4 |
    la'2~ la'4. sol'8 |
    re'2 re'4. mib'8 |
    re'2 do' |
    sib }
>> r2 | \allowPageTurn
R1*32 |
r4 re'' re'' re'' |
re''4 do''8. sib'16 la'4 la' |
re'2 r |
r r4 r8 sib' |
la'2 la'4 la'8 sib' |
sib'2( la'4)\trill si' |
do''4 sol' fad'4. sol'8 |
fad'2\trill r8 \twoVoices #'(haute-contre taille parties) <<
  { sib'8 la' sol' | fad'4\trill }
  { re'8 do' sib | la4 }
>> r4 r fa'\ademi |
fa'2. fa'4 |
fa'2. sol'4 |
sol'2. la'4 |
la'4. la'8 la'4 sib' |
\appoggiatura sib'16 do''4. sib'8\fort la' sol' fad' mi' |
re'2 re'4.\moinsfort sol'8 |
sol'2( fad'4.)\trill sol'8 |
sol'4 sol' la' sib' |
mib''2 r |
r4 fad' sol' la' |
re'1 |
r4 sol'8 sol' sol'4 re' |
sol'2 r |
r r4 re' |
sol'2 sol'4 r |
r2 r4 re'\fort |
sol' fa'8 mib' re' do' sib la |
sol4 r r2 |
r r4 <>\moinsfort \twoVoices #'(haute-contre taille parties) <<
  { fa'4 |
    fa'2 s4 sib' |
    sib'2. la'4 |

  }
  { re'4 |
    re'2 s4 sol' |
    sol'2. fa'4 |
    
  }
  { s4 |
    s2 r4 s |
    s1 |
  }
>>
la'4. la'8 la'4 sib' |
\appoggiatura sib'16 do''4. sib'8\fort la' sol' fad' mi' |
re'2 do''4.\moinsfort sib'8 |
sol'2( fad'4.)\trill sol'8 |
sol'4 sol' la' sib' |
mib''2 r |
r4 fad' sol' la' |
re'1 |
r2 r4 r8 re'' |
re'' do'' sib' la' do'' si' do'' re'' |
sol'1~ |
sol'2 r4 r8 do'' |
la'2\trill r |
sol'2 sol'4. sol'8 |
sol'1( |
\once\tieDashed fad'2)\trill~ fad'4. sol'8 |
sol'2
