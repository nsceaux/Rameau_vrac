<<
  \tag #'(antiochus basse) {
    \clef "vhaute-contre" <>^\markup\character Antiochus
    si4 do'4. re'8 |
    si4.( la8)\trill r re' |
    re' dod' dod'4.\trill re'8 |
    re'1 |
    R1 |
    r4 sol' sol'8[ fa'] mib'[ re'] |
    \appoggiatura re'16 mib'4 mib'8 re' \appoggiatura re'16 do'4\trill sib8[ la] |
    \appoggiatura la16 sib2 sib8 r r sol |
    sol4.\melisma la8 sib[ do' re' mib']( |
    re'4.)\melismaEnd do'8 re'[ mi'] fa'[ re'] |
    sol'2. fa'4 |
    mib' re' do'4. sib8 |
    la1\trill |
    r4 sib la\trill sib |
    do'8[\melisma sib la sib] do'[ re' mib' fa'] |
    re'[ do' sib do'] re'[ mi' fa' sol'] |
    mi'[ re' do' re'] mi'[ fad' sol' la']( |
    fad'4.)\trill\melismaEnd fad'8 fad'4 sol' |
    \appoggiatura sol'16 la'1 |
    re'2 re'4. sol'8 |
    sol'2( fad'4.)\trill sol'8 |
    sol'4 re' mib' re' |
    re'2( do'8)\trill r r4 |
    r4 do' re'8[ do'] sib[ la] |
    \appoggiatura la16 sib2 r |
  }
  \tag #'vdessus \clef "vdessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R2.*3 R1*22 }
>>
%%
<<
  \tag #'vdessus {
    <>^\markup\character Chœur r4 sol'' sol''8[ fa''] mib''[ re''] |
    \appoggiatura re''16 mib''4 mib''8 re'' re''8.([ do''16])\trill sib'8[ la'] |
    \appoggiatura la'16 sib'2 sib'8 r r sol' |
    sol'4.\melisma la'8 sib'[ do'' re'' mib'']( |
    re''4.)\melismaEnd do''8 re''[ mi''] fa''[ re''] |
    sol''2. fa''4 |
    mib'' re'' do''4. sib'8 |
    la'1\trill |
    r4 sib' la'\trill sib' |
    do''8[\melisma sib' la' sib'] do''[ re'' mib'' fa''] |
    re''[ do'' sib' do''] re''[ mi'' fa'' sol''] |
    mi''[ re'' do'' re''] mi''[ fad'' sol'' la'']( |
    fad''4.)\trill\melismaEnd fad''8 fad''4 sol'' |
    \appoggiatura sol''16 la''1 |
    re''2 re''4. sol''8 |
    sol''2( fad''4.)\trill sol''8 |
    sol''4 sib' la'\trill sib' |
    do''8[\melisma sib' la' sib'] do''[ re'' mib'' fa''] |
    re''[ do'' sib' do''] re''[ mi'' fa'' sol''] |
    mi''[ re'' do'' re''] mi''[ fad'' sol'' la'']( |
    fad''4.)\trill\melismaEnd fad''8 fad''4 sol'' |
    re''1 |
    la'2 la'4. sib'8 |
    sib'2( la'4.)\trill sol'8 |
    sol'1 |
  }
  \tag #'vhaute-contre {
    r4 sol' sol' sol' |
    sol' sol'8 sol' sol'4 fad'\trill |
    \appoggiatura fad'16 sol'2 sol'8 r r4 |
    r2 r4 r8 sib' |
    la'2 re'4 re'8 re' |
    re'2( do'8)\trill r si4 |
    do' sol' fad'4. sol'8 |
    fad'1\trill |
    r2 r4 fa'! |
    fa' do' fa'2~ |
    fa'4 re' sol'2~ |
    sol'4 mi' la'4. la'8 |
    la'4. re'8 re'4 re' |
    re'1 |
    do'2 do'4. sib8 |
    re'2~ re'4. la8 |
    \appoggiatura la16 sib2 r4 fa' |
    fa'4 do' fa'2~ fa'4 re' sol'2~ |
    sol'4 mib' la'4. la'8 |
    la'4. la'8 la'4 sib' |
    la'1\trill |
    re'2 re'4. sol'8 |
    sol'2( fad'4.)\trill sol'8 |
    sol'1 |
  }
  \tag #'vtaille {
    r4 re' re' re' |
    sol mib'8 mib' re'4 re' |
    re'2 re'8 r r4 |
    r2 r4 r8 sol |
    la2 la4 la8 sib |
    sib2( la8)\trill r sol4 |
    sol sol re'4. re'8 |
    re'1 |
    r2 r4 re' |
    do'2. do'4 |
    fa fa' re' sib |
    sol8[\melisma sib la sib] do'[ re' mib' do']( |
    la4.)\melismaEnd la8 la4 sib |
    \appoggiatura sib16 do'1 |
    la2 la4. sib8 |
    sib2( la4.)\trill sol8 |
    sol2 r4 re' |
    do'2. do'4 |
    fa fa' re' sib |
    sol8[\melisma sib la sib] do'[ re' mib' do']( |
    la4.)\melismaEnd re'8 re'4 re' |
    do'1 |
    do'2 do'4. sib8 |
    re'2~ re'4. la8 |
    \appoggiatura la16 sib1 |
  }
  \tag #'vbasse {
    r4 sol sib sol |
    do' do'8 la re'4 re |
    sol2 sol8 r r4 |
    r2 r4 r8 sol |
    fa2 fa4 mib8 re |
    \appoggiatura re16 mib2. re4 |
    do sib, la,4.\trill sol,8 |
    re1 |
    r2 r4 sib |
    la2.\trill fa4 |
    sib2. sol4 |
    do'4.\melisma sib8 la[ sib do' la]( |
    re'4)\melismaEnd mib'8[ re'] do'[ sib] la[ sol] |
    fad1\trill |
    fad2 fad4. sol8 |
    re2~ re4. sol8 |
    sol2 r4 sib |
    la2.\trill fa4 |
    sib4.\melisma la8 sol[ la sib sol] |
    do'4. sib8 la[ sib do' la]( |
    re'4)\melismaEnd mib'8[ re'] do'[ sib] la[ sol] |
    fad1\trill |
    fad2 fad4. sol8 |
    re2~ re4. sol8 |
    sol1 |
  }
  \tag #'(antiochus basse) R1*25
>>
<<
  \tag #'basse {
    R1*4 |
    r2 sib4 la8 sib |
    \appoggiatura sib16 do'1 |
    re'4 mib'8 re' do'4.\trill sib8 |
    fa'1~ |
    \custosNote fa'1
    s1*10 R1*25
  }
  \tag #'antiochus {
    R1*4 |
    r2 <>^\markup\character Antiochus sib4 la8 sib |
    \appoggiatura sib16 do'1 |
    re'4 mib'8 re' do'4.\trill sib8 |
    fa'1~ |
    fa'~ |
    fa'~ |
    fa'2 do'4 re'8 mib' |
    re'4. do'16[ sib] la4.\trill sib8 |
    \appoggiatura sib16 do'1 |
    fa |
    r2 fa'4 sol'8 la' |
    sib'1~ |
    sib'2 sib'8 r fa' sib' |
    sib'2.( la'4) |
    \appoggiatura la'16 sib'1 |
    R1*5 |
    r2 r4 sib' |
    \appoggiatura la'16 sol'2\trill sol'4 r |
    r2 r4 sib' |
    \appoggiatura la'16 sol'2\trill sol'4 r |
    R1*16 |
  }
  \tag #'vdessus {
    R1*7 |
    r2 <>^"Tous" sib'4 la'8 sib' |
    \appoggiatura sib'16 do''1 |
    re''4 mib''8 re'' do''4.\trill sib'8 |
    fa''1 ~ |
    fa'' ~ |
    fa''2 do''4 re''8 mib'' |
    re''4. do''16 [ sib' ] la'4. \trill sib'8 |
    \appoggiatura sib'16 do''1 |
    fa'2 re''4 mib''8 fa'' |
    do''2 do''8 r do'' re'' |
    re''2( do''2*3/4)\trill \afterGrace s8 sib'8 |
    sib'4 re'' mib'' re'' |
    re''2( do''8)\trill r r4 |
    r do'' re''8[ do''] sib'[ la'] |
    \appoggiatura la'16 sib'1 |
    r4 sol'' sol'' re'' |
    sol'' sol''8 sol'' sol''4 re'' |
    sol''2 sol''4 r |
    r2 r4 re'' |
    sol''2 sol''4 r |
    R1*2 |
    r4 sib' la'\trill sib' |
    do''8[\melisma sib' la' sib'] do''[ re'' mib'' fa''] |
    re''[ do'' sib' do''] re''[ mi'' fa'' sol''] |
    mi''[ re'' do'' re''] mi''[ fad'' sol'' la'']( |
    fad''4.)\trill\melismaEnd fad''8 fad''4 sol'' |
    \appoggiatura sol''16 la''1 |
    la'2 la'4. sib'8 |
    sib'2( la'4.\trill) sol'8 |
    sol'1 |
  }
  \tag #'vhaute-contre {
    R1*7 |
    r2 re'4 do'8 sib |
    la1\trill |
    sib4 do'8 sib la4.\trill sol8 |
    fa1~ |
    fa~ |
    fa2 la4 sib8 do' |
    sib4 sib8([ do'16 re']) re'4( do'8)\trill sib |
    la2\trill \appoggiatura sol16 fa2 |
    r2 fa'4 fa'8 sib' |
    \appoggiatura la'16 sol'2 sol'8 r fa' fa' |
    fa'1 |
    \appoggiatura mib'16 re'4 sib' do'' sib' |
    sib'2( la'8\trill) r r4 |
    r la' la' la' |
    re'1 |
    r4 sol' sol' re' |
    sol' sol'8 sol' sol'4 re' |
    sol'2 sol'4 r |
    r2 r4 re' |
    sol'2 sol'4 r |
    R1*2 |
    r2 r4 fa' |
    fa' do' fa'2~ |
    fa'4 re' sol'2~ |
    sol'4 mi' la'4. la'8 |
    la'4. la'8 la'4 sib' |
    \appoggiatura sib'16 do''1 |
    re'2 re'4. sol'8 |
    sol'2( fad'4.)\trill sol'8 |
    sol'1 |
  }
  \tag #'vtaille {
    R1*7 |
    r2 re'4 do'8 sib |
    la1\trill |
    sib4 do'8 sib la4.\trill sol8 |
    fa1~ |
    fa~ |
    fa2 la4 sib8 do' |
    sib4 sib8([ do'16 re']) re'4( do'8)\trill sib |
    la2\trill \appoggiatura sol16 fa2 |
    r2 sib4 sib8 sib |
    sib2 sib8 r la sib |
    fa1 |
    fa4 r r fa' |
    fa'1 |
    r2 r4 re' |
    re'1 |
    r4 sol' sol' re' |
    sol'4 sol'8 sol' sol'4 re' |
    sol'2 sol'4 r |
    r2 r4 re' |
    sol'2 sol'4 r |
    R1*2 |
    r2 r4 re' |
    do'2. do'4 |
    fa fa' re' sib |
    sol8[\melisma sib la sib] do'[ re' mib' do']( |
    la4.)\melismaEnd re'8 re'4 re' |
    re'1 |
    do'2 do'4. sib8 |
    re'2~ re'4. la8 |
    sib1 |
  }
  \tag #'vbasse {
    R1*15 |
    r2 mib4 mib8 mib |
    mib2 mib8 r mib re |
    fa1 |
    sib,2 r4 sib, |
    fa1 |
    r4 fad fad re |
    sol1 |
    r4 sol sol re |
    sol sol8 sol sol4 re |
    sol2 sol4 r |
    r2 r4 re |
    sol2 sol4 r |
    R1*2 |
    r2 r4 sib |
    la2\trill r4 fa |
    sib2. sol4 |
    do'4.\melisma sib8 la[ sib do' la]( |
    re'4)\melismaEnd mib'8[ re'] do'[ sib] la[ sol] |
    fad1\trill |
    fad2 fad4. sol8 |
    re2~ re4. sol8 |
    sol1 |
  }
>>
<<
  \tag #'(antiochus basse) {
    <>^\markup\character Antiochus
    sol'4 fa'8 mib' re'[ do'] si[ la] |
    sol4.( la8) si[ do'] re'[ mib'] |
    fa'2 fa'4 \appoggiatura mib'16 re'4 |
    \appoggiatura re'16 mib'1 |
    mib'4 r r mi' |
    mi'2.( fa'8) sol' |
    do'2 fa'~ |
    fa' fa'4 mi' |
    \appoggiatura mi'16 fa'1 |
    r2 r8 fa fa fa |
    la2 la4 la8 la |
    do'1 |
    do'4 r r8 fa fa fa |
    do'2 do'4 do'8 do' |
    mib'1~ |
    mib'2 mib'8 r mib'4 |
    re' re' \appoggiatura re'16 mib'4. fa'8 |
    \appoggiatura sib16 la2 do'4. re'8 |
    do'4.( sib8\trill) sib4. la8 |
    \appoggiatura la16 sib1 |
    R1 |
    r4 sol' sib' sol' |
    re'4 sib sol re |
    sol\melisma la8[ sol] sib[ la re' do'] |
    sib4 do'8[ sib] re'[ do' fa' mib'] |
    re'4 mib'8[ re'] mib'[ re' sol' fa'] |
    mi'2~ mi'8[ fa' mi' fad'] |
    fad'1\trill\melismaEnd |
    \appoggiatura mi'16 re'1 |
    r4 sol'8 fa' mib'4 re'8 do' |
    sib2~ sib8[ do' sib do']( |
    do'2\trill)~ do'4. re'8 |
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) R1*38
>>
<<
  \tag #'basse {
    re'4 r r2 |
    R1*46 |
  }
  \tag #'antiochus {
    re'4 r r2 |
    R1*8 |
    r4 sib la\trill sib |
    do'8[\melisma sib la sib] do'[ re' mib' fa'] |
    re'[ do' sib do'] re'[ mi' fa' sol'] |
    mi'[ re' do' re'] mi'[ fa' sol' la']( |
    fad'4.)\trill\melismaEnd fad'8 fad'4 sol' |
    \appoggiatura sol'16 la'1 |
    re'2 re'4. sol'8 |
    sol'2( fad'4.)\trill sol'8 |
    sol'1 |
    r2 fad' |
    la'1~ |
    la'2 re' |
    sol'1~ |
    sol'2 r4 sib' |
    la'4.( sol'8)\trill sol'4 r |
    r2 r4 sib' |
    la'4.( sol'8)\trill sol'4 r |
    R1 |
    r4 sib la\trill sib |
    do'8[\melisma sib la sib] do'[ re' mib' fa'] |
    re'[ do' sib do'] re'[ mi' fa' sol'] |
    mi'[ re' do' re'] mi'[ fa' sol' la']( |
    fad'4.)\trill\melismaEnd fad'8 fad'4 sol' |
    \appoggiatura sol'16 la'1 |
    re'2 re'4. sol'8 |
    sol'2( fad'4.)\trill sol'8 |
    sol'1 |
    r2 fad' |
    la'1~ |
    la'2 re' |
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'2~ sol'4 r8 la' |
    fad'2\trill fad'4 r |
    sib'2. la'8 sol' |
    sol'1( |
    fad'2\trill)~ fad'4. sol'8 |
    sol'2
  }
  \tag #'vdessus {
    R1 |
    r4 sol'' sol''8[ fa''] mib''[ re''] |
    \appoggiatura re''16 mib''4 mib''8 re'' re''8.([ do''16])\trill sib'8[ la'] |
    \appoggiatura la'16 sib'2 sib'8 r r sol' |
    sol'4.\melisma la'8 sib'[ do'' re'' mib'']( |
    re''4.)\melismaEnd do''8 re''[ mi''] fa''[ re''] |
    sol''2. fa''4 |
    mib''4 re'' do''4.\trill sib'8 |
    la'1\trill |
    r2 r4 sib' |
    sib'2. la'4\trill |
    sib'2. re''4 |
    re''2. do''4 |
    do''4. do''8 do''4 sib' |
    la'1\trill |
    la'2 la'4. sib'8 |
    sib'2( la'4.)\trill sol'8 |
    sol'4 re'' mib'' re'' |
    re''2( do''8)\trill r r4 |
    r4 do'' sib'\trill la' |
    sib'1 |
    r4 sol''8 sol'' sol''4 re'' |
    sol''2 sol''4 r |
    r2 r4 re'' |
    sol''2 sol''4 r |
    R1*2 |
    r2 r4 sib' |
    sib'2. la'4\trill |
    sib'2 r4 re'' |
    re''2. do''4 |
    do''4. do''8 do''4 sib' |
    la'1\trill |
    la'2 la'4. sib'8 |
    sib'2( la'4.)\trill sol'8 |
    sol'4 re'' mib'' re'' |
    re''2( do''8)\trill r r4 |
    r4 do'' sib'\trill la' |
    sib'1 |
    r2 r4 r8 sol'' |
    sol''8[\melisma fa'' mib'' re''] mib''[ re'' do'' si'] |
    do''1 |
    do''8[ re'' mib'' re''] do''[ sib' la' sol']( |
    re''2)\melismaEnd re''4 r |
    re''2. do''8 sib' |
    sib'1( |
    la'2)\trill~ la'4. sol'8 |
    sol'2
  }
  \tag #'vhaute-contre {
    R1 |
    r4 sol' sol' sol' |
    sol'4 sol'8 sol' sol'4 fad'\trill |
    \appoggiatura fad'16 sol'2 sol'8 r r4 |
    r2 r4 r8 sib' |
    la'2 re'4 re'8 re' |
    re'2( do'8)\trill r si4 |
    do'4 sol' fad'4. sol'8 |
    fad'1\trill |
    R1*2 |
    r2 r4 sib |
    sib2. la4 |
    la4. la8 re'4 re' |
    re'1 |
    re'2 re'4. re'8 |
    re'2~ re'4. la8 |
    sib4 sol' la' sib' |
    mib'2 r |
    r2 re' |
    re'1 |
    r4 sol'8 sol' sol'4 re' |
    sol'2 sol'4 r |
    r2 r4 re' |
    sol'2 sol'4 r |
    R1*4 |
    r2 r4 sib |
    sib2. la4 |
    la4. la8 re'4 re' |
    re'1 |
    re'2 re'4. re'8 |
    re'2~ re'4. la8 |
    \appoggiatura la16 sib4 sol' la' sib' |
    mib'2 r |
    r2 re' |
    re'1 |
    r2 r4 r8 re' |
    re'4\melisma mib'8[ re'] do'[ si do' re'] |
    sol1~ |
    sol2\melismaEnd sol8 r r do' |
    do'2 do'4 r |
    sib2. re'8 mib' |
    re'1~ |
    re'2~ re'4. la8 |
    sib2
  }
  \tag #'vtaille {
    R1 |
    r4 re' re' re' |
    sol4 mib'8 mib' re'4 re' |
    re'2 re'8 r r4 |
    r2 r4 r8 sol |
    la2 la4 la8 sib |
    sib2( la8\trill) r sol4 |
    sol4 sol re'4. re'8 |
    re'1 |
    R1 |
    r2 r4 fa |
    fa2. sol4 |
    sol2. do'4 |
    la4.\trill la8 la4 sib |
    \appoggiatura sib16 do'1 |
    do'2 do'4. sib8 |
    re'2~ re'4. la8 |
    sib4 sol' sol' sol' |
    sol'2( fad'8)\trill r r4 |
    r2 re' |
    re'1 |
    r4 sol'8 sol' sol'4 re' |
    sol'2 sol'4 r |
    r2 r4 re' |
    sol'2 sol'4 r |
    R1*3 |
    r2 r4 fa |
    fa2. sol4 |
    sol2. do'4 |
    la4.\trill la8 la4 sib |
    \appoggiatura sib16 do'1 |
    do'2 do'4. sib8 |
    re'2~ re'4. la8 |
    \appoggiatura la16 sib4 sol' sol' sol' |
    sol'2( fad'8)\trill r r4 |
    r2 re' |
    re'1 |
    r2 r4 r8 re' |
    re'4\melisma mib'8[ re'] do'[ si do' re']( |
    sol1)~ |
    sol2\melismaEnd sol8 r r do' |
    la2\trill la4 r |
    sol2. sol8 sol |
    sol1( |
    re2)~ re4. re8 |
    re2
  }
  \tag #'vbasse {
    r4 re' re'8[ do'] sib[ la] |
    sib4 sol si sol |
    do'4 do'8 la re'4 re |
    sol2 sol8 r r4 |
    r2 r4 r8 sol |
    fa2 fa4 mib8 re |
    \appoggiatura re16 mib2. re4 |
    do4 sib, la,4.\trill sol,8 |
    re1 |
    r2 r4 sib |
    fa2. fa4 |
    sib,2. sol,4 |
    do4.\melisma sib,8 la,[ sib, do la,] |
    re4.\melismaEnd re8 re4 sol |
    fad1\trill |
    fad2 fad4. sol8 |
    re2~ re4. sol8 |
    sol4 sib do' sib |
    sib2( la8)\trill r r4 |
    r2 re |
    sol1 |
    r4 sol8 sol sol4 re |
    sol2 sol4 r |
    r2 r4 re |
    sol2 sol4 r |
    R1*2 |
    r2 r4 sib |
    fa2. fa4 |
    sib,2. sol,4 |
    do4.\melisma sib,8 la,[ sib, do la,]( |
    re4.)\melismaEnd re8 re4 sol |
    fad1\trill |
    fad2 fad4. sol8 |
    re2~ re4. sol8 |
    sol4 sib do' sib |
    sib2( la8)\trill r r4 |
    r2 re |
    sol1 |
    r2 r4 r8 sol |
    sib[\melisma la sol fad] sol[ fa mib re] |
    mib1~ |
    mib2. fa8[ mib]( |
    re2)\melismaEnd re4 r |
    sib,2 sib,4. do8 |
    re1~ |
    re2~ re4. re8 |
    sol,2
  }
>>
