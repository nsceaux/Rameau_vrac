\tag #'(antiochus basse) {
  Peu -- ples heu -- reux, u -- nis -- sez- vous à moi.
  Chan -- tons, chan -- tons, cé -- lé -- brons sans ces -- se
  la gloi -- re et les bien -- faits
  de notre au -- gus -- te Roi.
  Chan -- tons la gloi -- re et les bien -- faits
  de notre au -- gus -- te Roi.
  Chan -- tons, chan -- tons, chan -- tons, chan -- tons,
}

\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Chan -- tons, chan -- tons, cé -- lé -- brons sans ces -- se
  la gloi -- re et les bien -- faits
  de notre au -- gus -- te Roi.

  \tag #'vdessus { Chan -- tons }
  \tag #'vhaute-contre { Chan -- tons, chan -- tons, chan -- tons, chan -- tons, }
  \tag #'vtaille { Chan -- tons, chan -- tons, chan -- tons, }
  \tag #'vbasse { Chan -- tons, chan -- tons, }
  la gloi -- re et les bien -- faits
  de notre au -- gus -- te Roi.
  \tag #'(vdessus vbasse) { Chan -- tons }
  \tag #'vhaute-contre { Chan -- tons, chan -- tons, chan -- tons, chan -- tons, }
  \tag #'vtaille { Chan -- tons, chan -- tons, chan -- tons, }
  la gloi -- re et les bien -- faits
  de notre au -- gus -- te Roi.
}

\tag #'(antiochus basse vdessus vhaute-contre vtaille) {
  No -- tre bon -- heur est la su -- prê -- me loi __
}
\tag #'(antiochus vdessus vhaute-contre vtaille) {
  que nous im -- po -- se la ten -- dres -- se,
  que nous im -- po -- se la ten -- dres -- se.
}
\tag #'vbasse {
  Que nous im -- po -- se la ten -- dres -- se.
}
\tag #'(vdessus vhaute-contre) {
  Chan -- tons, chan -- tons, chan -- tons,
  chan -- tons, chan -- tons, chan -- tons,
}
\tag #'vtaille {
  Chan -- tons, chan -- tons, chan -- tons,
  chan -- tons,
}
\tag #'vbasse {
  Chan -- tons, chan -- tons, chan -- tons,
  chan -- tons, chan -- tons,
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  cé -- lé -- brons sans ces -- se,
}
\tag #'antiochus { sans ces -- se, }
\tag #'(vdessus vhaute-contre vtaille vbasse) { sans ces -- se }
\tag #'antiochus { sans ces -- se, }
\tag #'vdessus { chan -- tons }
\tag #'vhaute-contre { chan -- tons, chan -- tons, chan -- tons, chan -- tons }
\tag #'vtaille { chan -- tons, chan -- tons, chan -- tons }
\tag #'vbasse { chan -- tons, chan -- tons }
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  la gloi -- re et les bien -- faits
  de notre au -- gus -- te Roi.
}
%%
\tag #'(antiochus basse) {
  Quand sa va -- leur
  ré -- pand la ter -- reur sur la ter -- re,
  son cœur gé -- mit de ses suc -- cès ;
  l’af -- freux ra -- va -- ge de la guer -- re,
  l’af -- freux ra -- va -- ge de la guer -- re
  lui fait ver -- ser ses pleurs
  sur ses tris -- tes su -- jets.
  Son bras ne s’ar -- me du ton -- ner -- re
  que pour fai -- re ré -- gner __ la paix.
}
%%
\tag #'(vdessus vhaute-contre vtaille) { Chan -- tons, chan -- tons, }
\tag #'vbasse { Chan -- tons, chan -- tons, chan -- tons, chan -- tons, }
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  cé -- lé -- brons sans ces -- se,
  la gloi -- re et les bien -- faits
  de notre au -- gus -- te Roi.
}
\tag #'antiochus { Chan -- tons }
\tag #'vdessus { Chan -- tons, chan -- tons, chan -- tons }
\tag #'vhaute-contre { Chan -- tons }
\tag #'(vtaille vbasse) { Chan -- tons, chan -- tons }
la gloi -- re et les bien -- faits
de notre au -- gus -- te Roi.
\tag #'antiochus {
  Chan -- tons, chan -- tons sans ces -- se, sans ces -- se,
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Chan -- tons, chan -- tons, chan -- tons,
  \tag #'vdessus { chan -- tons, }
  cé -- lé -- brons sans ces -- se,
  sans ces -- se,
}
\tag #'vdessus { Chan -- tons, chan -- tons, chan -- tons }
\tag #'(antiochus basse vhaute-contre) { Chan -- tons }
\tag #'(vtaille vbasse) { Chan -- tons, chan -- tons }
la gloi -- re et les bien -- faits
de notre au -- gus -- te Roi.

\tag #'antiochus { Chan -- tons, __ chan -- tons __ }
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Chan -- tons, chan -- tons, chan -- tons,
  \tag #'vdessus { chan -- tons }
}
\tag #'(vhaute-contre vtaille) { la gloi -- re, }
la gloi -- re de notre au -- gus -- te Roi.
