\piecePartSpecs
#`((violon1 #:notes "dessus" #:score-template "score-voix")
   (violon2 #:notes "dessus" #:score-template "score-voix")
   (hautbois #:notes "dessus" #:score-template "score-voix")
   (flutes #:notes "dessus" #:score-template "score-voix")
   (haute-contre #:notes "parties" #:tag-notes haute-contre
                 #:score-template "score-voix")
   (taille #:notes "parties" #:tag-notes taille
                 #:score-template "score-voix")
   (bassons #:notes "basse" #:score-template "score-voix")
   (basses #:score-template "score-basse-continue-voix"
           #:notes "basse"))
