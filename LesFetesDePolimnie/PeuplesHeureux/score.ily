\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \noHaraKiri \global \includeNotes "dessus" >> 
      \new Staff <<
        { s2.*3 s1*23\noHaraKiri s1*68\revertNoHaraKiri s1*33\noHaraKiri }
        \global \keepWithTag #'parties \includeNotes "parties"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        { s2.*3 s1*23\noHaraKiri s1*61\revertNoHaraKiri s1*40\noHaraKiri }
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s2.*3 s1*23\noHaraKiri s1*61\revertNoHaraKiri s1*40\noHaraKiri }
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s2.*3 s1*23\noHaraKiri s1*61\revertNoHaraKiri s1*40\noHaraKiri }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s2.*3 s1*23\noHaraKiri s1*61\revertNoHaraKiri s1*40\noHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'antiochus \includeNotes "voix"
    >> \keepWithTag #'antiochus \includeLyrics "paroles"
    \new Staff <<
      \noHaraKiri
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s2.*3 s1*22\break s1*63\break s1*38\break
      }
      \origLayout {
        s2.*3 s1*3\break \grace s16 s1*5\break \grace s16 s1*6\break s1*8\pageBreak
        s1*5\pageBreak
        \grace s16 s1*6\pageBreak
        s1*6\pageBreak
        % 5
        s1*5 s2 \bar "" \pageBreak
        s2 s1*6 s2 \bar "" \pageBreak
        s2 s1*5\pageBreak
        s1*5\pageBreak
        s1*5 s2 \bar "" \pageBreak
        % 10
        s2 s1*5\pageBreak
        s1*6\pageBreak
        s1*6\break s1*6\pageBreak
        s1*5\break s1*6\break s1*5\break s1*6\pageBreak
        s1*6\break \grace s16 s1*6\pageBreak
        % 15
        \grace s16 s1*5\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*7\pageBreak
      }
    >>
  >>
  \layout { indent = \smallindent }
  \midi { }
}
