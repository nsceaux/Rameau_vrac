\clef "dessus" R2.*3 |
<>_"Violons" r4 sol'' sol''8( fa'') mib''( re'') |
\appoggiatura re''16 mib''4 fa''8 mib'' \appoggiatura re''16 do''4 sib'8 la' |
\appoggiatura la'16 sib'2 r4 re''\doux |
sol'2. fad'4 |
\appoggiatura fad'16 sol'4. la'8 sib' do'' re'' mib'' |
re''2. do''8\trill sib' |
la'2\trill r4 re'' |
re''2( do''4)\trill sib' |
\appoggiatura sib'16 do''4 sol' fad' sol' |
fad'8\trill re''\fort do'' sib' la' re'' fad' la' |
re'4 r r fa''\doux |
fa''1~ |
fa''4 re'' sol''2~ |
sol''4 mi'' la''2~ |
la''2. sib''4 |
fad''4.\trill mi''8 re'' do'' sib' la' |
\appoggiatura la'16 sib'8 la' sol' la' sib' do'' re'' mib'' |
sib'2( la'4.)\trill sol'8 |
sol'2 r |
r fad''_\markup\general-align #X #-0.5 \italic un peu fort |
la''2 r |
r re''\fort |
sol''4 <>^"Tous" sol'' sol''8( fa'') mib''( re'') |
\appoggiatura re''16 mib''4 mib''8 re'' re''8.( do''16)\trill sib'8 la' |
\appoggiatura la'16 sib'2 r4 r8 sol' |
sol'4. la'8 sib' do'' re'' mib'' |
re''4. do''8 re'' mi'' fa'' re'' |
sol''2. fa''4 |
mib'' re'' do''4. sib'8 |
la'8\trill re'' do'' sib' la' re'' fad' sol' |
re'4 sib' la'\trill sib' |
do''8 sib' la' sib' do'' re'' mib'' fa'' |
re'' do'' sib' do'' re'' mi'' fa'' sol'' |
mi''8 re'' do'' re'' mi'' fad'' sol'' la'' |
fad''4.\trill fad''8 fad''4 sol'' |
\appoggiatura sol''16 la''1 |
re''2 re''4. sol''8 |
sol''2( fad''4.)\trill sol''8 |
sol''4 sib' la'\trill sib' |
do''8 sib' la' sib' do'' re'' mib'' fa'' |
re'' do'' sib' do'' re'' mi'' fa'' sol'' |
mi'' re'' do'' re'' mi'' fad'' sol'' la'' |
fad''4.\trill fad''8 fad''4 sol'' |
re'' mib''8 re'' do'' sib' la' sol' |
do''2 la'4. sib'8 |
sib'2( la'4.)\trill sol'8 |
sol'2. re''4 |
mi''8 re'' do'' re'' mi'' fad'' sol'' la'' |
fad''\trill mi'' re'' mi'' fad'' sol'' fad'' sol'' |
\appoggiatura sol''16 la''2 re''4. sol''8 |
sol''2( fad''4.)\trill sol''8 |
sol''2 r | \allowPageTurn \grace s16
R1*2 |
r2 <>^"Tous" sib'4 la'8 sib' |
\appoggiatura sib'16 do''1 |
re''4 mib''8 re'' do''4.\trill sib'8 |
fa''2 mib''4 re''8 do'' |
sib'4.( do''16 re'') re''4( do''8\trill sib') |
la'2\trill fa''~ |
fa''1~ |
fa''~ |
fa''2 re''4 mib''8 fa'' |
do''2. do''8 re'' |
re''2( do''4.)\trill sib'8 |
sib'4 re'' mib'' re'' |
re''4( do''8)\trill r la'2 |
re'4 do'' re''8( do'') sib'( la') |
\appoggiatura la'16 sib'2 sol' |
sol4 sol'' sol'' re'' |
sol'' sol''8 sol'' sol''4 re'' |
sol''2 sol''4 sib'' |
\appoggiatura la''16 sol''2\trill sol''4 re'' |
sol''2 sol''4 sib'' |
\appoggiatura la''16 sol''2 sol''4 re''\fort |
sol'' fa''8 mib'' re'' do'' sib' la' |
sol'4 sib' la'\trill sib' |
do''8 sib' la' sib' do'' re'' mib'' fa'' |
re'' do'' sib' do'' re'' mi'' fa'' sol'' |
mi'' re'' do'' re'' mi'' fad'' sol'' la'' |
fad''4.\trill fad''8 fad''4 sol'' |
\appoggiatura sol''16 la''1 |
la'2 la'4. sib'8 |
sib'2( la'4.\trill) sol'8 |
sol'2. sib'4 |
do''8 sib' la' sib' do'' re'' mib'' fa'' |
re'' do'' sib' do'' re'' mi'' fa'' sol'' |
mi'' re'' do'' re'' mi'' fad'' sol'' la'' |
fad'' mi'' re'' mi'' fad'' sol'' fad'' sol'' |
\appoggiatura sol''16 la''2 re''4. sol''8 |
sol''2( fad''4.)\trill sol''8 |
sol''2 r |
R1*2 | \allowPageTurn \grace s16
do'''4\doux sib''8 lab'' sol'' fa'' mib'' re'' |
do''4. re''8 mi'' fa'' sol'' la'' |
sib''2 sib''4 \appoggiatura la''16 sol''4 |
sol''2( la''4.) sib''8 |
la''2( sol''4.\trill) fa''8 |
fa''2 r8 fa' fa' fa' |
la'2 la'4 la'8 la' |
do''1 |
r2 r8 fa' fa' fa' |
la'2 la'4 la'8 do'' |
fa''1~ |
fa''2 fa''4. fa''8 |
la''2~ la''4. la''16( sib'') |
sib''2~ sib''4 r8 sib' |
\appoggiatura sib'16 do''2 \appoggiatura sib'16 la'4 \appoggiatura sol'16 fa'4 |
sol'2 do'4 fa' |
re'1\trill |
r4 sol'' sib'' sol'' |
re'' sib' sol' re' |
sol'4 sib' re'' sib'~ |
sib' do''8 sib' do'' sib' la' sol' |
re'4 sol'2 fa'4~ |
fa' r r sib'~ |
sib' sol' do''2~ |
do''1~ |
do''4 re''8( do'') re''( do'') sib'( la') |
\appoggiatura la'16 sib'4. do''8 do''4.\trill( sib'16 do'') |
re''2~ re''4. re''8 |
sol''2~ sol''4. do''8 |
la'4 fad'\fort sol' la' |
re' sol'' sol''8( fa'') mib''( re'') |
\appoggiatura re''16 mib''4 mib''8( re'') re''8.( do''16)\trill sib'8( la') |
\appoggiatura la'16 sib'2 r4 r8 sol' |
sol'4. la'8 sib' do'' re'' mib'' |
re''4. do''8 re'' mi'' fa'' re'' |
sol''2. fa''4 |
mib''4 re'' do''4.\trill sib'8 |
la' re'' do'' sib' la' re'' fad' sol' |
re'4 r r sib'\ademi |
sib'2. la'4\trill |
sib'2. re''4 |
re''2. do''4 |
do''4. do''8 do''4 sib' |
la'\trill re''8\fort mi'' fad'' sol'' la'' sib'' |
do'''2 la'4.\moinsfort sib'8 |
sib'2( la'4.)\trill sol'8 |
sol'4 re'' mib'' re'' |
re''2( do''8)\trill r r4 |
r4 do'' sib'\trill la' |
sib'1 |
r4 sol''8 sol'' sol''4 re'' |
sol''2 sol''8 r r4 |
r2 r4 re'' |
sol''2 sol''8 r r4 |
r2 r4 re''\fort |
sol''4 fa''8 mib'' re'' do'' sib' la' |
sol'4 r r sib'\moinsfort |
sib'2. la'4\trill |
sib'2 r4 re'' |
re''2. do''4 |
do''4. do''8 do''4 sib' |
la'\trill re''8\fort mi'' fad'' sol'' la'' sib'' |
do'''2 la'4.\moinsfort sib'8 |
sib'2( la'4.)\trill sol'8 |
sol'4 re'' mib'' re'' |
re''2( do''8)\trill r r4 |
r4 do'' sib'\trill la' |
sib'1 |
r2 r4 r8 sol'' |
sol'' fa'' mib'' re'' mib'' re'' do'' si' |
do''1~ |
do''8 re'' mib'' re'' do'' sib' la' sol' |
re''2 re''4 r |
re''2. do''8 sib' |
sib'1( la'2\trill)~ la'4. sol'8 |
sol'2

