\clef "basse" <>_"[B.C.]" r4 r sol |
sol2 fad8. sol16 |
\appoggiatura fad16 mi4 la2 |
re4 sol sib sol |
do' la re' re |
sol2 r4 si, |
do la, re re, |
sol,2 r |
r r4 sol |
fa2 r4 fa |
mib2. re4 |
do sib, la,4.\trill sol,8 |
re1 |
r2 r4 sib |
la2.\trill fa4 |
sib2. sol4 |
do'2. la4 |
re'2. sol4 |
re1 |
sol2 sib,4. do8 |
re2 re, |
sol,4 sib do' sib |
sib2( la4)\trill r |
r la re' re |
sol2 r |
r4 <>^"Tous" sol sib sol |
do' do'8 la re'4 re |
sol2 sol8 r r4 |
r2 r4 r8 sol |
fa2 fa4 mib8 re |
\appoggiatura re16 mib2. re4 |
do sib, la,4.\trill sol,8 |
re1 |
r2 r4 sib |
la2.\trill fa4 |
sib2. sol4 |
do'4. sib8 la sib do' la |
re'4 mib'8 re' do' sib la sol |
fad1\trill |
fad2 fad4. sol8 |
re1 |
sol2 r4 sib |
la2.\trill fa4 |
sib4. la8 sol la sib sol |
do'4. sib8 la sib do' la |
re'4 mib'8 re' do' sib la sol |
fad1\trill |
fad2 fad4. sol8 |
re1 |
sol2. sol4 |
do'2. la4 |
re'2~ re'4. sol8 |
fad2\trill sol4. do8 |
re2 re, |
sol, <>_"[B.C.]" r4 sol |
la2 fa |
sib2 la4.\trill sol8 |
fa2 <>_"Tous" re'4 do'8 sib |
la1\trill |
sib4 do'8 sib la4.\trill sol8 |
fa2 la, |
sib, fa4. sib,8 |
fa,1~ |
fa,~ |
fa,2 mib, |
re,2 re4 re8 re |
mib2. mib8 re |
fa2 fa, |
sib,2 r4 sib, |
fa1 |
r4 fad fad re |
sol1 |
r4 sol sol re |
sol4 sol8 sol sol4 re |
sol2 sol4 re |
sol2 sol4 re |
sol2 sol4 r |
r2 r4 re\fort |
sol4 fa8 mib re do sib, la, |
sol,4 r r sib |
la2.\trill fa4 |
sib2. sol4 |
do'4. sib8 la sib do' la |
re'4 mib'8 re' do' sib la sol |
fad1\trill |
fad2 fad4. sol8 |
re2~ re4. re8 |
sol,2. sol4 |
la2. fa4 |
sib2. sol4 |
do'2. la4 |
re'2~ re'4. sol8 |
fad2\trill sol4. do8 |
re2 re, |
sol,2 r |
<>^"[B.C.]" sol4\doux fa8( mib) re( do) si,( la,) |
sol,4 r r2 |
do'4 sib8( lab) sol( fa) mib( re) |
do4 sib,8( lab,) sol,( fa,) mi,( re,) |
do,4 r r mi |
fa2 sib, |
do1 |
fa, |
R1 | \allowPageTurn
r2 r8 fa fa fa |
la2 la4 la8 do' |
fa'4. fa8 mib re do sib, |
la,4 r r2 |
r2 r4 r8 do |
fa,2 r4 r8 fa |
sib2 re |
mib~ mib4. re8 |
mib2 fa |
sib,1 |
R1 |  \allowPageTurn
r2 r4 sol |
sib re' sol' sol |
sib,2 do4 re |
sol sol, la, fa, |
sib, r r sol, |
do r r la, |
re1~ |
re2 r |
r4 sol8 fa mib4 re8 do |
sib,4. la,8 sol, fa, mib, re, |
mib,2 mib,\trill |
re,4 <>_"Tous" re'4 re'8 do' sib la |
sib4 sol si sol |
do'4 do'8. la16 re'4 re |
sol2 sol8 r r4 |
r2 r4 r8 sol |
fa2 fa4 mib8 re |
\appoggiatura re16 mib2. re4 |
do4 sib, la,4.\trill sol,8 |
re1 |
r2 r4 sib\ademi |
fa2. fa4 |
sib,2. sol,4 |
do4. sib,8 la, sib, do la, |
re4. re8 re4 sol |
fad1\trill |
fad2 fad4. sol8 |
re2~ re4. re8 |
sol4 sib do' sib |
sib2( la8)\trill r r4 |
r2 re |
sol1 |
r4 sol8 sol sol4 re |
sol2 sol4 r |
r2 r4 re |
sol2 sol4 r |
r2 r4 re\fort |
sol4 fa8 mib re do sib, la, |
sol,4 r r sib\moinsfort |
fa2. fa4 |
sib,2. sol,4 |
do4. sib,8 la, sib, do la, |
re4. re8 re4 sol |
fad1\trill |
fad2 fad4. sol8 |
re2~ re4. re8 |
sol4 sib do' sib |
sib2( la8)\trill r r4 |
r2 re |
sol1 |
r2 r4 r8 sol |
sib la sol fad sol fa mib re |
mib1~ |
mib2. fa8 mib |
re2 re4 r |
sib,2 sib,4. do8 |
\once\tieDashed re1~ |
re2~ re4. re8 |
sol,2
