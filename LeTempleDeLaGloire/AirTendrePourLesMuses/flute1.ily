\clef "dessus" r4 sol''4. lab''8 |
sol''4.( fa''8\noBeam)\trill fa'' r |
r4 sib''4. sol''8 |
fa''4.( mib''8\noBeam)\trill mib'' r |
r4 lab''4. fa''8 |
mib''4( re''2)\trill |
r4 mib''4. fa''8 |
mib''4.( re''8\noBeam)\trill re'' r |
r4 sol''4. mib''8 |
re''4.( do''8\noBeam)\trill do'' r |
r4 fa''4. re''8 |
do''4( si'2)\trill |
R2. |
do'''4 fa''4. do'''8 |
\appoggiatura do'''8 re'''2 r4 |
sib'' mib''4. sib''8 |
\appoggiatura sib''8 do'''2 lab''4 |
fa'' r sib'' |
sib''2.~ |
sib''~ |
sib''~ |
sib''4 r r |
r32 mib''( fa'' sol'' lab'' sib'' do''' re''' mib'''4.*11/12) re'''32( do''' sib'' lab'' sol'') |
do'''4 fa''4.\trill mib''8 |
mib''2. |
mib'' |
r4 sol''4. lab''8 |
\appoggiatura sol''8 fa''4\trill fa'' sol''\trill |
\appoggiatura sol''8 lab''4 lab''4. sib''8 |
\appoggiatura lab''8 sol''4\trill sol''4. la''8 |
\appoggiatura la''8 sib''4 sib''4. do'''8 |
la''4\trill re'''2~ |
re'''4 do'''8( sib'') la''( sol'') |
fad''( la'') do'''( la'') sib''( sol'') |
r8 la'' do'''( la'') sib''( sol'') |
r la'' do'''( la'') sib''( sol'') |
la''2 r4 |
r32( sol'' la'' sib'' do''' re''' mi''' fad''' sol'''4.*11/12) fa'''32( mib''' re''' do''' sib'') |
mib'''4 la''4.\trill sol''8 |
sol'' sib'' lab''( sol'') fa''( mi'') |
fa''8.( sol''16) sol''4.(\trill fa''16 sol'') |
lab''8 lab'' sol''( fa'') mib''( re'') |
mib''8.( fa''16) fa''4.(\trill mib''16 fa'') |
sol''8 sol'' fa''( mib'') re''( do'') |
lab''( sol'') fa''( mib'') re''( do'') |
sol''2.~ |
sol''~ |
sol''~ |
sol''4 r r |
r32 do''( re'' mib'' fa'' sol'' la'' si'' do'''4.*11/12) sib''32( lab'' sol'' fa'' mib'') |
lab''4 re''4.\trill do''8 |
do''2. |
do'' |
