\piecePartSpecs
#`((flutes #:score "score-flutes")
   (dessus #:music , #{ s2.*18\break s2.*8\break s2.*19\break #})
   (violon1 #:music , #{ s2.*18\break s2.*8\break s2.*19\break #})
   (violon2 #:music , #{ s2.*18\break s2.*8\break s2.*19\break #})
   (basses))
