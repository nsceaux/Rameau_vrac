\score {
  \new GrandStaff \with { instrumentName = "Flûtes" } <<
    \new Staff << \global \includeNotes "flute1" >>
    \new Staff << \global \includeNotes "flute2" >>
  >>
  \layout { indent = \largeindent }
}
