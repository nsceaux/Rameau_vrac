\score {
  \new StaffGroup <<
    \new GrandStaff \with { instrumentName = "Flûtes" } <<
      \new Staff << \global \includeNotes "flute1" >>
      \new Staff << \global \includeNotes "flute2" >>
    >>
    \new GrandStaff \with { instrumentName = "Violons" } <<
      \new Staff << \global \keepWithTag #'violon1 \includeNotes "violon" >>
      \new Staff \with { \haraKiriFirst } <<
        \global \keepWithTag #'violon2 \includeNotes "violon"
      >>
    >>
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basses"
      \origLayout {
        s2.*9\break s2.*9\break s2.*4\pageBreak
        s2.*6\break \grace s8 s2.*6\pageBreak
        s2.*3\break s2.*5\pageBreak
        s2.*6\break
      }
      \modVersion { s2.*26\break }
    >>
  >>
  \layout { short-indent = \noindent }
  \midi { }
}
