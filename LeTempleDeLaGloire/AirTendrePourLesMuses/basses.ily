\clef "basse" r8 do'\doux lab sol lab fa |
r sib fa re sib, sib |
r sib sol fa sol mib |
r lab mib do lab, lab |
r lab fa mib fa re |
r sol re si, sol, sol |
r do' lab sol lab fa |
r sib fa re sib, sib |
r sib sol fa sol mib |
r lab mib do lab, lab |
r lab fa mib fa re |
r sol sol, fa' mib'\trill re' |
do' reb' do' sib lab sol |
fa sol fa mib re do |
sib, do sib, lab, sol, fa, |
mib,4 mib'8 reb' do' sib |
lab4~ lab4. fa8 |
sib4 mib2 |
sib,2 r4 |
sib,2 r4 |
sib,2 r4 |
lab,2 r4 |
sol,4 r sol, |
lab, sib,2 |
mib4. fa8 mib\trill re |
mib4. sol,8 lab, sib, |
mib, sol, sib, mib sol sib |
sib, re fa sib re' fa' |
fa, lab, do fa lab do' |
do mib sol do' mib' sol' |
sol, sib, re sol sib re' |
re4 sib,2 |
do4. re8 mib do |
re2 r4 |
re2 r4 |
re2 r4 |
do2 r4 |
sib,2. |
do4 re re, |
sol,2 r4 |
R2.*5 |
sol,2 r4 |
sol,2 r4 |
sol,2 r4 |
fa,2 r4 |
mib,4 r mib, |
fa, sol,2 |
do,4. sol,8 lab, sib, |
do,2. |
