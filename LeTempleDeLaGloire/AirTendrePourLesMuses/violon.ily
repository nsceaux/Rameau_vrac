\clef "dessus2" \tag #'violon2 \startHaraKiri
<>^"pincez" _\markup\whiteout { nourir les arpèges depuis le bas }
<do' sol' mib''>4 <lab fa' mib''> r |
<sib fa' mib''> <sib fa' re''> r |
<sol sol' mib''> r r |
<lab mib' do''> q r |
<fa' do'' lab''> r r |
<sol re' si' sol''> q r |
<do' sol' mib''> r <lab sol' mib''> |
<sib fa' mib''> <sib fa' re''> r |
<sol sol' mib''> r r |
<lab mib' do''> q r |
<fa' do'' lab''> r r |
<sol re' si' sol''> q r |
<do' sol' mib''> q r |
<fa' do'' fa''> <fa' do'' lab''> r |
<sib fa' sib'> <sib fa' re''> r |
<mib' sib'> <mib' sib' sol''> r |
<lab mib' do'' sol''> fa'' lab'' |
<sib fa' sib' lab''> <mib' sib' sol''> r |
r8 <<
  \tag #'violon1 { fa'8 lab' fa' sol' mib' | }
  \tag #'violon2 { \stopHaraKiri re' fa' re' mib' do' | }
>>
r8 <<
  \tag #'violon1 { fa'8 lab' fa' sol' mib' | }
  \tag #'violon2 { re' fa' re' mib' do' | }
>>
r8 <<
  \tag #'violon1 {
    fa'8 lab' fa' sol' mib' |
    fa'32 sib([^"avec l’archet" do' re' mib' fa' sol' lab']
    sib') fa'( sol' lab' sib' do'' re'' mib''
    fa'') sib'( do'' re'' mib'' fa'' sol'' lab'' |
    sib''8) r r r16 sib' mib''8. sol''16 |
    sol''8( fa''16.\trill) mib''32 re''4.\trill mib''8 |
    mib''2. |
    mib'' |
  }
  \tag #'violon2 {
    re'8 fa' re' mib' do' |
    re'8 r r4 r |
    r4^"pincez" r <sol mib' sib' sib''> |
    <lab mib' do'' sib''> lab'' <sib fa' sib' lab''> |
    <mib' sib' sol''>2. |
    q | \startHaraKiri
  }
>>
<mib' sib' sol''>4 sib'' r |
<sib fa' re''> sib'' r |
<fa' do''> <fa' do'' fa''> r |
<do' sol' do''> <do' sol' mib''> r |
<sol re' sib' sol''> q r |
<re' la' fad''> <re' re'' sib''> r |
<mib' re'' sib''> <do'' la''> r |
<re' do'' la''> r <re' sib' sol''> |
<re' la' fad''> r <re' sib' sol''> |
<re' la' fad''> r <re' sib' sol''> |
<re' la' fad''>32\noBeam <>^"archet" re'( mi' fad' sol' la' sib' do''
re'') la'( sib' do'' re'' mi'' fad'' sol'' la'') re''( mi'' fad'' sol'' la'' sib'' do''' |
re'''8) r r^"archet" r16 re'' sol''8. sib''16 |
\appoggiatura sib''8 la''8.\trill sol''16 fad''4.\trill sol''8 |
sol''4 r sib'\doux |
lab'8. sib'16 do''4 do' |
fa' r lab' |
sol'8. lab'16 sib'4 sib |
mib'4 r lab'8( sol') |
fa'( mib') re'( do') si( do') |
sol\noBeam <>^"pincé" <<
  \tag #'violon1 { 
    re'' fa''( re'') mib''( do'') |
    r re'' fa''( re'') mib''( do'') |
    r re'' fa'' re'' mib'' do'' |
    re''32\noBeam <>^"archet" sol32( la si do' re' mib' fa' 
    sol') re'( mib' fa' sol' la' si' do''
    re'') sol'( la' si' do'' re'' mib'' fa'' |
    sol''8) r r r16 sol' do''8. mib''16 |
    \appoggiatura mib''8 re''8.\trill do''16 si'4.\trill do''8 |
    do''2. |
    do'' |
  }
  \tag #'violon2 {
    \stopHaraKiri si'8 re'' si' do'' la' |
    sol si' re'' si' do'' la' |
    sol si' re'' si' do'' la' |
    si'8 r8 r4 r |
    <>^"tous pincé" r4 r <mib' do'' sol''> |
    <fa' do'' lab''> <sol re' si' sol''> fa'' |
    <do' sol' mib''>2. |
    q |
  }
>>
