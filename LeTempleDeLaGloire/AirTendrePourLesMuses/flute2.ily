\clef "dessus" R2.*6 |
r4 sol''4. lab''8 |
sol''4.( fa''8)\trill fa'' r |
r4 sib''4. sol''8 |
fa''4.( mib''8)\trill mib'' r |
r4 lab''4. fa''8 |
mib''4( re''2)\trill |
sol''4 do''4. sol''8 |
\appoggiatura sol''8 lab''2 r4 |
fa''4 sib'4. fa''8 |
\appoggiatura fa''8 sol''2.~ |
sol''8( fa'') lab''( sol'') fa''( mib'') |
\appoggiatura mib''8 re''8.( mib''32*2/3 re'' mib'') mib''4.(\trill re''16 mib'') |
fa''4 re'' mib'' |
lab'' re'' mib'' |
lab'' re'' mib'' |
re''2\trill \appoggiatura do''8 sib'4 |
r32 mib''( fa'' sol'' lab'' sib'' do''' re''' mib'''4.*11/12) re'''32( do''' sib'' lab'' sol'') |
do'''4 fa''4.\trill mib''8 |
mib''2. |
mib'' |
R2. |
r4 re''4. mib''8 |
\appoggiatura re''8 do''4\trill do'' re''\trill |
\appoggiatura re''8 mib''4 mib''4. fa''8 |
re''4\trill re''4. sol''8 |
fad''4\trill sol''4. fa''8 |
mib''8( fa'') mib''( re'') do''( sib') |
la'(\trill fad'') la''( fad'') sol''( mi'') |
r8 fad'' la'' fad'' sol'' mi'' |
r fad'' la'' fad'' sol'' mi'' |
fad''2 r4 |
r32( sol'' la'' sib'' do''' re''' mi''' fad''' sol'''4.*11/12) fa'''32( mib''' re''' do''' sib'') |
mib'''4 la''4.\trill sol''8 |
sol''4 r reb''\doux |
do''8. fa''16 mi''4.\trill fa''8 |
fa''4 r do'' |
sib'8. mib''16 re''4.\trill mib''8 |
mib''4. sol''8 fa''( mib'') |
re''( mib'') si'( do'') fa''( mib'') |
re''4\trill si' do'' |
fa'' si' do'' |
fa'' si' do'' |
si'2 \appoggiatura la'8 sol'4 |
r32 do''( re'' mib'' fa'' sol'' la'' si'' do'''4.*11/12) sib''32( lab'' sol'' fa'' mib'') |
lab''4 re''4.\trill do''8 |
do''2. |
do'' |
