\tag #'all \key mi \major \midiTempo#160
\digitTime\time 2/2 \tempo "Lent" s1*3
\tempo "Vite" s1*2 \segnoMark\bar "||" s1*61 \bar "|." \fineMark
\digitTime\time 3/4 \midiTempo#120 s2.*6 \bar ":|." s2.*12 \bar "||"
\digitTime\time 3/4 s2.*9 \bar "||"
\digitTime\time 2/2 \tempo "Vite" \midiTempo#160 s1*2 \dalSegnoMark \bar "|."
