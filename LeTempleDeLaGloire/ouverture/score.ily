\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new Staff \with {
      instrumentName = "Petites flûtes"
      shortInstrumentName = "Fl"
    }  <<
      \global \keepWithTag #'flutes \includeNotes "flutes"
      { \noHaraKiri s1*66 \revertNoHaraKiri }
    >>
    \new StaffGroupNoBracket <<
      \new Staff \with {
        instrumentName = "Trompettes"
        shortInstrumentName = "Tr"
      }  <<
        \keepWithTag #'() \global
        \keepWithTag #'trompettes \includeNotes "trompettes"
        { \noHaraKiri s1*66 \revertNoHaraKiri }
      >>
      \new Staff \with {
        instrumentName = "Cors"
        shortInstrumentName = "Cor"
      }  <<
        \keepWithTag #'() \global
        \keepWithTag #'cors \includeNotes "cors"
        { \noHaraKiri s1*66 \startHaraKiri }
      >>
      \new GrandStaff \with {
        shortInstrumentName = "Cor"
      } <<
        \new Staff <<
          \keepWithTag #'() \global
          \keepWithTag #'cor1 \includeNotes "cors"
          { \startHaraKiri s1*66 \stopHaraKiri }
        >>
        \new Staff <<
          \keepWithTag #'() \global
          \keepWithTag #'cor2 \includeNotes "cors"
          { \startHaraKiri s1*66 \stopHaraKiri }
        >>
      >>
      \new Staff \with {
        instrumentName = "Timbales"
        shortInstrumentName = "Tym"
      }  <<
        \keepWithTag #'() \global \includeNotes "timballes" 
        { \noHaraKiri s1*66 \revertNoHaraKiri }
      >>
    >>
    \new StaffGroupNoBracket <<
      \new Staff \with {
        instrumentName = \markup\center-column { Hautbois Violons }
        shortInstrumentName = \markup\center-column { Hb Vln }
      } << \global \keepWithTag #'dessus \includeNotes "dessus" >>
      \new Staff \with {
        instrumentName = "Parties"
        shortInstrumentName = \markup\center-column { Hc T }
      }  << \global \keepWithTag #'parties \includeNotes "parties" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Bassons Basses }
        shortInstrumentName = \markup\center-column { Bn Bs }
      } <<
        \global \includeNotes "basses"
        \origLayout {
          s1*7\break s1*9\pageBreak
          s1*9\break s1*9\pageBreak
          s1*9\break s1*10\pageBreak
          s1*8\break s1*5\pageBreak
          s2.*6\break s2.*6\break s2.*6\pageBreak
          s2.*9\break
        }
        \modVersion {
          s1*66\break s2.*6\break s2.*12\break
        }
      >>
    >>
  >>
  \layout { short-indent = 10\mm }
  \midi { }
}
