\clef "vdessus" \transposition mi
\twoVoices #'(cor1 cor2 cors) <<
  { do''2 do'' | do''1~ | do''2 }
  { sol'2 sol' | sol'1~ | sol'2 }
>> r2 |
R1*7 |
r2 \twoVoices #'(cor1 cor2 cors) <<
  { sol''2~ | sol''1\trill~ | sol''~ | sol''~ | sol'' | }
  { mi''2~ | mi''1\trill~ | mi''~ | mi''~ | mi'' | }
>>
R1*5 |
r4 \twoVoices #'(cor1 cor2 cors) <<
  { re''8 re'' re''4 re''8 re'' | re''1\trill | }
  { sol'8 sol' sol'4 sol'8 sol' | sol'1 | }
>>
R1 |
<>^\markup { \concat { 2 \super e } cor seul }
r8 do'' mi'' do'' mi'' do'' mi'' do'' |
sol' do'' mi'' do'' mi'' do'' mi'' do'' |
do''2 r |
r8 re'' fa'' re'' fa'' re'' fa'' re'' |
re'' re'' fa'' re'' fa'' re'' fa'' re'' |
mi''2 r |
R1*8 |
r8 do'' mi'' do'' mi'' do'' mi'' do'' |
do'' do'' mi'' do'' mi'' do'' mi'' do'' |
do'' do'' mi'' do'' mi'' do'' mi'' do'' |
sol'2 sol' |
mi' r |
R1*14 |
r8 \twoVoices #'(cor1 cor2 cors) <<
  { do'' mi'' do'' mi'' do'' mi'' do'' |
    do'' do'' mi'' do'' mi'' do'' mi'' do'' |
    do'' do'' mi'' do'' mi'' do'' mi'' do'' |
    do'' do'' mi'' do'' mi'' do'' mi'' do'' |
    do'' do'' mi'' do'' mi'' do'' mi'' do'' |
    do'' mi'' re'' do'' do'' mi'' re'' do'' |
    do'' mi'' re'' do'' do'' do'' sol' do'' |
    do''4 mi''8 mi'' mi''4 mi''8 mi'' |
    mi''4 mi''8 mi'' mi'' mi'' mi'' mi'' |
    mi''1\trill | }
  { mi'8 sol' mi' sol' mi' sol' mi' |
    mi' mi' sol' mi' sol' mi' sol' mi' |
    mi' mi' sol' mi' sol' mi' sol' mi' |
    mi' mi' sol' mi' sol' mi' sol' mi' |
    mi' mi' sol' mi' sol' mi' sol' mi' |
    do'2 r |
    sol' sol' |
    sol'4 sol'8 sol' sol'4 sol'8 sol' |
    sol'4 sol'8 sol' sol' sol' sol' sol' |
    sol'1 | }
>>
\twoVoices #'(cor1 cor2 cors) <<
  { sol''4 mi'' re'' |
    mi'' re''8 mi'' do''4 |
    mi''8*2/3 re'' mi'' mi'' re'' mi'' mi'' re'' mi'' |
    re'' do'' re'' re'' do'' re'' re'' do'' re'' |
    mi'' re'' mi'' mi'' re'' mi'' mi'' re'' do'' |
    re''2\trill }
  { mi''4 do'' sol' |
    do'' sol'8 do'' mi'4 |
    do''8*2/3 sol' do'' do'' sol' do'' do'' sol' do'' |
    sol' mi' sol' sol' mi' sol' sol' mi' sol' |
    do'' sol' do'' do'' sol' do'' do'' sol' mi' |
    sol'2 }
>> r4 |
\twoVoices #'(cor1 cor2 cors) <<
  { re''4 re'' re''8*2/3 mi'' fa'' |
    mi''4 mi'' mi''8*2/3 fa'' sol'' |
    re''4 re'' re''8*2/3 mi'' fa'' |
    mi''8 re'' mi''2 |
    sol''4 mi'' re'' |
    mi''4 re''8 mi'' do''4 |
    mi''8*2/3 re'' mi'' mi'' re'' mi'' mi'' re'' mi'' |
    re'' do'' re'' re'' do'' re'' re'' do'' re'' |
    mi'' re'' mi'' mi'' re'' mi'' mi'' re'' do'' |
    re''2 }
  { sol'4 sol' sol'8*2/3 do'' re'' |
    do''4 do'' do''8*2/3 re'' mi'' |
    sol'4 sol' sol'8*2/3 do'' re'' |
    do''8 sol' do''2 |
    mi''4 do'' sol' |
    do'' sol' mi' |
    do''8*2/3 sol' do'' do'' sol' do'' do'' sol' do'' |
    sol' mi' sol' sol' mi' sol' sol' mi' sol' |
    do'' sol' do'' do'' sol' do'' do'' sol' mi' |
    sol'2 }
>> r4 |
\twoVoices #'(cor1 cor2 cors) <<
  { fa''4 mi'' re''8 do'' | do''2 }
  { re''4 do'' sol' | mi'2 }
>> r4 |
R2.*9 |
R1*2 |
