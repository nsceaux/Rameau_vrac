\score {
  \new GrandStaff <<
    \new Staff << \global \keepWithTag #'hautbois1 \includeNotes "dessus" >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'hautbois2 \includeNotes "dessus"
      { s1*66\break }
    >>
  >>
  \layout { }
}
