\clef "taille" mi'2 mi' |
mi'1~ |
mi'2 r |
r4 mi'8 mi' sold' sold' si' si' |
sold' sold' mi' mi' sold' sold' si' si' |
sold' sold' sold' sold' si si mi' mi' |
si' si si si mi' mi' sold' sold' |
mi' mi' si' si' mi'' mi'' sold' sold' |
si' si' si' si' si' si' si' si' |
si' si' si' si' si' si' si' si' |
si'1~ |
si'~ |
si'~ |
si' |
R1 |
r2 r4 mi' |
mi'1~ |
mi'~ |
mi'2. mi'4 |
si'2 si |
si4 si8 si si4 si8 si |
si4 si8 si red' red' fad' fad' |
red' red' si si fad' fad' si' si' |
si'1 |
r2 mi' |
mi' si |
dod' r |
r red' |
red' sid' |
dod''1 |
R1 |
sold' |
la' |
sold'2 dod'' |
dod'' fad' |
fad' sid'4. red''8 |
sold'4 la' sold' fad' |
mi'8 dod'' mi'' dod'' mi'' dod'' mi'' dod'' |
si' si' mi'' si' mi'' si' mi'' si' |
dod'' dod'' mi'' dod'' mi'' dod'' mi'' dod'' |
red'' mi'' red'' dod'' si' la' sold' fad' |
si2. mi'4 |
mi'2. mi'4 |
la'2. la'4 |
la'2. fad'4 |
si'2. si'4 |
si'2. sold'4 |
dod''2 dod'4 fad' |
fad'2 red'' |
mi'' mi' |
mi'1~ |
mi'~ |
mi'~ |
mi'~ |
mi'~ |
mi'2 si |
si r4 mi' |
mi'1 |
si |
sold |
r2 r4 mi' |
mi'1~ |
mi'2 si |
si4 mi'8 mi' mi'4 mi'8 mi' |
mi'4 mi'8 mi' mi' mi' mi' mi' |
mi'1 |
R2.*18 |
r4 \twoVoices #'(haute-contre taille parties) <<
  { dod'4 dod' | \appoggiatura si8 la4 red' sid~ | sid dod' dod' | dod'2 }
  { r4 sold\fort | sold2.~ | sold2 sold4 | la2 }
>> <>^"tous" dod'4 |
sold \cesureInstr sold'\doux sold |
dod'2 dod'4 |
fad' dod'' dod'' |
dod'' la'2 |
red'2. |
r4 mi'8 mi' sold' sold' si' si' |
sold' sold' mi' mi' sold' sold' si' si' |
