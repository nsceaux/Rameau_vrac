\clef "basse" \transposition mi
 do2 do |
do4. do16 do do4. do16 do |
do4 do16 do do do do do do do do do do do |
do4 r r2 |
R1*9 |
r4 r16 do do do do4 do16 do do do |
do4 do16 do do do do do do do do do do do |
do4 r r2 |
R1*4 |
r4 sol,8 sol, sol,4 sol,8 sol, |
sol,4 r r2 |
R1*40 |
r2 sol, |
do4 do8 do do4 do8 do |
do4 do16 do do do do do do do do do do do |
do4 r r2 |
R2.*18 |
R2.*9 |
R1*2 |
