\clef "dessus"
<<
  \tag #'(violon1 violon2 dessus1 dessus2 dessus) \new Voice {
    \tag #'(dessus1 dessus2 dessus) \voiceOne
    \tag #'(dessus1 dessus) <>^"Violons"
    mi'''2 mi''' | mi'''1 |
  }
  \tag #'(hautbois1 hautbois2 dessus1 dessus2 dessus) \new Voice {
    \tag #'(dessus1 dessus2 dessus) \voiceTwo
    \tag #'(dessus2 dessus) <>_"Hautbois"
    mi''2 mi'' | mi''1 |    
  } 
>>
R1 |
r4 mi'8 mi' sold' sold' si' si' |
sold' sold' mi' mi' sold' sold' si' si' |
<<
  \tag #'(violon1 hautbois1 dessus1 dessus) \new Voice {
    \tag #'dessus \voiceOne
    sold'8 sold' si' si' mi'' mi'' sold'' sold'' |
    mi'' si' mi'' mi'' sold'' sold'' si'' si'' |
    sold'' mi'' sold'' sold'' si'' si'' mi''' mi''' |
    mi''' mi''' mi''' mi''' mi''' mi''' mi''' mi''' |
    mi''' mi''' mi''' mi''' mi''' mi''' mi''' mi''' |
    mi'''1~ |
    mi'''~ |
    mi'''~ |
    mi''' |
  }
  \tag #'(violon2 hautbois2 dessus2 dessus) \new Voice {
    \tag #'dessus \voiceTwo
    sold'8 sold' mi' mi' sold' sold' si' si' |
    sold' sold' sold' sold' si' si' mi'' mi'' |
    si' si' mi'' mi'' sold'' sold'' si'' si'' |
    sold'' sold'' sold'' sold'' sold'' sold'' sold'' sold'' |
    sold'' sold'' sold'' sold'' sold'' sold'' sold'' sold'' |
    sold''1~ |
    sold''~ |
    sold''~ |
    sold'' |
  }
>>
R1 |
<<
  \tag #'(violon1 hautbois1 dessus1 dessus) \new Voice {
    \tag #'dessus \voiceOne
    mi''8 la'' dod''' dod''' dod''' dod''' si'' la'' |
    mi'' sold'' si'' si'' si'' si'' la'' sold'' |
    mi'' la'' dod''' dod''' dod''' dod''' si'' la'' |
    mi'' sold'' si'' si'' si'' si'' la'' sold'' |
    la'' la'' sold'' fad'' sold'' sold'' fad'' mi'' |
    fad''4\trill
  }
  \tag #'(violon2 hautbois2 dessus2 dessus) \new Voice {
    \tag #'dessus \voiceTwo
    dod''8 mi'' la'' la'' la'' la'' sold'' fad'' |
    si'8 mi'' sold'' sold'' sold'' sold'' fad'' mi'' |
    dod'' mi'' la'' la'' la'' la'' sold'' fad'' |
    si' mi'' sold'' sold'' sold'' sold'' fad'' mi'' |
    fad'' fad'' mi'' red'' mi'' mi'' red'' dod'' |
    red''4\trill
    \tag #'(violon2 hautbois2 dessus2) \startHaraKiri
  }
>> red'''8 red''' red'''4 red'''8 red''' |
red'''1\trill |
R1*2 |
r2 si' |
dod''8 la' dod'' red'' mi'' fad'' sold'' mi'' |
la''1 |
r2 dod'' |
si'8 dod'' si' dod'' red'' mi'' fad'' red'' |
sold''1 |
R1 | \allowPageTurn
mi''8 sold'' dod''' dod''' dod''' dod''' dod''' dod''' |
mi'' la'' dod''' dod''' dod''' dod''' dod''' dod''' |
mi'' sold'' dod''' dod''' dod''' dod''' dod''' dod''' |
la'' sold'' fad'' mi'' fad'' mi'' red'' dod'' |
sid' dod'' sid' dod'' red'' mi'' fad'' red'' |
sold''4 la'' red''4.\trill dod''8 |
dod'' dod'' mi'' dod'' mi'' dod'' mi'' dod'' |
si' si' mi'' si' mi'' si' mi'' si' |
dod'' dod'' mi'' dod'' mi'' dod'' mi'' dod'' |
fad' mi'' red'' dod'' si' la' sold' fad' |
sold' mi' sold' si' si' mi' sold' si' |
dod'' mi' la' dod'' dod'' mi' la' dod'' |
dod'' fad' la' dod'' dod'' fad' la' dod'' |
red'' fad' si' red'' red'' fad' si' red'' |
red'' sold' si' red'' red'' sold' si' red'' |
mi'' la' dod'' mi'' mi'' la' dod'' mi'' |
la'' sold'' fad'' mi'' fad'' mi'' red'' dod'' |
red'' si' red'' mi'' fad'' sold'' la'' fad'' |
si'' <<
  \tag #'(violon1 hautbois1 dessus1 dessus) \new Voice {
    \tag #'dessus \voiceOne
    mi''8 sold'' mi'' sold'' mi'' sold'' mi'' |
    dod'' mi'' sold'' mi'' sold'' mi'' sold'' mi'' |
    si' mi'' sold'' mi'' sold'' mi'' sold'' mi'' |
    sold' mi'' sold'' mi'' sold'' mi'' sold'' mi'' |
    si' mi'' sold'' mi'' sold'' mi'' sold'' mi'' |
    dod'' mi'' sold'' mi'' sold'' mi'' sold'' mi'' |
    mi'' sold'' fad'' mi'' mi'' mi'' si' red'' |
    mi''2
  }
  \tag #'(violon2 hautbois2 dessus2 dessus) \new Voice {
    \tag #'dessus \voiceTwo
    \tag #'(violon2 hautbois2 dessus2) \stopHaraKiri
    sold'8 si' sold' si' sold' si' sold' |
    mi' sold' si' sold' si' sold' si' sold' |
    sold' sold' si' sold' si' sold' si' sold' |
    mi' sold' si' sold' si' sold' si' sold' |
    mi' sold' si' sold' si' sold' si' sold' |
    mi' sold' si' sold' si' sold' si' sold' |
    sold' si' la' sold' sold' sold' fad' la' |
    sold'2\trill
    \tag #'(violon2 hautbois2 dessus2) \startHaraKiri
  }
>> r4 si'' |
dod'''1 |
red''' |
mi''' |
r2 r4 si' |
dod''1 |
red''\trill |
mi''4 mi'''8 mi''' mi'''4 mi'''8 mi''' |
mi'''4 mi'''8 mi''' mi''' mi''' mi''' mi''' |
mi'''1 |
<>^"Violons" mi''2 red''8\trill fad'' |
si'2. |
r4 r si' |
red''8*2/3 mi'' red'' red'' mi'' red'' red'' mi'' red'' |
si'2 mi''4 |
red''2.\trill |
red''4 red'' red''8*2/3 mi'' fad'' |
si'4 si' mi'' |
red''\trill red''8*2/3 mi'' fad'' si'4 |
si'2. |
mi''2 red''8\trill fad'' |
si'2. |
r4 r si' |
red''8*2/3 mi'' red'' red'' mi'' red'' red'' mi'' red'' |
si'2 mi''4 |
red''2\trill r4 |
red''4 mi'' red''\trill |
mi''2 r4 |
<>^"Violons" r4 <>\fort <<
  \tag #'(violon1 hautbois1 dessus1 dessus) \new Voice {
    \tag #'dessus \voiceOne
    sold'4 dod''~ |
    dod''( sid') red''~ |
    red''( dod'') mi''~ |
    mi'' red''8.( mi''16) dod''8.( red''16) |
    sid'4\trill \cesureInstr red''\doux fad'' |
    mid''2
  }
  \tag #'(violon2 hautbois2 dessus2 dessus) \new Voice {
    \tag #'dessus \voiceTwo
    \tag #'(violon2 hautbois2 dessus2) \stopHaraKiri
    mi'4 mi'~ |
    mi'( red') fad'~ |
    fad'( mi') sold'~ |
    sold' fad'8.( sold'16) mi'8.( fad'16) |
    red'4\trill \cesureInstr sid'\doux red'' |
    sold'2
  }
>> r4 |
r4 <<
  \tag #'(violon1 hautbois1 dessus1 dessus) \new Voice {
    \tag #'dessus \voiceOne
    fad''4 mi''! |
    mi''( red''8.)\trill mi''16 \appoggiatura red''8 dod''4 |
    sold''2. |
  }
  \tag #'(violon2 hautbois2 dessus2 dessus) \new Voice {
    \tag #'dessus \voiceTwo
    la'4 sold' |
    sold'( fad'8.)\trill mi'16 fad'4 |
    red'2.\trill |
  }
>>
r4 mi'8 mi' sold' sold' si' si' |
sold' sold' mi' mi' sold' sold' si' si' |
