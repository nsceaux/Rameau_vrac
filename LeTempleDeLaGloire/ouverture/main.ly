\version "2.19.80"
\include "common.ily"

\header {
  title = \markup\center-column {
    Le Temple de la Gloire
    Ouverture
  }
  copyrightYear = "2011"
}

\includeScore "LeTempleDeLaGloire/ouverture"
