\score {
  \new GrandStaff <<
    \new Staff << \global \keepWithTag #'violon1 \includeNotes "dessus" >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'violon2 \includeNotes "dessus"
      { s1*66\break }
    >>
  >>
  \layout { }
}
