\clef "dessus"
\twoVoices #'(flute1 flute2 flutes) <<
  { sold''2 sold'' | sold''1\trill~ | sold''2 }
  { si'2 si' | si'1\trill~ | si'2 }
>> r2 |
R1*9 |
r2 \twoVoices #'(flute1 flute2 flutes) <<
  { si''~ | si''1\trill | }
  { sold''2~ | sold''1\trill | }
>>
r2 r4 <>^"tous" mi''4 |
mi''1\trill~ |
mi''~ |
mi''~ |
mi''2. mi''4 |
si'2 si' |
si'4 red''8 red'' red''4 red''8 red'' |
red''1\trill |
R1*7 |
\twoVoices #'(flute1 flute2 flutes) <<
  { mi''8 fad'' sold'' fad'' mi'' fad'' sold'' fad'' |
    mi'' fad'' sold'' fad'' mi'' fad'' sold'' fad'' |
    mi''1\trill~ |
    mi''~ |
    mi''2. }
  { dod''8 red'' mi'' red'' dod'' red'' mi'' red'' |
    dod'' red'' mi'' red'' dod'' red'' mi'' red'' |
    dod''1\trill~ |
    dod''~ |
    dod''2. }
>> <>^"tous" sold''4 |
red''8 mi'' fad'' mi'' red'' mi'' fad'' mi'' |
red'' mi'' red'' mi'' fad'' sold'' la'' sid' |
dod''4. red''8 sid'4.\trill dod''8 |
dod''1 |
R1*3 |
mi''8 fad'' sold'' fad'' mi'' fad'' sold'' fad'' |
mi''4 r r2 |
fad''8 sold'' la'' sold'' fad'' sold'' la'' sold'' |
fad''4 r r2 |
sold''8 la'' si'' la'' sold'' la'' si'' la'' |
sold''4 r r2 |
la''8 si'' dod''' si'' la'' si'' dod''' si'' |
la''4. sold''8 fad'' mi'' red'' dod'' |
si'2 r |
R1*6 |
\twoVoices #'(flute1 flute2 flutes) <<
  { si''8 la'' sold'' la'' si'' la'' sold'' la'' |
    si'' la'' sold'' la'' si'' la'' sold'' la'' |
    si'' la'' sold'' la'' si'' la'' sold'' la'' |
    si'' la'' sold'' la'' si'' la'' sold'' la'' |
    si'' la'' sold'' la'' si'' la'' sold'' la'' |
    si'' la'' sold'' la'' si'' la'' sold'' la'' |
    si'' la'' sold'' la'' si'' la'' sold'' la'' |
    sold''1\trill~ |
    sold''~ |
    sold'' | }
  { sold''8 fad'' mi'' fad'' sold'' fad'' mi'' fad'' |
    sold'' fad'' mi'' fad'' sold'' fad'' mi'' fad'' |
    sold'' fad'' mi'' fad'' sold'' fad'' mi'' fad'' |
    sold'' fad'' mi'' fad'' sold'' fad'' mi'' fad'' |
    sold'' fad'' mi'' fad'' sold'' fad'' mi'' fad'' |
    sold'' fad'' mi'' fad'' sold'' fad'' mi'' fad'' |
    sold'' fad'' mi'' fad'' sold'' fad'' mi'' fad'' |
    mi''1\trill~ |
    mi''~ |
    mi'' | }
>>
R2.*18 |
<>^"Flûtes" R2.*5 |
r4 \twoVoices #'(flute1 flute2 flutes) <<
  { sold''4 si'' | \appoggiatura si''8 la''2 }
  { mid''4 sold'' | dod''2 }
>> <>^"tous" sold''4 |
dod'''4 dod'''4. red'''8 |
sid''2.\trill |
R1*2 |
