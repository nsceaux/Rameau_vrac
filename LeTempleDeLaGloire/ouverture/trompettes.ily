\clef "vdessus" \transposition mi
\twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''2 mi'' | mi''1\trill~ | mi''2 }
  { sol'2 sol' | sol'1\trill~ | sol'2 }
>> r2 |
R1*8 |
r2 \twoVoices #'(trompette1 trompette2 trompettes) <<
  { sol''2~ | sol''1\trill~ | sol'' | }
  { mi''2~ | mi''1~ | mi'' | }
>>
R1*6 |
r4 \twoVoices #'(trompette1 trompette2 trompettes) <<
  { re''8 re'' re''4 re''8 re'' | re''1\trill | }
  { sol'8 sol' sol'4 sol'8 sol' | sol'1 | }
>>
R1*7 |
\twoVoices #'(trompette1 trompette2 trompettes) <<
  { r2 mi''\doux | mi''1\trill | mi''2 r | }
  { R1*3 }
>>
R1*5 |
r2 <>^\markup { \concat { P \super re } seule }
mi'' |
sol''1~ |
sol''2 fa'' |
re''2.\trill re''4 |
sol'2 r |
R1*7 |
r2 r4 <>\tresdoux \twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''4 | mi''2. fa''8( mi'') | mi''1\trill~ | mi''~ | mi'' | }
  { do''4 | do''2. re''8( do'') | do''1\trill~ | do''~ | do'' | }
>>
R1 |
r2 \twoVoices #'(trompette1 trompette2 trompettes) <<
  { sol'' | mi''\trill }
  { sol'2 | sol' }
>> <>^\markup { \concat { P \super re } seule }
r4 sol'' |
sol''1~ |
sol''2 sol'' |
mi''1\trill |
r2 r4 sol'' |
sol''1~ |
sol''2 sol'' |
 \twoVoices #'(trompette1 trompette2 trompettes) <<
  { mi''4\trill mi''8 mi'' mi''4 mi''8 mi'' |
    mi''4 mi''8 mi'' mi'' mi'' mi'' mi'' |
    mi''1\trill | }
  { r4 sol'8 sol' sol'4 sol'8 sol' |
    sol'4 sol'8 sol' sol' sol' sol' sol' |
    sol'1 | }
>>
R2.*18 |
R2.*9 |
R1*2 |
