\clef "basse" mi,2 mi, |
mi,1~ |
mi,1 |
r4 mi8 mi sold sold si si |
sold sold mi mi sold sold si si |
sold sold mi mi si, si, sold, sold, |
mi, mi, mi, mi, mi, mi, mi, mi, |
mi, mi, mi, mi, mi, mi, mi, mi, |
mi,1~ |
mi, |
mi,4 mi,8 mi, mi, mi, mi, mi, |
mi,1~ |
mi,~ |
mi, |
R1 |
la,2 r4 la, |
mi2. mi4 |
la,2. la,4 |
mi,2. mi4 |
red2\trill mi |
si,2 si, |
si,4 si,8 si, red red fad fad |
red red si, si, fad fad si si |
sold2 r |
r sold |
la sold |
fad r |
r la |
sold fad |
mi r |
R1 |
dod'1 |
la |
mi |
fad4 sold la fad |
sold2 fad |
mi4 fad sold sold, |
dod2 dod' |
sold1 |
la |
si2 si, |
mi2. mi4 |
la2. sold4 |
fad2. fad4 |
si2. la4 |
sold2. sold4 |
dod'2. si4 |
la2. fad4 |
si2 la |
sold1 |
la |
si |
dod' |
sold |
la |
si |
mi2 sold, |
la,1 |
si, |
dod |
sold, |
la, |
si, |
mi,4 mi,8 mi, mi,4 mi,8 mi, |
mi,4 mi,8 mi, mi, mi, mi, mi, |
mi,1 |
<>^"Basses" mi2 si,4 |
mi si, mi, |
r r mi |
si,2 si,4 |
mi2 mi,4 |
si,2. |
si4 si si |
mi mi mi |
si,2 si,4 |
mi,2. |
mi2 si,4 |
mi si, mi, |
r r mi |
si,2 si,4 |
mi2 mi,4 |
si,2 r4 |
si, mi si, |
mi,2 r4 |
<>^"Basses" r4 r dod\fort |
red2 red4 |
mi2 mi4 |
fad2 fad8.(\trill mi32 fad) |
sold2 r4 |
R2. |
r4 fad\doux sold |
la la,2 |
sold,2. |
r4 mi8 mi sold sold si si |
sold sold mi mi sold sold si si |
