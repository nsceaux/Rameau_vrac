\tag #'(bachus basse) {
  Par -- ta -- gez mes trans -- ports di -- vins,
  sur mon char de vic -- toire, au sein de la mo -- les -- se
  ren -- dez le Ciel ja -- loux, en -- chaî -- nez les hu -- mains,
  un Dieu plus fort que moi nous en -- traî -- ne et nous pres -- se.
  %%
  Que le Thyr -- se rè -- gne tou -- jours
  dans les plai -- sirs et dans la guer -- re,
  qu’il tien -- ne lieu du ton -- ner -- re,
  et des flè -- ches des  a -- mours.
  Qu’il tien -- ne lieu du ton -- ner -- re,
  et des flè -- ches des a -- mours,
  et des flè -- ches des a -- mours.
}

\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Que le Thyr -- se rè -- gne tou -- jours
  dans les plai -- sirs et dans la guer -- re,
  qu’il tien -- ne lieu du ton -- ner -- re,
  et des flè -- ches des  a -- mours,
  et des flè -- ches des  a -- mours.
  Que le Thyr -- se rè -- gne tou -- jours
  dans les plai -- sirs et dans la guer -- re,
  qu’il tien -- ne lieu du ton -- ner -- re,
  et des flè -- ches des  a -- mours.
  Qu’il tien -- ne lieu du ton -- ner -- re,
  et des flè -- ches des  a -- mours.
  Que le Thyr -- se rè -- gne tou -- jours
  dans les plai -- sirs et dans la guer -- re,
  qu’il tien -- ne lieu du ton -- ner -- re,
  et des flè -- ches des  a -- mours,
  et des flè -- ches des  a -- mours,
  et des flè -- ches des  a -- mours.
}
