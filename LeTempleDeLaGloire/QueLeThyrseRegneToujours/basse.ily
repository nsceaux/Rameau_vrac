\clef "basse" <>^"[B.C.]" la,2 fad |
sol~ sol8 fad |
mi4 la la, |
re1 |
re'2. |
dod' |
si2 mi'4 |
la2 la4 |
re'4. dod'8 si la |
re2. |
dod |
re4 mi2 |
la, <>^"Basses" r4 |
r r re8\doux mi |
fad2 la4 |
fad fad re |
la2 la,4 |
re2. |
R2. |
r4 r r8 re |
re2. |
dod2 re4 |
mi2. |
fad2 mi4 |
re2 dod4 |
re mi2 |
la,2. |
R2.*2 |
re4 fad la |
re fad la |
sol,2.~ |
sol, |
r4 r mi |
la2 fad4 |
mi\trill re2 |
la2. |
R2. |
re'2 fad4 |
sol la2 |
re2
%%
re8 mi |
fad2 la4 |
fad re la, |
re2 re4 |
la,2 la,4 |
re,2. |
re'4 re' re' |
re'2 re'8 la |
re'2~ re'16 mi' re' mi' |
re'2~ re'16 mi' re' mi' |
re'2 re'8 r |
re2 re4 |
dod2 dod4 |
dod2( si,8)\trill la, |
mi2. |
fad2 mi4 |
re2\trill dod4 |
re mi2 |
la,16 \ru#11 la |
re'8. re'16 re4 r |
\ru#12 si16 |
mi'8. mi'16 mi4 r |
dod2. |
r4 dod2 |
re2. |
dod8. re16 mi4 mi, |
la,2 r4 |
R2. |
r4 r fad8 fad |
si2 re'4 |
si2\trill si8 fad |
\appoggiatura fad sol4. sol8 sol si |
mi4. mi8 mi dod |
fad2. |
fad4 r r |
R2. |
si,4 re fad |
si4 si4. si8 |
si2.~ |
si4~ si16 do' si la sol la sol fad |
mi2.~ |
mi4~ mi16 fad mi re dod mi re mi |
fad2. |
sol4( fad)\trill mi |
re2 re4 |
mi fad2 |
si, r8 r16 si, |
mi2 r8 r16 mi |
re8. mi16 fad4 fad, |
si,2. |
sol4 sol4. sol8 |
fad2\trill fad8 si |
mi2. |
mi8 r r4 mi'8 mi' |
dod'2\trill dod'4 |
re'2~ re'8 re |
la2. |
R2.*4 |
r4 r re8 re |
sol2 sol4 |
sol2 sol8 sol |
sol2.~ |
sol4. sol8 sol mi |
la4. la8 la la |
la2.~ |
la2 r4 |
r r sol |
fad2 r4 |
re'4 re' re' |
re'2 re'8 la |
re'4~ re'16 mi' re' do' si do' si la |
sol2 sol8 r |
mi2 mi4 |
la2 re'4 |
dod'\trill re'2 |
la2. |
fad2 sol4 |
fad4.( mi8)\trill re4 |
sol la2 |
re2. |
sol2 sol4 |
sol2 fad4 |
sol la2 |
re2. |
sol,2 sol,4 |
sol,2 fad,4 |
sol, la,2 |
re2. |
