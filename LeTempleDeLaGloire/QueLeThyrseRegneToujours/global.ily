\key re \major
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*2
\once\omit Staff.TimeSignature \time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 s2.*8 s2 \bar "||"
\tempo "Très gai" \midiTempo#140 s4 s2.*18
\tempo "Lent" s2.*9 s2
%% Chœur
s4 s2.*90 \bar "|."