\clef "haute-contre" R1 R2.*2 R1 R2.*8 r2 r4 R2.*27 |
r4 r re''8 dod'' |
re''2 la'4 |
la'2 la'8 la' |
la'4. la'8 sol' fad' |
sol'4. sol'8 fad'\trill mi' |
\appoggiatura mi'8 fad'2 \appoggiatura mi'8 re'4 |
la'4 la' la' |
la'2 la'8 la' |
la'2~ la'16 la' la' la' |
la'2~ la'16 la' la' la' |
la'16 re' re' re' re' re' re' re' re'8 r |
fad'2 sold'4 |
la'2 la'4 |
re''4~ re''4. dod''8 |
sold'2.\trill |
la'4.( sold'8) la'4 |
\appoggiatura sold'8 fad'2 mi'4 |
la' sold'2\trill |
\ru#11 dod''16 mi'' |
la'4 la' r |
\ru#12 re''16 |
re''8. re''16 si'4 r |
\ru#12 mi''16 |
mi''2 mi''4 |
la'2 sold'4 |
la'8. si'16 sold'4.\trill la'8 |
la'2 r4 |
R2. |
r4 r fad'8 fad' |
fad'2 fad'4 |
fad'4. re''8 re'' re'' |
re''4. re''8 re''( dod''16 si') |
si'4. si'8 sol' mi' |
dod'2. |
R2.*2 |
re''4~ re''16 mi'' re'' dod'' si'8. fad'16 |
fad'4. fad'16 fad' fad' fad' fad' fad' |
\ru#12 fad' |
\ru#11 fad' sol' |
sol'2. |
sol' |
dod' |
dod''4 re'' mi'' |
fad'2 fad'4 |
sol' fad'2 |
fad' r8 r16 si' |
si'2 r8 r16 lad' |
si'8. dod''16 lad'4.\trill si'8 |
si'2. |
si'4 si'4. si'8 |
la'2 la'8[ \appoggiatura sol' fad'] |
\appoggiatura fad' sol'4~ sol'16 do'' si' la' sol' la' sol' fad' |
mi'4 r si'8 si' |
mi''2 mi''4 |
la'2~ la'8 re'' |
dod''2\trill la'4 |
la'2.~ |
la'~ |
la'2 r4 |
R2. |
r4 r fad'8 la' |
re'2 re'4 |
re'2 re''8 re'' |
re''2 re''4 |
re''4. re''8 re'' mi'' |
dod''4.\trill dod''8 dod'' dod'' |
dod''2.~ |
dod''2 la'4 |
mi'2 dod'4\trill |
\appoggiatura dod'8 re'2 r4 |
la'4 la' la' |
la'2 la'8 la' |
la'2~ la'8. re'16 |
re'2. |
si'2 mi'4 |
mi'2 fad'4 |
sol' fad' la' |
la'2. |
re''2 re'4 |
sol'2 fad'4 |
si' la' sol' |
fad'2. |
re'2 sol'4 |
mi'2\trill fad'4 |
mi' sol'2 |
fad'2. |
re'2 mi'4 |
dod'2\trill re'4 |
re' dod'2\trill |
re'2. |
