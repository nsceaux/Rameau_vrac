\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with {
        instrumentName = "Dessus"
        shortInstrumentName = "D."
      } <<
        \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
        \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
      >>
      \new Staff \with {
        instrumentName = "Parties"
        shortInstrumentName = \markup\center-column { H-c. T. }
      } << \global \includeNotes "parties" >>
    >>
    \new ChoirStaff \with { shortInstrumentName = "Ch." } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\character Bachus
      shortInstrumentName = \markup\character Ba.
    } \withLyrics <<
      \global \keepWithTag #'bachus \includeNotes "voix"
    >> \keepWithTag #'bachus \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = "Basses"
      shortInstrumentName = "B."
    } <<
      \global \includeNotes "basse"
      \origLayout {
        s1 s2.*2\break s1 s2.*4\break s2.*4 s2 \break
        s4 s2.*4\break \grace s8 s2.*6\pageBreak
        s2.*6\break s2.*7\break s2.*4 s2 \bar "" \pageBreak
        %% Chœur
        s4 s2.*5\pageBreak
        s2.*7\pageBreak
        %% 5
        s2.*8\pageBreak
        s2.*8\pageBreak
        \grace s8 s2.*5\pageBreak
        s2.*6\pageBreak
        s2.*8\pageBreak
        s2.*8\pageBreak
        s2.*8\pageBreak
        s2.*7\pageBreak
        s2.*7\pageBreak
        s2.*10\break
      }
    >>
  >>
  \layout { short-indent = 8\mm }
  \midi { }
}