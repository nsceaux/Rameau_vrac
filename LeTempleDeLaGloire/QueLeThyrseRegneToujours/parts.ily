\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes dessus1)
   (violon2 #:notes "dessus" #:tag-notes dessus2)
   (haute-contre #:score-template "score-voix")
   (taille #:score-template "score-voix")
   (hautbois #:score "score-dessus")
   (flutes #:score "score-dessus")
   (bassons #:notes "basse" #:score-template "score-voix")
   (basses #:notes "basse" #:score-template "score-voix"))
