\clef "dessus" R1 R2.*2 R1 R2.*8 |
r4 r \twoVoices #'(dessus1 dessus2 dessus) <<
  { <>^\markup { \concat { p \super rs } viol. }
    re'''8\doux dod''' |
    re'''2 re''4 |
    re''2 re''8 mi'' |
    fad''4 la'' re''' |
    re'''2 dod'''4\trill |
    \appoggiatura dod'''8 re'''2 la''8 r |
    la''4 re''' la'' |
    fad''2\trill fad''8( sold''16 la'') |
    sold''8.(\trill fad''16) mi''8.( sold''16) fad''8.( sold''16) |
    la''4~ la''16( sol''! fad'' mi'') re''8. dod''16 |
    si'2.\trill |
    la'2 dod''4 |
    fad'2 mi'4 |
    fad' si2\trill |
    la r4 |
    R2.*2 |
    fad''4 la'' re''' |
    fad'' la'' re''' |
    re''2.~ |
    re''16 mi'' re'' do'' si' do'' si' la' sol'8 r |
    si''2 sol''4 |
    \appoggiatura fad''8 mi''2\trill fad''4 |
    dod''8.( re''16) re''4.(\trill dod''16 re'') |
    mi''8. la'16 la'2 |
    R2. |
    la'2 re''4~ |
    re'' dod''4.(\trill si'16 dod'') |
    re''2 }
  { \tag#'dessus <>_\markup { \concat { 2 \super es } viol. }
    \tag#'dessus2 <>^\markup { \concat { 2 \super es } viol. }
    fad''8\doux sol'' |
    la''2 la'4 |
    la'2 fad'8 sol' |
    la'4 re''4. fad'8 |
    mi'2\trill la'4 |
    la'2. |
    R2. |
    re''4 fad'' re'' |
    dod''4( si'4.\trill la'16 si') |
    dod''4.( re''16 dod'') si'8 dod''16 la' |
    mi'8 r r4 r |
    la'2 la'4 |
    la'4.( sold'8) la'4 |
    re''4 \appoggiatura dod''8 si'4 mi'' |
    dod''8.(\trill re''16) mi''8.( fad''16) sol''8.( mi''16) |
    fad''4 r r |
    R2. |
    la'4 re'' fad'' |
    la'' re''' fad'' |
    fad''4~ fad''4.( mi''8) |
    mi''4 r r |
    mi'2 sol'4 |
    sol'( mi') la'~ |
    la'2 re''4 |
    dod''2.\trill |
    R2. |
    r4 r re''8.( dod''16) si'8.( la'16) sol'8.( fad'16) sol'8.( la'16) |
    fad'2\trill \startHaraKiri
  }
>>
%% Chœur
<>^"Tous" la'8 la' |
re''2 re''4 |
re''2\trill re''8 mi'' |
\appoggiatura mi''8 fad''4. fad''8 sol'' la'' |
dod''4.\trill dod''8 re'' mi'' |
la'2. |
fad''4 fad'' fad'' |
fad''2 fad''8 la'' |
fad''2~ fad''16 sol'' fad'' sol'' |
fad''2~ fad''16 sol'' fad'' sol'' |
fad''\trill re' re' re' re' re' re' re' re'8 r |
re''4( dod'')\trill si' |
mi''4~ mi''4. la'8 |
la'2( sold'8)\trill la' |
\appoggiatura la'8 si'2. |
re''2 dod''4 |
la'4.( sold'8)\trill la'4 |
si' si'2\trill |
\ru#12 mi''16 |
mi''8. la'16 fad''4 r |
\ru#12 fad''16 |
fad''8. si'16 sold''4 r |
la''16 sold'' fad'' mi'' la'' sold'' fad'' mi'' la'' sold'' fad'' mi'' |
la''2~ la''16 sold'' fad'' mi'' |
fad'' mi'' re'' dod'' re'' dod'' si' la' si' la' sold' fad' |
mi'8. re''16 si'4.\trill la'8 |
la'2 dod''8 dod'' |
fad''2 sol''4 |
fad''4.( mi''8)\trill re'' dod'' |
\appoggiatura dod''8 re''2.~ re''4. re''8 mi'' fad'' |
fad''2( mi''4)\trill |
dod''2 re''8 mi'' |
lad'2.\trill |
fad'4 lad' dod'' |
fad'' fad''4. fad''8 |
fad''4~ fad''16 sol'' fad'' mi'' re'' mi'' re'' dod'' |
si'4~ si'16 dod'' re'' mi'' fad'' sold'' la'' fad'' |
si'' \ru#11 re''' |
\ru#12 re''' |
re'''2~ re'''8 mi'''16 re''' |
dod'''2. |
lad''2\trill r4 |
si''2 lad''4\trill |
si''2 si''4 |
si'' lad''2\trill |
si''2~ si''16 la'' sol'' fad'' |
sol'' fad'' mi'' re'' mi'' re'' dod'' si' dod'' si' lad' sold' |
fad'8. re''16 dod''4.\trill si'8 |
si'2. |
re''4 re''4. re''8 |
red''2\trill red''8 mi'' |
mi''4~ mi''16 do'' si' la' sol' la' sol' fad' |
mi'4 r sol''8 sol'' |
sol''2~ sol''8 fad''16 mi'' |
fad''2( sol''16) fad'' mi''32*4/3([ re'' mi'']) |
mi''2\trill \twoVoices #'(dessus1 dessus2 dessus) <<
  { <>^\markup { \concat { p \super rs } d. }
    mi''4 |
    fad''( mi'') la'' |
    la''( sol'')\trill fad'' |
    mi''2\trill }
  { \stopHaraKiri <>^\markup { \concat { 2 \super es } d. }
    dod''4 |
    re''( dod'') fad'' |
    fad''( mi'')\trill re'' |
    dod''2\trill \startHaraKiri }
>> <>^"Tous" la'8 la' |
re''2 re''4 |
re''2 re''8 fad'' |
si'2.~ |
si'4. re''8 re'' si' |
mi''4. mi''8 mi'' mi'' |
mi''2.~ |
mi''~ |
mi''~ |
mi''2 sol''4 |
dod''2 mi''4 |
la'16 si' dod'' re'' mi'' fad'' sol'' mi'' la''8 r |
fad''4 fad'' fad'' |
fad''2 fad''8 la'' |
fad''4\trill~ fad''16 sol'' fad'' mi'' re'' mi'' re'' do'' |
si'4~ si'16 dod'' re'' mi'' fad'' sol'' la'' fad'' |
sol''2 mi''4 |
re''4.( dod''8)\trill re''4 |
la'4 fad''2 |
mi''2.\trill |
la''2 re''4 |
re''4.( dod''8)\trill re''4 |
mi''4 mi''2\trill |
re''2. |
si'4( la'\trill) sol' |
sol'4.( la'8) re''4 |
mi'' dod''2\trill |
re''2. |
si'4( la')\trill sol' |
sol'( la'8) r re''4 |
mi'4 mi'2\trill |
re'2. |
