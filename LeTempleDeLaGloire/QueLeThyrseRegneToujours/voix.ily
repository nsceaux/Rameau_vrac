<<
  \tag #'(bachus basse) {
    \clef "vhaute-contre" r4 r8 la16 la re'8 re'16 fad' mi'8[( re'16)]\trill re' |
    si4.\trill re'16 re' re'8 mi'16 fad' |
    \appoggiatura fad'8 sol'8. sol'16 fad'8.([ mi'16])\trill mi'8 fad'16 sol' |
    fad'2\trill \appoggiatura mi'8 re'4 r |
    fad'4 sol'8 fad' mi'\trill re' |
    \appoggiatura re' mi'2 la'8 dod' |
    \appoggiatura dod' re'2 si8 mi' |
    dod'2\trill mi'4 |
    fad'4. fad'8 sold' la' |
    \appoggiatura la'8 si'2 mi'8 la' |
    la'4.\melisma sold'16([ fad'] mi'4)\melismaEnd |
    re'16[ dod'] si[ la] mi2 |
    la
    %%
    re'8 mi' |
    fad'2 la'4 |
    fad' re' la |
    re'4. re'8 mi' fad' |
    mi'4.\trill mi'8 fad' sol' |
    fad'2\trill \appoggiatura mi'8 re'4 |
    fad'4 la' fad' |
    re'2 re'8 si |
    mi'2.~ |
    mi'4.\melisma re'16[ dod'] si8[ dod'16 la]( |
    sold2)\trill\melismaEnd sold4 |
    re'2 dod'4 |
    dod'4.( si8)\trill la4 |
    si sold2\trill |
    la2. |
    re'4 fad' la' |
    re' fad' la' |
    re''2.~ |
    re''8[\melisma la' sol' fad' mi' re'] |
    si'2.~ |
    si'2\melismaEnd si'8 r |
    sol'2 mi'4 |
    dod'2\trill re'4 |
    mi' fad'2 |
    mi'2.\trill |
    la'2 re''4 |
    fad'2\trill la'4 |
    mi' mi'2\trill |
    re'2
  }
  \tag #'vdessus \clef "vdessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R1 R2.*2 R1 R2.*36 | r4 r }
>>
<<
  \tag #'(bachus basse) { r4 R2.*90 }
  \tag #'vdessus {
    <>^\markup\character Chœur la'8 la' |
    re''2 re''4 |
    re''2\trill re''8 mi'' |
    \appoggiatura mi''8 fad''4. fad''8 sol'' la'' |
    dod''4.\trill dod''8 re'' mi'' |
    la'2 la'4 |
    fad''4 fad'' fad'' |
    fad''2 fad''8 la'' |
    fad''2\trill~ fad''16[\melisma sol'' fad'' sol''] |
    fad''2~ fad''16[ sol'' fad'' sol'']( |
    fad''2)\trill\melismaEnd \appoggiatura mi''8 re''4 |
    re''( dod'')\trill si' |
    mi''4~ mi''4. la'8 |
    la'2( sold'8)\trill la' |
    \appoggiatura la'8 si'2. |
    re''2 dod''4 |
    la'4.( sold'8)\trill la'4 |
    si' si'2\trill |
    la'2. |
    R2.*7 |
    r4 r dod''8 dod'' |
    fad''2 sol''4 |
    fad''4.( mi''8)\trill re'' dod'' |
    \appoggiatura dod''8 re''2.~ |
    re''4. re''8 mi'' fad'' |
    fad''2( mi''4)\trill |
    dod''2 re''8 mi'' |
    lad'2\trill lad'4 |
    fad'4 lad' dod'' |
    fad''4 fad''4. fad''8 |
    fad''2.~ |
    fad''~ |
    fad''~ |
    fad''~ |
    fad''2~ fad''8[\melisma sol''16 fad''] |
    mi''2.~ |
    mi''2\melismaEnd mi''8 r |
    mi''4( re'')\trill dod'' |
    fad''2 fad''4 |
    dod'' dod''2\trill |
    si'2. |
    R2.*3 |
    re''4 re''4. re''8 |
    red''2\trill red''8 mi'' |
    mi''2. |
    mi''8 r r4 sol''8 sol'' |
    sol''2~ sol''8 fad''16[ mi''] |
    fad''2( sol''16)[ fad''] mi''32*4/3[ re'' mi''] |
    mi''2.\trill |
    R2.*2 |
    r4 r la'8 la' |
    re''2 re''4 |
    re''2 re''8 re'' |
    re''2.~ |
    re''4. re''8 re'' si' |
    mi''4. mi''8 mi'' mi'' |
    mi''2.~ |
    mi''~ |
    mi''~ |
    mi''2 mi''8 r |
    R2.*2 |
    fad''4 fad'' fad'' |
    fad''2 fad''8 la'' |
    fad''4\trill~ fad''16[\melisma sol'' fad'' mi''] re''[ mi'' re'' do'']( |
    si'2)\melismaEnd si'8 r |
    sol''2 mi''4 |
    re''4.(\trill dod''8) re''4 |
    la'4 fad''2 |
    mi''2.\trill |
    la''2 re''4 |
    re''4.( dod''8)\trill re''4 |
    mi'' mi''2\trill |
    re''2. |
    si'4( la')\trill sol' |
    sol'4.( la'8) re''4 |
    mi'' dod''2\trill |
    re''2. |
    R2.*4 |
  }
  \tag #'vhaute-contre {
    fad'8 sol' |
    la'2 la'4 |
    la'2 la'8 la' |
    la'4. la'8 sol'\trill fad' |
    \appoggiatura fad' sol'4. sol'8 fad' mi' |
    re'2 la4 |
    la'4 la' la' |
    la'2 la'8 la' |
    la'2.~ |
    la'~ |
    la'2 la'4 |
    fad'( mi')\trill re' |
    mi'2 mi'4 |
    mi'2~ mi'8 la' |
    sold'2.\trill |
    la'4.( sold'8) la'4 |
    si'2 mi'4 |
    la' sold'2\trill |
    la'2. |
    R2.*9 |
    r4 r fad'8 fad' |
    fad'2 fad'4 |
    fad'2 fad'8 fad' |
    si'4. si8 si si |
    sol'4. sol'8 sol' sol' |
    dod'2. |
    dod'4 r r |
    R2.*3 |
    si4 re' fad' |
    si' si'4. si'8 |
    si'2.~ |
    si'2~ si'8[ dod''16 si']( |
    lad'2)\trill lad'8 r |
    dod'4( re') mi' |
    fad2 fad'4 |
    sol' fad'2 |
    \appoggiatura mi'8 re'2. |
    R2.*3 |
    si'4 si'4. si'8 |
    la'2 la'8 \appoggiatura sol' fad' |
    \appoggiatura fad' sol'2. |
    sol'8 r r4 si'8 si' |
    la'2 la'4 |
    la'2~ la'8 re' |
    dod'2.\trill |
    R2.*4 |
    r4 r la'8 la' |
    la'2 sol'4 |
    sol'2 sol'8 sol' |
    si'2.~ |
    si'4. sol'8 sol' sol' |
    sol'4. sol'8 sol' sol' |
    sol'2.~ |
    sol'2 sol'8 r |
    R2.*2 |
    la'4 la' la' |
    la'2 la'8 la' |
    la'2~ la'8[ sol'16 fad']( |
    si'2) si'8 r |
    si'2 \appoggiatura la'8 sol'4 |
    sol'2 fad'4 |
    la' la'2 |
    dod'2.\trill |
    fad'2 mi'4 |
    sol'2 fad'4 |
    mi' dod'2\trill |
    re'2. |
    sol'2 si'4 |
    mi'2 fad'4 |
    mi' mi'2\trill |
    re'2. |
    R2.*4 |
  }
  \tag #'vtaille {
    la8 la |
    la2 la4 |
    la2 la8 la |
    la4. la8 la la |
    la4. la8 la dod' |
    re'2 la4 |
    re'4 re' re' |
    re'2 re'8 la |
    re'2~ re'16[\melisma mi' re' mi'] |
    re'2~ re'16[ mi' re' mi']( |
    re'2)\melismaEnd la8 r |
    la2 sold4 |
    la2 la4 |
    re'2~ re'8 dod' |
    si2.\trill |
    si2 dod'4 |
    fad'2 mi'4 |
    fad' mi'2 |
    dod'2.\trill |
    R2.*9 |
    r4 r la8 la |
    si2 si4 |
    re'2 re'8 re' |
    re'4. re'8 re' dod'16[ si] |
    si4. si8 si dod' |
    dod'2. |
    dod'4 r r |
    R2. |
    re'4 re' re' |
    re' re'4. re'8 |
    re'2.~ |
    re'2~ re'16[ fad' mi' fad']( |
    sol'2.)~ |
    sol' |
    dod'2. |
    si2 lad4\trill |
    si2 si4 |
    si lad2\trill |
    si2. |
    R2.*3 |
    sol'4 sol'4. si8 |
    si2 si8 si |
    si2. |
    si8 r r4 mi'8 mi' |
    mi'2 mi'4 |
    la2~ la8 re' |
    dod'2.\trill |
    R2.*4 |
    r4 r fad'8 fad' |
    fad'2 mi'4 |
    mi'2 mi'8 re' |
    re'2.~ |
    re'4. re'8 re' mi' |
    dod'4.\trill dod'8 dod' dod' |
    dod'2.~ |
    dod'2 dod'8 r |
    R2.*2 |
    la4 la la |
    la2 la8 la |
    re'2.~ |
    re'2 re'8 r |
    mi'2 mi'4 |
    mi'2 fad'4 |
    mi' re'2 |
    dod'2.\trill |
    re'2 re'4 |
    la2 la4 |
    si la2 |
    la2. |
    re'2 mi'4 |
    dod'2\trill re'4 |
    si la2 |
    la2. |
    R2.*4 |
  }
  \tag #'vbasse {
    re8 mi |
    fad2 la4 |
    fad re la, |
    re4. re8 mi fad |
    mi4.\trill mi8 fad sol |
    fad2\trill \appoggiatura mi8 re4 |
    re'4 re' re' |
    re'2 re'8 la |
    re'2~ re'16[\melisma mi' re' mi'] |
    re'2~ re'16[ mi' re' mi']( |
    re'2)\melismaEnd re'8 r |
    re2 re4 |
    dod2 dod4 |
    dod2( si,8)\trill la, mi2. |
    fad2 mi4 |
    re2\trill dod4 |
    re mi2 |
    la,2. |
    R2.*9 |
    r4 r fad8 fad |
    si2 re'4 |
    si2\trill si8 fad |
    \appoggiatura fad8 sol4. sol8 sol si |
    mi4. mi8 mi dod |
    fad2. |
    fad4 r r |
    R2. |
    si,4 re fad |
    si si4. si8 |
    si2.~ |
    si4~ si16[\melisma do' si la] sol[ la sol fad] |
    mi2.~ |
    mi4~ mi16[ fad mi re] dod[ mi re mi]( |
    fad2)\melismaEnd fad8 r |
    sol4( fad)\trill mi |
    re2 re4 |
    mi fad2 |
    si,2. |
    R2.*3 |
    sol4 sol4. sol8 |
    fad2\trill fad8 si |
    mi2. |
    mi8 r r4 mi'8 mi' |
    dod'2\trill dod'4 |
    re'2~ re'8 re |
    la2. |
    R2.*4 |
    r4 r re8 re |
    sol2 sol4 |
    sol2 sol8 sol |
    sol2.~ |
    sol4. sol8 sol mi |
    la4. la8 la la |
    la2.~ |
    la2 la8 r |
    R2.*2 |
    re'4 re' re' |
    re'2 re'8 la |
    re'4~ re'16[\melisma mi' re' do'] si[ do' si la]( |
    sol2)\melismaEnd sol8 r |
    mi2 mi4 |
    la2 re'4 |
    dod'\trill re'2 |
    la2. |
    fad2 sol4 |
    fad4.( mi8)\trill re4 |
    sol la2 |
    re2. |
    sol2 sol4 |
    sol2 fad4 |
    sol la2 |
    re2. |
    R2.*4 |
  }
>>
