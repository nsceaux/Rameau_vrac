\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes violon1)
   (violon2 #:notes "dessus" #:tag-notes violon2)
   (flutes #:notes "dessus" #:tag-notes hautbois-part)
   (hautbois #:notes "dessus" #:tag-notes hautbois-part)
   (haute-contre #:notes "haute-contre")
   (taille #:notes "taille")
   (bassons #:notes "basse" #:tag-notes basson-part)
   (basses #:notes "basse" #:tag-notes basse))
