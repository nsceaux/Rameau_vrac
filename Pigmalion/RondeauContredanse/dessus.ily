\clef "dessus"
\setMusic #'rondeau <<
  \tag #'(hautbois hautbois-part) {
    fad''2 |
    fad''4.( sol''16 la'') mi''4 mi'' |
    mi''2 la''~ |
    la'' sol'' |
    fad''\trill fad'' |
    fad''4.( sol''16 la'') mi''4 mi'' |
    mi''2 la''~ |
    la'' sol'' |
    fad''
  }
  \tag #'(violon1 violon2 violons) {
    <re' la' fad''>2 |
    q4.( sol''16 la'') mi''4 mi'' |
    mi''2 fad''8 sol'' la'' fad'' |
    re'' mi'' fad'' re'' si'4 dod''\trill |
    re'' la' <re' la' fad''>2 |
    q4.( sol''16 la'') mi''4 mi'' |
    mi''2 fad''8 sol'' la'' fad'' |
    re'' mi'' fad'' re'' si'4 dod''\trill |
    re''2
  }
>>
\tag #'(hautbois-part) <>^"Hautbois et flageolet"
\keepWithTag #'(violon1 violon2 violons hautbois) \rondeau
<<
  \tag #'hautbois { r2 R1*5 r2 }
  \tag #'(violon1 violon2 violons hautbois-part) {
    \tag #'hautbois-part <>^"Violons"
    la''2 |
    si''4\trill la'' si''8 dod''' re'''4 |
    la''2 re'''8 dod''' si'' la'' |
    re''' dod''' si'' la'' re''' dod''' si'' la'' |
    re'''2 re'''8 dod''' si'' la'' |
    si'' la'' sol'' fad'' mi'' re'' dod'' si' |
    la'2
  }
>>
\tag #'hautbois-part <>^"Hautbois et flageolet"
\rondeau
re'8 mi' fad' sol' |
la' si' dod'' la' re'' mi'' fad'' sol'' |
la''4 la'' re'8 mi' fad' sol' |
la' si' dod'' la' re'' mi'' fad'' sol'' |
la''2 mi''8 dod'' la' dod''16( mi'') |
fad''8 re'' si' re''16( fad'') sold''8 mi'' dod'' mi''16( sold'') |
la''8 fad'' re'' fad''16( la'') si''8 sold'' mi'' sold''16( si'') |
re'''8( dod''') si''( la'') dod''' si'' la'' sold'' |
la''4 sold''8\trill fad'' mi'' fad'' re'' mi'' |
dod''4\trill si'8 la' si'4.(\trill la'16 si') |
la'2
\rondeau
<<
  \tag #'hautbois { r2 R1*7 r2 }
  \tag #'(violon1 violons hautbois-part) \new Voice {
    \tag #'(violons hautbois-part) \voiceOne
    \tag #'hautbois-part <>^"Violons"
    fad'8( sol') la'( si') |
    la'4.( si'8) la'( si') la'( si') |
    la'4.( si'8) la'( si') la'( si') |
    la'( si') la'( re'') la'4 sol'8 fad' |
    mi'2\trill dod''8( re'') mi''( fad'') |
    mi''4.( fad''8) mi''( fad'') mi''( fad'') |
    mi''4.( fad''8) mi''( fad'') mi''( la'') |
    mi''( fad'') mi''( la'') mi''4 re''8 dod'' |
    si'2\trill
  }
  \tag #'(violon2 violons hautbois-part) \new Voice {
    \tag #'(violons hautbois-part) \voiceTwo
    re'8( mi') fad'( sol') |
    fad'4.( sol'8) fad'( sol') fad'( sol') |
    fad'4.( sol'8) fad'( sol') fad'( sol') |
    fad'( sol') fad'( sol') fad'4 mi'8 re' |
    dod'2\trill la'8( si') dod''( re'') |
    dod''4.( re''8) dod''( re'') dod''( re'') |
    dod''4.( re''8) dod''( re'') dod''( re'') |
    dod''( re'') dod''( re'') dod''4 si'8 la' |
    sold'2\trill
  }
>>
<>^"Tous" la''8 sold'' la'' si'' |
la'' sold'' la'' si'' la'' sold'' la'' si'' la'' sold'' la'' si'' la'' sold'' la'' si'' |
la'' sold'' la'' si'' la'' sold'' la'' si'' |
la'' sold'' fad'' mi'' re'' dod'' si' la' |
fad'' mi'' re'' dod'' si' la' sold' fad' |
mi' fad' sold' la' si' dod'' re'' si' |
mi''4 la'8 si' si'4.\trill( la'16 si') |
la'2
\rondeau
