\clef "taille"
\setMusic #'rondeau {
  la'2 |
  la' si' |
  la' re'' |
  \appoggiatura dod''8 si'4 re''2 la'4 |
  la'2 la' |
  la' si' |
  la' re'' |
  \appoggiatura dod''8 si'4 re''2 la'4 |
  la'2
}
\keepWithTag #'() \rondeau
re''2 |
re''1 |
dod''2 r |
R1 |
la'2 la' re'' sol' |
mi'
\rondeau
la'2 |
la' sol' |
mi'\trill fad' |
mi' re' |
dod' dod''~ |
dod''4 si' si'2 |
la' mi' |
mi'4 fad'2 mi'4 |
mi'2 mi' |
mi'4 re'2 re'4 |
dod'2\trill
\rondeau
r2 |
R1*7 |
r2 mi''2 |
fad'' mi'' |
re'' dod'' |
si' dod''4. sold'8 |
la'1 |
la'2. re'4 |
si1\trill |
dod'4 fad' mi' re' |
dod'2\trill
\rondeau
