\clef "haute-contre"
\setMusic #'rondeau {
  re''2 |
  re'' re'' |
  dod'' re'' |
  \appoggiatura dod''8 si'4 re''2 la'4 |
  la'2 re'' |
  re'' re'' |
  dod'' re'' |
  \appoggiatura dod''8 si'4 re''2 la'4 |
  la'2
}
\keepWithTag #'() \rondeau
fad''2 |
mi''1 |
mi''2 r |
R1 |
fad''2 fad'' |
mi'' re'' |
dod''
\rondeau
fad''2 |
mi'' re'' |
dod''\trill la' |
la' sol' |
mi' mi''~ |
mi''4 re'' re''2 |
dod'' mi''~ |
mi''4 re''~ re'' re'' |
dod'' re''8 mi'' si'4 si' |
mi' la'2 sold'4\trill |
la'2
\rondeau
r2 |
R1*7 |
r2 mi'' |
fad'' mi'' |
re'' dod'' |
si' dod''4. sold'8 |
la'1 |
la'2. re'4 |
si1\trill |
dod'4 fad' mi' re' |
dod'2\trill
\rondeau
