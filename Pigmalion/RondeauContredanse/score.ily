\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Hautbois Flageolet }
    } <<
      \global \keepWithTag #'hautbois \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \keepWithTag #'violons \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "Hautes-contre" } <<
      \global \includeNotes "haute-contre"
    >>
    \new Staff \with { instrumentName = "Tailles" } <<
      \global \includeNotes "taille"
    >>
    \new Staff \with { instrumentName = "Bassons" } <<
      \global \keepWithTag #'basson \includeNotes "basse"
    >>
    \new Staff \with { instrumentName = "Basses" } <<
      \global \keepWithTag #'basse \includeNotes "basse"
      \origLayout {
        s2 s1*5 s2 \bar "" \break s2 s1*5\break s1*7\break s1*6\pageBreak
        s1*5\break s1*6\break s1*7 s2 \bar "" \pageBreak
        s2 s1*7\break s1*8\break \grace s8
      }
    >>
  >>
  \layout { }
  \midi { }
}
