\clef "basse"
\setMusic#'rondeau <<
  \tag #'basse {
    <re la>2 |
    q sol4 sol |
    sol2 fad |
    sol2. la4 |
    re2 <re la> |
    q sol,4 sol, |
    sol,2 fad, |
    sol,2. la,4 |
    re,2
  }
  \tag #'(basson basson-part) {
    re'2 |
    re' sol4 sol |
    sol2 fad' |
    fad' mi'\trill |
    re' re' |
    re' sol,4 sol, |
    la,2 fad'~ |
    fad' mi'\trill |
    re'
  }
>>
\keepWithTag #'(basse basson basson-part) \rondeau
<<
  \tag #'(basson) { r2 R1*5 r2 }
  \tag #'(basse basson-part) {
    \tag #'basson-part <>^"Basses"
    fad2 |
    sol1~ |
    sol2 r |
    R1 |
    fad2 fad, |
    sol, si, |
    la,
  }
>>
\tag #'basson-part <>^"Bassons"
\rondeau
re'2 |
dod' si |
la re |
dod si, |
la, dod |
re mi |
fad sold |
la4 fad re mi |
dod\trill si,8 la, sold,4 mi, |
la, fad, re, mi, |
la,2
\rondeau
<<
  \tag #'basson { r2 R1*7 r2 }
  \tag #'(basse basson-part) {
    \tag #'basson-part <>^"Basses"
    re'4 re' |
    re'2 re' |
    re' re' |
    re' re |
    la2 la4 la |
    la2 la |
    la la |
    la dod4. re8 |
    mi2
  }
>> <>^"Tous" dod2 |
re mi |
fad dod |
re mi |
fad1 |
re~ |
re |
dod4 re mi mi, |
la,2
\tag #'basson-part <>^"Bassons"
\rondeau
