\piecePartSpecs
#`((violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (haute-contre)
   (taille)
   (hautbois #:notes "dessus" #:tag-notes hautbois)
   (bassons #:notes "basse" #:tag-notes basson)
   (basses #:notes "basse" #:score-template "score-basse-continue"))
