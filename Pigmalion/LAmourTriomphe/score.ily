\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff \with {
        instrumentName = "Hautbois"
        shortInstrumentName = "Htb."
      } << \global \keepWithTag #'hautbois \includeNotes "dessus" >>
      \new GrandStaff \with {
        instrumentName = "Violons"
        shortInstrumentName = "Vln."
      } <<
        \new Staff << \global \keepWithTag #'violon1 \includeNotes "dessus" >>
        \new Staff <<
          { s1*30 \startHaraKiri s1*41 \stopHaraKiri s1*5 \startHaraKiri }
          \global \keepWithTag #'violon2 \includeNotes "dessus"
        >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Hautes-contre Tailles }
        shortInstrumentName = \markup\center-column { H-c. T. }
      } << \global \keepWithTag #'parties \includeNotes "parties" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Pigmalion
      shortInstrumentName = "P."
    } \withLyrics <<
      \global \keepWithTag #'pigmalion \includeNotes "voix"
    >> \keepWithTag #'pigmalion \includeLyrics "paroles"
    \new ChoirStaff \with {
      shortInstrumentName = "Ch."
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = "Basses"
      shortInstrumentName = "B."
    } <<
      \global \keepWithTag #'tous \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion { s1*30\break }
      \origLayout {
        s1*7\break s1*6\break s1*5\pageBreak
        s1*5\break s1*3\break s1*4\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
        s1*4\pageBreak
        \grace s8 s1*5\pageBreak
        s1*5\pageBreak
        s1*8\pageBreak
        s1*6\pageBreak
        s1*5\pageBreak
        s1*4\pageBreak
        s1*3\pageBreak
        \grace s8 s1*4\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*5\pageBreak
        s1*5\pageBreak
      }
    >>
  >>
  \layout { short-indent = 8\mm }
  \midi { }
}
