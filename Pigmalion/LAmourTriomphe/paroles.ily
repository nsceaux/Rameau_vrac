\tag #'(pigmalion basse) {
  L’A -- mour tri -- om -- phe
  an -- non -- cez sa vic -- toi -- re,
  il met tout son pou -- voir à com -- bler nos dé -- sirs
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.

  An -- non -- cez sa vic -- toi -- re,
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.

  L’A -- mour tri -- om -- phe
  an -- non -- cez sa vic -- toi -- re,
  ce Dieu n’est oc -- cu -- pé qu’à com -- bler nos dé -- sirs __
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.

  L’A -- mour tri -- om -- phe
  an -- non -- cez sa vic -- toi -- re,
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.

}
\tag #'vdessus {
  L’A -- mour tri -- om -- phe
  Ce Dieu n’est oc -- cu -- pé qu’à com -- bler nos dé -- sirs
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.

  An -- non -- çons sa vic -- toi -- re,
  an -- non -- çons sa vic -- toi -- re,
  on ne peut trop chan -- ter sa gloi -- re,
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve
  il la trou -- ve dans nos plai -- sirs.


  L’A -- mour tri -- om -- phe
  on ne peut trop chan -- ter sa gloi -- re,
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.
}
\tag #'vhaute-contre {
  L’A -- mour tri -- om -- phe
  Ce Dieu n’est oc -- cu -- pé qu’à com -- bler nos dé -- sirs
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.

  An -- non -- çons
  an -- non -- çons sa vic -- toi -- re,
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs.


  L’A -- mour tri -- om -- phe
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.
}
\tag #'vtaille {
  L’A -- mour tri -- om -- phe
  Ce Dieu n’est oc -- cu -- pé qu’à com -- bler nos dé -- sirs
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.

  An -- non -- çons
  an -- non -- çons sa vic -- toi -- re,
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve,
  il la trou -- ve dans nos plai -- sirs.


  L’A -- mour tri -- om -- phe
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.
}
\tag #'vbasse {
  L’A -- mour tri -- om -- phe
  Ce Dieu n’est oc -- cu -- pé qu’à com -- bler nos dé -- sirs
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.

  An -- non -- çons
  an -- non -- çons sa vic -- toi -- re,
  on ne peut trop chan -- ter sa gloi -- re, sa gloi -- re,
  il la trou -- ve,
  il la trou -- ve dans nos plai -- sirs.


  L’A -- mour tri -- om -- phe
  on ne peut trop chan -- ter sa gloi -- re,
  on ne peut trop chan -- ter sa gloi -- re,
  il la trou -- ve dans nos plai -- sirs,
  il la trou -- ve dans nos plai -- sirs.
}
