\clef "taille" R1*30 |
R1 |
r4 r8 do''8 do''4. do''8 |
do''1~ |
do''~ |
do''2 r |
R1*4 |
r2 r4 sol' |
fa'2. do''8 do'' |
do''2 do''4 re'' |
mi''2 re''8( do'') si'( la') |
sol'2 r4 sol'\tresdoux |
sol'2.( la'8) sol' |
fa'4 re' si sol |
re'2. re''4 |
\appoggiatura do''8 si'4 r r sol' |
do'2 r4 do'\fort |
sol si' do'' re'' |
sol'2 do''~ |
do''4 la'8 sol' fa' mi' re' do' |
re'2 r |
r do''4 do'' |
do'' do''8 do'' do''4 fa' |
re'1\trill |
sol'2 re'' |
do''4 do''8 sol' la'4 sol' |
sol'1 |
sol'2 si' |
do''~ do''4. re''8 |
do''2( si'4.)\trill do''8 |
do''1 |
R1*4 |
r2 r4 do''8 do'' |
do''2. do''8 sib' |
mi'2 mi'4 sol' |
sol'2. mi'4 |
la2 r4 la'\doux |
re'2 mi' |
fa' r4 fa |
sol2 sib |
la r |
r r4 re''8 re'' |
re''4 re'' mi'' re'' |
re'' sib'2 sib'4 |
mi'2 mi''~ |
mi'' re''4 re''8 re'' |
sib'4 sib'8 sib' sib'4 la' |
la'1 |
dod'2 la |
re'~ re'4. sol8 |
la1 |
re\fermata |
r4 r8 <>^"a 2. cordes" <re' sib'>8\fort q4. q8 |
q1~ |
q |
<do' la'>~ |
q~ |
q2 r |
R1*6 |
r4 r8 re'' re''4 re'' |
re'' re'' re''4. re''8 |
sol'1~ |
sol'~ |
sol'2 r4 do'8 do' |
fa'4 fa'8 fa' fa'4 fa8 fa |
sib4 sib8 sib sib4 re' |
sol2. sol8 sol |
do'2. mi'8 sol' |
do'4 do'8 do' re'4 do' |
do'1 |
sib'2 sol' |
do'2~ do'4. do'8 |
re'2 do' |
do'1 |
