<<
  \tag #'(pigmalion basse) {
    \clef "vhaute-contre" <>^\markup\character Pigmalion r4 r8 fa' fa'4. la'8 |
    \appoggiatura sol'8 fa'1~ |
    fa'4(\melisma mi'8)\trill[ re'] do'[ sib la sol] |
    fa1~ |
    fa\melismaEnd |
    fa4 r r la8 la |
    sib2 sib4 do' |
    re'4.\melisma mi'8[ re' mi' fa' sol'] |
    mi'[ fa' mi' fa'] fa'4.\trill mi'16[ fa'] |
    sol'1*3/4(\melismaEnd \afterGrace s4 do'8) |
    do'1 |
    r2 r4 fa' |
    mi' re' do'4. sib8 |
    la2\trill do'4 do' |
    fa'2 la4. re'8 |
    si1\trill |
    r4 sol la si |
    do' re' mi'4.\trill fa'8 |
    sol'2.\melisma la'8[ sol'] |
    fa'4 re' si sol |
    fa'2. sol'8[ fa'] |
    mi'[ re' mi' fa'] mi'2*3/4\trill( \afterGrace s8 re')\melismaEnd |
    re'2 do'4 si |
    la4\trill la8 la re'4. mi'8 |
    si2 re'4 sol' |
    mi'4\trill re'8 do' re'4 sol |
    do'2 re'4 sol' |
    fa'2 mi'4.\trill re'8 |
    re'2\trill~ re'4. do'8 |
    do'1 |
    R1*4 |
    r2 r4 la8 la |
    sib2 sib4 do' |
    re'4.\melisma mi'8[ re' mi' fa' sol'] |
    mi'[ fa' mi' fa'] fa'4.\trill( mi'16[ fa']) |
    sol'1*3/4( \afterGrace s4 do'8)\melismaEnd |
    do'1 |
    R1*3 |
    r4 sol la si |
    do' re' mi'4.\trill fa'8 |
    sol'2.\melisma la'8[ sol'] |
    fa'4 re' si sol |
    fa'2. sol'8[ fa'] |
    mi'[ re' mi' fa']( mi'2)\trill\melismaEnd |
    re'1 |
    R1*2 |
    r2 re'4 re' |
    sol'1~ |
    sol'4 sol'8 sol' sol'4 fa' |
    fa'1 |
    re'2 sol' |
    mi'4\trill re'8 do' re'4 sol |
    do'1 |
    R1*4 |
    r4 r8 fa' fa'4. la'8 |
    \appoggiatura sol'8 fa'1\melisma |
    fa'4 mi'8\trill[ re'] do'[ sib la sol]( |
    fa1)\melismaEnd |
    fa4 r r2 |
    r r4 fa'8 fa' |
    sib'2 sib'4 sol' |
    mi'1\trill |
    mi'4 r r mi' |
    fa'2 mi'4 re'8 dod' |
    \appoggiatura dod'8 re'2 mi'4 fa' |
    mi'2.\trill mi'8 re' |
    la'1~ |
    la'4. la'8 la'4 la' |
    la' sol' sol' sol' |
    sol'1~ |
    sol'2.\melisma la'8[ sol']( |
    fa'2)\melismaEnd fa'8 r fa' la' |
    re'4 re'8 re' re'4 dod' |
    re'1 |
    mi'2 la' |
    sol'( fa'4)\trill fa'8 mi' |
    mi'2\trill~ mi'4. re'8 |
    re'1\fermata |
    R1*2 |
    r4 r8 fa' fa'4.\trill sol'8 |
    la'1~ |
    la' |
    la'4 r r fa'8 fa' |
    do'2 do'4 re' |
    mib'8[\melisma re' do' re'] mib'[ sol' fa' mib'] |
    re'[ do' sib do'] re'[ fa' mi'! sol'] |
    fa'[ mi' re' mi'] fa'[ la' sol' fa']( |
    mi'1)\trill\melismaEnd |
    \appoggiatura re'8 do'1 |
    r4 fa' sol' la' |
    sib' sib' sib'4. sib'8 |
    sib'1~ |
    sib'4\melisma sol' mi' do' |
    sib'2.( la'8[ sol']) |
    la'1~ |
    la'2.( sib'8[ la'] |
    sol'2)\melismaEnd sol'4 r |
    r2 r4 sol'8 sib' |
    la'4 sol'8 fa' sol'4 do' |
    fa'1 |
    sol'2~ sol'4. sol'8 |
    do'2 do'4. fa'8 |
    fa'2( mi'4.)\trill fa'8 |
    fa'1 |
  }
  \tag #'vdessus {
    \clef "vdessus" R1*30 |
    <>^\markup\character Chœur r4 r8 fa'' fa''4. la''8 |
    \appoggiatura sol''8 fa''1~ |
    fa''4\melisma mi''8\trill[ re''] do''[ sib' la' sol']( |
    fa'1)\melismaEnd |
    fa'4 r r2 |
    R1*4 |
    r2 r4 mi'' |
    fa''2 fa''4 fa''8 sol'' |
    sol''4.( la''8) fa''4 re'' |
    sol''2 fa''8[ mi''] re''[ do''] |
    si'2\trill r |
    R1*3 |
    r4 sol' la' si' |
    do'' re'' mi''4.\trill fa''8 |
    sol''4\melisma la''8[ sol''] fa''4 sol''8[ fa''] |
    mi''1~ |
    mi''4 fa''8[ mi''] re''[ do'' si' la']( |
    sol'1)\melismaEnd |
    sol'4 r do'' mi'' |
    la' la'8 la' la'4 re'' |
    si'1\trill |
    re''2 sol'' |
    mi''4\trill re''8 do'' re''4 sol' |
    do''1 |
    R1*7 |
    r2 r4 do''8 do'' |
    fa''2 fa''4 la'' |
    re''2 re''8 r re'' re'' |
    re''2 re''4 mi'' |
    dod''1\trill |
    dod''4 r r2 |
    R1*3 |
    r4 la' si' dod'' |
    re'' mi'' fa'' re'' |
    \appoggiatura dod''8 si'4 si'8 si' dod''!4 re'' |
    mi''4 fa'' sol'' mi'' |
    dod''2 dod''8 r dod'' mi'' |
    la'2 la'8 r la'' sol'' |
    fa''4 mi''8 re'' mi''4 la' |
    re''1 |
    R1*3 R1^\fermataMarkup |
    r4 r8 sib' sib'4. sib'8 |
    sib'1~ |
    sib'2~ sib'8[ do'' la' sib']( |
    do''1)~ |
    do'' do''4 r r2 |
    R1*4 |
    r4 do'' re'' mi'' |
    fa'' fa'' fa''4.\trill mi''8 |
    re''4 re''8 re'' mi''4 fa'' |
    sol'' sol'' sol''4.\trill fa''8 |
    mi''2.( fa''8[ sol''] |
    do''1) |
    do''4 r r2 |
    R1 |
    r2 r4 fa'8 fa' |
    sib'4 sib'8 sib' sib'4 re'' |
    sol'2. do''8 sib' |
    la'4 sol'8 fa' sol'4 mi'\trill |
    fa'1 |
    sol'2 do'' |
    sib'( la'4)\trill la'8 sol' |
    sol'2\trill~ sol'4. fa'8 |
    fa'1 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R1*30 |
    R1 |
    r4 r8 do' do'4. do'8 |
    do'1~ |
    do'4\melisma re'8[ mi'] fa'[ sol' fa' sol']( |
    la'2)\melismaEnd la'4 r |
    R1*4 |
    r2 r4 sol' |
    la'2 do'4 fa'8 mi' |
    fa'2 la'4 si |
    do'2. re'8 mi' |
    re'2\trill r |
    R1*5 |
    r4 sol' sol' sol' |
    sol' sol' sol' sol' |
    sol'\melisma la'8[ sol'] fa'[ mi' re' do']( |
    re'1)\melismaEnd |
    re'4 r mi' mi' |
    mi' mi'8 mi' mi'4 re' |
    re'1 |
    sol'2 sol' |
    sol'4 sol'8 sol' fa'4 fa' |
    mi'1 |
    R1*8 |
    r2 r4 la'8 la' |
    la'2. sol'8 sol' |
    sol'2 sol'4 sol' |
    sol'1 |
    sol'4 r r2 |
    R1*4 |
    r4 r8 fa' fa'4 fa' |
    fa' mi' mi' mi' |
    mi'1~ |
    mi'~ |
    mi'4( fa'8[ mi']) re'8 r la' la' |
    la'4 la'8 la' sol'4 mi'\trill |
    fa'1 |
    R1*3 R1^\fermataMarkup |
    r4 r8 fa' fa'4. fa'8 |
    fa'1~ |
    fa'2~ fa'4.( sol'16[ mi'] |
    fa'1)~ |
    fa' |
    fa'4 r r2 |
    R1*6 |
    r4 r8 fa' fa'4 fa' |
    fa' re' sol'4. sol'8 |
    sol'1 |
    sol'4 r r2 |
    r r4 do'8 do' |
    fa'4 fa'8 fa' fa'4 la' |
    re'2. re'8 re' |
    sol'4 sol'8 sol' sol'4 sib' |
    mi'2. mi'8 mi' |
    fa'4 fa'8 fa' fa'4 do' |
    do'1 |
    do'2 do' |
    do'2 do'4. fa'8 |
    fa'2( mi'4.)\trill fa'8 |
    fa'1 |
  }
  \tag #'vtaille {
    \clef "vtaille" R1*30 |
    R1 |
    r4 r8 do' do'4. do'8 |
    do'1~ |
    do' |
    do'4 r r2 |
    R1*4 |
    r2 r4 do' |
    do'2 do'4 do'8 do' |
    do'2 do'4 si |
    do'2. re'8 mi' |
    re'2\trill r |
    R1*5 |
    r4 si do' re' |
    sol sol do' do' |
    \appoggiatura si8 la2.\melisma re'8[ do']( |
    si1)\melismaEnd |
    si4 r do' do' |
    do' do'8 do' do'4 fa' |
    re'1\trill |
    fa'2 re' |
    sol4 sol8 do' la4 sol |
    sol1 |
    R1*8 |
    r2 r4 fa'8 fa' |
    fa'2. fa'8 fa' |
    mi'2 mi'4 mi' |
    mi'1 |
    mi'4 r r2 |
    R1*4 |
    r4 r8 re' re'4 re' |
    re' re' mi' re' |
    re'2.( mi'8)[ re']( |
    la2) la8 r mi' mi' |
    fa'2 fa'8 r fa' fa' |
    \appoggiatura mi'8 re'4 re'8 re' sib4 la |
    la1 |
    R1*3 R1^\fermataMarkup |
    r4 r8 re' re'4. re'8 |
    re'1~ |
    re'2.( mi'8[ fa'] |
    do'1)~ |
    do' |
    do'4 r r2 |
    R1*6 |
    r4 r8 re' re'4 re' |
    re' re' re'4. re'8 |
    sol1 |
    sol4 r r2 |
    R1 |
    r2 r4 fa8 fa |
    sib4 sib8 sib sib4 re' |
    sol2. sol8 sol |
    do'2 do' |
    do' re'4 do' |
    la1\trill |
    do'2 mi' |
    fa' fa'4. do'8 |
    re'2 do' |
    la1\trill |
  }
  \tag #'vbasse {
    \clef "vbasse" R1*30 |
    R1 |
    r4 r8 la la4. do'8 |
    \appoggiatura sib8 la1~ |
    la2~ la8[ sib la sol]( |
    fa2) fa4 r |
    R1*4 |
    r2 r4 do' |
    la2\trill la4 la8 do' |
    fa2 fa4 fa |
    mi2.\trill mi8 fa |
    sol2 r |
    R1*5 |
    r4 sol la si |
    do' re' mi' mi |
    fa1~ |
    fa |
    fa4 r mi mi |
    fa4 fa8 fa fa4 re |
    sol1 |
    si2 sol |
    do'4 do'8 do' fa4 sol |
    do1 |
    R1*8 |
    r2 r4 fa8 fa |
    sib2. sib8 re' |
    sol2 sol4 mi |
    la1 |
    la4 r r2 |
    R1*4 |
    r2 r4 re8 re |
    sol4 sol8 sol sol4 sib |
    mi2. mi8 mi |
    la2 la8 r la la |
    re'2 re'8 r fa fa |
    sib4 sib8 sib sol4 la |
    re1 |
    R1*3 R1^\fermataMarkup |
    r4 r8 sib sib4. sib8 |
    sib1~ |
    sib2.( la8[\trill sol] |
    fa1)~ |
    fa |
    fa4 r r2 |
    R1*3 |
    r4 sol la si |
    do'4 do' do'4.\trill sib8 |
    la4 la8 fa sol4 la |
    sib sib sib4. la8 |
    sol2~ sol8[ sib la sib]( |
    do'1) |
    do'4 r r2 |
    R1*4 |
    r2 r4 do8 do |
    fa4 fa8 re sib,4 do |
    fa1 |
    mi2 do |
    fa2 fa4. fa8 |
    sib,2 do |
    fa,1 |
  }
>>
