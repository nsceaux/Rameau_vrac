\clef "dessus"
<<
  \tag #'violon1 {
    R1 |
    la'1\doux~ |
    la'~ |
    la'4 r8 fa'' fa''4. la''8 |
    \appoggiatura sol''8 fa''1~ |
    fa''4 mi''8 re'' do'' sib' la' sol' |
    fa'2 r4 fa'' |
    fa''2 sib''~ |
    sib''4 la''8( sol'') sib''( la'') sol''( fa'') |
    mi''2\trill mi''8( fa'') mi''( fa'') |
    sol''( fa'') mi''( re'') do''4 r |
    r2 r4 do'' |
    sib' la' sol'2~ |
    sol'4.( la'16 sol') fa'4 do' |
    r2 fa''~ |
    fa''2. sol'4 |
    la' si' do'' re'' |
    sol' si' do''4. re''8 |
    si'1\trill |
    R1 |
    r2 r4 re'' |
    re''4( do''8)\trill si' do''4. re''8 |
    si'2\trill sol''~ |
    sol''4( fa''8)\trill mi'' fa''2~ |
    fa'' re''4 sol'' |
    mi''\trill re''8 do'' re''4 sol' |
    do''4 do''8.(\trill si'32 do'') re''2~ |
    re''4.( do''16 si') \appoggiatura si'8 do''4. re''8 |
    do''2( si'4.)\trill do''8 |
    do''1 |
    
  }
  \tag #'(violon2 violon2-conducteur) {
    R1 |
    do'1\doux~ |
    do'~ |
    do'4. la'8 la'4. do''8 |
    la'1\trill~ |
    la'2 r4 do'' |
    re''2. la'4\trill |
    sib'2 re'' |
    sol' do''~ |
    do''2. sib'8( la') |
    sib'( la') sol'( fa') mi'4 r |
    r2 r4 do' |
    do' re' mi' sol' |
    do'2 la'~ |
    la'2. fa'4 |
    re'1\trill |
    r4 re' mi' fa' |
    mi' sol' sol'4.( fa'16 mi') |
    re'1\trill |
    R1 |
    r4 si' do'' re'' |
    sol'1 |
    r2 do''~ |
    do'' la' |
    re' re''4 si' |
    \appoggiatura la'8 sol'4 do''2 si'8\trill la' |
    sol'1~ |
    sol'2. la'4 |
    sol'2 fa' |
    mi'1 |
  }
  \tag #'hautbois { R1*30 }
>>
r4 r8 fa'' fa''4. la''8 |
\appoggiatura sol''8 fa''1~ |
fa''4 mi''8\trill re'' do'' sib' la' sol' |
fa'4. la'8 do''4. fa''8 |
la''2 do'''4 r |
R1*4 |
do''8 re'' mi'' fa'' sol'' la'' sib'' sol'' |
do'''2. do'''8 sol'' |
sol''4.( la''8) fa''4 re'' |
sol''2 fa''8( mi'') re''( do'') |
si'2\trill <<
  \tag #'(violon1 violon2) {
    r4 sol'\doux |
    la' si' do''4. fa''8 |
    \appoggiatura mi''8 re''4 si' sol' re' |
    si2. re'4 |
    sol
  }
  \tag #'hautbois { r2 | R1*3 | r4 }
>> sol'4\fort la' si' |
do'' re'' mi''4.\trill fa''8 |
sol''4 la''8 sol'' fa''4 sol''8 fa'' |
mi''1~ |
mi''4 fa''8 mi'' re'' do'' si' la' |
sol'2 <<
  \tag #'hautbois {
    re''4 re'' |
    sol''1~ |
    sol''4 sol''8 sol'' sol''4 fa'' |
    fa''1 |
  }
  \tag #'(violon1 violon2) {
    r2 |
    r do''4 mi'' |
    la' la'8 la' la'4 re'' |
    si'1
  }
>>
re''2 sol'' |
mi''4\trill re''8 do'' re''4 sol' |
do''1 |
re''2 sol'' |
fa''2( mi''4.)\trill re''8 |
re''2\trill~ re''4. do''8 |
do''1 |
R1*3 |
r2 r4 do''8 do'' |
fa''2 fa''4 la'' |
re''2. <<
  \tag #'hautbois {
    fa''8 fa'' |
    sib''2 sib''4 sol'' |
    mi''1\trill |
  }
  \tag #'(violon1 violon2) {
    re''8 re'' |
    re''2 re''4 mi'' |
    dod''1\trill |
  }
>>
<<
  \tag #'violon1 {
    r2 r4 dod''\doux |
    re''2 sol''4 fa''8 mi'' |
    la''2 sol''4 fa'' |
    sib''2. sol''8 sib'' |
    mi''4
  }
  \tag #'violon2 {
    r2 r4 la'4\doux |
    la'1~ |
    la'2 r4 re'' |
    re''2. dod''8 re'' |
    dod''4
  }
  \tag #'hautbois { R1*4 | r4 }
>> la'4 si' dod'' |
<<
  \tag #'hautbois {
    re''4. la''8 la''4 la'' |
    la'' sol'' sol'' sol'' |
    sol''1~ |
    sol''2.( la''8 sol'') |
    fa''2
  }
  \tag #'(violon1 violon2) {
    re''4 mi'' fa'' re'' |
    \appoggiatura dod''8 si'4. si'8 dod''!4 re'' |
    mi''4 fa'' sol'' mi'' |
    dod''2.\trill dod''8 mi'' |
    la'2
  }
>> r4 la''8 sol'' |
fa''4 mi''8 re'' mi''4 la' |
re''1 |
<<
  \tag #'hautbois { R1*3 R1^\fermataMarkup | }
  \tag #'(violon1 violon2) {
    mi''2\doux~ mi''4. mi''8 |
    la'2~ la'4. re''8 |
    re''2( dod''4.)\trill re''8 |
    re''1\fermata |
  }
>>
r4 r8 sib''\fort sib''4. sib''8 |
sib''4 la''8\trill sol'' fa'' mi'' re'' do'' |
<<
  \tag #'hautbois {
    sib'4 r8 fa'' fa''4.\trill sol''8 |
    \appoggiatura sol''8 la''1~ |
    la''~ |
    la''2 r |
    R1*6 |
    r4 fa'' sol'' la'' |
    sib''4 sib'' sib''4. sib''8 |
    sib''1~ |
    sib''~ |
    sib''2.( la''8\trill sol'') |
    la''1~ |
    la''2.( sib''8 la'') |
    re'''2 \appoggiatura do'''8 sib''2~ |
    sib''2
  }
  \tag #'(violon1 violon2) {
    sib'2~ sib'8 do'' la' sib' |
    do''1~ |
    do''1~ |
    do''2 r |
    r2 r4 fa'8\doux fa' |
    do'2 la |
    sib1 |
    si2. re'4 |
    sol4  do''4\fort re'' mi'' |
    fa'' fa'' fa''4.\trill mi''8 |
    re''4. re''8 mi''4 fa'' |
    sol'' sol'' sol''4.\trill fa''8 |
    mi''2.( fa''8 sol'') |
    do''1 |
    R1*2 |
    r2 r4 fa'8 fa' |
    sib'4 sib'8 sib' sib'4 re'' |
    sol'2
  }
>> r4 do''8 sib' |
la'4 sol'8 fa' sol'4 do' |
fa'1 |
<<
  \tag #'hautbois {
    sol''2~ sol''4. sol''8 |
    do''2~ do''4. fa''8 |
    fa''2( mi''4.\trill) fa''8 |
    fa''1 |
  }
  \tag #'(violon1 violon2) {
    sol'2 do'' |
    sib'2( la'4)\trill la'8 sol' |
    sol'2\trill~ sol'4. fa'8 |
    fa'1 |
  }
>>
