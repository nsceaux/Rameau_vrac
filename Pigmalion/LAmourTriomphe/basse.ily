\clef "basse" <>^"[B.C.]" R1 |
fa,1\doux~ |
fa,~ |
fa,~ |
fa,~ |
fa,2 r4 fa8 mib |
re2 re4 do |
sib,2 sol, |
do fa, |
do,1~ |
do,2. sib8( la) |
sol( fa) mi( fa) sol4 la |
sol fa mi do |
fa2 fa4 la |
re2. re4 |
sol1 |
r4 fa mi\trill re |
do1 |
sol,1~ |
sol,~ |
sol,4 sol, la, si, |
do re mi fa |
sol2 mi |
fa2. re4 |
sol2 si4 sol |
do'4 la fa sol |
mi4 re8 do si,4 sol, |
do2. fa,4 |
sol,1 |
do, |
<>^"Basses et bassons" R1 |
r4 r8 fa, fa,4. fa,8 |
fa,1~ |
fa,~ |
fa,2. <>^"B.C." fa8 mib |
re2 re4 la, |
\appoggiatura la,8 sib,2. sol,4 |
do2 la,4 fa, |
do,1 |
r2 r4 <>^"Tous" do' |
la2\trill la4 la8 do' |
fa2 fa4 fa |
mi2.\trill mi8 fa |
sol2 r4 fa4^\doux ^"[B.C.]" |
mi re do4. re8 |
si,2.\trill re4 |
sol,1 |
R1 |
r2 \clef "alto" r4 do' |
sol4 \clef "bass" <>^"[Tous]" sol la si |
do' re' mi' mi |
fa1~ |
fa |
r2 mi4 mi |
fa fa8 fa fa4 re |
sol1 |
si2 sol |
do'4 do'8 do' fa4 sol |
do1 |
si,2 sol, |
la,2~ la,4. fa,8 |
sol,1 |
do, |
R1*4 |
r2 r4 fa8 fa |
sib2. sib8 re' |
sol2 sol4 mi |
la1 |
r2 \clef "alto" r4 la'^\doux |
re'2 mi' |
fa' r4 fa |
sol2 sib |
la r |
r \clef "bass" r4 re8 re |
sol4 sol8 sol sol4 si |
mi2. mi8 mi |
la2 r4 la8 la |
re'2 r4 fa8 fa |
sib4 sib8 sib sol4 la |
re1 |
\clef "alto" dod'2 la |
re'~ re'4. sol8 |
la1 |
re\fermata |
\clef "bass"
r4 r8 \twoVoices #'(basse basson tous) <<
  { \tag #'tous <>^"Basses" sib,8 sib,4. sib,8 | sib,1 | }
  { \tag #'tous <>_"Bassons" sib,,8 sib,,4. sib,,8 | sib,,1 | }
>>
sib,2.( la,8)\trill sol, |
fa,1~ |
fa,~ |
fa,2 r |
R1*3 |
r4 sol la si |
do'4 do' do'4.\trill sib8 |
la4. fa8 sol4 la |
sib sib sib4.\trill la8 |
sol2~ sol8 sib la sib |
do'1 |
do2 r |
R1*4 |
r2 r4 do8 do |
fa4 fa8 re sib,4 do |
fa1 |
mi2 do |
fa2~ fa4. fa8 |
sib,2 do |
fa,1 |
