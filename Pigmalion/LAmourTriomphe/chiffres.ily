s1*6 <6>2. <6\\ _->4 s2 <7> <7>1 s1 \new FiguredBass { <"">2.\figExtOn <"">4\figExtOff }
s2. <6>4 <6\\ >2 <5/>4\figExtOn <5/>\figExtOff <9>2 <8>4\figExtOn <8>\figExtOff
<7>1 <7 _!>1 s4 <4\+> <6> <6\\ > s1 <_!> <7> <"">2.\figExtOn <"">4\figExtOff
<"">2.\figExtOn <"">4\figExtOff <_!>2 <6> <9> <6 5>4\figExtOn <6>\figExtOff
<7 _!>1 <"">4\figExtOn <"">\figExtOff <6 5> <_!> <6>4.\figExtOn <6>8 <5/>4 <5/>\figExtOff
<9>2 <8>4 <6 5> <4>2 <7 _!> s1

s1*5 <6>2. <5/>4 s2. <7>4 <7>2 <6>4\figExtOn <6>\figExtOff s1*5
s2. <4\+>4 <6> <6\\ > s4. <7>8 <5/>2.\figExtOn <5/>4\figExtOff
s1*25
s2. <_+>4 s2 <6\\ > <6>2. <6>4 <6 5>2\figExtOn <6>\figExtOff
s1*8 <5/>2\figExtOn <5/>\figExtOff s2.. <6 5>8 <6 4>2 <7 _+> s1
