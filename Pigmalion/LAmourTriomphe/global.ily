\key fa \major
\digitTime\time 2/2 \midiTempo#160
\tempo "Majestueusement" s1*5 s2 \tempo "Gai" s2 s1*24 \bar "||"
%% Chœur
\digitTime\time 2/2 s1*33
\tempo "Lent" s1*5 s2 \tempo "Gay" s2 s1*18
\tempo "Lent" s1*5 s2 \tempo "Gay" s2 s1*21 \bar "|."
