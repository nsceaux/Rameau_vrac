Rè -- gne a -- mour, fais bril -- ler __ tes flam -- mes,
lan -- ce, lan -- ce, lan -- ce tes traits,
lan -- ce, lan -- ce, lan -- ce tes traits dans nos â -- mes.
Sur des cœurs sou -- mis à tes lois,
é -- pui -- se ton car -- quois,
lan -- ce, lan -- ce, lan -- ce tes traits,
lan -- ce, lan -- ce, lan -- ce tes traits dans nos â -- mes,
lan -- ce tes traits dans nos â -- mes.

Tu nous fais, Dieu char -- mant, le plus heu -- reux __ des -- tin
je tiens de toi l’ob -- jet dont mon âme est ra -- vi -- e,
et cet ob -- jet si cher res -- pi -- re, tient la vi -- e
des feux de ton flam -- beau di -- vin.
