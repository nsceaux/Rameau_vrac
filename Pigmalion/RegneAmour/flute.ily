\clef "dessus" R1*43 |
do'''1~ |
do'''~ |
do'''~ |
do'''~ |
do'''~ |
do'''~ |
do'''2 si'' |
do''' r |
R1*38 |
fa''1 |
fa''\trill |
sol'' |
sol''\trill |
do''' |
sib''8( la'') sol''( fa'') la''( sol'') fa''( mi'') |
fa''4 mi''8\trill re'' do'' re'' sib' do'' |
la'4. sol'8 sol'4.\trill fa'8 |
fa'2 r4 |
<>_"Flûte seule" r la''4.( sol''16\trill fa'') |
\appoggiatura fa''8 sol''4 sib''4. la''8 |
la''4~ la''4. la''16( sol'') |
\appoggiatura sol''8 la''4. sol''16\trill fa'' mi''8 fa'' |
dod''2\trill \appoggiatura si'8 la'4 |
r2 r4 r8 la'' |
re''1 |
re'''2.( mi'''8 fa''') |
dod'''2\trill~ dod'''16( re''' dod''' re'''32 mi''') |
la''2 si''8.( la''32\trill sold'') |
\appoggiatura sold''8 la''2 r4 |
r r r8 la'' |
re'''2 r4 |
r4 r r8 si'' |
mi'''2 do'''4.( si''16\trill la'') |
la''2~ la''4. si''16 la'' la''4.\trill la''16( sold'') |
\appoggiatura sold''8 la''1 |
