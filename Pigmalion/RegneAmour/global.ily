\key fa \major \midiTempo#160
\beginMark "Prélude"
\digitTime\time 2/2 s1*8 \bar "||" \segnoMark
s1*81 \bar "|." \fineMark s1*8
\digitTime\time 3/4 \midiTempo#80 s2.*6
\digitTime\time 2/2 \midiTempo#160 s1*3
\digitTime\time 3/4 \midiTempo#80 \grace s8 s2.*6
\digitTime\time 2/2 \midiTempo#160 s1
\time 3/2 s1.
\digitTime\time 2/2 \grace s8 s1 \bar "|." \dalSegnoMark