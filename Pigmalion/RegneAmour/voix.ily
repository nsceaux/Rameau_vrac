\clef "vhaute-contre" R1*8 |
fa'1~ |
fa'~ |
fa'~ |
fa'~ |
fa'2~ fa'8[\melisma mi' re' do'] |
re'[ mi' fa' mi'] re'[ do' sib la] |
la2\(\melismaEnd sol4.\)\trill do'8 |
la1\trill |
R1 |
r2 la4. fa8 |
do'4.\melisma re'8 do'[ re' do' mi'] |
re'4. mi'8([ re' mi' re'])\melismaEnd mi' |
mi'1\trill |
\appoggiatura re'8 do'1 |
la'2. sol'8[ fa'] |
sol'[\melisma fa' mi' re'] do'4 mi' |
fa' la sib sol'8[ fa'] |
mi'[ re' do' sib] la4 do' |
re' fa sol re' |
do'8[ sib la sol] fa4.\melismaEnd do'8 |
do'2( sib4)\trill sib8 la |
sol1\trill |
la8[\melisma si do' re'] si[ do' la si] |
do'[ re' mi' fa'] re'[ mi' do' re']( |
mi'4)\trill\melismaEnd re' sol'2~ |
sol'4.\melisma la'8 sol'2~ |
sol'4. la'8[ sol' la' sol' la']( |
sol'2)\melismaEnd mi'2\trill |
la'8[\melisma sol' fa' mi'] re'[ do' si la] |
sol([ la si do'])\melismaEnd re'([ mi']) fa'([ re']) |
sol'1 |
do'2~ do'4. fa'8 |
mi'1( |
re')\trill |
do' |
R1*8 |
fa'2. do'4 |
mib'2~ mib'4. re'8 |
re'4.( do'8)\trill do'4. fa'8 |
sib2. r8 re' |
sol'1~ |
sol'2 re'4 mi'8 fa' |
mi'1\trill |
la'2. sol'8([ fa']) |
sol'8[\melisma fa' mi' re'] do'[ mi' re' mi'] |
fa'[ mi' re' do'] re'[ do' sib fa'] |
mi'[ re' do' sib] la[ do' sib do'] |
re'[ do' sib la] sib[ la sol re'] |
do'[ sib la sol]( fa4.)\melismaEnd do'8 |
do'2( sib4)\trill sib8 la |
sol1\trill |
do'8[\melisma sib la sib] do'[ re' do' mib'] |
re'[ do' sib do'] re'[ fa' mi' sol'] |
fa'[ mi' re' do'] si[ la sol fa'] |
mi'[ re' do' re'] mi'[ sol' fa' la'] |
sol'[ fa' mi' re'] dod'[ si la sol'] |
fa'[ mi' re' mi'] fa'[ sol' la' fa'] |
sib'1~ |
sib'~ |
sib'4. la'8([ sol' fa' mi' re'] |
do'1)\fermata\melismaEnd |
do'4 r r2 |
R1 |
sib'2~ sib'4. la'16([ sol']) |
la'4.( sib'8) la'8([ sol']) fa'([ mi']) |
re'1 |
mi'2.( do'8) fa' |
fa'2~ fa'4.( mi'8) |
fa'1 |
re'4.( do'8) sib8([ la]) sib([ sol]) |
do'1 |
fa'2~ fa'4. sib8 |
la2( sol)\trill |
fa1\fermata |
R1*8 |
r4 r la8 re' |
re'4 fa'4. mi'16([ re']) |
\appoggiatura re'8 mi'2. |
fa'8 fa'16 mi' mi'4~ mi'16([ re' dod']) re' |
dod'2.\trill |
r4 r r8 mi' |
fa'4. sol'8 fa'4( mi'8)\trill fad' |
fad'2( sol'8) r fa'\trill mi' |
la'2. dod'8 re' |
\appoggiatura re'8 mi'2*3/4( \afterGrace s8 la8) la r |
fa'4 fa'8 mi' re' \appoggiatura do' si |
mi'2 r8 sol' |
sol'4~ sol'8.([ fad'16])\prall fad'8 r |
r4 r sold'8 la' |
la'4.( sold'8)\trill \appoggiatura fad'8 mi' r |
r2 la' |
re' re'4 mi'8 fa' si4.\trill do'8 |
\appoggiatura si8 la1 |

