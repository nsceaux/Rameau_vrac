\clef "dessus" la''2.\ademi fa''8.(\trill mi''32 fa'') |
sol''8 fa'' mi'' re'' do''4 mi''\trill |
fa'' la' sib' sol''8 fa'' |
mi'' re'' do'' sib' la'4 do''8.(\trill sib'32 do'') |
re''4 fa' sol' re'' |
do''8 sib' la' sol' fa'4 do'' |
do''2( sib'4)\trill la' |
sol'8 fa' mi' re' do'2 |
fa''8(\doux do'') la'( fa') fa''( do'') la'( fa') |
fa''( re'') sib'( fa') fa''( re'') sib'( fa') |
fa''8( do'') la'( fa') fa''( do'') la'( fa') |
fa''( re'') sib'( fa') fa''( re'') sib'( fa') |
fa''8( do'') la'( fa') fa''( do'') la'( fa') |
fa''1~ |
fa''2. mi''4\trill |
<<
  \tag #'dessus1 {
    \appoggiatura mi''8 fa''4 fa'8 do' la' fa' do'' la' |
    fa''4 la'8 fa' do'' la' fa'' do'' |
    la''4 do''8 la' fa'' do'' la'' fa'' |
    do'''1~ |
    do'''2. si''4\trill |
    \appoggiatura si''8 do'''1 |
  }
  \tag #'dessus2 {
    \appoggiatura mi''8 fa''2 r |
    r4 fa'8 do' la' fa' do'' la' |
    fa''4 la'8 fa' do'' la' fa'' do'' |
    sol'1 |
    fa'4 sol' la' sol'~ |
    sol'1 |
  }
>>
R1 |
<<
  \tag #'dessus1 {
    la''1 |
    sol'' |
    fa'' |
    mi'' |
    re'' |
    do'' |
    re''2 mi''4. fa''8 |
    mi''4\trill do''8 sol' mi'' do'' sol'' mi'' |
    do'''4 fa''8 mi'' re''4(\trill do''8) si' |
    do''4 si'8 la' sol'2~ |
    sol' r4 re'' |
    mi''\trill re'' r re'' |
    mi''\trill re'' r fa'' |
    mi''2\trill r |
    la''1 |
    re'' |
    r4 sol''8 do''' mi'' sol'' do'' mi'' |
    la'2~ la'4. si'16 do'' |
  }
  \tag #'dessus2 {
    do''1~ |
    do''~ |
    do''2 sib'~ |
    sib' la'~ |
    la' sol'~ |
    sol' fa'~ |
    fa' sib'4. la'8 |
    sol'2\trill do'4 r |
    fa''1 |
    mi''2 re'' |
    do''4 sol' r si' |
    do'' sol' r si' |
    do'' sol' r si' |
    do''2 sol'4 r |
    do''1 |
    si' |
    do''2 mi''~ |
    mi''2.( re''8)\trill do'' |
  }
>>
do''1 |
si'2\trill~ si'4. do''8 |
do''1 |
<>\fort <<
  \tag #'dessus1 {
    do'''8( sol'') mi''( do'') do'''( sol'') mi''( do'') |
    do'''( la'') fa''( re'') do'''( la'') fa''( re'') |
    do'''8( sol'') mi''( do'') do'''( sol'') mi''( do'') |
    do'''( la'') fa''( re'') do'''( la'') fa''( re'') |
    do'''( si'') la''( sol'') fa''( mi'') re''( do'') |
    la''( sol'') fa''( mi'') re''( do'') si'( la') |
    sol'( do'') si'( la') sol'( fa') mi'( re') |
    do'2 r |
  }
  \tag #'dessus2 {
    sol''1 |
    la'' |
    mi'' |
    fa'' |
    sol'' |
    do''2. fa''4 |
    mi''2( re''4.) sol''8 |
    mi''2 r |
  }
>>
R1*7 |
<<
  \tag #'dessus1 {
    la''1 |
    sol'' |
    fa'' |
    mi'' |
    re'' |
    do'' |
    re''2 mi''4. fa''8 |
    mi''4\trill do''8 sol' mi'' do'' sol'' mi'' |
    do'''4 r do''2~ |
    do''4.( sib'8) re''2 |
    r re''~ |
    re''4. do''8 mi''2 |
    r mi''2~ |
    mi''4.( la'8) re''4 r |
  }
  \tag #'dessus2 {
    do''1~ |
    do''~ |
    do''2 sib'~ |
    sib' la'~ |
    la' sol'~ |
    sol' fa'~ |
    fa' sib'4. la'8 |
    sol'2\trill r |
    fa'1~ |
    fa'2~ fa'4. sol'8 |
    sol'1~ |
    sol'2~ sol'4. la'8 |
    la'2~ la'~ |
    la'1 |
  }
>>
R1*4 |
<<
  \tag #'dessus1 {
    r4 mi'8*2/3( fa' mi') sol'([ la' sol']) do''( re'') do'' |
    mi''4 sol'8*2/3( la' sol') do''([ re'' do'']) mi''( fa'' mi'') |
    sol''1~ |
    sol''4. do''8 fa''2~ |
    fa''2.( mi''8 re'') |
    do''2~ do''4. re''8 |
    do''2 sib' |
    la'4 fa'8 do' la' fa' do'' la' |
    re''1 mi''2. fa''8 sol'' |
    do''2~ do''4. fa''8 |
    fa''2( mi''4.)\trill fa''8 |
    fa''1\fermata |
  }
  \tag #'dessus2 {
    r4 do'8*2/3( re' do') mi'([ fa' mi']) sol'( la' sol') |
    do''4 mi'8*2/3( fa' mi') sol'([ la' sol']) do''( re'' do'') |
    mi''4. sol''8 do''2~ |
    do''1~ |
    do''4.( fa'8) sib'4.( la'16\trill sol') |
    sol'2. la'8 sol' |
    la'2( sol'4.)\trill fa'8 |
    fa'2 r |
    fa'1 |
    sol' |
    la'2~ la'4. re''8 |
    do''2 sib' |
    la'1\fermata |
  }
>>
<<
  \tag #'dessus1 {
    fa''8(\ademi do'') la'( fa') fa''( do'') la'( fa') |
    fa''( re'') sib'( fa') fa''( re'') sib'( fa') |
    sol''( re'') si'( sol') sol''( re'') si'( sol') |
    sol''8( mi'') do''( sol') sol''( mi'') do''( sol') |
    do'''( sol'') mi''( do'') do'''( sol'') mi''( do'') |
    sib''( la'') sol''( fa'') la''( sol'') fa''( mi'') |
    fa''4 mi''8\trill re'' do'' re'' sib' do'' |
  }
  \tag #'dessus2 {
    do''1\ademi |
    re'' |
    re'' |
    mi'' |
    sol'' |
    do''4 fa'' re'' sol' |
    do'' la' sol' sol' |
  }
>>
la'4. sol'8 sol'4.\trill fa'8 |
fa'2 r4 |
R2.*5 R1*3 R2.*6 R1 R1. R1 |
