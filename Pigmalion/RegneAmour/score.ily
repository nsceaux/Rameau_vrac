\score {
  \new ChoirStaff \with { \haraKiri } <<
    \new Staff \with { instrumentName = "Flûtes" \haraKiri } <<
      \global \includeNotes "flute"
    >>
    \new GrandStaff \with { instrumentName = "Violons" } <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff \with { \haraKiriFirst } <<
        { \startHaraKiri s1*16 \stopHaraKiri }
        \global \keepWithTag #'dessus2 \includeNotes "dessus"
      >>
    >>
    \new Staff \with { instrumentName = \markup\character Pigmalion }
    \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*8\break s1*7\break \grace s8 s1*7\pageBreak
        s1*7\break s1*6\break s1*6\pageBreak
        s1*6\break s1*6\break s1*6\pageBreak
        s1*5\break s1*5\break s1*7\pageBreak
        s1*5\break s1*5\break s1*6\pageBreak
        s1*4\break s1 s2.*3\break s2.*3 s1\pageBreak
        s1*2 s2.\break s2.*4\break
      }
      \modVersion { s1*97 s2.\break }
    >>
  >>
  \layout { }
  \midi { }
}
