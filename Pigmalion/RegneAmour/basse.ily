\clef "basse" \mergeDifferentlyDottedOn <<
  { fa4 la sib re' |
    mi sol la do' |
    re fa sol sib |
    do mi fa la |
    sib, re mi sol |
    la, do re fa |
    sol,2 mi4 fa |
    do1 | } \\
  { <>_\markup\center-align "Contrebasses" fa,1 |
    mi, |
    re, |
    do, |
    sib,, |
    la,, |
    sol,2. fa,4 |
    do,1 | }
>>
la4^\doux la la la |
sib sib sib sib |
do' do' do' do' |
re' re' re' re' |
la, la, la, la, |
sib, sib, sib, sib, |
do2 do, |
fa,1 |
R1 |
r2 fa, |
mi,1 |
re, |
do, |
R1 |
fa4 fa fa fa |
mi mi mi mi |
re re re re |
do do do do |
sib, sib, sib, sib, |
la, la, la, la, |
sol, sol, sol, fa, |
do1 |
fa4 re sol mi |
la fa si sol |
do' si r sol |
do' si r sol |
do' si r sol |
do'1 |
fa~ |
fa |
mi |
fa |
sol~ |
sol |
do |
mi4^\fort mi mi mi |
fa fa fa fa |
sol sol sol sol |
la la la la |
mi mi mi mi |
fa fa fa fa |
sol2 sol, |
do1 |
R1 |
la2~ la4. sib8 |
mib2 fa |
sib,1 |
si,~ |
si, |
do2 r |
fa4 fa fa fa |
mi mi mi mi |
re re re re |
do do do do |
sib, sib, sib, sib, |
la, la, la, la, |
sol, sol, sol, fa, |
do2 r |
la4 la la la |
sib sib sib sib |
si si si si |
do' do' do' do' |
dod' dod' dod' dod' |
re'1 |
<sol sol,>2 <fa fa,> |
<mi mi,> <re re,> |
<do do,>1~ |
q\fermata |
R1*2 |
r2 mi |
fa1 |
sib,~ |
sib,2. la,8 sib, |
do2 do, |
fa,1 |
sib,~ |
sib, |
la,2\trill~ la,4. sib,8 |
do2 do, |
fa,1\fermata |
la4^\ademi la la la |
sib sib sib sib |
si si si si |
do' do' do' do' |
mi' mi' mi' mi' |
fa' re' sib do' |
la fa mi do |
fa re sib, do |
fa,2 r4 |
r re'2^\tresdoux |
\appoggiatura re'8 dod'2. |
re'8.( la16) sib2 |
la2. |
r4 r r8 la |
re'2 do' |
sib2. la8 sol |
fa2. mi8\trill re |
la,2. |
re' |
do'2 r8 la |
re'2 r4 |
r r r8 si |
mi'2 r4 |
r2 do |
fa2. re4 mi2 |
la,1 |
