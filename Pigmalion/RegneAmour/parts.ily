\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes dessus1)
   (violon2 #:notes "dessus" #:tag-notes dessus2)
   (flutes #:notes "flute" #:score-template "score-voix")
   (bassons #:notes "basse" #:instrument "Basses")
   (basses #:notes "basse" #:score-template "score-basse-continue-voix"))
