\key sol \minor
\digitTime\time 3/4
\midiTempo#132
s2.*15 s2 \fineMark s4 \bar "|."
\beginMark\markup { \concat { I \super ere } Reprise }
s2.*16 \bar "|." \endMark "[Da Capo]"