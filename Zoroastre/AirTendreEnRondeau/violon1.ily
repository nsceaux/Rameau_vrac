\clef "dessus" <>\doux re''2 \appoggiatura re''8 do''4\trill |
sib'2 re''4 |
do''( sib')\trill la' |
\appoggiatura la'8 sib'4 \appoggiatura la'8 sol'4 la' |
sib' do'' re'' |
\appoggiatura re''8 mib''4.( re''16)\trill do'' \appoggiatura do''8 re''4~ |
re'' re''4. sol''8 |
\appoggiatura sol''8 fad''2\trill r4 |
re''2 do''4 |
sib'2 re''4 |
do''( sib')\trill la' |
\appoggiatura la'8 sib'4 \appoggiatura la'8 sol'4 la' |
sib' do'' re'' |
\appoggiatura re''8 mib''4.( re''16\trill do'') \appoggiatura do''8 re''4~ |
re''8 sol'' fad''4.\trill sol''8 |
sol''2. |
r8 dod''( re'' mi'' fa'' sol'') |
r8 si'( dod'' re'' mi'' fa'') |
r re'' dod''( mi'') la'4 |
R2. |
r8 fa'' mi''( re'') fa''( sib') |
la'4( sol'8)\trill sib' la'4 |
r la' re'' |
dod''2.\trill |
r8 re''( dod'') re''4 dod''8 |
r4 mi''4.( fa''8) |
r8 la'' sol''( fa'') mi''( re'') |
dod'' si' la' sol' fa' mi' |
la''( sol'') fa''( mi'') re''4~ |
re''2. |
\once\override TextScript.script-priority = #-100
re''4.\trill^\markup\tiny\natural re''8 dod''4\trill |
\appoggiatura dod''?8 re''2. |
