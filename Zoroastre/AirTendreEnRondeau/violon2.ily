\clef "dessus2" <>\doux sol'8( re'') sib'( sol') re'( do'') |
mib'( sib') sol'( mib') sib( sol') |
do'( mib') re'4 re' |
sol2 mib'8( sol') |
re'( sol') do'( fad') sib( sol') |
do'( mib') la( do') sib( sib') |
fad'( re') sol'4 sol |
re' re''8( do'') sib'(\trill la') |
sol'( re'') sib'( sol') re'( do'') |
mib'( sib') sol'( mib') sib( sol') |
do'( mib') re'4 re' |
sol2 mib'8( sol') |
re'( sol') do'( fad') sib( sol') |
do'( mib') la( do') sib( sol') |
do'( mib') re'4 re' |
sol2. |
sol' |
fa' |
mi'4 la' la |
re'2. |
re''2 r8 fa' |
fa'4( mi'8)\trill( sol') fa'( la') |
dod'( la) re'4 re' |
la8( sib') la'( sol') fa'( mi') |
fa'4.( sol'8) mi'4-! |
sol'2 fa'4 |
sib' sib'2\trill |
la'8( sol') fa'( mi') re'( dod') |
fa''( mi'') re''( do'') sib'( la') |
sol'( fad') sib'( la') sol'( sib') |
la'2 la4 |
re'8( mib'') re''( do'') sib'( la') |
