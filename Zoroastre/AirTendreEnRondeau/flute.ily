\clef "dessus" sib''2 re'''4 |
sol''2 sib''4 |
la''( sol''\trill) fad'' |
sol''2 do'''4 |
sib''( la'')\trill sol'' |
sib''8( la'') sol''( fad'') \appoggiatura fad'' sol''4 |
la'' \appoggiatura la''8 sib''4. do'''8 |
la''2.\trill |
sib''2 re'''4 |
sol''2 sib''4 |
la''( sol'')\trill fad'' |
sol''2 do'''4 |
sib''( la'')\trill sol'' |
sib''8( la'') sol''( fad'') \appoggiatura fad'' sol''4~ |
sol''8 la'' la''4.\trill sol''8 |
sol''2. |
sib''2 mi''4 |
la''2 re''4 |
sol''( fa'')\trill mi'' |
\appoggiatura mi''8 fa''2 \appoggiatura mi''8 re''4 |
fa''( sib''4.) re''8 |
re''4( dod''8)\trill r re''4 |
mi''4 \appoggiatura mi''8 fa''4. sol''8 |
mi''2.\trill |
la''4.( sib''8) sol''4-! |
sib''2 la''4 |
re'''( dod'''4.)\trill re'''8 |
\appoggiatura re'''8 mi'''2 la''4 |
re'''8( do''') sib''( la'') sol''( fad'') |
sib''( la'') sol''( fa'') mi''( re'') |
fa''4.\trill mi''8 re''4 |
re''2. |
