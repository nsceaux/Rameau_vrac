\score {
  \new GrandStaff <<
    \new Staff \with {
      instrumentName = \markup { \concat { I \super ers } viol[ons] }
    } << \global \includeNotes "violon1" >>
    \new Staff \with {
      instrumentName = \markup { \concat { 2 \super es } viol[ons] }
    } << \global \includeNotes "violon2" >>
  >>
  \layout { indent = \largeindent }
}