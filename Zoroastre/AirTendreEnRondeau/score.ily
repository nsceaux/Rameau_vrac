\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Fl[ûte] Seule" } <<
      \global \includeNotes "flute"
      \origLayout {
        s2.*6\break s2.*7\break \grace s8
        s2.*6\break \grace s8 s2.*6\break \grace s8
      }
    >>
    \new Staff \with {
      instrumentName = \markup { \concat { I \super ers } viol[ons] }
    } << \global \includeNotes "violon1" >>
    \new Staff \with {
      instrumentName = \markup { \concat { 2 \super es } viol[ons] }
    } << \global \includeNotes "violon2" >>
  >>
  \layout { indent = \largeindent }
  \midi { }
  \header {
    source = "BNF MUS-549²"
    editor = "Nicolas Sceaux"
  }
}