\clef "petrucci-c4/tenor" fa8 do fa do fa la16 sol |
fa8 do fa do fa la16 sol |
fa8 fa16 sol la8 la16 sib do'8 re'16 mi' |
fa'2.~ |
fa'2 mi'4\trill |
fa'4. fa8 la do'~ |
do'4. mi8 sol do'~ |
do'4. fa8 sib sib |
sib4. sol8 do' do' |
do'2.~ |
do'2 fa'4~ |
fa' re' si\trill |
do'8 do16 re mi8 mi16 fa sol8 la16 si |
do'4 sol'2~ |
sol'8 fa' mi' re' do'4~ |
do' la'2~ |
la'8 sol' fa' mi' re' do' |
si mi' re' do' si la |
sol4 sol'2~ |
sol'2.~ |
sol'4 fa'8 mi' la si |
do'2 si4\trill |
do'2. |
do'8 sol do' sol do' mi'16 re' |
do'8 sol do' sol do' mi'16 re' |
do'8 sol do' do'16 re' mi'8 mi'16 re' |
sol'2.~ |
sol'8 fa'16 mi' re'4 sol~ |
sol8 mi'4 sol'4.~ |
sol'8 do' fa' la do' fa |
r fa' re' sib r fa' |
fa2 fa'4 |
mi'2\trill fa'4 |
do'2.~ |
do'~ |
do'~ |
do'4. do16 do fa8 fa16 sol |
la4 fa'2~ |
fa'8 sib,16 do re8 re16 mi fa8 sol16 la |
sib4 sol'2~ |
sol'8 do16 re mi8 mi16 fa sol8 do'16 re' |
mi'4 sol'2 |
do'2.~ |
do'~ |
do'8 sib16 la re'4. mi'8 |
fa'4 mi'4.\trill fa'8 |
fa8 do fa do fa la16 sol |
fa8 do fa do fa la16 sol |
fa8 fa16 sol la8 la16 sib do'8 re'16 mi' |
fa'8 do'16 do' do'4. fa'16 fa' |
la'2.~ |
la' |
\appoggiatura sol'8 fa'2.~ |
fa'8. la16 sol4.\trill fa8 |
fa2. |
