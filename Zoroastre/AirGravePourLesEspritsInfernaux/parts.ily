\piecePartSpecs
#`((dessus #:score-template "score" #:notes "dessus")
   (violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (hautbois #:notes "dessus")
   (flutes #:notes "dessus"
           #:instrument ,#{\markup\center-column { Hautbois Violons } #})
   (parties)
   (haute-contre)
   (taille)
   (bassons #:clef "tenor" #:notes "bassons")
   (basses))
