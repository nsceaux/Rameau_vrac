\clef "taille" fa'8 do' fa' do' fa' la'16 sol' |
fa'8 do' fa' do' fa' la'16 sol' |
fa'8 fa'16 sol' la'8 la'16 sib' do''8. do''16 |
do''2 la'4 |
sol'2 do'4 |
do'2 re'4 |
mi' sol'2 |
la' sol'4~ |
sol'2 sol'4 |
sol'2 fa'4 |
la'2 la'4 |
re'2 re'4 |
mi'8 do'16 re' mi'8 mi'16 fa' sol'8 do''16 re'' |
mi''8 re'' do''4 do' |
do'8 fa'16 sol' la'8 la'16 sib' do''8 do'' |
do''8 sib' la' sol' fa' mi' |
re' sol'16 la' si'8 si'16 do'' re''8 fa''16 fa'' |
fa''2 re''4\trill |
mi''4. sol'8 do'' mi'' |
do''2\trill mi''4~ |
mi''2 re''16 do'' si' la' |
sol'8. la'16 sol'4 fa' |
mi'2. |
do''8 sol' do'' sol' do'' mi''16 re'' |
do''8 sol' do'' sol' do'' mi''16 re'' |
do''8 do''16 re'' mi''8 mi''16 fa'' sol''8 sol'' |
sol''2 mi''4 |
re''8 do'' do''4 si'\trill \appoggiatura si'8 do''2 do'4 |
do'2 fa'4 |
fa'2 fa'4 |
fa'2 do'4 |
do'2 sib4 |
\twoVoices #'(haute-contre taille parties) <<
  { sol'16 fa' mi' fa' sol' la' sol' la' sib' la' sib' la' |
    sol' fa' mi' fa' sol' la' sol' la' sib' la' sib' la' |
    mi' re' do' re' mi' fa' mi' fa' sol' fa' sol' fa' |
    sol'4. }
  { mi'16 re' do' re' mi' fa' mi' fa' sol' fa' sol' fa' |
    mi' re' do' re' mi' fa' mi' fa' sol' fa' sol' fa' |
    mi' re' do' re' mi' fa' mi' fa' sol' fa' sol' fa' |
    mi'4. }
>> do''16 sib' la' sib' la' sol' |
fa'4 do''2~ |
do''8 re''16 do'' fa'2~ |
fa'8 mi' re' do' sib sib' |
sib'2.~ |
sib'4~ sib'4. la'16 sol' |
la'8 r do'' sib' la' sol' |
la' sol' fa'2~ |
fa'4~ fa'4. sol'8 |
la'8 sol'16 fa' do'4 sib |
la8 do' fa' do' fa' la'16 sol' |
la8 do' fa' do' fa' la'16 sol' |
fa'8 fa'16 sol' la'8 la'16 sib' do''8 re''16 mi'' |
fa''8 do''16 sib' la' sib' la' sol' fa'8 fa''16 fa'' |
fa'' mi'' re'' do'' re'' do'' sib' la' sib' la' sol' fa' |
fa'' mi'' re'' do'' re'' do'' sib' la' sib' la' sol' fa' |
la8 la'16 sib' do''2~ |
do''8 sib'16 la' la'4( sib'8.) do''16 |
la'2.\trill |
