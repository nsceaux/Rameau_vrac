\clef "dessus" fa'8 do' fa' do' fa' la'16 sol' |
fa'8 do' fa' do' fa' la'16 sol' |
fa'8 fa'16 sol' la'8 la'16 sib' do''8 re''16 mi'' |
fa''2~ fa''16 mi'' re'' do'' |
re''4 re''( do''8.)\trill sib'16 |
la'8\trill fa' la' do''4 sib'16 la' |
sol'8 mi' sol' do''4 la'16 sol' |
fa'8 re' fa' do''4 sol'16 fa' |
mi'8 do'16 re' mi' fa' mi' fa' sol' la' sib' sol' |
la' sol' fa' sol' la' sib' la' sib' do'' re'' mi'' do'' |
fa''8 la'16 si' do'' si' la' si' do'' si' do'' re'' |
si' la' sol' fa' si' do'' si' do'' re'' mi'' fa'' re'' |
sol''2.~ |
sol''8 fa''^"égales" mi'' re'' do'' sib' |
la' fa' la''2~ |
la''8 sol'' fa'' mi'' re'' do'' |
si' sol' re'''2~ |
re'''8 do''' si'' la'' sol'' fa'' |
do'''16 si'' do''' re''' do''' si'' do''' re''' do''' si'' do''' re''' |
do'''8 si'' la'' sol'' fa'' mi'' |
la''16 sol'' fa'' mi'' fa'' mi'' re'' do'' re'' do'' si' la' |
sol'8 do''16 re'' re''4.\trill( do''16 re'') |
do''2. |
do''8 sol' do'' sol' do'' mi''16 re'' |
do''8 sol' do'' sol' do'' mi''16 re'' |
do''8 do''16 re'' mi''8 mi''16 fa'' sol''8 la''16 si'' |
do'''2~ do'''16 si'' la'' sol'' |
la''4 la''4( sol''8.)\trill fa''16 |
mi''8\trill sol''4 do'' sib'8 |
la' fa' do'' fa''4 do''8 |
re'' sib' re'' fa''4 mi''16 re'' |
do''8 la' do'' fa''4 re''16 do'' |
sib'2 la'4 |
<<
  { sol'16 fa' mi' fa' sol' la' sol' la' sib' la' sib' la' |
    sol' fa' mi' fa' sol' la' sol' la' sib' la' sib' la' |
    sol' fa' mi' fa' sol' la' sol' la' sib' la' sib' la' |
    sol'4 } \\
  { mi'16 re' do' re' mi' fa' mi' fa' sol' fa' sol' fa' |
    mi' re' do' re' mi' fa' mi' fa' sol' fa' sol' fa' |
    mi' re' do' re' mi' fa' mi' fa' sol' fa' sol' fa' |
    mi'4 }
>> do''2~ |
do''8 sib' la' sol' fa' mi' |
re' sib re''2~ |
re''8 do'' sib' la' sol' fa' |
mi' do' sol''2~ |
sol''8 fa'' mi'' re'' do'' sib' |
fa''16 mi'' fa'' sol'' fa'' mi'' fa'' sol'' fa'' mi'' fa'' sol'' |
fa''8 mi'' re'' do'' sib' la' |
re''16 do'' sib' la' sib' la' sol' fa' sol' fa' mi' re' |
do'8 fa'16 sol' sol'4.(\trill fa'16 sol') |
fa'8 do' fa' do' fa' la'16 sol' |
fa'8 do' fa' do' fa' la'16 sol' |
fa'8 fa'16 sol' la'8 la'16 sib' do''8 re''16 mi'' |
fa''8 do''16 do'' fa''8 fa''16 sol'' la''8 la''16 sib'' |
do'''2.~ |
do'''~ |
do'''4~ do'''8. sib''16 la'' sol'' fa'' mi'' |
re''8. sib''16 mi''4.\trill fa''8 |
fa''2. |
