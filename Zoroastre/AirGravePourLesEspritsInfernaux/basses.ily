\clef "basse" fa8 do fa do fa la16 sol |
fa8 do fa do fa la16 sol |
fa8 do fa do fa do'16 sib |
la sib do' sib la sol fa mi fa8. la,16 |
sib,4 do do, |
fa,2 fa4 |
mi2 mi4 |
re2 re4 |
do2 do4 |
fa2 mi4 |
re2 re4 |
sol2 fa4 |
mi8 do16 re mi8 mi16 fa sol8 la16 si |
do'2 mi4 |
fa8 fa16 sol la8 la16 sib do'8 re'16 mi' |
fa'2 re4 |
sol8 sol16 la si8 si16 do' re'8 mi'16 fa' |
sol'2 sol8 fa |
mi2.~ |
mi2 mi4 |
fa2 fa4 |
mi8 fa sol4 sol, |
do2. |
do'8 sol do' sol do' mi'16 re' |
do'8 sol do' sol do' mi'16 re' |
do'8 sol do' sol do' sol16 fa |
mi fa sol fa mi re do si, do8. mi,16 |
fa,4 sol, sol, |
do,2 mi4 |
fa2 la,4 |
sib,2 sib,4 |
la,2 la,4 |
sol,2\trill fa,4 |
do,2.~ |
do,~ |
do,~ |
do,4. do16 do fa8 fa16 sol |
la2 la,4 |
sib,8 sib,16 do re8 re16 mi fa8 sol16 la |
sib2 sol,4 |
do8 do16 re mi8 mi16 fa sol8 la16 sib |
do'2 do8 sib, |
la,2.~ |
la,2 la,4 |
sib,2 sib,4 |
la,8. sib,16 do2 |
fa8 do fa do fa la16 sol |
fa8 do fa do fa la16 sol |
fa8 fa16 sol la8 la16 sib do'8 re'16 mi' |
fa'2. |
fa'16 mi' re' do' re' do' sib la sib la sol fa |
fa' mi' re' do' re' do' sib la sib la sol fa |
la,2 la,4 |
sib, do do, |
fa,2. |
