\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Hautbois Violons }
    } << \global \includeNotes "dessus" >>
    \new Staff \with { instrumentName = "Parties" } <<
      \global \keepWithTag #'parties \includeNotes "parties"
    >>
    \new Staff \with { instrumentName = "Bassons" } <<
      \global \includeNotes "bassons"
    >>
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basses"
      \origLayout {
        s2.*5\break s2.*5\break s2.*5\pageBreak
        s2.*5\break s2.*5\break s2.*5\break s2.*5\pageBreak
        s2.*5\break s2.*5\break s2.*5\break
      }
    >>
  >>
  \layout { short-indent = 0 }
  \midi { }
}
