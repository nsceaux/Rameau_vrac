\key sib \major \midiTempo#160
\digitTime\time 2/2 \partial 4 s4 s1*10
\alternatives {
  \measure 3/4 s2.
} { \digitTime\time 3/4 \tempo "Vite" \midiTempo#120 s2. }
\bar ".|:" s2.*28 \alternatives s2.*2 s2. \bar "|."