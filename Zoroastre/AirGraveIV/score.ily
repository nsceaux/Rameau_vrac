\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Hautbois" } <<
      \global \keepWithTag #'hautbois \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "Violons"} <<
      \global \keepWithTag #'violons \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "Parties" } <<
      \global \keepWithTag #'parties \includeNotes "parties"
    >>
    \new Staff \with { instrumentName = "Bassons" } <<
      \global \includeNotes "basson"
    >>
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \origLayout {
        s4 s1*5\break s1*5 s2.\pageBreak
        s2.*7\break s2.*4\pageBreak
        s2.*7\break s2.*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
