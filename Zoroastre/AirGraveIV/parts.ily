\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes violon1)
   (violon2 #:notes "dessus" #:tag-notes violon2)
   (hautbois #:notes "dessus" #:tag-notes hautbois)
   (haute-contre #:notes "parties")
   (taille #:notes "parties")
   (bassons #:notes "basson" #:clef "tenor")
   (basses #:notes "basse"))
