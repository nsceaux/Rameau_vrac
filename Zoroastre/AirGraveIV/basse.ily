\clef "basse" r4 |
r4 r8 sib,16 sib, sib,2 |
r4 r8 sib,16 sib, la,4 r8 lab,16 lab, |
lab,2 r4 r8 lab,16 lab, |
sol,2. do4 |
fa2 sib, |
fa,4. fa16 fa mi4 do |
fa2. fa,4 |
sib,2. sib,4 |
sib,2 lab, |
sib, do |
fa,2. |
fa,2.~ fa,2 fa4 |
sib2 sib,4 |
fa2 fad4 |
sol2 sol,4 |
re2. |
R2. |
\ru#24 re32 |
re8 r r4 r |
\ru#24 re32 |
sol2 r8 sol16 sol |
do4 sib, do~ |
do re2 |
mib2 mib,4 |
sib,2 sib,4 |
fa2 fa,4 |
do2 do4 |
sol2 sol,4 |
re2 re4 |
mib2 do4 |
fa sib,2 |
fa,2. |
R2. |
\ru#24 fa,32 |
fa,8 r r4 r |
\ru#24 fa,32 |
sib,2 sib4 |
mib re mib~ |
mib fa2 |
sib,2. |
fa,2.*23/24~ \once\hideNotes fa,32 |
sib,2. |
