\clef "dessus" r16 r32 sib'' la'' sol'' fa'' mib'' |
re''2 r |
r8 sib''16 la'' sol'' fa'' mib'' re'' do''2 |
r r8 fa''16 mib'' re'' do'' sib' la' |
sib'2 mib''4.*11/12 re''32 do'' sib' la' sol' |
la'4.*13/12 sib'32( la' sib') sib'4.(\trill la'16 sib') |
do''2 r4 r16 r32 do'' re'' mib'' fa'' sol'' |
lab''1~ |
lab''4.*13/12 lab''32 sol'' fa'' sol''4 re''! |
mi''2\trill fa''4~ fa''32 sol'' lab'' sol'' fa'' mib'' reb'' do'' |
reb''4~ reb''32 mib'' fa'' mib'' reb'' do'' sib' lab' sol'4 do'' |
la'2.\trill |
la'8 fa'16 sol' la'8 do'' mib''4~ |
mib''4~ mib''16 sol'' fa'' mib'' fa'' mib'' re'' do'' |
re''8 sib'16 do'' re''8 fa'' sib''4~ |
sib''4. do'''16 sib'' la''4~ |
la'' re'' sol''~ |
sol''4. la''16 sol'' fad''4 |
<<
  \tag #'hautbois {
    <<
      { re''2. | do''4 do'' } \\
      { sib'2. | la'4 la' }
    >> r4 |
    <<
      { sib'2. | la'4 la' } \\
      { sol'2. | fad'4 fad' }
    >> r4 |
  }
  \twoVoices #'(violon1 violon2 violons) <<
    { \ru#16 re''32 re'' re'' re'' mi'' fad'' sol'' la'' sib'' |
      do''4 do'' s |
      \ru#16 sib'32 sib' sib' sib' do'' re'' mi'' fad'' sol'' |
      la'4 la' s | }
    { \ru#16 sib'32 sib' sib' sib' do'' re'' mi'' fad'' sol'' |
      la'4 la' s |
      \ru#16 sol'32 sol' sol' sol' la' sib' do'' re'' mi'' |
      fad'4 fad' s | }
    { s2. | s2 r4 | s2. | s2 r4 }
  >>
>>
sib'16 la' sol' fad' sol' la' sib' do'' re'' mi'' fad'' sol'' |
la''8 re''16 re'' sol''8 sib'16 sib' mib''4~ |
mib''8. la'16 la'4.\trill sol'8 |
sol'2. |
r8 sib'16 do'' re''8 fa'' sib''4~ |
sib''8 do'''16 sib'' lab'' sib'' do''' sib'' lab'' sol'' fa'' lab'' |
sol''8 do''16 re'' mib''8 sol'' do'''4~ |
do'''8 re'''16 do''' sib'' do''' re''' do''' sib'' la'' sol'' sib'' |
la''8 re''16 mib'' fa''8 la'' re'''4~ |
re'''4. do'''16 sib'' do''' re''' sib'' do''' |
la''8. sib''16 sib''4.(\trill la''16 sib'') |
do'''2. |
<<
  \tag #'hautbois {
    <<
      { fa''2. | mib''4 mib'' } \\
      { re''2. | do''4 do'' }
    >> r4 |
    <<
      { re''2. | do''4 do'' } \\
      { sib'2. | la'4 la' }
    >> r4 |
  }
  \twoVoices #'(violon1 violon2 violons) <<
    { \ru#16 fa''32 fa'' fa'' fa'' sol'' la'' sib'' do''' re''' |
      mib''4 mib'' s |
      \ru#16 re''32 re'' re'' re'' mib'' fa'' sol'' la'' sib'' |
      do''4 do'' s | }
    { \ru#16 re''32 re'' re'' re'' mib'' fa'' sol'' la'' sib'' |
      do''4 do'' s |
      \ru#16 sib'32 sib' sib' sib' do'' re'' mib'' fa'' sol'' |
      la'4 la' s | }
    { s2. | s2 r4 | s2. | s2 r4 }
  >>
>>
re''16 do'' sib' la' sib' do'' re'' mib'' fa'' sol'' la'' sib'' |
do'''8 fa''16 fa'' sib''8 re''16 re'' sol''4~ |
sol''8. do''16 do''4.\trill sib'8 |
sib'2. |
r8 fa'16 sol' la'8 do'' mib''4*7/8~ \once\hideNotes mib''32 |
sib'2. |
