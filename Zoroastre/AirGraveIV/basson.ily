\clef "petrucci-c4/tenor" r4 |
r2 r4 r8 fa'16 fa' |
fa'2 r |
r4 r8 fa'16 fa' fa'4. fa'16 fa' |
fa'4. sib16 sib sib4 mib'~ |
mib'4. fa'16 mib' re'4 sib |
fa'4.*13/12 fa'32 mi' fa' sol'4 mi' |
do'4. do'16 do' do'4 fa |
reb'4. reb'16 do' sib4 sib |
sol4. do'16 do' do'4. fa'16 fa' |
fa'2 fa'4 mi'\trill |
fa'2. |
fa'2. |
r8 fa16 sol la8 do' fa'4~ |
fa'4. fa'16 mib' re'8 mib'16 fa' |
do'2.~ |
do'8 sol16 la sib8 sib16 do' re'8 re' |
la2. |
r8 sol16 la sib8 re' sol'4 |
fad'16 re' do' sib la sol fad mi re8 r |
r sol16 la sib8 re' sol'4 |
re'16 re' do' sib la sol fad mi re8 r |
sol8. la16 sib do' re' la sib8. sol16 |
mib'8 re' re'4. mib'16 re' |
do'4~ do'4. re'16 do' |
sib8 mib16 fa sol8 sib mib'4~ |
mib'8 fa'16 mib' re'8 fa' sib re' |
do' fa16 sol lab8 do' fa'4~ |
fa'8 sol'16 fa' mib'8 sol' do' mib' |
re' sol16 la sib8 re' sol'4~ |
sol'4. fa'16 mib' fa'4~ |
fa'4. mib'16 re' mib' fa' re' mib' |
do'4 re'2 |
la2.\trill |
r8 sib,16 do re8 fa sib4 |
la16\trill fa' mib' re' do' sib la sol fa8 r |
r8 sib,16 do re8 fa sib4 |
fa'8 mib'16 re' do' sib la sol fa8 r |
sib8. do'16 re' mib' fa' do' re'8 do'16 sib |
la8 fa'16 fa' fa'4. sol'16 fa' |
mib'4~ mib'4. fa'16 mib' |
re'2. |
R2. |
re'2. |
