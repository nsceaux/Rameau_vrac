\clef "taille" r4 |
r2 r4 r8 fa'16 fa' |
fa'2 r |
r4 r8 fa'16 fa' fa'4. re'16 re' |
re'4. sib'16 sib' sib'4 do'~ |
do'4. fa'16 fa' fa'4 sib' |
la'4.*13/12 la'32\trill sol' la' sib'4 sol''~ |
sol''~ sol''8.( do''16) fa''2 |
fa'4. re''16 re'' re''4 sol' |
do''4. sol'16 sol' lab'4. do''16 do'' |
do''4 sib' do'2 |
do'2. |
do' |
r8 fa'16 sol' la'8 do'' do''4~ |
do''8 re''16 do'' sib'4 fa' |
fa'2 re''4~ |
re''8 sib'16 la' sol'8 sol'16 la' sib'8 sib' |
la'8 re''16 re' re'2 |
r8 sol16 la sib8 re' sol'4 |
\ru#24 fad'32 |
sol'8 sol16 la sib8 re' sol'4 |
do' do' la'16 sol' fad' mi' |
re'2~ re'16 re'' do'' sib' |
la'4 sib'4. la'16 sol' |
sol'2 fad'4\trill |
sol'8 mib'16 fa' sol'8 sib' mib''4 |
fa'4 fa''2~ |
fa''4 do''2~ |
do''4 sol''2~ sol''4 re''2~ |
re''2 sib'4 |
sib'2 mib''4~ |
mib'' re'' sib' |
fa'2. |
r8 sib16 do' re'8 fa' sib'4 |
la'16\trill fa' mib' re' do' sib la sol fa8 r |
r8 sib16 do' re'8 fa' sib'4 |
mib' mib' do''16 sib' la' sol' |
fa'2~ fa'8 mib''16 re'' do''4 re''4. do''16 sib' |
sib'2 la'4\trill |
sib'2. |
R2. |
sib'2. |
