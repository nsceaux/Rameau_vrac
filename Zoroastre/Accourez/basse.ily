\clef "basse" re8 re |
sol4 sol si4.\trill la16 sol |
si4.\trill la16 sol si4.\trill la16 sol |
sol4 sol, r sol |
do'4 do2 do'4 |
si si,2 si4 |
la2 re' |
sol4 re si,\trill la,8 sol, |
re4 re, r2 |
r r4 re8\doux re |
sol4 sol si4.\fort la16 sol |
sol2 sol,4 r |
r2 r4 sol\doux |
do' do2 do'4 |
si4 si,2 si4 |
la2 re' |
sol r4 re' |
si\trill la8 sol sol4 sol, |
re fad'2\fort sol'4 |
re' fad2 sol4 |
re2 r4 re'8 do' |
si4 re'8 mi' fa' r re\ademi mi |
fa4 r r r8 fa\doux |
mi( re do) r \clef "tenor" r4 r8 re'\ademi |
mi'( re' mi') r r4 mi'8\doux re' |
dod'4 mi'8 fad' sol'4 mi8\ademi fad |
sol4 r r r8 sol |
fad(\trill mi re) r r4 r8 mi'\ademi |
fad'( mi' fad') r r4 re |
la, la8 si do'2~ |
do'4 r r fad |
sol r r2 |
r4 re'\fort sol'4 sol'8 sol' |
sol'2. re'4 |
do'2 mi' |
re' re |
\clef "bass" sol4 si si, si,8 si, |
do2 mi |
re1 re, |
sol,2 r4 sol |
sol2 sol, |
do1~ |
do4 si,8 do re4 re, |
sol,2. r4 |
r2 r4 mi |
sol4 fad8 mi sol4\fort fad8 mi |
mi2 mi,4 r |
r2 r4 mi |
si2 si,8 r r4 |
r2 r4 si8\doux la |
sol4.( fad16 sol) sol4.\trill fad8 |
mi2 mi,4 r |
sol2 fad4\trill mi |
re dod2 re4 |
sol2 lad, |
si,4 dod re r |
sol2 mi\trill |
fad1 |
R1 |
r4 fad\fort fad,2 |
mi1\doux |
re2 mi4 fad |
sol fad8 mi lad,2 |
si, r4 sol |
fad2 fad, |
si,2 r4 re8 re |
