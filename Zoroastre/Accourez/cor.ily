\clef "treble" \transposition sol
\twoVoices #'(un deux tous) <<
  { sol'8 sol' |
    do''4 do'' mi''4.\trill re''16 do'' |
    mi''4.\trill re''16 do'' mi''4.\trill re''16 do'' |
    do''4 s s sol'' |
    sol''( fa''8) r r4 re'' |
    mi'' sol''2 sol''4 |
    la''2 re'' |
    mi''4 sol'' mi''\trill re''8 do'' |
    sol''1 | }
  { sol'8 sol' |
    sol'4 sol' sol'2 |
    sol'4 sol' sol'2 |
    mi'4 s s do'' |
    do''2 do'' |
    do''2. mi''4 |
    re''2 re''\trill |
    mi''4 sol'' mi''\trill re''8 do'' |
    re''4 sol' r2 | }
  { s4 | s1*2 | s4 r r s | }
>>
R1 |
r2 r4 \twoVoices #'(un deux tous) <<
  { do''4 | do''1 | sol''2 }
  { sol''4 | sol''1 | mi''2\trill }
>> r2 |
R1*5 |
r4 \twoVoices #'(un deux tous) <<
  { sol''4 sol''2 |
    sol''2. sol''4 |
    sol''1 | }
  { re''4 re'' do'' |
    fa'' re'' re'' do'' |
    sol'1 | }
>>
<<
  \tag #'(un tous) {
    R1 |
    r2 r4 r8 <>^"Seul" do'' |
    do''1~ |
    do''2 r |
    R1 |
    r2 r4 r8 re'' |
    re''1~ |
    re''2 r |
    R1*3 |
    r4 sol' do'' do''8 do'' |
    do''2. do''4 |
    do''2 do'' |
    sol' sol'' |
    mi''4\trill sol'' sol'' do'' |
    do''2 do'' |
    sol'1 |
    sol'' |
    mi''2\trill
  }
  \tag #'deux { R1*19 | r2 }
>> r4 \twoVoices #'(un deux tous) <<
  { sol''4 |
    sol''1~ |
    sol''2 fa'' |
    re''4\trill mi'' re'' sol'' |
    mi''2\trill }
  { do''4 |
    do''1 |
    do'' |
    sol'4. do''8 sol'4 sol' |
    sol'2 }
>> r4 r |
R1*21 |
r2 r4 sol'8 sol' |
