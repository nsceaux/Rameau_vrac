\score {
  \new StaffGroupNoBar <<
    \new GrandStaff \with { instrumentName = "Cors en sol" \haraKiri } <<
      \new Staff <<
        \keepWithTag #'() \global \keepWithTag #'un \includeNotes "cor"
      >>
      \new Staff <<
        \keepWithTag #'() \global \keepWithTag #'deux \includeNotes "cor"
      >>
    >>
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { instrumentName = "Violons" } <<
        \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
        \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
      >>
      \new Staff \with { instrumentName = "Parties" } <<
        \global \includeNotes "parties"
      >>
    >>
    \new Staff \with { instrumentName = \markup\character Zoroastre } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \origLayout {
        s4 s1*6\break s1*7\pageBreak
        s1*7\break s1*6\pageBreak
        s1*7\break s1*8\pageBreak
        s1*8\break s1*7\pageBreak
        s1*7\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
