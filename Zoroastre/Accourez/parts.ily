\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes dessus1)
   (violon2 #:notes "dessus" #:tag-notes dessus2)
   (haute-contre)
   (taille)
   (cors #:score "score-cor")
   (bassons #:notes "basse")
   (basses #:notes "basse"))
