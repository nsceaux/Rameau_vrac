\clef "vhaute-contre" r4 |
R1*7 |
r2 r4 re'8 re' |
sol'4 sol' si'4. la'16 sol' |
sol'2 re'4 r |
R1 |
r2 r4 re'8 re' |
mi'4\melisma sol'2( fad'8)\trill[ mi'] |
re'[ fad' sol' fad'] sol'[ fad' sol' re'] |
do'[ fad' sol' fad'] sol'[ fad' sol' do']( |
si4)\melismaEnd re' si\trill la8 sol |
re'2 re'4. sol'8 |
fad'1\trill |
R1 |
r2 r4 re'8 mi' |
fa'4(\melisma re'8[ do'] si4) r |
r4 re'8[ do'] si[ la sol si] |
do'[ si do']\melismaEnd r r2 |
r2 r4 mi'8 fad' |
sol'4\melisma mi'8[ re'] dod'4 r |
r mi'8[ re'] dod'[ si la dod'] |
re'[ dod' re']\melismaEnd r r2 |
r r4 la'8 si' |
do''4\melisma la'2 mi'8[ sol'] |
fad'[ sol' la' sol'] fad'[ mi' re' do']( |
si4)\melismaEnd re' sol' sol'8 sol' |
si'1~ |
si'4 sol' si\trill si8 si |
do'2.~ do'16[ re' si do'] |
re'2~ re'4. sol'8 |
sol'4 si' si si8 si |
do'2.~ do'16[ re' si do'] |
re'2.( mi'8[ re'] |
re'2)\trill~ re'4. re'8 |
sol1 |
R1*3 |
r2 r4 mi' |
sol' fad'8 mi' sol'4 fad'8 mi' |
mi'2 si4 r |
R1*2 |
r2 r4 r8 fad' |
la'2 sol'8[ fad'] mi'[ red'] |
mi'2 mi'4. fad'8 |
\appoggiatura fad'8 sol'2\prall sol'4 r |
mi'2 re'4\trill dod' |
fad'4(\melisma lad'2) si'4 |
mi' sol'2 fad'8\trill[ mi'] |
re'[ fad' mi' sol']( fad'4)\melismaEnd si'8 r |
mi'2 sol' |
dod'1 |
R1*2 |
fad'2 sold'4 lad' |
si'\melisma fad'8[ la'] sol'[ la' red' fad'] |
mi'4 fad'8[ sol'] fad'[ sol' dod' mi'] |
re'4. dod'8 re'[ fad']\melismaEnd mi'[ sol'] |
fad'2 fad |
si1 |
