\clef "dessus" re''8 re'' |
sol''4 sol'' si''4.\trill la''16 sol'' |
si''4.\trill la''16 sol'' si''4.\trill la''16 sol'' |
sol''4 sol' r <<
  \tag #'dessus1 {
    re''4 |
    mi'' sol''2( fad''8\trill) mi'' |
    re'' fad''( sol'' fad'') sol''( fad'' sol'') re'' |
    do'' fad''( sol'' fad'') sol''( fad'' sol'') do'' |
  }
  \tag #'dessus2 {
    sol'4 |
    sol'2 sol' |
    sol'1~ |
    sol'2 fad'\trill |
  }
>>
si'4 re'' si'\trill la'8 sol' |
re''4 re' <<
  \tag #'dessus1 {
    r4 re''8\doux re'' |
    sol''4 sol'' si''4.\trill la''16 sol'' |
    sol''2 r4 sol''4\fort |
    si''4.\trill la''16 sol'' si''4.\trill la''16 sol'' |
    sol''4 sol' r re''\doux |
    mi''4 sol''2( fad''8)\trill mi'' |
    re''8 fad'' sol''( fad'') sol''( fad'') sol''( re'') |
    do''( fad'') sol''( fad'') sol''( fad'') sol''( do'') |
    si'4 re'' si'\trill la'8 sol' |
    re''2 re''4. sol''8 |
    fad''4\trill la''8\fort si'' do'''4 si''\trill |
    la''8 si'' la'' si'' do'''4 si''\trill |
    la''4.( re''8) re'' r re''8\doux mi'' |
    fa''4 re''8 do'' si' r re''\ademi do'' |
    si' r re''\doux do'' si' la' sol' re'' |
    mi''([ re'' mi''16]) r sol''8\ademi sol''([ fad'' sol''16]) r si''8 |
    do'''( si'' do''') r r4 mi''8\doux fad'' |
    sol''4 mi''8 re'' dod'' r mi''\ademi re'' |
    dod'' r mi'' re'' dod'' si' la' mi'' |
    fad''8([ mi'' fad''16]) r la''8\ademi la''([ sol'' la''16]) r dod'''8 |
    re'''( dod''' re''') r r2 |
    R1*3 |
    r4 si''\fort re''' re'''8 re''' |
    re'''2. r8 re''' |
    re'''4( do'''8)\trill si'' la''( si'') sol''( do''') |
    si''2 la''\trill |
    sol'' r4 r8 re'' |
    re''4( do''8)\trill si' la' si' sol'( do'') |
    si'1 |
    la'\trill |
    sol'2 r4 re'''\fort |
    re'''4.\trill do'''16 si'' re'''4. do'''16\trill si'' |
    mi'''8 re''' do''' si'' la'' sol'' fad'' mi'' |
    re''4 sol''2 fad''4\trill |
    sol'' re'' sol'8 r
  }
  \tag #'dessus2 {
    r2 |
    r r4 re''8\doux do'' |
    si'2\trill \appoggiatura la'8 sol' r si'4\fort |
    re''4. do''16 si' re''4. do''16 si' |
    si'2\trill r4 sol'\doux |
    sol'2 sol' |
    sol'1~ |
    sol'2 fad'\trill |
    sol' r |
    r4 re'' si'\trill do''8 re'' |
    la'4 fad''8\fort sol'' la''4 sol''\trill |
    fad''8 sol'' fad'' sol'' la''4 sol''\trill |
    fad''8\trill r r la'' re''' r la'4 |
    re' r r sol' |
    sol r r r8 sol' |
    do''([ si' do''16]) r re''8\ademi mi''8([ re'' mi''16]) r sol''8 |
    sol''2 r4 si'\doux |
    mi' r r la' |
    la r r r8 la' |
    re''([ dod'' re''16]) r mi''8\ademi fad''([ mi'' fad''16]) r la''8 |
    la''2 r |
    R1*3 |
    r4 sol''\fort si'' si''8 si'' |
    si''4 re'''8 do''' si'' la'' sol'' fad'' |
    mi''2 sol''~ |
    sol'' fad''\trill |
    sol''4 re''8 do'' si' la' sol' fad' |
    mi'2 sol'~ |
    sol'1 |
    fad'\trill |
    sol'2 r4 si''\fort |
    si''4.\trill la''16 sol'' si''4. la''16\trill sol'' |
    sol''4 mi''~ mi''4.( fad''16 sol'') |
    fad''4\trill sol''8 la'' la''4.\trill( sol''16 la'') |
    sol''4 re'' sol'8 r
  }
>>
<<
  \tag #'dessus1 {
    mi''4\doux |
    sol'' fad''8 mi'' sol''4 fad''8 mi'' |
    mi''2 si'8 r sol''4\fort |
    si''2.( la''8 sol'') |
    si''2.( la''8 sol'') |
    sol''2\trill fad''8 r r fad''\doux |
    la''2 sol''8( fad'') mi''( red'') |
    \appoggiatura red''8 mi''2\prall mi''4. fad''8 |
    \appoggiatura fad''8 sol''\prall do''' si''( red''') mi''' r r4 |
    mi''2 re''4\trill dod'' |
    fad'' lad''2 si''4 |
    mi''4 sol''2( fad''8\trill mi'') |
    re''( fad'') mi''( sol'') fad''4 si''8 r |
    mi''2 sol'' |
    dod''4( mi''8 re'') dod''( si') lad' r |
    lad''2\fort~ lad''4.( si''16 lad'') |
    lad''1\trill |
    fad''2\doux sold''4 lad'' |
    si'' fad''8( la'') sol''( la'') red''( fad'') |
    mi''4 fad''8( sol'') fad''( sol'') dod'' mi'' |
    re''4.( dod''8) re''8( fad'') mi'' sol'' |
    dod''2 dod''\trill |
    si' r4 re''8\fort re'' |
  }
  \tag #'dessus2 {
    sol'4\doux |
    si'4 la'8 sol' si'4 la'8 sol' |
    si'2 r4 mi''\fort |
    sol''2.( fad''8 mi'') |
    sol''2.( fad''8 mi'') |
    mi''2\trill red''8 r r red''\doux |
    fad''2 mi''8( red'') mi''( fad'') |
    si'2 si'4.( do''16 si') |
    si'8( la')\trill sol'( fad') sol'( fad') mi' r |
    dod''2 si'4\trill lad' |
    si' mi''2 re''4 |
    dod'' mi''2 re''8( dod'') |
    fad'4 lad' si' fad'8 r |
    dod''2 mi'' |
    lad'4( dod''8 si') lad'( sold') fad' r |
    dod''2\fort~ dod''4.( re''16 dod'') |
    dod''1\trill |
    dod''2\doux dod''4.( re''16 dod'') |
    fad'4 si'2.~ |
    si'8( dod'') lad'( si') dod''4 fad' |
    r2 r4 si' |
    si'2 lad'\trill |
    si' r4 re''8\fort re'' |
  }
>>
