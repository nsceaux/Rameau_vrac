\score {
  <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >>{ \set fontSize = -2 \includeLyrics "paroles" }
    \new GrandStaff \with { instrumentName = "Cors en sol" \haraKiri } <<
      \new Staff <<
        \keepWithTag #'() \global \keepWithTag #'un \includeNotes "cor"
      >>
      \new Staff <<
        \keepWithTag #'() \global \keepWithTag #'deux \includeNotes "cor"
      >>
    >>
  >>
  \layout { indent = \largeindent }
}
