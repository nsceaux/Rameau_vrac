\clef "taille" re'8 re' |
sol'4 sol' si'4.\trill la'16 sol' |
si'4.\trill la'16 sol' si'4.\trill la'16 sol' |
sol'4 sol r sol' |
do''4 do'2 do''4 |
si' si2 si'4 |
la'2 re'' |
sol'4 re' si\trill la8 sol |
re'4 re r2 |
r r4 re'8\doux re' |
sol'4 sol' si'4.\fort la'16 sol' |
sol'2 sol4 r |
r2 r4 sol'\doux |
do'' do'2 do''4 |
si'4 si2 si'4 |
la'2 re'' |
sol' r4 re'' |
si'\trill la'8 sol' sol'4 sol |
re' fad''2\fort sol''4 |
re'' fad'2 sol'4 |
re'2 r4 re''8 do'' |
si'4 re''8 mi'' fa'' r re'\ademi mi' |
fa'4 r r r8 fa'\doux |
mi'( re' do') r r4 r8 re''\ademi |
mi''( re'' mi'') r r4 mi''8\doux re'' |
dod''4 mi''8 fad'' sol''4 mi'8\ademi fad' |
sol'4 r r r8 sol' |
fad'(\trill mi' re') r r4 r8 mi''\ademi |
fad''( mi'' fad'') r r4 re' |
la la'8 si' do''2~ |
do''4 r r fad' |
sol' r r2 |
r4 re''\fort sol''4 sol''8 sol'' |
sol''2. re''4 |
do''2 mi'' |
re'' re' |
sol'4 si' si si8 si |
do'2 mi' |
re'1 re |
sol2 r4 sol' |
sol'2 sol |
do'1~ |
do'4 si8 do' re'4 re |
sol2. r4 |
r2 r4 mi' |
sol'4 fad'8 mi' sol'4\fort fad'8 mi' |
mi'2 mi4 r |
r2 r4 mi' |
si'2 si8 r r4 |
r2 r4 si'8\doux la' |
sol'4.( fad'16 sol') sol'4.\trill fad'8 |
mi'2 mi4 r |
sol'2 fad'4\trill mi' |
re' dod'2 re'4 |
sol'2 lad |
si4 dod' re' r |
sol'2 mi'\trill |
fad'1 |
R1 |
r4 fad'\fort fad2 |
mi'1\doux |
re'2 mi'4 fad' |
sol' fad'8 mi' lad2 |
si r4 sol' |
fad'2 fad |
si2 r4 re'8 re' |
