\tag #'taille \clef "taille"
\tag #'haute-contre \clef "haute-contre"
\modVersion r8 \origVersion s8
\setMusic #'debut {
  sol16 sol la la si si do'8 si16 si do' do' re' re' |
  mi'8 re'16 re' mi' mi' fa' fa' sol'8 sol' do''8 mi' |
  re' re' sol'4. la'16 sol' fa' mi' fa' sol' |
  mi'8 mi' la'2 sol'4~ |
  sol' << \origVersion { fa'4~ fa' } \modVersion fa'2 >> mi'4 |
  re'8 re''16 re'' re'' re'' re'' re'' sol'8 mi''16 mi'' mi'' mi'' fad'' sol'' |
  fad''8\trill la'16 la' la' la' re'' re''
  si'8 re''16 re'' re'' re'' re'' re'' |
  re'' re'' re'' re'' re'' re'' re'' re'' re''4 sol'' |
  sol'16 sol' sol' sol' do'' do'' do'' do'' fad'8 sol'4 fad'8
}

\modVersion { \debut sol'8 \debut sol'4 }
\origVersion { \debut sol'8 }

%%%
r4 sol'2 sol' sol'4~ | sol'
<<
  \tag #'haute-contre {
    sol'2 sol' sol'4~ |
    sol' sol'2 sol' sol'4~ |
    sol' sol'2 sol' sol'4~ |
    sol' \ru#10 do'8 |
    \ru#12 do'8 |
    do'4
  }
  \tag #'taille {
    sol2 sol sol4~ |
    sol sol2 sol sol4~ |
    sol sol2 sol sol4~ |
    sol \ru#10 mi'8 |
    \ru#12 mi'8 |
    mi'4
  }
>> r4 do' do' mi' mi' |
sol'2 sol'4 sol' si' si' |
do''8 re'' mi'' re'' do'' re'' mi'' re'' do'' si' la' do'' |
si'2 si4 si re' re' |
re' re' re' re' <<
  \tag #'haute-contre {
    si'4 si' |
    si' si' si' si' si' si' |
    do'' sol' sol' sol' mi'' mi'' |
    mi'' mi'' mi'' mi'' mi'' mi'' |
    la'1. |
  }
  \tag #'taille {
    re'4 re' |
    re' re' re' re' re' re' |
    do' do'2 do' do'4~ |
    do' sol' sol' sol' sol' sol' |
    sol'2 fa' r |
  }
>>
fa'2 fa' r4 r8 fa' |
do'2 mi' sol' |
si' sol' sol' |
do'' sol' sol' |
re' re'4 r r2 |
la'2 la' la' |
mi' mi'4 r r2 |
la'2 la' r4 r8 la' |
la'2 la' r4 r8 la' |
la'2 la' r4 r8 la' |
la'2 la' r4 r8 la' |
si'2 si4 si mi' mi' |
sold'4 si8 mi' sold'4 mi'8 sold' si'4 sold'8 si' |
mi''4 sold' si' re''2 sold'4 |
la' mi'8 la' do''4 la'8 do'' mi''4 do''8 mi'' |
mi'4 \ru#10 mi'8 |
\ru#12 fa'8 |
fad'1. |
R1. |\allowPageTurn
r4 la'2 la' la'4~ |
la' la'2 la' fad'4 |
sol'2 r r4 sib'8 sib' |
sib'?1 r4 <<
  \tag #'haute-contre {
    sib'8 sib' |
    sib'?1 r4 sib'8 sib' |
    sib'2 lab' r4 lab'8 lab' |
    lab'1 r4 lab'8 lab' |
    lab'?1 r4 lab'8 lab' |
    lab'?2 sol' lab'! |
    \appoggiatura sol'16 fa'2 mi'~ mi'4.\trill fa'8 |
  }
  \tag #'taille {
    mi'8 mi' |
    mi'1 r4 sol'8 sol' |
    sol'2 fa' r4 do'8 do' |
    re'1 r4 re'8 re' |
    re'1 r4 fa'8 fa' |
    fa'2 do' do' |
    fa' mi'~ mi'4. fa'8 |
  }
>>
fa'4 do'8 fa' la'4 fa'8 la' do''4 la'8 do'' |
<<
  \tag #'haute-contre { la'4\trill \ru#10 la'8 | sib'4 }
  \tag #'taille { do'4 \ru#10 do'8 | re'4 }
>> fa'8 sib' re''4 sib'8 re'' fa'4 fa'8 fa' |
fa'4 <<
  \tag #'haute-contre {
    \ru#10 re''8 |
    re''1. |
    mib''4 << \origVersion { sib'4~ sib' } \modVersion sib'2 >>
    sib'2 sib'4~ |
    sib' sib'2 sib' sib'4 |
    fa' do''2 do''
  }
  \tag #'taille {
    \ru#10 fa'8 |
    fa'1. |
    sib4 << \origVersion { mib'4~ mib' } \modVersion mib'2 >>
    mib'2 do'4 |
    re' fa'2 fa' fa'4~ |
    fa'4 fa'2 fa'
  }
>> fa'4~ |
fa' fa'2 fa' sol'4 |
do'2 do'4 r r2 |
r4 sol'2 sol' sol'4 |
re'2 re'4 r r2 |
\ru#12 la8 |
\ru#12 la8 |
la4 <<
  \tag #'haute-contre {
    mi'4 fa' sol' fa' mi' |
    fa' mi' fa' sol' fa' mi' |
    fa' mi' fa' sol' fa' mi' |
    la2 la4 r r2 |
    r4 re''2 re'' re''4~ |
    re'' re''2 re'' re''4 |
    re''2 r r |
    do''4. mi''8 la'4
  }
  \tag #'taille {
    dod' re' mi' re' dod' |
    re' dod' re' mi' re' dod' |
    re' dod' re' mi' re' la |
    re2 re4 r r2 |
    r4 fa'2 fa' fa'4~ |
    fa' fa'2 fa' fa'4 |
    mi'2 r r |
    mi'2 fa'4
  }
>> fa'8 la' re''4 la'8 re'' |
fa''2 r r |
<<
  \tag #'haute-contre { re''2 mi''4 }
  \tag #'taille { sol'2 sol'4 }
>> mi'8 sol' do''4 sol'8 do'' |
mi''2 r r |
<<
  \tag #'haute-contre { do''2 re''4 }
  \tag #'taille { fa'2 fa'4 }
>> re'8 fa' sib'4 fa'8 sib' |
re''4 r mib''2 mib'' |
mib'' mib'' mib'' |
mib'' mib'' re'' |
do'' r r |
R1. |
r2 sol'4 sol' sib' sib' |
re''2 re'4 re' fad' fad' |
sol'2 sib4 re' do' sib |
la2 r r |
<<
  \tag #'haute-contre {
    \ru#12 sib'8 |
    sib'2 mib''4 r r2 |
    \ru#12 la'8 |
    la'2 re'4 r r2 |
    \ru#12 sol'8 |
    sol'2 fad'4 r r2 |
  }
  \tag #'taille {
    \ru#12 sol'8 |
    sol'2 la'4 r r2 |
    \ru#12 fad'8 |
    fad'?2 sol'4 r r2 |
    \ru#12 mi'8 |
    mi'2 do''4 r r2 |
  }
>>
r2 fad'4 fad' la' la' |
re'4. mib'8 la2\trill~ la4. sol8 |
sol2 r r |
r4 re'4 sol' si' re'' si' |
sol' si' re'' si' sol' fa' |
mi'2 r r |
r4 sol do' mi' sol' mi' |
do' mi' sol' mi' do' do' |
do'2 fa' fa' |
do' mi' sol' |
si' sol' sol' |
do'' sol' sol' |
re'2 re'4 r r2 |
la'2 la' r4 r8 fa' |
mi'2 mi'4 r r2 |
r re'4 re' fa' fa' |
la'2 fa'4 fa' la' la' |
re''2 fa'4 fa' fa' fa' |
si2 mi'8 fa' re' mi' do' re' si sold |
la2 r r4 do''8 do'' |
fad'2 r r4 fad'8 fad' |
mi'2 r r4 r8
<<
  \tag #'haute-contre { re''8 | do''2 do''4 }
  \tag #'taille { mi'8 | mi'2 mi'4 }
>> r4 r2 |
r4 sol'2 sol' sol'4~ |
sol'4
<<
  \tag #'haute-contre {
    sol2 sol sol4~ |
    sol sol2 sol sol4~ |
    sol sol2 sol sol4~ |
    sol
  }
  \tag #'taille {
    sol'2 sol' sol'4~ |
    sol' sol'2 sol' sol'4~ |
    sol' sol'2 sol' sol'4~ |
    sol'
  }
>> \ru#10 do'8 |
\ru#12 do' |
do'4 r do' do' mi' mi' |
sol'2 sol'4 sol' si' si' |
do''8 re'' mi'' re'' do'' re'' mi'' re'' do'' si' la' do'' |
si'2 r r4
<<
  \tag #'haute-contre {
    si8 si |
    do'2 do'4 r r sol'8 sol' |
    la'2 la'4 r r la'8 la' |
    la'2 re'4 r r la'8 la' |
    si'2 sol'4 r r si'8 si' |
    si'2 si'4 r r2 |
    R1. |
    \ru#12 fa''8 |
    fa''2 fa''4 r r r8 si' |
    do''4.
  }
  \tag #'taille {
    re'8 re' |
    sol2 sol4 r r do'8 do' |
    do'2 do'4 r r do'8 do' |
    re'2 re'4 r r re'8 re' |
    re'2 re'4 r r re'8 re' |
    re'2 sol4 r r2 |
    R1. |
    \ru#12 sol'8 |
    sol'2 sol'4 r r r8 sol' |
    sol'4.
  }
>> la'8 sol'2 re' |
re' r r |
<<
  \tag #'haute-contre {
    \ru#12 do''8 |
    si'2 si'4 r r2 |
    r r r4 r8 sol' |
    sol'4. fa'8 fa'2.( mi'8) fa' |
    mi'4\trill sol'2 sol' sol'4~ |
    sol' sol'2 sol' sol'4~ |
    sol' sol'2 sol' sol'4 |
    sol'2
  }
  \tag #'taille {
    \ru#12 re'8 |
    re'2 re'4 r r2 |
    r r r4 r8 sol' |
    sol'4 fa'8 mi' re'2~ re'4.\trill do'8 |
    do'4 sol'2 sol' sol'4~ |
    sol' sol2 sol sol4~ |
    sol sol2 sol sol4 |
    sol2
  }
>>
