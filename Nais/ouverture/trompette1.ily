\clef "dessus"
\origVersion { R1*9 r8 }
\modVersion { R1*18 r4 }
R1. |
r4 sol''2 sol'' fa''4 |
mi'' sol''2 sol'' fa''4 |
mi'' sol''2 sol'' fa''4 |
mi'' sol'2 sol' sol'4~ |
sol' sol'2 sol' sol'4~ |
sol' r r2 r |
r4 re''2 re'' sol''4 |
mi'' sol''2 mi'' do''4 |
sol' r r2 r |
R1.*5 |
r4 do''2 do'' do''4~ |
do'' do''2 do'' sol'4~ |
sol' re''2 re'' sol'4~ |
sol' mi''2 mi'' mi''4 |
fad''2\trill sol''4 r r2 |
r4 re''2 re'' re''4 |
mi''2 r r |
R1.*6 |
r4 mi'2 mi' mi'4~ |
mi' r r2 r |
r4 do''2 do'' do''4~ |
do'' do''2 do'' do''4 |
do''1. |
R1. |
r4 do''2 do'' do''4~ |
do'' do''2 do'' re''4 |
re''2 r r4 re''8 re'' |
mi''2 r r |
sol''4 sol'' sol'' sol'' sol'' sol'' |
sol''4 mib'' fa'' r r2 |
r4 re''8 re'' re''4 re''8 re'' re''4 re''8 re'' |
re''4 r r2 r |
r4 re'' mib''2 fa'' |
r do' do' |
do' r r |
R1.*11 |
r4 mi''2 mi'' mi''4~ |
mi'' mi''2 mi'' mi''4 |
la'' r r2 r |
R1.*30 |
r4 sol'2 sol' sol'4~ |
sol' sol'2 sol' sol'4 |
sol'2 r r |
R1. |
r4 do''2 do'' do''4~ |
do'' do''2 do'' sol'4~ |
sol' re''2 re'' re''4 |
sol' mi''2 mi'' mi''4 |
fad''2\trill sol''4 r r2 |
r4 re''2 re'' re''4 |
mi''2 r r |
R1.*8 |
r4 sol''2 sol'' fa''4 |
mi''4 sol''2 sol'' fa''4 |
mi'' sol''2 sol'' fa''4 |
mi'' sol''2 sol'' fa''4 |
mi'' sol'2 sol' sol'4~ |
sol' sol'2 sol' sol'4~ |
sol' r r2 r |
r4 re''2 re'' sol''4 |
mi'' sol''2 mi'' do''4 |
sol' r r2 r |
R1. |
r4 do''2 do'' do''4~ |
do'' r4 r2 r |
r4 re''2 re'' re''4~ |
re'' r r2 r |
R1. |
r4 re''2 re'' re''4 |
sol''2 sol''4 r r2 |
sol''4 fa''8 mi'' re''2\trill~ re''4. do''8 |
do''1. |
R1.*3 |
r4 sol''2 sol'' fa''4 |
mi''2 r r |
r mi''4 mi'' fa'' do'' |
do'' sol''2 sol'' sol''4 |
sol''2
