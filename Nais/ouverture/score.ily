\score {
  <<
    \origVersion\new StaffGroupNoBar <<
      \new Staff <<
        { <>^"Hautbois" s1*9 s8 <>^"Hautbois" }
        \global \keepWithTag #'hautbois1 \includeNotes "hautbois"
      >>
      \new Staff \with { \haraKiriFirst } <<
        { \startHaraKiri s1*9 s8 s1.*22
          \stopHaraKiri s1.*5
          \startHaraKiri s1.*18
          \stopHaraKiri s1.*64 \startHaraKiri }
        \global \keepWithTag #'hautbois2 \includeNotes "hautbois"
      >>
      \new Staff <<
        { <>^"Violons" s1*9 s8 <>^"Violons" }
        \global \includeNotes "violon1"
      >>
      \new Staff <<
        { <>^"Violons" s1*9 s8 <>^"Violons" }
        \global \includeNotes "violon2"
      >>
      \new Staff <<
        { s1*9 s8 <>^"Trompettes" }
        \global \includeNotes "trompette1"
      >>
      \new Staff <<
        { s1*9 s8 <>^"Trompettes" }
        \global \includeNotes "trompette2"
      >>
      \new Staff <<
        { <>^"Haute-contre" s1*9 s8 <>^"Haute-contre" }
        \global \keepWithTag #'haute-contre \includeNotes "parties"
      >>
      \new Staff <<
        { <>^"Taille" s1*9 s8 <>^"Taille" }
        \global \keepWithTag #'taille \includeNotes "parties"
      >>
      \new Staff <<
        { <>^"Bassons" s1*9 s8 <>^"Bassons" }
        \global \keepWithTag #'basson \includeNotes "basse"
      >>
      \new Staff << \global \includeNotes "timbales" >>
      \new Staff <<
        { <>^"Basses" s1*9 s8 <>^"Basses" }
        \global \keepWithTag #'basse \includeNotes "basse"
        \origLayout {
          s1*4 s2 \bar "" \break s2 s1*3 s2 \bar "" \pageBreak
          s2 s8 s1.*5\pageBreak
          s1.*5\pageBreak
          s1.*6\pageBreak
          s1.*6\pageBreak
          s1.*5\pageBreak
          s1.*7\pageBreak
          s1.*5\pageBreak
          s1.*5 s1 \bar "" \pageBreak
          %% 10
          s2 s1.*4 s2\bar "" \pageBreak
          s1 s1.*4\pageBreak
          s1.*5 s2\bar "" \pageBreak
          s1 s1.*5\pageBreak
          s1.*5\pageBreak
          s1.*6\pageBreak
          s1.*5\pageBreak
          s1.*6\pageBreak
          s1.*5 s1 \bar "" \pageBreak
          s2 s1.*5\pageBreak
          %% 20
          s1.*5 s2\bar "" \pageBreak
          s1 s1.*5\pageBreak
          s1.*6\pageBreak
          s1.*5 s1 \bar "" \pageBreak
          s2 s1.*5\pageBreak
          s1.*6\pageBreak
          s1.*3 s2\break
        }
      >>
    >>

    \modVersion\new StaffGroupNoBar <<
      \new StaffGroupNoBracket <<
        \new GrandStaff \with {
          instrumentName = \markup { Trompettes }
          shortInstrumentName = "Tr" }
        <<
          \new Staff \with { \haraKiriFirst } <<
            { s1*18 s4 <>^"Trompettes" s1\noHaraKiri }
            \global \includeNotes "trompette1"
          >>
          \new Staff \with { \haraKiriFirst } <<
            { s1*18 s4 s1\noHaraKiri }
            \global \includeNotes "trompette2"
          >>
        >>
        \new Staff \with { \haraKiriFirst shortInstrumentName = "Tm" } <<
          \instrumentName "Timbales"
          { s1*18 s4 s1 \noHaraKiri }
          \global \includeNotes "timbales"
        >>
      >>
      \new StaffGroupNoBracket <<
        \new GrandStaff \with {
          instrumentName = \markup { Hautbois }
          shortInstrumentName = \markup\concat { H \super tb } }
        <<
          \new Staff <<
            \global \keepWithTag #'hautbois1 \includeNotes "hautbois"
          >>
          \new Staff <<
            \global \keepWithTag #'hautbois2 \includeNotes "hautbois"
          >>
        >>
        \new Staff \with {
          shortInstrumentName = \markup\concat { B \super on } }
        <<
          \instrumentName "Bassons"
          \global \keepWithTag #'basson \includeNotes "basse"
        >>
      >>
      \new StaffGroupNoBracket <<
        \new GrandStaff \with {
          instrumentName = \markup { Violons }
          shortInstrumentName = \markup\concat { V \super on } }
        <<
          \new Staff << \global \includeNotes "violon1" >>
          \new Staff << \global \includeNotes "violon2" >>
        >>
        \new Staff \with { shortInstrumentName = "H-c" } <<
          \instrumentName "Haute-contre"
          \global \keepWithTag #'haute-contre \includeNotes "parties"
        >>
        \new Staff \with { shortInstrumentName = "T" } <<
          \instrumentName "Taille"
          \global \keepWithTag #'taille \includeNotes "parties"
        >>
        \new Staff \with { shortInstrumentName = "B" } <<
          \instrumentName "Basses"
          \global \keepWithTag #'basse \includeNotes "basse"
        >>
      >>
    >>
  >>
  \layout { short-indent = 8\mm }
  \midi { }
}
