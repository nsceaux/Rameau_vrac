\clef "basse"
\origVersion { R1*9 r8 }
\modVersion { R1*18 r4 }
<>^"Timbales" do2 do do |
do do do |
do do do |
do do do |
do4 \ru#10 do8 
\ru#12 do8 |
do4 r r2 r |
R1. |
r2 do4 do do do |
sol, sol,8 sol, sol,4 sol,8 sol, sol,4 sol,8 sol, |
sol,4 sol, sol, sol, sol, sol, |
sol,1 r4 sol, |
do2 r r |
R1.*2 |
\ru#12 do8 |
\ru#12 do8 |
\ru#12 do8 |
\ru#12 do8 |
do2 do4 r r2 |
\ru#12 do8 |
do2 do4 r r2 |
R1.*14 |
r2 r r4 <>^\markup\whiteout Tambour re8 re |
do1 r4 do8 do |
do1 r4 do8 do |
do1 r4 do8 do |
do1 r4 do8 do |
sib,?1 r4 sib,8 sib, |
sib,?1 r2 |
R1.*6 |\allowPageTurn
\ru#12 do8 |
\ru#12 do8 |
\ru#12 do8 |
\ru#12 do8 |
do2 do4 r r2 |
\ru#12 do8 |
do2 do4 r r2 |
R1.*9 |
r2 r8 \ru#7 la,8 |
la,4 r r2 r |
r2 r8 \ru#7 la,8 |
la,4 r r2 r |
R1.*8 |
\ru#12 do8 |
\ru#4 do8 do4 r r2 |
\ru#12 do8 |
do2 do4 r r2 |
\ru#12 do8 |
do2 do4 r r2 |
R1.*8 |
\ru#12 do8 |
\ru#12 do8 |
\ru#12 do8 |
\ru#12 do8 |
do2 do4 r r2 |
\ru#12 do8 |
do2 do4 r r2 |
R1.*9 |
<>^"Timbales" do2 do do |
do do do |
do do do |
do4 \ru#10 do8 |
\ru#12 do8 |
do4 r r2 r |
R1. |
r2 do4 do do do |
sol,2 r r |
R1.*6 |
r2 sol, sol, |
sol, r r |
R1.*6 |
do2 do do |
do r r |
R1.*2 |
r2 sol, sol, |
do
