\key do \major
\digitTime\time 2/2 \midiTempo #120
\origVersion {
  s1*9 \set Score.measureLength = #(ly:make-moment 1 8)
  s8 \bar "|:|"
}
\modVersion {
  s1*18 \set Score.measureLength = #(ly:make-moment 1 4) s4 \bar "||"
}
\digitTime\time 3/2 \midiTempo #240 s1.*135 s2
\bar "|."