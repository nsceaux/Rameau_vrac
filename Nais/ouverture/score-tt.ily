\score {
  \new StaffGroup <<
    \new GrandStaff \with { instrumentName = "Trompettes" } <<
      \new Staff << \global \includeNotes "trompette1" >>
      \new Staff \with { \haraKiriFirst } <<
        \global \includeNotes "trompette2"
      >>
    >>
  \new Staff <<
    \instrumentName "Timbales"
    \global \includeNotes "timbales"
    >>
  >>
  \layout { }
}
