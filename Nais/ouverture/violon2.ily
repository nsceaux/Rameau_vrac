\clef "dessus2"
\modVersion r8 \origVersion s8
\setMusic #'debut {
  sol16 sol la la si si do'8 si16 si do' do' re' re' |
  mi'8 re'16 re' mi' mi' fa' fa' sol'8 sol' do''8 mi' |
  re' re' sol'4. la'16 sol' fa' mi' fa' sol' |
  mi'8 mi' la'2 sol'4~ |
  sol' << \origVersion { fa'4~ fa' } \modVersion fa'2 >> mi'4 |
  re'8 re''16 re'' re'' re'' re'' re'' sol'8 mi''16 mi'' mi'' mi'' fad'' sol'' |
  fad''8\trill la'16 la' la' la' re'' re''
  si'8 re''16 re'' re'' re'' re'' re'' |
  re'' re'' re'' re'' re'' re'' re'' re'' re''4 sol'' |
  sol'16 sol' sol' sol' do'' do'' do'' do'' do''8 si' la' re''
}

\modVersion { \debut si'8\trill \debut si'4\trill }
\origVersion { \debut si'8\trill }

%%%
\clef "dessus" r4 mi''2 mi'' mi''4~ |
mi'' mi''2 mi'' mi''4~ |
mi'' mi''2 mi'' mi''4~ |
mi'' mi''2 mi'' mi''4~ |
mi'' \clef "dessus2" <>^"a 2 cordes" \ru#10 <sol mi'>8 |
\ru#12 <sol mi'>8 |
q4 r r2 
\clef "dessus" r4 do''' |
si''8 do''' re''' do''' si'' do''' re''' do''' si'' la'' sol'' fa'' |
mi'' fa'' sol'' fa'' mi'' fa'' sol'' fa'' mi'' re'' do'' mi'' |
re''2 re'4 re' sol' sol' |
si'4 re'8 sol' si'4 sol'8 si' re''4 si'8 re'' |
sol''4 sol'' sol'' sol'' sol'' sol'' |
sol'' sol'8 do'' mi''4 do''8 mi'' sol''4 mi''8 sol'' |
do'''4 do''' do''' do''' do''' do''' |
do'''1 r2 |
la''4 do'' la'' do'' la''8 la'' sol'' fa'' |
sol''4 do'' sol'' do'' sol''8 sol'' fa'' mi'' |
fa''4 sol' fa'' sol' fa''8 fa'' mi'' re'' |
mi''4 do''2 do'' do''4 |
la'2\trill si'4 r r2 |
fad''8 fad'' re'' re'' la' la' fad' fad' re' fad'' sold'' la'' |
sold''2\trill la''4 r r2 |
re''8 mi'' fa'' mi'' re'' mi'' fa'' mi'' re'' mi'' fa'' mi'' |
re'' mi'' fa'' mi'' re'' mi'' fa'' mi'' re'' do'' si' la' |
si' do'' re'' do'' si' do'' re'' do'' si' do'' re'' do'' |
si' do'' re'' do'' si' do'' re'' do'' si' la' sold' la' |
sold'2\trill si4 si mi' mi' |
sold'4 si8 mi' sold'4 mi'8 sold' si'4 sold'8 si' |
mi''4 si'2 si' si'4 |
do'' mi'8 la' do''4 la'8 do'' mi''4 do''8 mi'' |
la''4 \ru#10 la''8 |
\ru#12 la'' |
la''1. |
R1. |\allowPageTurn
fad''8 sol'' la'' sol'' fad'' sol'' la'' sol'' fad'' sol'' la'' sol'' |
fad'' sol'' la'' sol'' fad'' sol'' fad'' mi'' re'' do'' sib' la' |
sib'4 sib'8 sib' sol''4 sib'8 sib' sol''2~ |
sol''4 sib'8 sib' sol''4 sib'8 sib' sol''2~ |
sol''4 sib'8 sib' sol''4 sib'8 sib' mib''2~ |
mib''4 lab'8 lab' fa''4 lab'8 lab' fa''2~ |
fa''4 lab'8 lab' fa''4 lab'8 lab' fa''2~ |
fa''4 lab'8 lab' fa''4 fa'8 fa' re''2~ |
re''4 sol'8 sol' mib''4 do''8 do'' fa''4. do''8 |
reb''4. sol'8 sol'2\trill~ sol'4. fa'8 |
fa'4 r8 fa' la'4 fa'8 la' do''4 la'8 do'' |
%% 10
fa''4 la'2 la' la'4 |
sib' fa'8 sib' re''4 sib'8 re'' fa''4 re''8 fa'' |
sib''4 fa''2 fa'' fa''4~ |
fa'' fa''2 fa'' fa''4 |
sol'' << \origVersion { sib'4~ sib'} \modVersion sib'2 >> sib' sib'4~ |
sib' sib'2 sib' sib'4 |
la' la'2 la' la'4 |
sib' re''2 sib' sol'4 |
sol'2 do'4 r r2 |
r4 sib'2 sib' sib'4 |
la'2\trill sol'4 r r2 |
r4 sol'2 sol' sol'4~ |
sol' sol'2 sol' sol'4 |
la''8 fa'' sol'' mi'' fa'' re'' mi'' dod'' re'' si' dod'' la' |
la'' fa'' sol'' mi'' fa'' re'' mi'' dod'' re'' si' dod'' la' |
la'' fa'' sol'' mi'' fa'' re'' mi'' dod'' re'' si' dod'' la' |
re'2 re'4 r r2 |
r4 sib''2 sib'' sib''4~ |
sib''4 sib''2 sib'' sib''4~ |
sib''4 sib''8 sib'' re'''4 sib''8 sib'' re'''4 sib''8 sib'' |
la''2 la''4 r r2 |
r4 la''8 la'' re'''4 la''8 la'' re'''4 la''8 la'' |
si''2 do'''4 r r2 |
r4 sol''8 sol'' do'''4 sol''8 sol'' do'''4 sol''8 sol'' |
fa''2 fa''4 r r2 |
r4 lab''2 lab'' lab''4~ |
lab'' lab''2 lab'' lab''4~ |
lab'' lab''2 lab'' sol''4~ |
sol'' fad'' r2 r |
r2 re''4 re'' fad'' fad'' |
sol''2 sib'4 re'' do'' sib' |
la'2 fad' la' |
re'4 re''2 re'' re''4~ |
re'' r r2 r |
\ru#12 mib''8 |
mib''2 sol''4 r r2 |
\ru#12 re''8 |
re''2 sol''4 r r2 |
\ru#12 do''8 |
do''2 la''4 r r2 |
la''8 la'' fad'' fad'' re'' re'' la' la' re' do'' sib' la' |
sib'4 la'8 sol' re'2 fad'\trill |
sol'4 re'8 sol' si'!4 sol'8 si' re''4 sol''8 re'' |
sol''4 \ru#10 sol''8 |
sol''2 sol''4 r r2 |
r4 sol'8 do'' mi''4 do''8 mi'' sol''4 mi''8 sol'' |
do'''4 \ru#10 do'''8 |
do'''2 do'''4 r r2 |
la''4 do'' la'' do'' la''8 la'' sol'' fa'' |
sol''4 do'' sol'' do'' sol''8 sol'' fa'' mi'' |
fa''4 sol' fa'' sol' fa''8 fa'' mi'' re'' |
mi''4 do''2 do'' do''4 |
la'2\trill sol'4 r r2 |
r4 re''2 re'' re''4 |
si'2\trill la'4 r r2 |
fa''8 sol'' mi'' fa'' re'' mi'' do'' re'' si' do'' la' re'' |
fa'' sol'' mi'' fa'' re'' mi'' do'' re'' si' do'' la' re'' |
fa'' sol'' mi'' fa'' re'' mi'' do'' re'' si' do'' la' si' |
sold' la' fad' sold' mi'8 fa'? re' mi' do' r r4 |
r4 do''8 do'' la''4 do''8 do'' la''2~ |
la''4 do''8 do'' la''4 do''8 do'' la''2~ |
la''4 do''8 do'' la''4 do''8 do'' si'4 la''8 sold'' |
la''2 la'4 r r2 |
r4 do'''2 do''' do'''4~ |
%% 22
do'''4 do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4~ |
do''' \clef "dessus2" \ru#10 <sol mi'>8 |
\ru#12 <sol mi'>8 |
q4 r r2 \clef "dessus" r4 do''' |
%% 23
si''8 do''' re''' do''' si'' do''' re''' do''' si'' la'' sol'' fa'' |
mi'' fa'' sol'' fa'' mi'' fa'' sol'' fa'' mi'' re'' do'' mi'' |
re'' sol'' fa'' mi'' re'' do'' si' la' sol'4 r |
r4 do'8 mi' sol'4 mi'8 sol' do''2~ |
do''4 do'8 fa' la'4 fa'8 la' do''2~ |
do''4 re'8 fad' la'4 fad'8 la' re''2~ |
re''4 re'8 sol' si'4 sol'8 si' re''2~ |
re''4 r4 r2 r |
r4 sol'8 si' re''4 si'8 re'' sol''4 re''8 sol'' |
\ru#12 si''8 |
re'''8 re''' re''' re''' re''' do''' si'' la'' sol'' fa'' mi'' re'' |
mi''4 re''8 do'' si'2\trill~ si'4. do''8 |
fad'2 r r |
\ru#12 fad''8 |
sol''2 fa''!4 r r2 |
re'''8 re''' re''' re''' re''' do''' si'' la'' sol'' fa'' mi'' re'' |
mi''4 re''8 do'' si'2\trill~ si'4. do''8 |
do''4 do'''2 do''' do'''4~ |
do'''4 do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4 |
do'''2
