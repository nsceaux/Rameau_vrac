\clef "tenor/tenor"
\modVersion { r2 } \origVersion { s4 r4 }
\setMusic #'debut {
  r8 sol16 sol la la si si |
  do'8 si16 si do' do' re' re' mi'8 do16 do mi mi fa fa |
  sol8 sol16 sol si si do' do' re'8 re16 re fa fa sol sol |
  la8 la16 la do' do' re' re' mi'8 mi16 mi sol sol mi mi |
  fa8 re16 re fa fa re re mi8 do16 do mi mi fa fa |
  sol8 sol16 sol sol sol la si do'8 la16 la la la si do' |
  re'8 re'16 re' re' re' mi' fad' \ru#8 sol'16 |
  \ru#8 sol'16 sol' fad' mi' re' do' si la sol |
  mi' re' do' si la sol fad mi \clef "basse" re do si, do re8
  << \tag #'basson re \tag #'basse re, >>
}

\modVersion { \debut sol,8 r8 r4 \clef "tenor" \debut sol,4 }
\origVersion { \debut sol,8 }

%%%
r4 do'2 do' do'4~ |
do' do'2 do' do'4~ |
do' do'2 do' do'4~ |
do' do'2 do' do'4~ |
do' <<
  \tag #'basson {
    do'2 do' do'4~ |
    do' do'2 do' do'4~ |
    do' r r2 r |
    r2 sol4 sol si si |
    do'2
  }
  \tag #'basse {
    <>^"a 2 cordes" \ru#10 <do, do>8 |
    \ru#12 <do, do>8 |
    q4 r r2 r |
    R1. |
    r2
  }
>> do4 do mi mi |
sol si8 sol re4 sol8 re si,4 re8 si, |
sol,4 <<
  \tag #'basson {
    \clef "tenor" sol'2 sol' sol'4~ |
    sol' sol si re' si sol |
    mi do'2 do' do'4~ |
    do' sol do' sol mi do |
    fa1 r2 |
    fa la do' |
    mi sol do' |
    re si re' |
    do sol do' |
    do' si4 r r2 |
    re la re' |
    re'2 do'4 r r2 |
    \clef "basse"
  }
  \tag #'basse {
    sol,4 sol, sol, sol, sol, |
    sol, sol si re' si sol |
    mi mi, mi, mi, mi, mi, |
    mi, sol do' sol mi do |
    fa1. |
    r2 r r4 r8 fa |
    mi2 mi, r4 r8 mi |
    re2 re, r4 r8 re |
    do2 do, r4 r8 do |
    do2 si,4 r r2 |
    re re, r4 r8 re |
    re2 do4 r r2 |
  }
>>
re2 re, r4 r8 re |
re2 re, r4 r8 re |
fa2 fa, r4 r8 fa |
fa2 fa, r4 r8 fa |
mi2 mi, r |
R1. |\allowPageTurn
r4 <<
  \tag #'basson { si2 si si4 | do'2 }
  \tag #'basse { mi4 sold si sold mi | do2 }
>> r2 r |
r4 la do' mi' do' la |
fa la do' la fa mi |
red1. |
R1. |
%% p8
re4 re re re re re |
re re re re re re |
re1 r4 re8 re |
do!1 r4 do8 do |
do1 r4 do8 do |
do1 r4 do8 do |
do1 r4 do8 do |
sib,?1 r4 sib,8 sib, |
sib,?1 <<
  \tag #'basson { r4 sib,?8 sib, }
  \tag #'basse lab,2
>>
sib,2 do do, |
fa, r r |
r4 fa la do' la fa |
<<
  \tag #'basson { re4 sib2 sib sib4 | re' }
  \tag #'basse { re2 r r | r4 }
>> sib4 re' fa' re' sib |
fa sib2 fa4 re sib, |
mib2 <<
  \tag #'basson {
    sol2 sib |
    re fa sib |
    do la do' |
    sib, fa sib |
    sib2 la4 r r2 |
    do2 sol do' |
    do' sib4 r r2 |
    \clef "tenor" r4 sol'2 sol' sol'4~ |
    sol' sol'2 sol' sol'4 |
    fa' mi' re' la r4 sol' |
    fa' mi' re' la r sol' |
    fa' mi' re' la r2 |
    \clef "basse" sib2 sib4 r r2 |
  }
  \tag #'basse {
    mib,2 r4 r8 mib |
    re2 re, r4 r8 re |
    do2 do, r4 r8 do |
    sib,2 sib, r4 r8 sib, |
    sib,2 la,4 r r2 |
    do2 do, r4 r8 do |
    do2 sib,4 r r2 |
    \ru#12 la,8 |
    \ru#12 la,8 |
    \ru#12 la,8 |
    \ru#12 la,8 |
    \ru#12 la,8 |
    la,2 la,4 r r2 |
  }
>>
r2 la2 la |
la la la |
sol r r |
<<
  \tag #'basson {
    \clef "tenor" sol2 fa4 fa8 la re'4 la8 re' |
    fa'2 r r |
    fa2 mi4 sol8 do' mi'4 do'8 mi' |
    sol'2 r r |
    mib re4 fa8 lab re'4 lab8 re' |
    fa'4 r
  }
  \tag #'basse {
    sol2 fa4 r r2 |
    R1. |
    fa2 mi4 r r2 |
    R1. |
    mib2 re4 r r2 |
    r
  }
>> do'2 do' |
do' do' do' |
do' do' sib |
la r r |
R1.*2
<<
  \tag #'basson {
    r2 re'4 re' fad' fad' |
    sol'2 sib4 re' do' sib |
    la2 r r |
  }
  \tag #'basse {
    R1. |
    r2 sol,4 sol, sib, sib, |
    re2 r r |
  }
>>
\ru#12 re'8 |
re'2 do'4 r r2 |
\ru#12 do'8 |
do'2 sib4 r r2 |
\clef "basse" \ru#12 sib8 |
sib2 la4 r r2 |
r re4 re fad fad |
sol do re2 re, |
sol,2 r r |
\clef "tenor" r4 sol si re' sol' re' |
si re' sol' re' si sol |
mi2 r r |
r4 sol do' mi' sol' mi' |
do' sol do' sol \tag #'basse \clef "basse" mi do |
<<
  \tag #'basson {
    \clef "tenor" fa2 la do' |
    mi sol do' |
    re si re' |
    do sol do' |
    do'2 si4 r r2 |
    re2 la re' |
    re' do'4 r r2 |
    \clef "basse"
  }
  \tag #'basse {
    fa2 fa, r4 r8 fa |
    mi2 mi, r4 r8 mi |
    re2 re,2 r4 r8 re |
    do2 do, r4 r8 do |
    do2 si,4 r r2 |
    re2 re, r4 r8 re |
    re2 do4 r r2 |
  }
>>
r2 re4 re fa fa |
la2 fa4 fa la la |
re'2 la4 la re' si |
mi'2 r r4 r8 mi |
fa2 r r4 mi8 mi |
red2 r r4 red8 red |
mi2 r r4 r8 mi |
la,2 la,4 r r2 |
\tag #'basson \clef "tenor/bass"
r4 do'2 do' do'4~ |
do'4 do'2 do' do'4~ |
do' do'2 do' do'4~ |
do'4 do'2 do' do'4~ |
do'4 <<
  \tag #'basson {
    do'2 do' do'4~ |
    do' do'2 do' do'4~ |
    do'4 r4 r2 r |
    \clef "basse" r2 sol4 sol si si |
    do'2
  }
  \tag #'basse {
    \ru#10 <do, do>8 |
    \ru#12 <do, do>8 |
    q4 r4 r2 r |
    R1. |
    r2
  }
>> do4 do mi mi |
sol2 r r8 la sol fa |
mi2 mi4 r r mi8 mi |
fa2 fa4 r r fa8 fa |
fad2 fad4 r r fad8 fad |
sol2 sol4 r r sol8 sol |
sol,2 sol,4 r r2 |
R1. |
\ru#12 sol8 |
si,2 si,4 r r r8 sol |
do'4. fa8 sol2 sol, |
la,2 r r |
\ru#12 la8 |
sol2 sol,4 r r2 |
r r r4 r8 sol |
do'4. fa8 sol2 sol, |
<< \tag #'basson do4 \tag #'basse do,4 >>
do'2 do' do'4~ |
do' do'2 do' do'4~ |
do' do'2 do' do'4 |
do'2
