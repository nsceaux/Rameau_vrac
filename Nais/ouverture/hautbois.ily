\clef "dessus"
\modVersion r8 \origVersion s8
\setMusic #'debut {
  sol'16 sol' la' la' si' si' do''8 si'16 si' do'' do'' re'' re'' |
  mi'8 re'16 re' mi' mi' fa' fa' sol'8 sol' do''4~ |
  do''8 re''16 do'' si' la' si' do'' la'8 la' re''4~ |
  re''8 mi''16 re'' do'' si' do'' re'' si'8 si' mi''4~ |
  mi''8 la' << \origVersion { re''4~ re''8 } \modVersion re''4. >> sol'8 do''4 |
  si'8 si'16 si' si' si' do'' re'' mi''8 do''16 do'' do'' do'' re'' mi'' |
  la'8 fad''16 fad'' fad'' fad'' sol'' la''
  re''8 si''16 si'' si'' si'' si'' si'' |
  si'' si'' si'' si'' si'' si'' si'' si''
  si'' la'' sol'' fad'' mi'' re'' do'' si' |
  do'' re'' mi'' re'' do'' si' la' sol'
  \clef "dessus" re'' do'' si' do'' re''8 re' |
}

\modVersion { \debut sol'8 \debut sol'4 }
\origVersion { \debut sol'8 }

%%%
r4 do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4~ |
do''' sol''2 sol'' sol''4~ |
sol'' do''2 do'' do''4~ |
do''4 do''2 do'' do''4~ |
do'' r r2 r4 do''' |
si''8 do''' re''' do''' si'' do''' re''' do''' si'' la'' sol'' fa'' |
mi'' fa'' sol'' fa'' mi'' fa'' sol'' fa'' mi'' re'' do'' mi'' |
re''2 re'4 re' sol' sol' |
si'4 re'8 sol' si'4 sol'8 si' re''4 si'8 re'' |
sol''4 sol'' sol'' sol'' sol'' sol'' |
sol'' sol'8 do'' mi''4 do''8 mi'' sol''4 mi''8 sol'' |
do'''4 do''' do''' do''' do''' do''' |
do'''1 r2 |
la''4 do'' la'' do'' la''8 la'' sol'' fa'' |
sol''4 do'' sol'' do'' sol''8 sol'' fa'' mi'' |
fa''4 sol' fa'' sol' fa''8 fa'' mi'' re'' |
mi''4 do''2 do'' do''4 |
la'2\trill si'4 r r2 |
fad''4 la' fad'' la' fad''8 la' si' do'' |
si'2\trill do''4 r r2 |
<<
  \tag #'hautbois1 {
    fa''8 sol'' la'' sol'' fa'' sol'' la'' sol'' fa'' sol'' la'' sol'' |
    fa'' sol'' la'' sol'' fa'' sol'' la'' sol'' fa'' mi'' re'' dod'' |
    re'' mi'' fa'' mi'' re'' mi'' fa'' mi'' re'' mi'' fa'' mi'' |
    re'' mi'' fa'' mi'' re'' mi'' fa'' mi'' re'' do'' si' la' |
    mi''2
  }
  \tag #'hautbois2 {
    re''8 mi'' fa'' mi'' re'' mi'' fa'' mi'' re'' mi'' fa'' mi'' |
    re'' mi'' fa'' mi'' re'' mi'' fa'' mi'' re'' do'' si' la' |
    si' do'' re'' do'' si' do'' re'' do'' si' do'' re'' do'' |
    si' do'' re'' do'' si' do'' re'' do'' si' la' sold' la' |
    sold'2\trill
  }
>> r2 r |
r2 r4 mi'8 sold' si'4 sold'8 si' |
mi''4 si'2 si' si'4 |
do'' mi'8 la' do''4 la'8 do'' mi''4 do''8 mi'' |
la''4 \ru#10 la''8 |
\ru#12 la'' |
la''1. |
R1. | \allowPageTurn
fad''8 sol'' la'' sol'' fad'' sol'' la'' sol'' fad'' sol'' la'' sol'' |
fad'' sol'' la'' sol'' fad'' sol'' fad'' mi'' re'' do'' sib' la' |
sib'4 sib'8 sib' sol''4 sib'8 sib' sol''2~ |
sol''4 sib'8 sib' sol''4 sib'8 sib' sol''2~ |
sol''4 sib'8 sib' sol''4 sib'8 sib' mib''2~ |
mib''4 lab'8 lab' fa''4 lab'8 lab' fa''2~ |
fa''4 lab'8 lab' fa''4 lab'8 lab' fa''2~ |
fa''4 lab'8 lab' fa''4 fa'8 fa' re''2~ |
re''4 sol'8 sol' mib''4 do''8 do'' fa''4. do''8 |
reb''4. sol'8 sol'2\trill~ sol'4. fa'8 |
fa'4 r8 fa' la'4 fa'8 la' do''4 la'8 do'' |
fa''4 <<
  \tag #'hautbois1 { \ru#10 fa''8 | fa''4 }
  \tag #'hautbois2 { la'2 la' la'4 | sib' }
>>
fa'8 sib' re''4 sib'8 re'' fa''4 re''8 fa'' |
sib''4 <<
  \tag #'hautbois1 {
    \ru#10 sib''8 |
    sib''1. |
    sol''8 sol'' mib'' mib'' sib' sib' sol' sol' mib' sol'' fa'' mib''! |
    fa'' fa'' re'' re'' sib' sib' fa' fa' re' fa'' mib'' re'' |
    mib'' mib'' do'' do'' la' la' fa' fa' la' mib'' re'' do'' |
    re'' re'' sib' sib' fa' fa' re' re' sib' re'' mi'' fa'' |
    mi''2\trill fa''4 r r2 |
    mi''8 mi'' do'' do'' sol' sol' mi' mi' do' mi'' fad'' sol'' |
    fad''2\trill sol''4 r r2 |
    dod''4 dod'' dod'' dod'' dod'' dod'' |
    dod''4 dod'' dod'' dod'' dod'' dod'' |
  }
  \tag #'hautbois2 {
    fa''2 fa'' fa''4~ |
    fa'' fa''2 fa'' fa''4 |
    sol'' << \origVersion { sib'4~ sib' } \modVersion sib'2 >> sib'2 sib'4~ |
    sib'4 sib'2 sib' sib'4 |
    la' la'2 la' la'4 |
    sib' re''2 sib' sol'4 |
    sol'2\trill fa'4 r r2 |
    r4 sib'2 sib' sib'4 |
    la'2\trill sol'4 r r2 |
    r4 sol'2 sol' sol'4~ |
    sol' sol'2 sol' sol'4 |
  }
>>
la''8 fa'' sol'' mi'' fa'' re'' mi'' dod'' re'' si' dod'' la' |
la'' fa'' sol'' mi'' fa'' re'' mi'' dod'' re'' si' dod'' la' |
la'' fa'' sol'' mi'' fa'' re'' mi'' dod'' re'' si' dod'' la' |
re'2 re'4 r r2 |
r4 sib''2 sib'' sib''4~ |
sib''4 sib''2 sib'' sib''4~ |
sib''4 sib''8 sib'' re'''4 sib''8 sib'' re'''4 sib''8 sib'' |
<<
  \tag #'hautbois1 {
    la''4. dod'''8 dod'''4.( re'''8) r2 |
    r4 la''8 la'' re'''4 la''8 la'' re'''4 la''8 la'' |
    si''2 do'''4 r r2 |
    r4 sol''8 sol'' do'''4 sol''8 sol'' do'''4 sol''8 sol'' |
    la''2 sib''4 r r2 |
  }
  \tag #'hautbois2 {
    la''2 la''4 r r2 |
    r4 la''8 la'' re'''4 la''8 la'' re'''4 la''8 la'' |
    sol''2 sol''4 r r2 |
    r4 sol''8 sol'' do'''4 sol''8 sol'' do'''4 sol''8 sol'' |
    fa''2 fa''4 r r2 |
  }
>>
r4 lab''2 lab'' lab''4~ |
lab'' lab''2 lab'' lab''4~ |
lab'' lab''2 lab'' sol''4~ |
sol'' fad'' r2 r |
<<
  \tag #'hautbois1 {
    la''8 la'' fad'' fad'' re'' re'' la' la' re' la'' sol'' la'' |
    sib'' sib'' sol'' sol'' re'' re'' sib' sib' sol' sib'' la'' sol'' |
    fad'' fad'' re'' re'' la' la' fad' fad' re' la'' sol'' la'' |
    sib'' sib'' sol'' sol'' re'' re'' sib' sib' sol' sib'' la'' sol'' |
    fad''2 re'''4 r r2 |
  }
  \tag #'hautbois2 {
    r2 re''4 re'' fad'' fad'' |
    sol''2 sib'4 re'' do'' sib' |
    la'2 fad' la' |
    re'4 re''2 re'' re''4~ |
    re'' r r2 r |
  }
>>
\ru#12 mib''8 |
mib''2 sol''4 r r2 |
\ru#12 re''8 |
re''2 sol''4 r r2 |
\ru#12 do''8 |
do''2 la''4 r r2 |
la''8 la'' fad'' fad'' re'' re'' la' la' re' do'' sib' la' |
sib'4 la'8 sol' re'2 fad'\trill |
sol'4 re'8 sol' si'!4 sol'8 si' re''4 sol''8 re'' |
sol''4 \ru#10 sol''8 |
sol''2 sol''4 r r2 |
r4 sol'8 do'' mi''4 do''8 mi'' sol''4 mi''8 sol'' |
do'''4 \ru#10 do'''8 |
do'''2 do'''4 r r2 |
la''4 do'' la'' do'' la''8 la'' sol'' fa'' |
sol''4 do'' sol'' do'' sol''8 sol'' fa'' mi'' |
fa''4 sol' fa'' sol' fa''8 fa'' mi'' re'' |
mi''4 do''2 do'' do''4 |
la'2\trill sol'4 r r2 |
<<
  \tag #'hautbois1 {
    fad''8 fad'' re'' re'' la' la' fad' fad' re' fad'' sold'' la'' |
    sold''2\trill la''4 r r2 |
  }
  \tag #'hautbois2 {
    r4 re''2 re'' re''4 |
    si'2\trill la'4 r r2 |
  }
>>
fa''8 sol'' mi'' fa'' re'' mi'' do'' re'' si' do'' la' re'' |
fa'' sol'' mi'' fa'' re'' mi'' do'' re'' si' do'' la' re'' |
fa'' sol'' mi'' fa'' re'' mi'' do'' re'' si' do'' la' si' |
sold' la' fad' sold' mi'8 r r4 r2 |
r4 do''8 do'' la''4 do''8 do'' la''2~ |
la''4 do''8 do'' la''4 do''8 do'' la''2~ |
la''4 do''8 do'' la''4 do''8 do'' si'4 la''8 sold'' |
la''2 la'4 r r2 |
r4 do'''2 do''' do'''4~ |
do'''4 do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4~ |
do''' sol''2 sol'' sol''4~ |
sol''4 do''2 do'' do''4~ |
do''4 do''2 do'' do''4~ |
do'' r4 r2 r4 do''' |
si''8 do''' re''' do''' si'' do''' re''' do''' si'' la'' sol'' fa'' |
mi'' fa'' sol'' fa'' mi'' fa'' sol'' fa'' mi'' re'' do'' mi'' |
re'' sol'' fa'' mi'' re'' do'' si' la' sol'4 r |
R1.*2 |
r4 r8 fad' la'4 fad'8 la' re''2~ |
re''4 re'8 sol' si'4 sol'8 si' re''2~ |
re''4 r4 r2 r |
r4 sol'8 si' re''4 si'8 re'' sol''4 re''8 sol'' |
\ru#12 si''8 |
re'''8 re''' re''' re''' re''' do''' si'' la'' sol'' fa'' mi'' re'' |
mi''4 re''8 do'' si'2\trill~ si'4. do''8 |
fad'2 r r |
\ru#12 fad''8 |
sol''2 fa''!4 r r2 |
re'''8 re''' re''' re''' re''' do''' si'' la'' sol'' fa'' mi'' re'' |
mi''4 re''8 do'' si'2\trill~ si'4. do''8 |
do''4 do'''2 do''' do'''4~ |
do'''4 do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4 |
do'''2
