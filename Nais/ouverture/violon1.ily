\clef "dessus2"

\modVersion r8 \origVersion s8
\setMusic #'debut {
  sol16 sol la la si si do'8 si16 si do' do' re' re' |
  mi'8 re'16 re' mi' mi' fa' fa' sol'8 sol' do''4~ |
  do''8 re''16 do'' si' la' si' do'' la'8 la' re''4~ |
  re''8 mi''16 re'' do'' si' do'' re'' si'8 si' mi''4~ |
  mi''8 la' << \origVersion { re''4~ re''8 } \modVersion re''4. >> sol'8 do''4 |
  si'8 si'16 si' si' si' do'' re'' mi''8 do''16 do'' do'' do'' re'' mi'' |
  \clef "dessus"  la'8 fad''16 fad'' fad'' fad'' sol'' la''
  re''8 si''16 si'' si'' si'' si'' si'' |
  si'' si'' si'' si'' si'' si'' si'' si''
  si'' la'' sol'' fad'' mi'' re'' do'' si' |
  do'' re'' mi'' re'' do'' si' la' sol'
  re'' do'' si' do'' re''8 re' |
}

\modVersion { \debut sol'8 \debut sol'4 }
\origVersion { \debut sol'8 }

%%%
r4 do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4~ |
do''' \clef "dessus2" <>^"a 2 cordes" \ru#10 <sol mi'>8 |
\ru#12 <sol mi'>8 |
q4 r r2 \clef "dessus" r4 do''' |
si''8 do''' re''' do''' si'' do''' re''' do''' si'' la'' sol'' fa'' |
mi'' fa'' sol'' fa'' mi'' fa'' sol'' fa'' mi'' re'' do'' mi'' |
re''2 re'4 re' sol' sol' |
si'4 re'8 sol' si'4 sol'8 si' re''4 si'8 re'' |
sol''4 sol'' sol'' sol'' sol'' sol'' |
sol'' sol'8 do'' mi''4 do''8 mi'' sol''4 mi''8 sol'' |
do'''4 do''' do''' do''' do''' do''' |
do'''1 r2 |
la''8 la'' fa'' fa'' do'' do'' la' la' fa' la'' sol'' fa'' |
sol'' sol'' mi'' mi'' do'' do'' sol' sol' mi' sol'' fa'' mi'' |
fa'' fa'' re'' re'' si' si' sol' sol' si fa'' mi'' re'' |
mi'' mi'' do'' do'' sol' sol' mi' mi' do' mi'' fad'' sol'' |
fad''2\trill sol''4 r r2 |
fad''8 fad'' re'' re'' la' la' fad' fad' re' fad'' sold'' la'' |
sold''2\trill la''4 r r2 |
fa''8 sol'' la'' sol'' fa'' sol'' la'' sol'' fa'' sol'' la'' sol'' |
fa'' sol'' la'' sol'' fa'' sol'' la'' sol'' fa'' mi'' re'' dod'' |
re'' mi'' fa'' mi'' re'' mi'' fa'' mi'' re'' mi'' fa'' mi'' |
re'' mi'' fa'' mi'' re'' mi'' fa'' mi'' re'' do'' si' la' |
mi''2 si4 si mi' mi' |
sold'4 si8 mi' sold'4 mi'8 sold' si'4 sold'8 si' |
mi''4 mi'' mi'' mi'' mi'' mi'' |
mi'' mi'8 la' do''4 la'8 do'' mi''4 do''8 mi'' |
la''4 \ru#10 la''8 |
\ru#12 la'' |
la''1. |
R1. |\allowPageTurn
fad''8 sol'' la'' sol'' fad'' sol'' la'' sol'' fad'' sol'' la'' sol'' |
fad'' sol'' la'' sol'' fad'' sol'' fad'' mi'' re'' do'' sib' la' |
sib'4 sib'8 sib' sol''4 sib'8 sib' sol''2~ |
sol''4 sib'8 sib' sol''4 sib'8 sib' sol''2~ |
sol''4 sib'8 sib' sol''4 sib'8 sib' mib''2~ |
mib''4 lab'8 lab' fa''4 lab'8 lab' fa''2~ |
fa''4 lab'8 lab' fa''4 lab'8 lab' fa''2~ |
fa''4 lab'8 lab' fa''4 fa'8 fa' re''2~ |
re''4 sol'8 sol' mib''4 do''8 do'' fa''4. do''8 |
reb''4. sol'8 sol'2\trill~ sol'4. fa'8 |
fa'4 r8 fa' la'4 fa'8 la' do''4 la'8 do'' |
%% 10
fa''4 \ru#10 fa''8 |
fa''4 fa'8 sib' re''4 sib'8 re'' fa''4 re''8 fa'' |
sib''4 \ru#10 sib''8 |
sib''1. |
sol''8 sol'' mib'' mib'' sib' sib' sol' sol' mib' sol'' fa'' mib''! |
fa'' fa'' re'' re'' sib' sib' fa' fa' re' fa'' mib'' re'' |
mib'' mib'' do'' do'' la' la' fa' fa' la' mib'' re'' do'' |
re'' re'' sib' sib' fa' fa' re' re' sib' re'' mi'' fa'' |
mi''2\trill fa''4 r r2 |
mi''8 mi'' do'' do'' sol' sol' mi' mi' do' mi'' fad'' sol'' |
fad''2\trill sol''4 r r2 |
\ru#12 <mi' dod''>8 |
\ru#12 <mi' dod''>8 |
la''8 fa'' sol'' mi'' fa'' re'' mi'' dod'' re'' si' dod'' la' |
la'' fa'' sol'' mi'' fa'' re'' mi'' dod'' re'' si' dod'' la' |
la'' fa'' sol'' mi'' fa'' re'' mi'' dod'' re'' si' dod'' la' |
sib2 sib4 r r2 |
r4 sib''2 sib'' sib''4~ |
sib''4 sib''2 sib'' sib''4~ |
sib''4 sib''8 sib'' re'''4 sib''8 sib'' re'''4 sib''8 sib'' |
la''4. dod'''8 dod'''4.( re'''8) r2 |
r4 la''8 la'' re'''4 la''8 la'' re'''4 la''8 la'' |
si''2 do'''4 r r2 |
r4 sol''8 sol'' do'''4 sol''8 sol'' do'''4 sol''8 sol'' |
la''2 sib''4 r r2 |
r4 lab''2 lab'' lab''4~ |
lab'' lab''2 lab'' lab''4~ |
lab'' lab''2 lab'' sol''4~ |
sol'' fad'' r2 r |
la''8 la'' fad'' fad'' re'' re'' la' la' re' la'' sol'' la'' |
sib'' sib'' sol'' sol'' re'' re'' sib' sib' sol' sib'' la'' sol'' |
fad'' fad'' re'' re'' la' la' fad' fad' re' la'' sol'' la'' |
sib'' sib'' sol'' sol'' re'' re'' sib' sib' sol' sib'' la'' sol'' |
fad''2 re'''4 r r2 |
\ru#12 mib''8 |
mib''2 sol''4 r r2 |
\ru#12 re''8 |
re''2 sol''4 r r2 |
\ru#12 do''8 |
do''2 la''4 r r2 |
la''8 la'' fad'' fad'' re'' re'' la' la' re' do'' sib' la' |
sib'4 la'8 sol' re'2 fad'\trill |
sol'4 re'8 sol' si'!4 sol'8 si' re''4 sol''8 re'' |
sol''4 \ru#10 sol''8 |
sol''2 sol''4 r r2 |
r4 sol'8 do'' mi''4 do''8 mi'' sol''4 mi''8 sol'' |
do'''4 \ru#10 do'''8 |
do'''2 do'''4 r r2 |
la''8 la'' fa'' fa'' do'' do'' la' la' fa' la'' sol'' fa'' |
sol'' sol'' mi'' mi'' do'' do'' sol' sol' mi' sol'' fa'' mi'' |
fa'' fa'' re'' re'' si' si' sol' sol' re' fa'' mi'' re'' |
mi'' mi'' do'' do'' sol' sol' mi' mi' do' mi'' fad'' sol'' |
fad''2\trill sol''4 r r2 |
fad''8 fad'' re'' re'' la' la' fad' fad' re' fad'' sold'' la'' |
sold''2\trill la''4 r r2 |
fa''8 sol'' mi'' fa'' re'' mi'' do'' re'' si' do'' la' re'' |
fa'' sol'' mi'' fa'' re'' mi'' do'' re'' si' do'' la' re'' |
fa'' sol'' mi'' fa'' re'' mi'' do'' re'' si' do'' la' si' |
sold' la' fad' sold' mi'8 fa'? re' mi' do' re' si do' |
la4 do''8 do'' la''4 do''8 do'' la''2~ |
la''4 do''8 do'' la''4 do''8 do'' la''2~ |
la''4 do''8 do'' la''4 do''8 do'' si'4 la''8 sold'' |
la''2 la'4 r r2 |
r4 do'''2 do''' do'''4~ |
%% 22
do'''4 do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4~ |
do''' sol''2 sol'' sol''4~ |
sol''4 do''2 do'' do''4~ |
do''4 do''2 do'' do''4~ |
do'' r4 r2 r4 do''' |
%% 23
si''8 do''' re''' do''' si'' do''' re''' do''' si'' la'' sol'' fa'' |
mi'' fa'' sol'' fa'' mi'' fa'' sol'' fa'' mi'' re'' do'' mi'' |
re'' sol'' fa'' mi'' re'' do'' si' la' sol'4 r |
r4 r8 mi' sol'4 mi'8 sol' do''2~ |
do''4 r8 fa' la'4 fa'8 la' do''2~ |
do''4 re'8 fad' la'4 fad'8 la' re''2~ |
re''4 re'8 sol' si'4 sol'8 si' re''2~ |
re''4 sol8 si re'4 si8 re' sol'4 re'8 sol' |
si'4 sol'8 si' re''4 si'8 re'' sol''4 re''8 sol'' |
\ru#12 si''8 |
re'''8 re''' re''' re''' re''' do''' si'' la'' sol'' fa'' mi'' re'' |
mi''4 re''8 do'' si'2\trill~ si'4. do''8 |
fad'2 r r |
\ru#12 fad''8 |
sol''2 fa''!4 r r2 |
re'''8 re''' re''' re''' re''' do''' si'' la'' sol'' fa'' mi'' re'' |
mi''4 re''8 do'' si'2\trill~ si'4. do''8 |
do''4 do'''2 do''' do'''4~ |
do'''4 do'''2 do''' do'''4~ |
do''' do'''2 do''' do'''4 |
do'''2
