\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes dessus1)
   (violon2 #:notes "dessus" #:tag-notes dessus2)
   (hautbois #:notes "dessus" #:tag-notes dessus)
   (flutes #:notes "dessus" #:tag-notes dessus)
   (bassons #:notes "basse")
   (basses #:notes "basse"))
