\key re \major
\tempo "Vivement" \midiTempo#160
\digitTime\time 2/2 s1*31 \bar "||"
\segnoMark s1*39 s2 \fineMark s2
s1*2 s2. \tempo "Lent" s4 s1*17 \bar "|." \dalSegnoMark
