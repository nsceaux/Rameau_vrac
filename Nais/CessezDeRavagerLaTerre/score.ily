\score {
  \new ChoirStaff <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout {
        s1*7\pageBreak
        s1*5\break s1*5\break s1*6\break s1*7\pageBreak
        s1*6\break s1*6\break s1*7\break s1*7\pageBreak
        s1*7\break s1*8\break s1*6\break s1*5\pageBreak
        \grace s8 s1*4\break
      }
    >>
  >>
  \layout { indent = \smallindent }
  \midi { }
}
