\clef "dessus" r4 la'' fad'' re'' |
si'1 |
si'4 si'8 si' sol'4. la'8 |
re'4 mi'8 fad' sol' la' si' dod'' |
re''4 mi''8 fad'' sol'' la'' si'' dod''' |
re'''4 r r2 |
R1*2 |
re'4 mi'8 fad' sol' la' si' dod'' |
re''4 mi''8 fad'' sol'' la'' si'' dod''' |
re'''4 r r2 |
re'' fad'4 la' |
re'4. dod'8 re' mi' fad' re' |
sol'4. si'8 dod'' re'' mi'' fad'' |
sol''1~ |
sol'' |
sol''8( la'') mi''( sol'') fad''( sol'') re''( fad'') |
mi'' fad'' dod'' mi'' re'' mi'' si' re'' |
dod'' re'' mi'' re'' dod'' re'' dod'' si' |
la'4 r r2 |
la'2 \twoVoices #'(dessus1 dessus2 dessus) <<
  { sold'8 la' re'' dod'' |
    si'4\trill mi''8 re'' dod'' re'' dod'' si' |
    la'1~ |
    la'2 re''4 dod''8\trill si' |
    mi''2~ mi''8 fad'' re'' mi'' |
    dod'' mi'' re'' dod'' si' la' sol' fad' |
    sold''2 la''~ |
    la'' sold''\trill | }
  { sold'8 la' si' la' |
    sold' mi' fad' sold' la' si' dod'' mi' |
    fad'2 dod' |
    re'8 mi' fad' sol' la' si' la' si' |
    dod''4( si'8\trill) la' si'2 |
    mi'1 |
    re''2 dod'' |
    si' mi'' | }
>>
la4 si8 dod' re' mi' fad' sold' |
la'4 si'8 dod'' re'' mi'' fad'' sol'' |
la''4 r <>\doux \twoVoices #'(dessus1 dessus2 dessus) <<
  { la''2 |
    la''2. si''8 la'' |
    sol''2 sol''\trill | }
  { fad''2 |
    fad''2. sol''8 fad'' |
    mi''4 re''2 dod''4\trill | }
>>
<>^"Tous" re'4 mi'8\ademi fad' sol' la' si' dod'' |
re''4 mi''8 fad'' sol'' la'' si'' dod''' |
re'''4 r r2 |
re''2 fad'4 la' |
re'4. dod'8 re' mi' fad' re' |
sol'4. si'8 dod'' re'' mi'' fad'' |
sol''1~ |
sol''~ |
sol''8 la'' mi'' sol'' fad'' sol'' re'' fad'' |
mi''2 dod''\trill |
re''1~ |
re''2 dod''\trill |
\appoggiatura dod''8 re''2 r |
R1 |
re'4 mi'8 fad' sol' la' si' dod'' |
re''4 mi''8 fad'' sol'' la'' si'' dod''' |
re'''4 r r2 |
R1 |
re'4 mi'8 fad' sol' la' si' dod'' |
re''4 mi''8 fad'' sol'' la'' si'' dod''' |
re'''4 r si''\doux r |
mi''2~ mi''8 re'' dod'' si' |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { la'2 re''~ |
    re''1~ |
    re''2. mi''8 re'' |
    re''2.\trill re''8 dod'' |
    \appoggiatura dod''8 re''1 | }
  { la'2 la'4.\trill( sol'16 la') |
    si'8( dod'') re''( dod'') si'( la') sol'( fad') |
    mi'1~ |
    mi'2 mi'4. la'8 |
    fad'1 | }
>>
R1 |
re'4 mi'8 fad' sol' la' si' dod'' |
re''4 mi''8 fad'' sol'' la'' si'' dod''' |
re'''4 r si'' r |
mi''2 mi''8( re'') dod''( si') |
la'2 \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''2~ |
    re''1~ |
    re''2.( mi''8) re'' |
    re''1\trill~ |
    re''2. re''8 dod'' |
    \appoggiatura dod''8 re''2 r8 dod''\fort si' la' |
    si' dod'' re'' dod'' si' la' sol' fad' |
    mi'4 fad' mi'4.\trill re'8 |
    re'2. }
  { la'4.\trill( sol'16 la') |
    si'8( dod'') re''( dod'') si'( la') sol'( fad') |
    mi'2 fad' |
    mi'1~ |
    mi'2 mi'4. la'8 |
    fad'2.\trill r4 |
    re'1 |
    dod'4 re' dod'4.\trill re'8 |
    re'2. }
>> r4 |
<>^"Tous" re''4.(\doux mi''8) re''4.( mi''8) |
re''4.( mi''8) re''4.( mi''8) |
re''2 \appoggiatura dod''8 si'4 r8 re'' |
re''4.( dod''8)\trill si'4 mi''8 re'' |
\appoggiatura re''8 dod''2.\trill r8 si' |
\appoggiatura la'8 sold'2 sold'4. sold'8 |
mi'2. r8 lad' |
lad'4.( si'8) fad' r fad' sold' |
\appoggiatura fad' mid'2.\trill r8 \twoVoices #'(dessus1 dessus2 dessus) <<
  { sold'8 |
    la'2 sold' |
    dod''4 fad''2 mid''4 |
    \appoggiatura mid''8 fad''1~ |
    fad''~ |
    fad''2 mid''\trill |
    fad''1 | }
  { mid'8 |
    fad'2 dod' |
    dod'4 dod'' si'4. dod''16 si' |
    la'2 dod'' |
    sold' dod'' |
    sold' sold'\trill |
    fad'1 | }
>>
re'4 mi'8 fad' sol' la' si' dod'' |
re''4 r \twoVoices #'(dessus1 dessus2 dessus) << la''2 fad'' >> |
