\clef "vhaute-contre" <>^\markup\character Neptune
R1*5 |
r4 la' fad' re' |
si1 |
si4 si8 si sol4. la8 |
re1 |
re4 r r2 |
r2 r4 fad8 sol |
la2 la4 fad |
si4. la8 si dod' re' si |
mi'1~ |
mi'4\melisma fad'8[ mi'] re'[ dod' re' mi'] |
dod'1\trill\melismaEnd |
\appoggiatura si8 la1 |
r2 r4 r8 mi' |
la'2 mi' |
fad'8[\melisma sol' la' sol'] fad'[ mi' re' dod']( |
re'8)[ mi']\melismaEnd fad'[ mi'] re'[ dod'] si[ la] |
mi'2. r8 dod' |
re'2 mi'\trill |
fad'8[\melisma mi' re' mi'] fad'[ sol' fad' sold'] |
la'4( sold'8)\trill[ fad'] mi'[ fad' re' mi'] |
dod'[ mi' re' dod'] si[ la sol fad] |
mi1\melismaEnd |
mi4 r mi4. la8 |
la1 |
R1 |
r4 la' fad' re' |
si1 |
si4 si8 si sol4. la8 |
re1 |
re4 r r2 |
r r4 fad8 sol |
la2 la4 fad |
si4. la8 si dod' re' si |
mi'1~ |
mi'4\melisma fad'8[ mi'] re'[ dod' re' mi'] |
dod'1\trill\melismaEnd |
la2 r4 r8 si |
dod'2 mi' |
la2. si8[ la] |
la2( sol8)[\trill fad] sol4 |
fad2. r8 la |
re'2 re' |
fad'1~ |
fad' |
fad'4 r r4 r8 la |
re'2 fad' |
la'1~ |
la'~ |
la'4\melisma sol'8[ fad'] mi'[ fad' re' mi'] |
dod'[ re' mi' re'] dod'[ si la sol] |
fad2. sol8[ la] |
sol2~ sol8[ la]\melismaEnd fad[ sol] |
la2.( si8[ la] |
la2\trill)~ la4. re'8 |
re'2 r4 r8 la |
re'2 fad' |
la'1~ |
la'~ |
la'4\melisma sol'8[ fad'] mi'[ fad' re' mi'] |
dod'[ re' mi' re'] dod'[ si la sol] |
fad4 sol8[ la] si[ dod' re' mi'] |
sol4 la8[ si] dod'[ re' mi' fad'] |
la2\melismaEnd re' |
la2.( si8[ la] |
la2)~ la4. la8 |
re1 |
R1*2 |
r2 r4 r8 fad' |
fad'2~ fad'4. sol'8 |
fad'4. sol'8 fad'4. si'8 |
fad'2 \appoggiatura mi'8 re'4 r8 fad' |
fad'4.( mi'8)\trill re'4 dod'8 si |
mi'2. r8 re' |
dod'4. re'8 si4. dod'8 |
lad2\trill lad4 r8 dod' |
dod'4.( re'8) re' r si si |
\appoggiatura la8 sold2.\trill r8 dod' |
red'4.\melisma mi'32[ red' mid' fad'] mid'4.\trill red'16[ dod'] |
fad'2~ fad'16[ sold' mid' fad'] sold'[ la' fad' sold'] |
la'2~ la'16[ sold' fad' mid'] fad'[ mi' re' dod']( |
re'2)\melismaEnd la |
si dod' |
fad1 |
R1 |
r4 la' fad' re' |
