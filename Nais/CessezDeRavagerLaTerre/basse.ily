\clef "basse" R1 |
r4 re' si sol |
sol,2 sol,4. la,8 |
re,4 mi,8 fad, sol, la, si, dod |
re4 mi8 fad sol la si dod' |
re'4 r r fad |
sol si sol re |
sol,2 sol,4. la,8 |
re,4 mi,8 fad, sol, la, si, dod |
re4 mi8 fad sol la si dod' |
re'4 r r2 |
fad2. re4 |
sol2. fad4 |
mi1~ |
mi2. mi4 |
la1~ |
la~ |
la~ |
la2 la, |
re2~ re4. mi8 |
fad mi re dod si, la, sold, la, |
mi,2 r4 r8 sol |
fad2 mi\trill |
re1 |
dod2 sold, |
la,~ la,8 si, dod re |
mi4 re8 dod si, la, sold, fad, |
mi,1 |
la,4 si,8 dod re mi fad sold |
la2~ la8 si la sol |
fad2 r8 re mi fad |
sol2 r |
sol,~ sol,4. la,8 |
re,4 mi,8\ademi fad, sol, la, si, dod |
re4 mi8 fad sol la si dod' |
re'4 r r2 |
fad2. re4 |
sol2. fad4 |
mi1~ |
mi2. mi4 |
la1~ |
la2 re |
la, sol, |
fad,1 |
mi,2 la, |
re, r |
R1 |
re,4 mi,8 fad, sol, la, si, dod |
re4 mi8 fad sol la si dod' |
re'4 r r2 |
R1 |
re,4 mi,8 fad, sol, la, si, dod |
re4 mi8 fad sol la si dod' |
re'4 fad\doux sol2 |
la dod |
re1 |
sol,2 sol,8( la,) fad,( sol,) |
la,1~ |
la, |
re |
R1 |
re,4 mi,8 fad, sol, la, si, dod |
re4 mi8 fad sol la si dod' |
re'4 fad sol2 |
la dod |
re1 |
sol,2 sol,8( la,) fad,( sol,) |
la,1~ |
la,~ |
la, |
re,2. re4\fort |
sol1 |
la4 re la la, |
re2. r4 |
si1~ |
si~ |
si2 si,4 r |
sol1~ |
sol2. r8 fad |
mid1 |
fad |
si,2. sold,4 |
dod2. r8 dod' |
dod'2 si |
la sold |
fad la, |
si, la, |
si, dod |
fad,1 |
re,4 mi,8 fad, sol, la, si, dod |
re4 r r8 re mi fad |
