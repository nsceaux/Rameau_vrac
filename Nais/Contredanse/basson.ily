\clef "petrucci-c4/tenor" fad'2~ |
fad'4 mi'2 sol'4 |
dod' mi' la re'~ |
re'2 dod'\trill |
\appoggiatura dod'8 re'4 la fad'2~ |
fad'4 mi'2 sol'4 |
dod' mi' la re'~ |
re'2 dod'\trill |
\appoggiatura dod'8 re'2
fad'4 fad'8.\trill( mi'32 fad') |
sol'4 fad' fad'4.\trill mi'8 |
re' mi' fad' mi' re'2 |
re' dod'\trill |
\appoggiatura dod'8 re'2 fad'4. mi'16 re' |
dod'4 r fad'4.\doux mi'16 re' |
dod'4 r fad'4.\fort mi'16 re' |
re'2. mi'8 fad' |
dod'2\trill
\ru#4 { \appoggiatura { re'16[ mi'] } fad'2 } mi'8 re' re' dod' |
re'4 re' re'2\trill |
dod' <>\doux \ru#4 { \appoggiatura { re'16[ mi'] } fad'2 } mi'8 re' re' dod' |
re'4 fad' si mi' |
mi'2
