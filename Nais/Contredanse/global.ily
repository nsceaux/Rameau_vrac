\key re \major \midiTempo#200
\digitTime\time 2/2 \partial 2 s2 s1*7 s4. \fineMark s8 \bar "|."
\beginMark\markup { \concat { P \super re } Reprise }
s2 s1*7 s2 \bar "|." \segnoMarkEnd
\grace s8 \beginMark\markup { \concat { 2 \super e } Reprise }
s2 s1*7 s2 \bar "|." \segnoMarkEnd
