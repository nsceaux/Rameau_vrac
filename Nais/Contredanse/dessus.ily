\clef "dessus" la'4 re''~ |
re'' si'2 mi''4~ |
mi'' dod''\trill re''8 mi'' fad'' sol'' |
mi'' re'' dod'' re'' mi'' fad'' sol'' mi'' |
fad''4\trill \appoggiatura mi''8 re'' r la'4 re''~ |
re'' si'2 mi''4~ |
mi'' dod''\trill re''8 mi'' fad'' sol'' |
mi'' re'' dod'' re'' mi'' sol'' fad'' mi'' |
re''2
la''4 la''8.(\trill sol''32 la'') |
si''4 la'' la''4.( sol''8)\trill |
fad'' mi'' re'' mi'' fad'' sol'' la'' fad'' |
si''4 la'' la''4.( sol''8)\trill |
fad''2\trill \cesureInstrDown la''4.( sol''16\trill fad'') |
mi''4 r la''4.\doux sol''16 fad'' |
mi''4 r la''4.\fort sol''16 fad'' |
mi''8 fad'' sol'' fad'' mi'' fad'' dod'' re'' |
mi''2\trill
la''4 re''' |
re''' la'' la'' re''' |
re'''2 dod'''8 si'' la'' sol'' |
fad''4 la'' sol''8 fad'' mi'' re'' |
mi''2\trill \cesureInstr la''4\doux re''' |
re''' la'' la'' re''' |
re'''2 dod'''8 si'' la'' sol'' |
fad''4 la'' sol''8 fad'' mi'' re'' |
la'2
