\piecePartSpecs
#(let ((breaks #{ s2 s1*7 s2\break s2 s1*7 s2 \break \grace s8 #}))
   `((hautbois #:notes "dessus" #:music ,breaks)
     (violon1 #:notes "dessus" #:tag-notes dessus1 #:music ,breaks)
     (violon2 #:notes "dessus" #:tag-notes dessus2 #:music ,breaks)
     (haute-contre #:music ,breaks)
     (taille #:music ,breaks)
     (bassons #:clef "tenor" #:notes "basson" #:music ,breaks)
     (basses #:notes "basse" #:music ,breaks)))
