\clef "haute-contre" la'2 |
la'4 sol'2 si'4 |
mi' la'2 la'4 |
si'2 la' |
la' la'~ |
la'4 sol'2 si'4 |
mi' la'2 la'4 |
si'2 la' |
fad'\trill
re''4 re'' |
re'' re'' re'' la' |
la'2 la'4 fad'' |
mi''2 mi''4 la' |
la'2 la' |
la'4 r la2\doux |
la4 r la'2\fort |
si'4 la' sol' si' |
mi'2
\ru4 { \appoggiatura { fad'16[ sol'] } la'2 } mi''2 |
re''4 la' si'2 |
la'2 <>\doux \ru4 { \appoggiatura { fad'16[ sol'] } la'2 } mi''2 |
re''4 re'' re''2\trill |
dod''\trill
