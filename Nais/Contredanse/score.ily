\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Violons Hautbois }
    } << \global \includeNotes "dessus" >>
    \new Staff \with { instrumentName = "Parties" } <<
      \global \includeNotes "parties"
    >>
    \new Staff \with { instrumentName = "Bassons" } <<
      \global \includeNotes "basson"
    >>
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \origLayout {
        s2 s1*7 s2 \pageBreak
        s2 s1*6\break s1*6\break \grace s8
      }
    >>
  >>
  \layout { }
  \midi { }
}
