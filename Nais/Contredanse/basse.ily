\clef "basse" re2 |
sol mi |
la fad |
sol la |
re re, |
sol, mi, |
la, fad, |
sol, la, |
re,
re' |
sol4 la si dod' |
re'2 fad |
sol la |
re re |
la re,\doux |
la,4 r re2\fort |
sol,4 la, si,2 |
la,
re2 |
re re |
fad, sol,4 la, |
si, fad, sol,2 |
la, re\doux |
re re |
fad, sol,4 la, |
si, fad, sol,2 |
la,
