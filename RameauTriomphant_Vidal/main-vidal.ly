%#(ly:set-option 'part 'basses)
%#(ly:set-option 'urtext #t)
\version "2.19.80"
\include "common-vidal.ily"

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = "Rameau Triomphant" }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #8
  \with-line-width-ratio#0.7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 1)
  \table-of-contents
}

\bookpart {
  \act "Rameau Triomphant"
  \scene "Les Festes de Poliminie" "Les Festes de Poliminie"
  %% 1
  \pieceToc "Ouverture"
  \includeScore "LesFetesDePolimnie/Ouverture"
}
\bookpart {
  \scene "Dardanus" "Dardanus"
  %% 2
  \pieceToc\markup\wordwrap {
    Dardanus : \italic { Hâtons-nous, courons à la Gloire } [IV.3]
  }
  \includeScore "Dardanus/OuSuisJe"
  \includeScore "Dardanus/HatonsNous"
}
\bookpart {
  \scene "Zoroastre" "Zoroastre"
  %% 3
  \pieceToc\markup\wordwrap {
    Zoroastre : \italic { Accourez, jeunesse brillante } [III.5]
  }
  \includeScore "Zoroastre/Accourez"
}
\bookpart {
  \scene "Dardanus" "Dardanus"
  %% 4
  \pieceToc "Tambourins [Pr.]"
  \includeScore "Dardanus/PrologueTambourinI"
  \includeScore "Dardanus/PrologueTambourinII"
}
\bookpart {
  %% 5
  \pieceToc "Calme des sens [IV.3]"
  \includeScore "Dardanus/CalmeDesSens"
  %% 6
  \pieceToc "Gavotte vive [IV.3]"
  \includeScore "Dardanus/GavotteVive"
}
\bookpart {
  %% 7
  \pieceToc\markup\wordwrap {
    Trois songes : \italic { Il est temps de courir aux armes } [IV.3]
  }
  \includeScore "Dardanus/IlEstTempsDeCourirAuxArmes"
}
\bookpart {
  \scene "Les Paladins" "Les Paladins"
  %% 8
  \pieceToc\markup\wordwrap {
    Atis : \italic { Lance, Amour } [III.4]
  }
  \includeScore "LesPaladins/LanceAmour"
}
\bookpart {
  \scene "Les Indes Galantes" "Les Indes Galantes"
  %% 9
  \pieceToc "Air pour les esclaves africains [Le Turc Généreux]"
  \includeScore "LesIndesGalantes/AirEsclavesAfricains"
  %% 10
  \pieceToc\markup\wordwrap {
    Valère : \italic { Hâtez-vous de vous embarquer } [Le Turc Généreux]
  }
  \includeScore "LesIndesGalantes/HatezVous"
}
\bookpart {
  \scene "Platée" "Platée"
  %% 11
  \pieceToc\markup\wordwrap { Orage [I.6] }
  \includeScore "Platee/Orage"
}
\bookpart {
  \scene "Zoroastre" "Zoroastre"
  %% 12
  \pieceToc "Air grave pour les esprits infernaux"
  \includeScore "Zoroastre/AirGraveIV"
}
\bookpart {
  \scene "Naïs" "Naïs"
  %% 13
  \pieceToc\markup\wordwrap {
    Neptune : \italic { Cessez de ravager la Terre } [III]
  }
  \includeScore "Nais/CessezDeRavagerLaTerre"
}
\bookpart {
  %% 14
  \pieceToc "Contredanse"
  \includeScore "Nais/Contredanse"
}
\bookpart {
  \scene "Le Temple de la Gloire" "Le Temple de la Gloire"
  %% 15
  \pieceToc\markup\wordwrap {
    Bachus : \italic { Que le Tirse règne toujours } [II.2]
  }
  \includeScore "LeTempleDeLaGloire/QueLeThyrseRegneToujours"
}
\bookpart {
  \scene "Les Festes de Polymnie" "Les Festes de Polymnie"
  %% 16
  \pieceToc "Air vif [III.7]"
  \includeScore "LesFetesDePolimnie/AirVif"
}
\bookpart {
  \scene "Pigmalion" "Pigmalion"
  %% 17
  \pieceToc\markup\wordwrap {
    Pigmalion, chœur : \italic { L’Amour triomphe } [sc.5]
  }
  \includeScore "Pigmalion/LAmourTriomphe"
}
\bookpart {
  \scene "Dardanus" "Dardanus"
  %% 18
  \pieceToc "Chaconne [V.5]"
  \includeScore "Dardanus/Chaconne"
}
\bookpart {
  \scene "Pigmalion" "Pigmalion"
  %% 19
  \pieceToc\markup\wordwrap {
    Pigmalion : \italic { Règne, Amour } [sc.5]
  }
  \includeScore "Pigmalion/RegneAmour"
}
\bookpart {
  \scene "Castor et Pollux" "Castor et Pollux"
  %% 20
  \pieceToc\markup\wordwrap { Air pour les athlètes }
  \includeScore "CastorEtPollux/airAthletes"
  %% 21
  \pieceToc\markup\wordwrap {
    Un athlète : \italic { Éclatez fières trompettes } [II.5]
  }
  \includeScore "CastorEtPollux/EclatezFieresTrompettes"
}
\bookpart {
  \paper { min-systems-per-page = 2 }
  \scene "Les Boréades" "Les Boréades"
  %% 22
  \pieceToc\markup\wordwrap {
    Calisis, choeur : \italic { Jouissons de nos beaux ans } [III.3]
  }
  \includeScore "Boreades/Jouissons"
}
\bookpart {
  %% 23
  \pieceToc "Entrée de Polymnie [IV]"
  \includeScore "Boreades/EntreePolymnie"
}
\bookpart {
  \scene "Platée" "Platée"
  %% 24
  \pieceToc\markup\wordwrap {
    Thespis : \italic { Momus, Amour, Dieu des raisins } [Pr.3]
  }
  \includeScore "Platee/MomusAmourDieuDesRaisins"
}
\bookpart {
  %% 25
  \pieceToc\markup\wordwrap {
    Thespis, Chœur : \italic { Formons un spectacle nouveau } [Pr.3]
  }
  \includeScore "Platee/FormonsUnSpectacleNouveau"
}
\bookpart {
  %% 26
  \pieceToc "Contredanse"
  \includeScore "Platee/Contredanse"
}
\bookpart {
  %% 27
  \pieceToc\markup\wordwrap {
    Thespis, Chœur : \italic { Chantons Bacchus } [Pr.3]
  }
  \includeScore "Platee/ChantonsBacchus"
}
\bookpart {
  \scene "Les Festes de Polymnie" "Les Festes de Polymnie"
  %% 28
  \pieceToc\markup\wordwrap {
    Antiochus : \italic { Dans l’objet qu’on aime } [II.5]
  }
  \includeScore "LesFetesDePolimnie/DansLObjetQuonAime"
}
\bookpart {
  %% 29
  \pieceToc "Air gracieux en rondeau [II.5]"
  \includeScore "LesFetesDePolimnie/AirGracieux"
}
\bookpart {
  %% 30
  \pieceToc\markup\wordwrap {
    Antiochus, chœur : \italic { Peuple heureux } [II.6]
  }
  \includeScore "LesFetesDePolimnie/PeuplesHeureux"
}
