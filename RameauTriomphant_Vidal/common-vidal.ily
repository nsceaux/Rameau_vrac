\include "common.ily"
\paper { bookTitleMarkup = \nenuvarBookTitleMarkup }
\header {
  maintainer = \markup {
    Éditions Nicolas Sceaux - Ensemble Marguerite Louise
  }
}
\opusTitle "Rameau Triomphant"

%% Rehearsal number: one number instead of two.
#(let ((minor-number 0))
  (set! increase-rehearsal-major-number
        (lambda () #t))
  (set! rehearsal-number
        (lambda ()
          (set! minor-number (1+ minor-number))
          (format #f "~a" minor-number))))

