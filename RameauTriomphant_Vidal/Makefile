OUTPUT_DIR=out
#DELIVERY_DIR=delivery
DELIVERY_DIR=/Users/nicolas/Dropbox/GJarry/RameauTriomphant
NENUVAR_LIB_PATH=$$(pwd)/../../nenuvar-lib
LILYPOND_CMD=lilypond -I$$(pwd)/../ -I$(NENUVAR_LIB_PATH) \
  --loglevel=WARN -ddelete-intermediate-files \
  -dno-protected-scheme-parsing
PROJECT=Rameau_Triomphant_Vidal

# génération des partitions
# Conducteur
conducteur:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)  \
	main-vidal.ly
.PHONY: conducteur
# Instruments
violon1:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-violon1 -dpart=violon1  \
	part-vidal.ly
.PHONY: violon1
violon2:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-violon2 -dpart=violon2  \
	part-vidal.ly
.PHONY: violon2
flutes:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-flutes -dpart=flutes  \
	part-vidal.ly
.PHONY: flutes
hautbois:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-hautbois -dpart=hautbois  \
	part-vidal.ly
.PHONY: hautbois
bassons:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-bassons -dpart=bassons  \
	part-vidal.ly
.PHONY: bassons
trompette:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-trompette -dpart=trompette \
	part-vidal.ly
.PHONY: trompette
percussions:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-percussions -dpart=percussions \
	part-vidal.ly
.PHONY: percussions
haute-contre:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-haute-contre -dpart=haute-contre  \
	part-vidal.ly
.PHONY: haute-contre
taille:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-taille -dpart=taille  \
	part-vidal.ly
.PHONY: taille
basses:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-basses -dpart=basses  \
	part-vidal.ly
.PHONY: basses

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e $(OUTPUT_DIR)/$(PROJECT).pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT).pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-violon1.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-violon1.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-violon2.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-violon2.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-flutes.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-flutes.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-hautbois.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-hautbois.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-bassons.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-bassons.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-trompette.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-trompette.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-percussions.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-percussions.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-haute-contre.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-haute-contre.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-taille.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-taille.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-basses.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-basses.pdf $(DELIVERY_DIR)/; fi
.PHONY: delivery

clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)-* $(OUTPUT_DIR)/$(PROJECT).*
.PHONY: clean

parts: flutes hautbois bassons \
	violon1 violon2 haute-contre taille basses \
	trompette percussions
.PHONY: parts

all: check parts conducteur delivery clean
.PHONY: all

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in parent directory:"; \
	  echo " cd .. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: check
