\clef "petrucci-c4/tenor" r4 |
R2.*3 |
r4 r r8 mib'\fortSug |
mib' re' re'4( do'8)\trill si |
\appoggiatura si8 do'2 sol'4 |
fa'8 mib' re'4 fa'~ |
fa'2~ fa'8 sol |
sol4 r r |
r r <>\ademiSug \twoVoices #'(basson1 basson2 bassons) <<
  { re'8.( mib'16) |
    re'8.( sol'16) sol'4.( fa'16)\trill mib' |
    re'2.\trill | }
  { si8.( do'16) |
    si8.( mib'16) mib'4.( re'16\trill) do' |
    si2.\trill | }
>>
R2. |
<>^"Tous" do'4\fort~ do'8.( re'16) sib8.( do'16) |
re'4 re'4.(\trill do'16 re') |
mib'2 sol'4~ |
sol'2 fa'8 mib' |
re'2~ re'8.( sol16) |
sol4 sol'2~ |
sol'4~ sol'8.( lab'16) fa'8.( sol'16) |
lab'8.( sol'16) fa'8.( mib'16) re'4~ |
re' \twoVoices #'(basson1 basson2 bassons) <<
  { fa'8.( mi'16) fa'4~ |
    fa'8.( mi'16) fa'8.( mi'16) fa'4~ |
    fa'4 }
  { re'8.( do'16) re'4~ |
    re'8.( do'16) re'8.( do'16) re'4~ |
    re' }
>> <>^"Tous" mib'8.(\doux re'16) do'8.( re'16) |
sol2 r4 |
r sol' lab' |
do'( si4.)\trill do'8 |
do'2
