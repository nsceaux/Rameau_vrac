\clef "taille" r8 sol'8\fortSug |
sol' fa' fa'4( mib'8)\trill re' |
sol2 r8 do'' |
\appoggiatura sib' lab'2 sol'4 |
sol'2 r8 sol' |
lab'4~ lab'4.( sol'16 lab') |
sol'2\trill do'4 |
re'8 mib' fa'4 lab' |
sol'2 r4 |
r8 do' do'4.( si16\trill) do' |
sol2 r4 |
R2.*4 |
r4 sol'2~ |
sol' mib''4~ |
mib''2 re''8 do'' |
do''4~ do''8. la'16 re''4 |
r4 sol'2~ |
sol'4~ sol'8.( lab'16) fa'8.( sol'16) |
lab'2. |
sol'4 \twoVoices #'(haute-contre taille parties) <<
  { fa'8.(^"H-c" mi'16) fa'4~ |
    fa'8.( mi'16) fa'8.( mi'16) fa'4~ |
    fa' }
  { re'8.(_"T" do'16) re'4~ |
    re'8.( do'16) re'8.( do'16) re'4~ |
    re' }
>> <>^"Tous" do'4\doux si\trill |
\appoggiatura si8 do'4 sol' fa' |
mib' sol' do''~ |
do'' sol' fa' |
mib'2
