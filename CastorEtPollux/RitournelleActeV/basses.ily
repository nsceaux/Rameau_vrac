\clef "basse" r8 do8\fortSug |
re2. |
mib |
fa2 sol4 |
do2 r8 do, |
re,2. |
mib, |
fa,8 sol, lab,4. fa,8 |
sol,2 r4 |
R2.*2 |
r8 do\ademi do4.( si,16\trill) do |
sol,2 r4 |
do'2\fort do4 |
fa mib2 |
re4 fa2 |
mib do4 |
fa2. |
fad |
sol2 fa!4 |
mib2. |
fa |
sol2 r4 |
sol,2 r4 |
sol,2 sol,4\doux |
do2 re4 |
mib2 fa4 |
sol sol,2 |
do,
