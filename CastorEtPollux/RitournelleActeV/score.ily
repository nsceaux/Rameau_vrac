\score {
  \new StaffGroup <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { Flûtes Violons }
    } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with { instrumentName = "Parties" } <<
      \global \keepWithTag #'parties \includeNotes "parties"
    >>
    \new Staff \with { instrumentName = "Bassons" } <<
      \global \keepWithTag #'bassons \includeNotes "bassons"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Basses Basse continue }
    } <<
      \global \includeNotes "basses"
    >>
  >>
  \layout { short-indent = 0 }
  \midi { }
}
