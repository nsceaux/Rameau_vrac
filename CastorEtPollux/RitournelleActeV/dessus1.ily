\clef "dessus" r8 mib''8\fortSug |
mib'' re'' re''4( do''8)\trill si' |
\appoggiatura si'8 do''2 r8 sol'' |
sol'' fa'' fa''4( mib''8)\trill re'' |
\appoggiatura re''8 mib''2 r8 do''' |
do'''4 si'' do'''8 re''' |
sol''2 do'''8 sib'' |
lab'' sol'' fa''4~ fa''16([ mib''\trill re'']) mib'' |
re''2\trill re''8.( mib''16) |
re''8.( sol''16) sol''4.( fa''16\trill) mib'' |
re''2\trill <>^"Flûtes" sol''4 |
sol''2.~ |
sol'' |
<>^"Tous" sol''4~ sol''8.( lab''16) fa''8.( sol''16) |
\appoggiatura sol''8 lab''4 la''4.(\trill sol''16 la'') |
sib''4 si''4.(\trill la''16 si'') |
do'''2.~ |
do'''4~ do'''8.( re'''16) si''8.( do'''16) |
re'''2.~ |
re'''4~ re'''8.( mib'''16) do'''8.( re'''16) |
mib'''2.~ |
mib'''4 re'''8.( mib'''16) do'''8.( re'''16) |
si''2\trill r8 <>^"Flûtes" re''' |
si''2\trill r8 <>^"Tous" re'' |
si'8.(\trill lab''16) sol''8.( fa''16) mib''8.(\trill re''16) |
mib''8.( fa''16) mib''8.( re''16) do''8.\trill( si'16) |
si'4( do''8) r re''8.( mib''32 fa'') |
mib''4( re''4.)\trill do''8 |
do''2
