\clef "dessus" r4 |
R2. |
r4 r r8 mib''\fortSug |
mib'' re'' re''4( do''8)\trill si' |
\appoggiatura si'8 do''2 r8 sol'' |
sol'' fa'' fa''4( mib''8)\trill re'' |
do''2 mib''8 re'' |
do''4~ do''4.( si'16 do'') |
si'2\trill si'8.( do''16) |
si'8.( mib''16) mib''4.( re''16)\trill do'' |
si'2\trill <>^"Flûtes" sol''4 |
sol''2.~ |
sol'' |
<>^"Tous" mi''4~ mi''8.( fa''16) re''8.( mi''16) |
fa''2.~ |
fa''4 \appoggiatura mib''8 re''4 r |
sol''4~ sol''8.( lab''16) fa''8.( sol''16) |
lab''2 r4 |
la''4~ la''8.( sib''16) sol''8.( la''16) |
sib''4 si''4.(\trill la''16 si'') |
do'''2.~ |
do'''8.( sib''16) lab''8.( sol''16) fa''4~ |
fa'' r r |
r r r8 re''\doux |
si'8.(\trill lab''16) sol''8.( fa''16) mib''8.(\trill re''16) |
mib''8.( fa''16) mib''8.( re''16) do''8.\trill( si'16) |
si'4( do''8) r re''8.( mib''32 fa'') |
mib''4( re''4.)\trill do''8 |
do''2
