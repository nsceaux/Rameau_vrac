\clef "tenor" R1 |
r4\fort mib fa lab |
r4 fa re' fa' |
r4 sol sib mib' |
r4 sib mib' sol' |
r4 sib mib' mib |
r do' mib' lab |
r lab fa' lab |
r sol sib mib' |
r lab do' mib' |
r sib fa' lab' |
sol'1 |
R1 |
r4 fa lab do' |
R1 |
r4 sol sib mib' |
R1 |
r4 sib mib' mib |
r do' mib' lab |
R1 |
r4 do' fa' fa |
r re' fa' sib |
R1 |
r4 re' sol' sol |
r do' mib' sol |
R1 |
R2. |
r4 re' fa' lab' |
r re' fa' sib |
r mib' sol' sib |
R2. |
r2 mib'4 fa'8 sol' |
re'4 fa' sib r |
R1*2 |
r4 do' mib' la |
r lab! fa' lab |
r sol mib' sol |
r fa mib' re' |
\appoggiatura re'8 mib'1 |
R1*11 |
r4 sib fa' lab' |
sol'2 r |
%
r4 fa lab do' |
R1 |
r4 sol sib mib' |
R1 |
r4 sib mib' mib |
r do' mib' lab |
R1 |
r4 do' fa' fa |
r re' fa' sib |
R1 |
r4 re' sol' sol |
r do' mib' sol |
R1 |
R2. |
r4 re' fa' lab' |
r re' fa' sib |
r mib' sol' sib |
R2. |
r2 mib'4 fa'8 sol' |
re'4 fa' sib r |
R1*2 |
r4 do' mib' la |
r lab! fa' lab |
r sol mib' sol |
r fa mib' re' |
\appoggiatura re'8 mib'1 |