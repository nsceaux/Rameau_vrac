\clef "treble" sol'1\fort |
fa' |
re' |
mib' |
mib' |
mib' |
mib'2. lab4 |
lab1 |
sib |
do' |
sib |
sib |
R1 |
do'\doux |
r2 re' |
mib'1 |
R1 |
sol |
lab |
R1 |
do' |
re' |
R1 |
re' |
mib' |
r2 r4 mib'8 sol |
lab4. mib'8 fa'8. sol'16 |
re'2\trill r |
sib'1 |
sib' |
R2. |
lab'2 r |
re'2. mib'4 |
sib do' sib lab |
sol1 |
la\fort |
sib |
sib |
sib |
sib |
mib'2.\doux sib4 |
re'2. si4\trill |
\appoggiatura si8 do'1 |
R1 |
sib2 r |
r sib |
sib r4 do' |
sib2 mib'4 mib' |
mib'2. reb'4 |
do'2 r |
do'1 |
sib |
sib2 r |
%%
do'1\doux |
r2 re' |
mib'1 |
R1 |
sol |
lab |
R1 |
do' |
re' |
R1 |
re' |
mib' |
r2 r4 mib'8 sol |
lab4. mib'8 fa'8. sol'16 |
re'2\trill r |
sib'1 |
sib' |
R2. |
lab'2 r |
re'2. mib'4 |
sib do' sib lab |
sol1 |
la\fort |
sib |
sib |
sib |
sib |
