\score {
  \new StaffGroupNoBar <<
    \new Staff \with { instrumentName = "Basson" } <<
      \global \includeNotes "basson"
    >>
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { instrumentName = "Violons" } <<
        \new Staff << \global \includeNotes "violon1" >>
        \new Staff << \global \includeNotes "violon2" >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Hautes-contre Tailles }
      } << \global \includeNotes "parties" >>
    >>
    \new Staff \with { instrumentName = \markup\character Télaïre } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { short-indent = \noindent }
  \midi { }
}