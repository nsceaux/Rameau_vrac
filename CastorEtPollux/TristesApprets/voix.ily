\clef "treble" R1*12 |
mib''2 mib''4. mib''8 |
lab'1 |
lab'2 lab'4 lab' |
mib'1 |
sib'2. sib'8 mib'' |
\appoggiatura lab'8 sol'2 sol'4 sol'8 lab' |
lab'2 lab'8 r r4 |
do''2 do''4. do''8 |
\appoggiatura sib'8 la'2 la'4 la'8 sib' |
sib'1 |
re''2 re''4. re''8 |
\appoggiatura do''8 si'2 si'4 si'8 do'' |
do''1 |
mib''2 sol'8 sol' lab' sib' |
\appoggiatura sib' do''4 do''8 do''16 sib' sib'8([ lab'16])\trill sol' |
fa'2\trill fa'4 r |
fa''1 |
sol'' |
r4 mib''8. reb''?16 reb''8[ do''16] reb'' |
\appoggiatura reb''8 do''2\trill do''4 re''8 mib'' |
fa''2.( sib'8) mib'' |
mib''2.( re''4) |
\appoggiatura re''8 mib''1 |
R1*5 |
sib'4. sib'8 sib'4 sib' |
si'2\trill si'4 re'' |
sol'2 r |
lab'2 lab'4. lab'8 |
lab'2 re''4. fa''8 |
sib'2 fa''4. sib'8 |
sol'4\trill sol'8 r r16 sib' sib' sib' mib''8 \appoggiatura reb'' do'' |
\appoggiatura do'' reb''2 reb''4 do'' |
do''4.( sib'8)\trill sib'4 do'' |
\appoggiatura sib'8 lab'2 do''4. do''16 do'' |
fa''2 do''4 re''8 mib'' |
re''2\trill re''8 r r4 |
mib''2. mib''8 mib'' |
%%
lab'1 |
lab'2 lab'4 lab' |
mib'1 |
sib'2. sib'8 mib'' |
\appoggiatura lab'8 sol'2 sol'4 sol'8 lab' |
lab'2 lab'8 r r4 |
do''2 do''4. do''8 |
\appoggiatura sib'8 la'2 la'4 la'8 sib' |
sib'1 |
re''2 re''4. re''8 |
\appoggiatura do''8 si'2 si'4 si'8 do'' |
do''1 |
mib''2 sol'8 sol' lab' sib' |
\appoggiatura sib' do''4 do''8 do''16 sib' sib'8([ lab'16])\trill sol' |
fa'2\trill fa'4 r |
fa''1 |
sol'' |
r4 mib''8. reb''?16 reb''8[ do''16] reb'' |
\appoggiatura reb''8 do''2\trill do''4 re''8 mib'' |
fa''2.( sib'8) mib'' |
mib''2.( re''4) |
\appoggiatura re''8 mib''1 |
R1*5 |
