\clef "alto" mib'1\fort |
do' |
sib |
sib |
sol |
sib |
do' |
fa |
sol |
fa |
fa2 lab |
sol1 |
R1 |
lab\douxSug |
r2 sib |
sib1 |
R1 |
mib |
mib |
R1 |
fa |
fa |
R1 |
sol |
sol |
r2 r4 mib |
mib2 sib4 |
sib2 r |
fa'1 |
mib' |
R2. |
mib'2 r |
sib fa4 sol8 mib |
mib4 lab fa sib |
sib2 r |
fa1\fortSug |
fa |
sol |
fa2~ fa\trill |
mib1 |
sib2.\doux sol4 |
sol1 |
sol |
R1 |
fa2 r |
r fa |
sol r4 sol8 fa |
fa2 sib4 mib |
mib1 |
mib2 r |
fa1 |
fa |
mib2 r |
%%
lab1\douxSug |
r2 sib |
sib1 |
R1 |
mib |
mib |
R1 |
fa |
fa |
R1 |
sol |
sol |
r2 r4 mib |
mib2 sib4 |
sib2 r |
fa'1 |
mib' |
R2. |
mib'2 r |
sib fa4 sol8 mib |
mib4 lab fa sib |
sib2 r |
fa1\fortSug |
fa |
sol |
fa2~ fa\trill |
mib1 |
