\tempo "Très lent"
\key mib \major
\digitTime\time 2/2 \midiTempo#120 s1*26
\digitTime\time 3/4 \midiTempo#60 \grace s8 s2.
\digitTime\time 2/2 \midiTempo#120 s1*3
\digitTime\time 3/4 \midiTempo#60 s2.
\digitTime\time 2/2 \midiTempo#120 \grace s8 s1*4 \bar "||"
s1*5 \bar "||"
s1*6
\time 4/4 \midiTempo#60 s1
\digitTime\time 2/2 \midiTempo#120 \grace s8 s1*6
%%
s1*13
\digitTime\time 3/4 \midiTempo#60 \grace s8 s2.
\digitTime\time 2/2 \midiTempo#120 s1*3
\digitTime\time 3/4 \midiTempo#60 s2.
\digitTime\time 2/2 \midiTempo#120 \grace s8 s1*4 \bar "||"
s1*5 \bar "|."
