\clef "treble" sib'2.\fort mib''4 |
lab'2. do''4 |
\appoggiatura sib'8 lab'2. lab'8 lab' |
mib'1 |
sib'2. sib'8 mib'' |
sol'2 sol'4 sol'8 lab' |
lab'2. fa'4 |
\appoggiatura mib'8 re'2. sib4 |
mib'1~ |
mib'~ |
mib'2. mib'8 re' |
\appoggiatura re'8 mib'1 |
R1 |
fa'1\doux |
r2 fa' |
sol'1 |
R1 |
sib1 |
mib' |
R1 |
fa' |
fa' |
R |
sol' |
sol' |
r2 r4 mib'8 sol |
lab4. mib'8 re'8. mib'16 |
re'2\trill r |
re''1 |
mib'' |
R2. |
mib''2 r |
sib'2.~ sib'8 do'' |
sol'2 fa'4.\trill mib'8 |
mib'1 |
mib'\fort |
re' |
mib' |
fa'4 sol' lab'2 |
sol'1\trill |
sol'\doux |
fa' |
mib' |
R1 |
re'2 r |
r re' |
mib' r4 sol'8 la' |
sib'2 sib'4 lab' |
lab'2. sol'4\trill |
lab'2 r |
do''1 |
fa'2 sib'4 r |
R1 |
%%
fa'1\doux |
r2 fa' |
sol'1 |
R1 |
sib1 |
mib' |
R1 |
fa' |
fa' |
R |
sol' |
sol' |
r2 r4 mib'8 sol |
lab4. mib'8 re'8. mib'16 |
re'2\trill r |
re''1 |
mib'' |
R2. |
mib''2 r |
sib'2.~ sib'8 do'' |
sol'2 fa'4.\trill mib'8 |
mib'1 |
mib'\fort |
re' |
mib' |
fa'4 sol' lab'2 |
sol'1\trill |
