\clef "bass" R1 |
mib,1^\fort |
mib, |
mib, |
mib |
reb |
do |
sib,2. lab,4 |
sol,1 |
lab, |
sib, |
mib, |
R1 |
mib,\douxSug |
R1 |
mib, |
R1 |
reb, |
do, |
R1 |
mib, |
re, |
R1 |
fa, |
mib,2. re,4 |
do,2 r4 do8 sib, |
lab,4. sol,8 fa,8. mib,16 |
sib,2 r |
lab,1 |
sol, |
R2. |
lab,2 r |
lab,2. sol,8 lab, |
sib,4 lab, sib, sib, |
mib,1 |
do |
sib, |
sib, |
sib, |
mib, |
mib |
re |
do |
R1 |
sib,2 r |
r sib, |
mib r8 reb do fa |
sib,2 sol,4 lab, |
mib,1 |
lab,2 r |
la,1 |
sib, |
mib,2 r |
%%
mib,1\douxSug |
R1 |
mib, |
R1 |
reb, |
do, |
R1 |
mib, |
re, |
R1 |
fa, |
mib,2. re,4 |
do,2 r4 do8 sib, |
lab,4. sol,8 fa,8. mib,16 |
sib,2 r |
lab,1 |
sol, |
R2. |
lab,2 r |
lab,2. sol,8 lab, |
sib,4 lab, sib, sib, |
mib,1 |
do |
sib, |
sib, |
sib, |
mib, |
