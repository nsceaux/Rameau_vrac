Tris -- tes ap -- prêts, pâ -- les flam -- beaux,
Jours plus af -- freux que les té -- nè -- bres,
As -- tres lu -- gu -- bres des tom -- beaux,
As -- tres lu -- gu -- bres des tom -- beaux,
Non, je ne ver -- rai plus que vos clar -- tés fu -- nè -- bres.
Non, non, je ne ver -- rai plus que vos clar -- tés fu -- nè -- bres.

Toi, qui vois mon cœur é -- per -- du,
Pè -- re du jour, ô so -- leil, ô mon pè -- re !
Je ne veux plus d’un bien que Cas -- tor a per -- du,
Et je re -- nonce à ta lu -- miè -- re.

Tris -- tes ap -- prêts, pâ -- les flam -- beaux,
Jours plus af -- freux que les té -- nè -- bres,
As -- tres lu -- gu -- bres des tom -- beaux,
As -- tres lu -- gu -- bres des tom -- beaux.
Non, je ne ver -- rai plus que vos clar -- tés fu -- nè -- bres.
Non, non, je ne ver -- rai plus que vos clar -- tés fu -- nè -- bres.
