\clef "basse" do'2 mi'4 do' |
sol2 do'4 sol |
mi do sol sol, |
la,2. la,4 |
si,2. si,4 |
do2 mi4 do |
fa re sol do |
sol,1 |
sol2 si4 sol |
re2 sol4 re |
si, sol, sol sol, |
re,1 |
la2 do'4 la |
mi2 la4 mi |
do la, la la, |
mi,2. mi4 |
fa2. re4 |
sol1 |
do'2 mi'4 do' |
sol2 do'4 sol |
mi do sol sol, |
do,1 |
