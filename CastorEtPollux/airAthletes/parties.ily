\clef "haute-contre" do'2 mi'4 do' |
sol2 do'4 sol |
mi do sol sol |
la si8 do' re' mi' do'4 |
si4 do'8 re' mi' fa' <re' sol>4 |
<sol mi'>4 do' do' mi' |
do' fa' re' mi' |
sol'1 |
sol'2 si'4 sol' |
re'2 sol'4 re' |
si sol si si' |
la'1 |
la'2 do''4 la' |
mi'2 la'4 mi' |
do' la do' do'' |
do'2. sol4 |
do'2. re'4 |
re'2 fa' |
mi' mi'4 do' |
sol'2 do''4 sol' |
mi' do' sol' sol |
do1 |
