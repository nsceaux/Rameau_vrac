\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "Parties" } <<
      \global \includeNotes "parties"
    >>
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \origLayout { s1*7\break s1*8\break }
    >>
  >>
  \layout { }
  \midi { }
}
