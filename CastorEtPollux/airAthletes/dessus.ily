\clef "dessus2" do''2 mi''4 do'' |
sol'2 do''4 sol' |
mi' do' sol' sol |
la si8 do' re' mi' fa'4 |
si4 do'8 re' mi' fa' sol'4 |
mi' do' sol' do'' |
la'\trill re'' si'8.(\trill do''16) do''8.(\trill si'32 do'') |
re''1 |
sol'2 si'4 sol' |
re'2 sol'4 re' |
si sol re'' mi''\trill |
\appoggiatura mi''8 fa''1 |
la'2 do''4 la' |
mi'2 la'4 mi' |
do' la mi'' fa''8.\trill( mi''32 fa'') |
sol''4 sol'8 fa' mi' re' do'4 |
la' la'8 sol' fa' mi' re'4 |
si'1*3/4\trill( \afterGrace s4 { la'16[ si']) } |
do''2 mi''4 do'' |
sol'2 do''4 sol' |
mi' do' sol' sol |
do'1 |
