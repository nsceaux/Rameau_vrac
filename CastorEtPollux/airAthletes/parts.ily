\piecePartSpecs
#`((violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (haute-contre)
   (taille)
   (hautbois #:notes "dessus" #:instrument "Violons")
   (flutes #:notes "dessus" #:instrument "Violons")
   (bassons #:notes "basse")
   (basses #:notes "basse")
   (percussions #:notes "dessus"
                #:score-template "score-percussion"))
