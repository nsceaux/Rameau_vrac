É -- cla -- tez, é -- cla -- tez __ é -- cla -- tez fiè -- res trom -- pet -- tes,
fai -- tes bril -- ler __ dans ces re -- trai -- tes
la gloi -- re de nos hé -- ros.
É -- cla -- tez, é -- cla -- tez __ é -- cla -- tez fiè -- res trom -- pet -- tes,
fai -- tes bril -- ler __ dans ces re -- trai -- tes
la gloi -- re de nos hé -- ros,
la gloi -- re de nos hé -- ros.

Par des chants de vic -- troi -- re
trou -- blons le re -- pos des é -- chos,
qu’ils ne chan -- tent plus que la gloi -- re,
qu’ils ne chan -- tent plus que la gloi -- re.

