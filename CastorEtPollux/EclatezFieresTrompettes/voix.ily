\clef "vhaute-contre" R1*16 |
r4 sol8 sol do'4 do'8 do' |
mi'1~ |
mi'2. mi'8 mi' |
sol'1 |
sol'2 sol'4 sol' |
do''1~ |
do''2\circA do''4 r |
fa'2 fa'4 fa' |
fa'8[\melisma la' sol' sib'] la'[ do'' fa' la'] |
sol'[ fa' mi' fa'] sol'[ do'' mi' sol'] |
fa'[ mi' re' mi'] fa'[ sol' re' fa']( |
mi'4)\trill\melismaEnd sol'4 fa'8[ mi'] re'[ do'] |
re'2 si4\trill r8 re' |
mi'[\melisma re' do' re'] mi'[ fad' sol' la'] |
fad'[\trill mi' re' mi'] fad'[ sol' mi' fad'] |
sol'2.*2/3 s4\trill fad'8[ mi']( |
re'2.)\melismaEnd do'8 si |
si2( la4.)\trill sol8 |
sol2 r |
R1*6 |
r4 sol8 sol do'4 do'8 do' |
mi'1~ |
mi'2. mi'8 mi' |
sol'1 |
sol'2 sol'4 sol' |
do''1~ |
do''2\circA do''4 r |
R1 |
sol'2 sol'4 do' |
fa'8[\melisma mi' fa' sol'] fa'[ sol' re' fa'] |
mi'[ fa' do' mi'] re'[ mi' si re']( |
do'4)\melismaEnd \appoggiatura si8 la4 re'4.-! mi'8 |
si2\trill si4 r8 sol |
do'[\melisma re' si do'] re'[ mi' do' re'] |
mi'4.\trill re'8 do'[ re' mi' fa'] |
sol'1~ |
sol'8[ la' sol' la'] sol'[ la' sol' la'16 sol'] |
sol'1\trill~ |
sol'2\melismaEnd fa'8[ mi'] re'[ do'] |
sol2~ sol4. do'8 |
do'2 r4 r8 do' |
fa'4\melisma mi'8[\trill re'] sol'4 fa'8[\trill mi'] |
la'1~ |
la'2. si'8[ do''] |
sol'1~ |
sol'2\melismaEnd fa'4.-! mi'8 |
mi'2( re'4.)\trill do'8 |
do'1 |
R1*9 |
r2 la4 la |
do'2. do'8 la |
fa'2 fa'8 r la'4 |
re'2 re'4. mi'8 |
re'2( do'4)\prall do'8 si |
si1\trill |
R1 |
si4. do'8 re'4. mi'8 |
do'2( si4)\prall si8 do' |
sold2\arcTrill sold4 r |
mi'4. fad'8 sold'4.-! la'8 |
re'2. \appoggiatura do'8 si do' |
do'2( si)\trill |
la1 |
