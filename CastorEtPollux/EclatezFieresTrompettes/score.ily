\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { instrumentName = "[Violons]" } <<
        \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
        \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
      >>
      \new Staff \with { instrumentName = "Parties" } <<
        \global \includeNotes "parties"
      >>
    >>
    \new Staff \with { instrumentName = \markup\character Un Athlète }
    \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*9\break s1*9\break s1*8\pageBreak
        s1*9\break s1*8\break s1*8\pageBreak
        s1*9\break s1*10\break s1*9\pageBreak
        s1*7\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
