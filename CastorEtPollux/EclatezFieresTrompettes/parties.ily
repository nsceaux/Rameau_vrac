\clef "haute-contre" R1 |
\ru#2 { sol4.*5/6 sol32*2 sol sol } |
sol2 r |
\ru#2 { sol4.*5/6 sol32*2 sol sol } |
sol2 r |
\ru#2 { sol'4.*5/6 sol'32*2 sol' sol' } |
fa'4 do'8 do' do' do' do' do' |
do'2 r8 do'' do'' do'' |
la'4 la'8 la' sol' sol' sol' si' |
si'4 do'' r r8 do' |
fa'4 r8 re' sol'4 r8 mi' |
la'4 mi'8 mi' mi' mi' mi' mi' |
sol'2~ sol'8 sol' fa' mi' |
re'1~ |
re'2 fa' |
mi'4 sol8 sol sol sol sol sol |
sol2 r |
sol2\doux sol |
sol1 |
sol2 sol |
sol1 sol2 sol |
sol1 |
do''4 do''8 do'' do'' do'' do'' do'' |
do''4 do' do' do' |
sol2 r4 sol |
la2 r4 sol |
sol1 |
sol4 si'8 si' si' si' si' re'' |
sol'4 r r r8 mi' |
re'2 r |
sol'4 sol'8 sol' sol' sol' sol' sol' |
sol'2~ sol'4. la'8 |
re'2 do' |
si2. sol'4\fort |
sol'8 sol' sol' sol' sol'4 fad' |
sol'8 sol' sol' sol' sol'4 re' |
mi'8 mi' mi' mi' la4 re' |
re'8 re' re' re' re'4 sol8 do' |
la2 re' |
si4 si8 si si si si si |
si2 r |
sol\doux sol |
sol1 |
sol2 sol |
sol1 |
sol2 sol |
sol1 |
do''4 do''8\fort do'' do'' do'' do'' do'' |
do''4 do'' r r8 do''\doux |
do''2 do''4 si'\trill |
do'' r sol' r |
sol' r fad' r |
sol'2 r |
R1*7 |
r2 r4 r8 do' |
fa'4 mi'8\trill re' sol'4 fa'8\trill mi' |
la'4 r r2 |
r4 mi''8 mi'' mi'' mi'' mi'' mi'' |
mi''1~ |
mi''2 re''4. do''8 |
sol'1 |
sol'2 r |
sol'\fort sol' |
fa'4 la' la' la' |
do''2 la' |
sol'4 si' si' si' |
si' si'8 si' si' si' si' re'' |
sol'2 sol' |
sol' la' |
re' fa' |
mi'4 mi'8 mi' mi' mi' mi' mi' |
mi'2 r |
la'4\doux la'8 la' do'' do'' do'' do'' |
do''4 re''8 do'' si'4 r |
r2 si |
la mi' |
mi' r |
R1*4 |
r4 r8 la' si'4. do''8 |
sold'4.( si'8) re'4.( mi'16 fa') |
mi'2 re' |
do'1 |
