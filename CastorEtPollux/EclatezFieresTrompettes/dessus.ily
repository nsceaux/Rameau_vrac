\clef "dessus" <>^"Tous" do''4 do''8 re'' do'' re'' do'' re'' |
do'' sol' mi' do' do'' sol' mi' do' |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''4 mi''8 fa'' mi'' fa'' mi'' fa'' |
    mi'' do'' sol' mi' mi'' do'' sol' mi' |
    sol''4 sol''8 la'' sol'' la'' sol'' la'' |
    sol'' mi'' do'' sol' mi'' do'' sol' mi' |
    la''4 la''8 la'' la'' la'' la'' la'' |
    la'' fa'' sol''4. mi''8 sol'' mi'' |
    fa''4 fa''8 fa'' fa'' fa'' fa'' fa'' |
    fa'' re'' mi''4 }
  { do''4 do''8 re'' do'' re'' do'' re'' |
    do'' sol' mi' do' do'' sol' mi' do' |
    mi''4 mi''8 fa'' mi'' fa'' mi'' fa'' |
    mi'' do'' sol' mi' do'' sol' mi' do' |
    fa''4 fa''8 fa'' fa'' fa'' fa'' fa'' |
    fa'' re'' mi''4. do''8 mi'' do'' |
    re''4 re''8 re'' re'' re'' re'' re'' |
    re'' sol' sol'4 }
>> r4 mi''16 re'' do'' si' |
la'4 fa''16 mi'' re'' do'' si'4 sol''16 fa'' mi'' re'' |
do''4 do'''8 do''' do''' do''' do''' do''' |
do''' si'' la'' sol'' fa'' mi'' re'' do'' |
la'' sol'' fa'' mi'' re'' do'' si' la' |
sol'2 re''\trill |
do''4 do'8 do' do' do' do' do' |
do'2 r |
do''4\doux do''8 re'' do'' re'' do'' re'' |
do'' sol' mi' do' do'' sol' mi' do' |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''4 mi''8 fa'' mi'' fa'' mi'' fa'' |
    mi'' do'' sol' mi' mi'' do'' sol' mi' |
    sol''4 sol''8 la'' sol'' la'' sol'' la'' |
    sol'' mi'' do'' sol' mi'' do'' sol' do' | }
  { do''4 do''8 re'' do'' re'' do'' re'' |
    do'' sol' mi' do' do'' sol' mi' do' |
    mi''4 mi''8 fa'' mi'' fa'' mi'' fa'' |
    mi'' do'' sol' mi' do'' sol' mi' do' | }
>>
la''4 la''8 la'' la'' la'' la'' la'' |
la''4 fa'' fa' la' |
do''2 r4 do'' |
do''2 r4 si'\trill |
do''2~ do''4. re''8 |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { si'4\trill sol''8 sol'' sol'' sol'' sol'' sol'' |
    sol''4 r r r8 do'' |
    la'2\trill r | }
  { si'4\trill r r r8 re'' |
    re''4.( do''8) do''2~ |
    do''~ do''4. la'8 | }
>>
re''4 re''8 mi'' re'' mi'' re'' mi'' |
re'' si' sol' re' mi'4. sol'8 |
sol'2( fad'4.)\trill sol'8 |
sol'2. re''4\fort |
mi''8 mi'' mi'' mi'' mi''4 la''16 sol'' fad'' mi'' |
re''8 re'' re'' re'' re''4 sol''16 fa'' mi'' re'' |
do''8 do'' do'' do'' do'' la''16 sol'' fad'' mi'' re'' do'' |
si'8 si' si' si' si'16 re'' do'' si' la' sol' fad' mi' |
re'2 la'\trill |
sol'4 sol8 sol sol sol sol sol |
sol2 r |
do''4\doux do''8 re'' do'' re'' do'' re'' |
do'' sol' mi' do' do'' sol' mi' do' |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''4 mi''8 fa'' mi'' fa'' mi'' fa'' |
    mi'' do'' sol' mi' mi'' do'' sol' mi' |
    sol''4 sol''8 la'' sol'' la'' sol'' la'' |
    sol'' mi'' do'' sol' sol'' mi'' do'' sol' |
    la''4 la''8\fort la'' la'' la'' la'' la'' |
    la''[ fa''] sol'' }
  { do''4 do''8 re'' do'' re'' do'' re'' |
    do'' sol' mi' do' do'' sol' mi' do' |
    mi''4 mi''8 fa'' mi'' fa'' mi'' fa'' |
    mi'' do'' sol' mi' mi'' do'' sol' mi' |
    fa''4 fa''8\fort fa'' fa'' fa'' fa'' fa'' |
    fa''[ re''] mi'' }
>> r8 r4 r8 do''\doux |
fa'' mi'' fa'' sol'' fa'' sol'' re'' fa'' |
mi''4 r re'' r |
do'' r la' r |
re''2 r |
R1 |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { R1 |
    r4 do'''8 do''' do''' do''' do''' do''' |
    do'''2 r |
    \tag#'dessus { \voiceTwo <>_\markup\concat { 1 \super rs } }
    do''2 r |
    la' r |
    re' fa' |
    mi' }
  { r2 r4 do''8 re'' |
    mi''1~ |
    mi''8 fa'' mi'' fa'' mi'' fa'' mi'' fa''16 mi'' |
    \tag#'dessus { \voiceOne <>^\markup\concat { 2 \super ds } }
    mi''1\trill~ |
    mi''2 re''8( do'' si' do'') |
    do''2( si'4.)\trill do''8 |
    do''2 }
>> r4 mi''16 re'' do'' si' |
la'4 fa''16 mi'' re'' do'' si'4 sol''16 fa'' mi'' re'' |
do''4 <>^"Trompette" do''8 do'' do'' do'' do'' do'' |
do''4 <>^"Violons" do'''8 do''' do''' do''' do''' do''' |
do''' si'' la'' sol'' fa'' mi'' re'' do'' |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { la''8 sol'' fa'' mi'' re'' do'' si' do'' |
    \tag#'dessus { \voiceTwo <>_\markup\concat { 1 \super rs } }
    sol'2 fa' | mi' }
  { la''2 la'4 si'8 do'' |
    \tag#'dessus { \voiceOne <>^\markup\concat { 2 \super ds } }
    do''2( si'4.)\trill do''8 | do''2 }
>> r2 |
<>\fort \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''4 sol''8 la'' sol'' la'' sol'' sib'' |
    la''8 fa'' do'' la' fa'' do'' la' fa' |
    la''4 la''8 si''! la'' si'' la'' do''' |
    si'' sol'' re'' si' sol'' re'' si' sol' | }
  { mi''4 mi''8 fa'' mi'' fa'' mi'' sol'' |
    do''2 r |
    fad''4 fad''8 sol'' fad'' sol'' fad'' la'' |
    re''2 r | }
>>
re'''4 re'''8 re''' re''' re''' re''' re''' |
re''' do''' si'' la'' sol'' fa'' mi'' re'' |
mi'' sol'' fa'' mi'' re'' do'' si' la' |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { sol'2 re''\trill | do''4 }
  { sol'2 fa' | mi'4 }
>> do'8 do' do' do' do' do' |
do'2 %%
r2 |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { R1 |
    la''4\doux la''8 la'' la'' la'' la'' la'' |
    la''4. la'8 sold'4\trill la'8 si' |
    mi'2. mi'8 la' |
    sold'4\trill sold''8( la'') sold''( la'') sold''( la'') |
    sold''2 }
  { mi''4\doux mi''8 mi'' mi'' mi'' mi'' mi'' |
    mi''4 fa''8 mi'' re''4 r8 fa' |
    fa'4.( mi'8) mi'4 r |
    R1 |
    r4 si'8( do'') si'( do'') si'( do'') |
    si'2 }
>> r2 |
sold'4. la'8 si'4. do''8 |
re''4.( mi''8) fa''4 sold'8 la' |
\appoggiatura la'8 si'2 r |
r4 r8 mi'' mi''4( re''8)\trill do'' |
si'2. \appoggiatura la'8 sold' la' |
la'2( sold'4.)\trill la'8 |
la'1 |
