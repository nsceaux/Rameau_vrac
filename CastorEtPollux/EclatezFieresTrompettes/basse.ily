\clef "basse" R1 |
\ru#2 { <do, do>4.*5/6 q32*2 q q } |
q2 r |
\ru#2 { <do, do>4.*5/6 q32*2 q q } |
q2 r |
\ru#2 { <do, do>4.*5/6 q32*2 q q } |
fa,4 fa8 fa fa fa fa fa |
do'2 r8 do do do |
fa,4 fa,8 fa, sol, sol, sol, sol, |
do,2 \clef "alto" r4 r8 do' |
fa'4 r8 re' sol'4 r8 mi' |
la'4 \clef "bass" la8 la la la la la |
mi2 r |
fa1 |
sol2 sol, |
do4 <do do,>8 q q q q q |
q2 r |
<>\doux \ru#2 { <do, do>4.*5/6 q32*2 q q } |
q1 |
\ru#2 { <do, do>4.*5/6 q32*2 q q } |
q1 |
\ru#2 { <do, do>4.*5/6 q32*2 q q } |
q1 |
R1 |
r4 fa fa, fa |
mi2 r4 mi |
re2 r4 sol |
do2 r4 do, |
sol,2 r4 r8 sol |
do'2 r4 r8 la8 |
re'2 do' |
si1 |
si,2 do |
re re, |
sol,2. sol4\fort |
do'8 do' do' do' do'4 do |
si,8 si si si si4 si, |
la,8 la la la re'4 re |
sol8 sol sol sol sol4 si,8 do |
re2 re, |
sol,4 sol,8 sol, sol, sol, sol, sol, |
sol,2 r |
<>\doux \ru#2 { <do, do>4.*5/6 q32*2 q q } |
q1 |
\ru#2 { <do, do>4.*5/6 q32*2 q q } |
q1 |
\ru#2 { <do, do>4.*5/6 q32*2 q q } |
q1 |
fa4 fa8\fort fa fa fa fa fa |
do'2 r4 r8 do'\doux |
la2 fa4 sol |
do4 r si, r |
la, r re r |
sol,2 r4 sol8 fa |
mi4 re8 do si,4 la,8 sol, |
do1 |
mi,2 r |
R1 |
mi,2 r |
fa, r |
sol,1 |
do,2 \clef "alto" r4 r8 do' |
fa'4 mi'8\trill re' sol'4 fa'8\trill mi' |
la'4 r r2 |
\clef "bass" r4 la8 la la la la la |
mi2 r |
fa r |
sol sol, |
do r |
do'\fort do |
fa4 fa, fa, fa, |
re'2 re |
sol4 sol, sol, sol, |
R1 |
si2 sol |
do' fa |
sol sol, |
do4 <do do,>8 q q q q q |
q2 r |
r2 la4\doux la |
re'2. r8 re' |
\appoggiatura do'8 si2 mi |
la la, |
mi r |
r4 mi8( fa) mi( fa) mi( fa) |
mi2 r |
R1 |
mi4. fad8 sold4. la8 |
re4. do8 si,4.\trill la,8 |
fa2~ fa4. do16 re |
mi2 mi, |
la,1 |
