\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes dessus1)
   (violon2 #:notes "dessus" #:tag-notes dessus2)
   (haute-contre)
   (taille)
   (hautbois #:notes "dessus"
             #:tag-notes dessus
             #:instrument "Violons")
   (trompette #:notes "dessus"
              #:tag-notes dessus
              #:instrument "Violons")
   (bassons #:notes "basse" #:instrument "Basses")
   (basses #:notes "basse")
   (percussions #:notes "dessus" #:tag-notes dessus1
                #:score-template "score-percussion"))
