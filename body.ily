\newBookPart#'()
\act "Dardanus"
\pieceToc\markup\wordwrap {
  Chaconne
}
\includeScore "Dardanus/Chaconne"
\newBookPart#'()

\pieceToc\markup\wordwrap {
  Trois Songes, chœur : \italic { Par un sommeil agréable }
}
\includeScore "Dardanus/ParUnSommeilAgreable"
\newBookPart#'()

\pieceToc\markup\wordwrap {
  Anténor : \italic { Voici les tristes lieux } – \italic { Monstre affreux }
}
\includeScore "Dardanus/VoiciLesTristesLieux"
\includeScore "Dardanus/MonstreAffreux"
\newBookPart#'()

\pieceToc\markup\wordwrap {
  Prologue – Tambourins
}
\includeScore "Dardanus/PrologueTambourinI" \noPageTurn
\includeScore "Dardanus/PrologueTambourinII"
\newBookPart#'()

\act "Les Fêtes d’Hébé"
\pieceToc\markup\wordwrap { Musette en Rondeau }
\includeScore "LesFetesdHebe/MusetteEnRondeau"
\newBookPart#'(full-rehearsal)

\pieceToc\markup\wordwrap { Tambourin en Rondeau }
\includeScore "LesFetesdHebe/TambourinEnRondeau"
\newBookPart#'()

\pieceToc\markup\wordwrap {
  Chœur : \italic { Suivez les loix }
}
\includeScore "LesFetesdHebe/SuivezLesLoix"
\newBookPart#'()

\act "Naïs"
\pieceToc "Ouverture"
\includeScore "Nais/ouverture"
\newBookPart#'()

\act "Les Paladins"
\pieceToc "Entrée très gaye de Troubadours"
\includeScore "LesPaladins/EntreeTresGayeDeTroubadours"
\newBookPart#'()

\act "Platée"
\pieceToc\markup\wordwrap {
  La Folie : \italic { Formons les plus brillants concerts }
  – \italic { Au langueurs d’Apollon }
}
\includeScore "Platee/FormonsLesPlusBrillantsConcerts"
\newBookPart#'()

\act "Le Temple de la Gloire"
\pieceToc "Ouverture"
\includeScore "LeTempleDeLaGloire/ouverture"
\newBookPart#'()

\pieceToc "Air tendre pour les Muses"
\includeScore "LeTempleDeLaGloire/AirTendrePourLesMuses"
\newBookPart#'()

\act "Zaïs"
\pieceToc "Ouverture"
\includeScore "Zais/ouverture"
\newBookPart#'()

\act "Zoroastre"
\pieceToc "Air tendre en rondeau"
\includeScore "Zoroastre/AirTendreEnRondeau"
\newBookPart#'()

\pieceToc "Air grave pour les esprits infernaux"
\includeScore "Zoroastre/AirGravePourLesEspritsInfernaux"
