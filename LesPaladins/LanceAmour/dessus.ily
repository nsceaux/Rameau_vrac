\clef "dessus" sol'4 re' si' sol' |
re''8 sol' la' si' do'' re'' mi'' fad'' |
sol'' re'' sol'' la'' si'' sol'' si'' do''' |
re'''2 re''4 r |
mi''4(\doux re''8) r fad''4( sol''8) r |
re''4( do''8) r fad''4( sol''8) r |
do''4( si'8) r fad''4( sol''8) r |
la'2 re'4 r |
sol''4\fort sol''8 sol'' sol'' sol'' sol'' si'' |
sol''2\trill re''4 r |
mi''8 fad'' sol'' fad'' mi'' re'' do'' si' |
do'' re'' mi'' re'' do'' si' la' sol' |
re''2 re' |
sol'4 si'8 re'' sol'' re'' si' sol' |
re'4 sol'8 si' re'' si' sol' re' |
sol2 r |
R1 |
<>\doux \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''4 si' sol'' re'' |
    si'' sol'' re''' do'''8( si'') |
    la''([ si'' la'']) re''' re'''( sol'' fad'' la'') |
    re''2 re''4.(\trill do''16 re'') |
    mi''8 fad'' sol'' fad'' mi''4. fa''16( mi'') |
    mi''4\trill sol''2 fad''4\trill |
    \appoggiatura fad''8 sol''1\prall |
    r2 fad''4 sol''8 r |
    re''2 sol''~ |
    sol''1~ |
    sol''4 mi''8 fad'' sol''4 la''8 sol'' |
    fad''4 la'' fad'' re'' |
    sol''2. mi''4 |
    la'2 r4 la' |
    si' la'8 si' dod''4 si'8 dod'' |
    re''2 r4 la'~ |
    la' sol'8 la' si' do'' re''4 |
    re''2. mi''8( re'') |
    dod''4. re''8 mi'' fad'' sol'' la'' |
    mi''4 la'' dod'' mi'' |
    la'2 r |
    r4 la'' la''2~ |
    la''1~ |
    la'' |
    R1 |
    r4 fad''8\ademi fad'' fad'' fad'' fad'' la'' |
    fad''4\trill fad''8 la'' fad''4\trill fad''8 la'' |
    fad''4\trill re'''8 re''' re'''2 |
    re''2.( re''16 mi'' dod'' re'') |
    mi''4 r r16 sol''( fad'' sol'') la''8 r |
    r2 la''4 re'''8 sol'' |
    fad''2( mi'')\trill |
    re''4\fort la''8 sol'' fad''4 re'''8 la'' |
    la''2 la''4.(\trill sol''16 la'') |
    si''8 la'' sol'' fad'' mi'' re'' dod'' si' |
    dod'' la' re'' mi'' mi''4.\trill( re''16 mi'') |
    re''8 }
  { si'4 sol' re'' si' |
    sol'' re'' si'' la''8( sol'') |
    fad''([ sol'' fad'']) la'' la''( mi'' re'' do'') |
    si'( re'' sol'' sol') sol'2 |
    sol'1~ |
    sol'4 mi'' re'' do'' |
    si'8 do'' re'' si' sol'4 r |
    la' si'8 r do''4 si'8 r |
    la'4\trill sol'8 r re''( mi'') do''( re'') |
    mi''1~ |
    mi''4 dod''8 re'' mi''4 dod''8 mi'' |
    la'4 fad'' re'' la' |
    si' la' dod''2 |
    re'' r4 la''8( fad'') |
    sol''2. dod''8 mi'' |
    la'2 r4 re'~ |
    re'2 sol'8 la' si'4 |
    si'2~ si'4. mi'8 |
    mi'2 la'4. re''8 |
    dod''4 mi'' mi'~ mi'8.( re'32 mi') |
    fad'8 mi' re' r r2 |
    r4 re''8 re'' re''4 la' |
    re'' re''8 re'' re'' re'' re'' fad'' |
    re''1\trill |
    R1 |
    r4 re''8\ademi re'' re'' re'' re'' fad'' |
    re''4\trill re''8 fad'' re''4\trill re''8 fad'' |
    re''4\trill fad''8 fad'' fad''2 |
    si'~ si'4.( la'8) |
    la'4~ la'16( re'' dod'' re'') mi''4. fad''8 |
    fad''4.( mi''8)\trill re''4 fad''8.( mi''32 re'') |
    re''2~ re''4.( dod''8) |
    re''4\fort fad''8 re'' re''4 la''8 fad'' |
    fad''2 fad''4.(\trill mi''16 fad'') |
    sol''8 fad'' mi'' re'' dod'' si' mi''4 |
    la'4. re''8 dod''4.\trill re''8 |
    re''8 }
>> r8 fad'' fad'' re'' re'' la' la' |
fad'4 re''8 re'' la' la' fad' fad' |
re'2 r |
R1 | \allowPageTurn
<>\doux \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''4 re'' si'' sol'' |
    re'''2~ re'''8( do''' si'' la'') |
    sol''4 r r fa'' |
    mi''8( re'') do''( si') do''4 r |
    R1 |
    la''4 mi'' dod''' la'' |
    mi'''2~ mi'''8 re''' dod''' si'' |
    la''4 r r sol'' |
    fad''8( mi'') re''( dod'') re'' r la''(\ademi si'') |
    do'''( si'') la''( do''') si''( re''') do'''( si'') |
    si''4\trill la''8 r r4 la''8\doux re'' |
    si''2 r |
    r4 sol''8 sol'' sol'' sol'' sol'' si'' |
    sol''1\trill~ |
    sol''4 sol''8 sol'' sol'' sol'' sol'' si'' |
    sol''1\trill |
    R1 |
    r4 si''8 si'' si'' si'' si'' re''' |
    si''4\trill si''8 re''' si''4\trill si''8 re''' |
    si''1\trill |
    r2 fad''4( sol''8) r |
    re''2~ re''8( mi'' do'' re'') |
    mi''1~ |
    mi''4 dod''8 re'' mi''4 dod''8 mi'' |
    la'4 la''8 sol'' fad'' mi'' re'' do'' |
    si'4. do''8 re''4( do''8\trill si') |
    do''4 re''8( do'') re''( do'') re''( do'') |
    do''4 do''8( si') do''( si') do''( si') |
    do''8 r r4 la''2 |
    do'''8 la'' si'' sol'' la'' fad'' sol'' mi'' |
    re''4 r r2 |
    re''4 re'''8 si'' sol'' re'' si' sol' |
    si4 r si''2 |
    si''4.( do'''16 si'') la''2~ |
    la''~ la''4.( si''16 sol'') |
    sol''2( fad''4.)\trill sol''8 |
    sol''4 re''8\ademi re'' re'' re'' re'' sol'' |
    si'4 do''8 re'' sol'2~ |
    sol'4 la'8( sol') la'( sol') do''( si') |
    la'2\trill r |
    r8 re'' mi'' fad'' sol'' la'' si''4~ |
    si''2 la''4\trill sol'' |
    re''2~ re''4.( mi''16 re'') |
    re'2 la'4.( si'16 do'') |
    si'4\trill sol''8\fort sol'' re'' re'' si' si' |
    mi'' mi'' sol'' sol'' mi'' mi'' do'' do'' |
    la'4 si' la'4.\trill sol'8 |
    sol'4 si'8 re'' sol'' re'' si' sol' |
    re'4 }
  { re''4 si' sol'' re'' |
    si''2~ si''8( do''' si'' la'') |
    sol''4 r re''8( do'') si'( re'') |
    sol'( re'') mi''( fa'') mi''4 r |
    R1 |
    mi''4 dod'' la'' mi'' |
    dod'''2~ dod'''8 re''' dod''' si'' |
    la''4 sol''8( fad'') mi''( re'') dod''( mi'') |
    la'( mi'') fad''( sol'') fad'' r fad''(\ademi sol'') |
    la''( sol'') fad''( la'') sol''( si'') la''( sol'') |
    sol''4\trill fad''8 r r4 r8 la''\doux |
    re''2 r |
    r4 re''8 re'' re'' re'' re'' re'' |
    re''1\trill~ |
    re''4 re''8 re'' re'' re'' re'' re'' |
    re''1\trill |
    R1 |
    r4 re''8 re'' re'' re'' re'' sol'' |
    re''4 re''8 sol'' re''4 re''8 sol'' |
    re''1 |
    r2 do''4( si'8) r |
    la'4\trill sol'8 r sol''2~ |
    sol''1~ |
    sol''4 mi''8 fad'' sol''4 la''8 sol'' |
    fad'' re'' do'' si' la' sol' fad' mi' |
    re'4.( sol'8) sol'2 |
    sol'2. sol'8.(\trill fa'32 sol') |
    la'2 do''8( si') mi'( re') |
    do'4 r r mi'' |
    la'2 r |
    R1 |
    si'4 si''8 sol'' re'' si' sol' re' |
    si4 r re''2~ |
    re''4.( do''16 si') mi''4.( fad''16 sol'') |
    sol''4. fad''8 mi'' re'' do'' si' |
    si'2( la'4.)\prall re''8 |
    si'4\trill si'8\ademi si' si' si' si' re'' |
    sol'4 sol''8 re'' mi''4 si'\trill |
    \appoggiatura si'8 do''4 r do'''2~ |
    do'''4. si''8 la'' sol'' fad'' mi'' |
    \appoggiatura { mi''16[ fad''] } fad''2\trill \appoggiatura mi''8 re''4 sol'' |
    sol''4. fad''8 mi'' re'' do'' si' |
    la'2 si' |
    si'( la'4.)\trill sol'8 |
    sol'4 re''8\fort re'' si' si' sol' sol' |
    sol'4 mi''8 mi'' do'' do'' la' la' |
    fad'4 sol'2 re'4 |
    si re'8 sol' si' re'' sol'' sol' |
    re'4 }
>> sol'8 si' re'' si' sol' re' |
sol2 r2 |
R1 |
r4 mi''8 si' sol'' mi'' si'' sol'' |
fad''2\trill \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''8 la'' red'' fad'' |
    mi''2 mi'4 r |
    mi''2~ mi''4. si'8 |
    si'2.\trill si'8 do'' |
    fad'4 sol''8(\ademi la'') sol''( si'') dod''( mi'') |
    \appoggiatura mi''8 red''2\trill r |
    red''4\doux mi''8 fad'' si'4 r |
    si'2. re''8 dod'' |
    \appoggiatura dod''8 re''4 si'8\ademi fad' re'' si' fad'' re'' |
    si''4 re''8 si' fad'' re'' si'' fad'' |
    fad''2\doux mi''4\trill re'' |
    dod''2. dod''8 re'' |
    \appoggiatura dod'' si'1 |
    sol'4\ademi re' si' sol' |
    re''4.( mi''16 fad'') sol''8 r r4 | }
  { si'4 la' |
    sol'2 r |
    fad'~ fad'4 sol'8. si'16 |
    mi'2~ mi'4.( red'16 mi') |
    red'4\trill red''8(\ademi fad'') mi''( sol'') lad'( dod'') |
    fad'2 r |
    si'4\doux dod''8 red'' dod''4 red'' |
    red''4( mi''8.)\prall re''16 dod''4 si'8 lad' |
    \appoggiatura lad'8 si'2 fad'4 r |
    r si'8\ademi fad' re'' si' fad'' re'' |
    \appoggiatura dod''8 si'2\doux lad'4 si' |
    si'2.\trill si'8 lad' |
    \appoggiatura lad'8 si'1 |
    sol'4\ademi re' si' sol' |
    re''2 r | }
>>
