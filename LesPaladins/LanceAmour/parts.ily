\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes dessus1)
   (violon2 #:notes "dessus" #:tag-notes dessus2)
   (haute-contre #:notes "parties" #:tag-notes haute-contre)
   (taille #:notes "parties" #:tag-notes taille)
   (bassons #:notes "basse")
   (basses #:notes "basse"
           #:score-template "score-basse-continue"))
