\clef "taille" sol'4 re' si' sol' |
re''8 si do' re' mi' fad' sol' la' |
si' sol' si' do'' re'' si' re'' mi'' |
la'2 fad'4\trill r |
sol'\doux r r2 |
sol'4( fad'8) r r2 |
fad'4( sol'8) r r2 |
fad'2\trill r |
<re' re''>4\fort q8 q q4 q |
q1 |
do''8 re'' mi'' re'' do'' si' la' sol' |
la' si' do'' si' la' sol' fad' sol' |
re''2 re' |
sol'4 si'8 re'' si' re'' si' sol' |
re'4 sol'8 si' re'' si' sol' re' |
sol2 r |
R1*2 |
re''4\doux si' sol' re' |
re'1 |
\twoVoices #'(haute-contre taille parties) <<
  { sol'2 re''~ | re''2. mi''8 re'' | do''4 }
  { sol'2 si'~ | si'2. do''8 si' | la'4 }
>> r4 re'2 |
re' r |
sol' re'4 r |
R1*11 |
la4 dod' mi' la' |
fad'8 sol' la' sol' fad' sol' fad' mi' |
re'4 la'8 la' la'4 la'8 la' |
la'1\trill~ |
la'\trill |
R1 |
r2 re''4 la' |
fad' re' re'' la' |
la' la'8 la' la'2 |
si'8( dod'') re''( dod'') si'( la') sol'( fad') |
mi'( re') dod'( si) la4 r8 re'' |
re''4.( dod''8)\trill re''4 la'8 si' |
la'2 sol' |
fad'4\fort la'8 la' la'4 re''8 re'' |
re''2 re'' |
re''4 \twoVoices #'(haute-contre taille parties) <<
  { si'4 si'2 }
  { sol'4 sol'2 }
>>
mi'4 la'8 si' la'4 sol' |
fad' fad''8 fad'' re'' re'' la' la' fad'4 re''8 re'' la' la' fad' fad' |
re'2 r |
R1*2 |
sol'4\doux re' si' la' |
si8 do' re' mi' fa' mi' fa' si |
do'4 sol do r |
R1*2 |
la'4 mi' dod'' la' |
dod'8( re') mi'( fad') sol'( fad') sol'( dod') |
re'4 la re re''\ademi |
re''2 re'' |
re'' r |
R1*6 |
r4 <sol sol'>8 q q2:8 |
q4 q8 q q2: |
q1 |
r2 re'4 r |
do' r r2 |
R1*3 |
sol'4.( re'8) re'2 |
mi'2. fa'8( mi') |
re'4 mi'8( re') sol2 |
sol4 sol'2 la'8 sol' |
fad'4\trill la' re'' r |
R1 |
r2 re' |
sol2 sol' |
sol'~ sol'4.( fad'16\trill mi') |
re'2 r |
r re'~ |
re'4 sol'8\ademi sol' sol' sol' sol' re' |
re'4 do'8 si do'4 fa' |
mi' sol' r mi' |
re'2 la'~ |
la' si'8 do'' re''4~ |
re''2 do''8 si' do'' si' |
la'2 si'4.( la'16\trill sol') |
sol'2 re' |
re'4 re''8\fort re'' re''2:8 |
re''4 do''8 si' do''2: |
do''4 si' re'' re' |
re' sol'8 si' re'' si' sol' re' |
si4 sol'8 si' re'' si' sol' re' |
sol2
%%
r2 |
R1*17 |
