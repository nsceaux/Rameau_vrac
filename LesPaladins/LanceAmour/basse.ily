\clef "basse" sol4 re si sol |
sol,2 r |
sol, r8 sol si do' |
re'2 re4 r |
do'4(\doux si8) r r2 |
si4( la8) r r2 |
la4( sol8) r r2 |
re' re4 r |
si4\fort si8 si si si si re' |
si1\trill |
do'8 re' mi' re' do' si la sol |
la si do' si la sol fad sol |
re'2 re |
sol4 si8 re' sol' re' si sol |
re4 sol8 si re' si sol re |
sol,2 r |
R1*2 |
sol4\doux re si la8 sol |
re'4 la fad re |
si,1 |
do |
la,2 re |
sol,1 |
do'4 si8 r la4 sol8 r |
fad4 sol8 r si,2 |
do dod |
re1~ |
re4 r r fad |
mi2 la, |
re r4 fad |
mi2 la, |
re r4 fad |
sol2 r |
r4 mi8 fad sol la si4 |
la,4. si,8 dod re mi fad |
sol2. la8 sol |
fad sol la sol fad sol fad mi |
re4 re'8 re' re'4 la |
re'4 re'8 re' re' re' re' fad' |
re'1 |
R1 |
re'4 la fad re |
re' la fad re |
re,4 re'8 re' re'2 |
sol1~ |
sol2 r4 r8 fad |
sol2 fad4\trill fad8 sol |
la2 la, |
re2 r4 re'8\fort re' |
re'2 re |
sol sol |
sol4 fad8 sol la4 la, |
re fad'8 fad' re' re' la la |
fad4 re'8 re' la la fad fad |
re2 r |
R1*2 |
sol4^\doux re si sol |
si,2. sol,4 |
do sol, do, r |
R1*2 |
la4 mi dod' la |
dod2. la,4 |
re la, re, re'^\ademi |
fad2 sol |
re r4 r8 re'^\doux |
si2 si4 si8 re' |
si4\trill si8 si si si si re' |
si1\trill~ |
si4 si8 si si si si re' |
si1\trill |
R1*4 |
do'4 si8 r la4\trill sol8 r |
fad4 sol8 r si,2 |
do dod |
re1~ |
re2 r |
fa r |
mi1 |
fa2 sol |
do2. la,4 |
re2 r |
r do |
si,1~ |
si,2 si,4.(\trill la,16 si,) |
do2.~ do16 re si, do |
re1 |
re, |
sol,2 r |
sol4 fa mi re |
do2. la,4 |
re1~ |
re2 si, |
do1 |
re |
re, |
sol,4 sol8\fort sol sol2:8 |
do'4 la8 la la2: |
re'4 sol re2 |
sol4 si8 re' sol' re' si sol |
re4 sol8 si re' si sol re |
sol,2
%%
r2 |
R1 |
r4 mi8 si, sol mi si mi' |
red'2 mi'4 si |
do'4.( si16\trill la) sol4 r |
la2 la,4 si, |
do2~ do\trill |
si,1~ |
si,4. dod8 red mi fad sold |
la2^\doux sol4 fad |
mi2 sol |
re1 |
R1 |
re2^\doux dod4\trill si, |
fad2 fad, |
si,1 |
R1*2 |
