\clef "vhaute-contre" R1*16 |
sol4 re si sol |
re'2 re'4 r |
sol'2~ sol'4. la'8 |
fad'1\trill |
sol'2.\melisma re'8[ fa'] |
mi'[ fad'! sol' fad'] mi'[ fa' si re']( |
do'4)\melismaEnd do'8 do' la4 re' |
si1\trill |
mi'4 re'8 r r2 |
do'4 si8 r r2 |
la4 do'8 si la4 la |
la1\trill |
r2 re'~ |
re'4\melisma dod' mi' sol'~ |
sol' fad'8[ mi'] fad'[ la'] re'4~ |
re' dod'8[ re'] mi'[ fad'] sol'4~ |
sol' fad'8[ mi'] fad'[ la'] do'4~ |
do'\melismaEnd si8 r re'4. sol'8 |
sol'1~ |
sol'4.\melisma fad'8 mi'[ re']\melismaEnd dod'[ si] |
la2. r8 la |
re'2 re'4 re'8 re' |
fad'1~ |
fad' |
fad'4 r r r8 re' |
fad'2\trill fad'4 fad'8 fad' |
la'1~ |
la'~ |
la'2 la'4 r |
mi'8[\melisma fad' sol' fad'] mi'[ fad' re' mi'] |
dod'2\trill\melismaEnd dod'4 r8 re' |
re'4.( mi'8) fad'4 re'8 sol' |
fad'2( mi')\trill |
re'1 |
R1*6 |
sol'4 re' si sol |
sol'1~ |
sol'2 sol'8 r si[\melisma do'] |
re'[ do' si do'] re'[ mi' re' fa']( |
mi'8)[ re']\melismaEnd do'[ si] do'4 r |
la' mi' dod' la |
la'1~ |
la'2 la'8 r dod'[\melisma re'] |
mi'[ re' dod' re'] mi'[ fad' mi' sol']( |
fad'8)[ mi']\melismaEnd re'[ dod'] re'4 r |
R1 |
r2 r4 r8 re' |
sol'2 sol'4 sol'8 si' |
\appoggiatura la'8 sol'1~ |
sol'4\melisma si'8[ la'] sol'4 re' |
sol'1~ |
sol'4 si'8[ la'] sol'4 re' |
sol'4 fad'8[ mi'] re'[ do' si la] |
sol1~ |
sol\melismaEnd |
sol4 r r2 |
mi'4 re'8 r r2 |
do'4 si8 r r2 |
la4 do'8 si la4 la |
la1\trill~ |
la2 r4 r8 re' |
re'4. do'8 si4 la8[ sol] |
do'4\melisma re'8[ do'] re'[ do' fa' mi'] |
re'4 do'8[ si] mi'[ re' sol' fa'] |
mi'4 re'8[ do'] fa'[ mi' la' sol'] |
fad'1\trill |
r4 mi'8[ re'] fad'[ mi' sol' fad'] |
sol'1~ |
sol'4 si'8[ sol'] re'[ mi' si re'] |
do'2.~ do'16[ re' si do'] |
re'2~ re'4.( mi'16[ re']) |
re'1\trill\melismaEnd |
sol2. r8 re' |
re'4 do'8[ si] do'4 re' |
mi'\melisma fad'8[ mi'] fad'[ mi' la' sol'] |
fad'1\trill\melismaEnd |
\appoggiatura mi'8 re'1 |
mi'2 fad'4 sol' |
sol'1~ |
sol'2~ sol'4.( fad'8) |
\appoggiatura fad' sol'1 |
R1*4 |
r2 r4 mi'8 si |
mi'4 mi'8 mi' sol'4 mi' |
si'1~ |
si'4\melisma fad'8[ la'] sol'[ la' red' fad'] |
mi'4.( re'!16[\trill do'])\melismaEnd si4 r |
do'2. do'8 mi' |
sol2~ sol8 sol sol la |
\appoggiatura la8 si1\prall |
r2 r4 si8 si |
fad'4 mi'8 red' mi'4. fad'8 |
fad'4(\melisma sol'8.)\prall[ fad'16] mi'4 re'8\trill[ dod'] |
fad'1~ |
fad'2\melismaEnd fad'4 r |
sold'2 lad'4 si' |
dod'2\trill~ dod'8 dod' dod' re' |
\appoggiatura dod'8 si1 |
R1 |
sol4 re si sol |
