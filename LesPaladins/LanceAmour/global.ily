\key sol \major
\beginMark\markup\column { Ariette gaie Prélude } \midiTempo#200
\digitTime\time 2/2 s1*17 \bar "||"
\segnoMark s1*30
\tempo "Lent" s1*2
\tempo "Vif" s1*41
\tempo "Lent" s1*3
\tempo "Vif" s1*6
\tempo "Lent" s1*2
\tempo "Vif" s1*5 s2 \bar "|." \fineMark
s2 s1*15
\tempo "Plus vite" s1*2 \bar "|." \dalSegnoMark
