\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { instrumentName = "Violons" } <<
        \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus">>
        \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus">>
      >>
      \new Staff \with { instrumentName = "Parties" } <<
        \global \keepWithTag #'parties \includeNotes "parties"
      >>
    >>
    \new Staff \with { instrumentName = \markup\character Atis } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*8\break s1*8\break s1*6\break s1*6\pageBreak
        s1*8\break s1*7\break s1*7\pageBreak
        s1*8\break s1*6\break s1*5\pageBreak
        \grace s8 s1*8\break s1*6\break s1*6\pageBreak
        s1*7\break s1*6\break s1*4 s2\pageBreak
        s2 s1*5\break s1*4\break s1*5\break \grace s8
      }
    >>
  >>
  \layout { }
  \midi { }
}
