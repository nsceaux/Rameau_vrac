\clef "vdessus" \transposition re
do''2. mi''8 re'' |
do''4 do'' do'' do'' |
do''2.\trill <<
  { sol''8 fa'' |
    mi''4 mi'' mi'' mi'' |
    mi''2.\trill sol''8 sol'' |
    sol''4 sol'' sol'' sol'' |
    sol''2 \trill } \\
  { mi''8 re'' |
    do''4 do'' do'' do'' |
    do''2.\trill sol''8\douxSug fa'' |
    mi''4 mi'' mi'' mi'' |
    mi''2\trill }
>> r4 sol'\fortSug |
sol'2. do''4 |
<< { \appoggiatura do''8 re''2\prall } \\ sol'2 >> r4 <<
  { mi''4 | re'' mi'' re'' sol''~ | sol'' fa''8 mi'' re''4 } \\
  { do''4 | sol' do'' sol' mi''~ | mi'' re''8 do'' sol'4 }
>> r4 |
R1 |
r2 r4 <<
  { mi''4 | re'' mi'' re'' sol''~ | sol'' fa''8 mi'' re''4 } \\
  { do''4 | sol' do'' sol' mi''~ | mi'' re''8 do'' sol'4 }
>> r4 |
R1*2 |
r2 <<
  { sol''2 | sol''1~ | sol'' | fad''4 } \\
  { sol'2 | do''1~ | do''~ | do''4 }
>> r4 r2 |
<<
  { r4 re''\doux re''2~ | re'' r | r4 re'' re''2~ | re'' r | } \\
  R1*4
>>
r4 <>\fort <<
  { re''4 re''2 |
    sol''2. fad''4\trill |
    sol''1 |
    fad''4 sol''2 fad''4\trill |
    sol''1 |
    fad''4 sol''2 fad''4\trill |
    sol''2 sol'4 } \\
  { re''4 re''2 |
    re''4 mi''2 re''4 |
    re''1 |
    re''4 mi''2 re''4 |
    re''1 |
    re''4 mi''2 re''4 |
    re''2 sol'4 }
>> r4 |
R1*6 |
r2 r4 <<
  { sol''4 | sol''1~ | sol''~ | sol''~ | sol''2 } \\
  { sol'4\fortSug | sol'1~ | sol'~ | sol'~ | sol'2 }
>> r2 |
R1*5 |
r2 r4 <<
  { mi''4~ | mi''2. mi''4 | \appoggiatura re''8 do''2 } \\
  { mi'4~ | mi'2. mi'4 | mi'2 }
>> r2 |
r r4 <<
  { do''4 | do''1~ | do''~ | do''~ | do''2. do''4 |
    re'' mi'' fa'' mi'' | re'' sol'' fa'' mi'' | re''2\trill } \\
  { do'4 | do'1~ | do'~ | do'~ | do'2. do'4 |
    sol'1~ | sol'~ | sol'2 }
>> r2 |
R1 |
r4 <>\doux <<
  { sol''4 sol''2 | sol''4 } \\
  { sol'4 sol'2 | sol'4 }
>> r4 r2 |
r4 <>\fort <<
  { mi''4 re''2 |
    sol'4 mi'' re'' sol'' |
    do''8 re'' mi'' fa'' sol'' la'' si'' do''' |
    si''4 sol'' re'' sol'' |
    do''8 re'' mi'' fa'' sol'' la'' si'' do''' |
    si''4 sol'' re'' sol'' |
    mi''4\trill mi'8 sol' do'4 } \\
  { mi''4 re''2 |
    sol'4 do''2 sol'4 |
    mi' sol' do'' mi'' |
    re'' do''2 sol'4 |
    mi' sol' do'' mi'' |
    re'' do''2 sol'4 |
    sol' mi'8 sol' do'4 }
>> r4 |
