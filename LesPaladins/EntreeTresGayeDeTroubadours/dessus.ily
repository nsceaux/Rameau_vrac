\clef "dessus" re''2. fad''8 mi'' |
re''4 re'' re'' re'' |
re''2.\trill
<<
  \tag #'(vents violons) <<
    { la''8 sol'' | fad''4 fad'' fad'' fad'' | fad''2.\trill } \\
    { fad''8 mi'' | re''4 re'' re'' re'' | re''2.\trill }
  >>
  \tag #'violon1 {
    la''8 sol'' | fad''4 fad'' fad'' fad'' | fad''2.\trill
  }
  \tag #'violon2 {
    fad''8 mi'' | re''4 re'' re'' re'' | re''2.\trill
  }
>>
\tag #'(violons violon1 violon2) <>^"Tous" \doux
\tag #'vents <>^\markup\whiteout "Sans hautbois"
re'''8( dod''') |
re'''( dod''') re'''( dod''') re'''( dod''') re'''( dod''') |
re'''8 r r4 r
\tag #'vents <>^\markup\whiteout "avec hautbois"
la''4\fort |
sol''8 fad'' mi'' re'' dod''4 re'' |
la'2 r |
<<
  \tag #'(violons violon1 violon2) {
    R1 |
    r2 r4 <>^\markup\whiteout "pincé" <la mi' la' la''>4 |
    q q q q |
    q r r2 |
    R1 |
    r2 r4 q |
    q q q q |
    q r r2 |
    <>^\markup\whiteout "archet" r8 la' dod'' re'' mi'' fad'' sol'' mi'' |
    fad'' re'' fad'' sol'' la'' si'' dod''' la'' |
    re'''
  }
  \tag #'vents {
    R1*2 |
    r4 <>^\markup\whiteout "Petites flûtes" <<
      { mi''8 fad'' sol'' la'' sol'' fad'' |
        mi'' fad'' mi'' fad'' sol'' la'' sol'' fad'' |
        mi''8 } \\
      { dod''8 re'' mi'' fad'' mi'' re'' |
        dod'' re'' dod'' re'' mi'' fad'' mi'' re'' |
        dod'' }
    >> r8 r4 r2 |
    R1 |
    r4 <<
      { mi''8 fad'' sol'' la'' sol'' fad'' |
        mi'' fad'' mi'' fad'' sol'' la'' sol'' fad'' |
        mi''8 } \\
      { dod''8 re'' mi'' fad'' mi'' re'' |
        dod'' re'' dod'' re'' mi'' fad'' mi'' re'' |
        dod'' }
    >> <>^\markup\whiteout "avec hautbois" la' dod'' re'' mi'' fad'' sol'' mi'' |
    fad''4 r r2 |
    r8
  }
>> si'8 re'' mi'' fad'' sold'' la'' fad'' |
<<
  \tag #'(violons violon1 violon2) {
    sold''8 mi'' sold'' la'' si'' dod''' re''' si'' |
    mi'''4 r r2 |
    r4 fad''8(\doux mi'') la''( fad'') si''( sold'') |
    la''4 r r2 |
    r4 fad''8(\douxSug mi'') la''( fad'') si''( sold'') |
    la''4 mi''\fort mi''4.( re''8)\trill |
    dod''8
  }
  \tag #'vents {
    sold''4 r r2 |
    r4 <>^\markup\whiteout "Hautbois" <<
      { mi''4 mi''4.( re''8\trill) | dod''4 } \\
      { dod''4 si'2 | mi'4 }
    >> r4 r2 |
    r4 <<
      { mi''4 mi''4.( re''8\trill) | dod''4 } \\
      { dod''4 si'2 | mi'4 }
    >> r4 r2 |
    r4 <>^\markup\whiteout "Tous" dod''4\fortSug si'2 |
    mi'8
  }
>> mi'' dod'' la' si' re'' si' sold' |
la' si' dod'' re'' mi'' fad'' sold'' la'' |
mi'( re'') dod'' la' si'( re'') si' sold' |
la' si' dod'' re'' mi'' fad'' sold'' la'' |
mi'( re'') dod'' la' si'( re'') si' sold' |
la'4 dod'8 mi' la4 r |
la'2. dod''8 si' |
la'4 la' la' la' |
la'2.\trill <<
  \tag #'(violons vents) <<
    { mi''8 re'' | dod''4 dod'' dod'' dod'' | dod''2. } \\
    { dod''8 si' | la'4 la' la' la' | la'2.\trill }
  >>
  \tag #'violon1 { mi''8 re'' | dod''4 dod'' dod'' dod'' | dod''2. }
  \tag #'violon2 { dod''8 si' | la'4 la' la' la' | la'2.\trill }
>>
<<
  \tag #'violons {
    <>\doux << 
      { mi''8 mi'' | mi''4 mi'' mi'' mi'' | mi''2.\trill } \\
      { mi''8 re'' | dod''4 dod'' dod'' dod'' | dod''2.\trill }
    >> <>\fort
  }
  \tag #'violon1 {
    <>\doux mi''8 mi'' | mi''4 mi'' mi'' mi'' | mi''2.\trill <>\fort
  }
  \tag #'violon2 {
    <>\doux mi''8 re'' | dod''4 dod'' dod'' dod'' | dod''2.\trill <>\fort
  }
  \tag #'vents {
    <>^\markup\whiteout "sans hautbois" la''8( sold'') |
    la''( sold'') la''( sold'') la''( sold'') la''( sold'') |
    la''4 r r <>^\markup\whiteout "Hautbois" <>\fortSug
  }
>> mi''8 fad'' |
sol'' mi'' dod'' mi'' la' sol'' fad'' mi'' |
fad'' mi'' re'' dod'' <<
  \tag #'(violons violon1 violon2) {
    re''8 r r4 |
    R1 |
    r2 r4 <>\doux \twoVoices #'(violon1 violon2 violons) <<
      { fad''4 |
        fad''( mi''8) \oneVoice r \tag #'violons \voiceOne lad''4( si''8) \oneVoice r \tag #'violons \voiceOne |
        mi''4( re''8) \oneVoice r r4 \tag #'violons \voiceOne mi'' |
        mi''4( re''8) \oneVoice r \tag #'violons \voiceOne lad''4( si''8) \oneVoice r \tag #'violons \voiceOne |
        re''4( dod''8) \oneVoice r }
      { si'4 |
        si'( lad'8) s dod''4( re''8) s |
        lad'4( si'8) s s4 lad'4 |
        lad'( si'8) s mi''4( re''8) s |
        si'4( lad'8) s }
    >> r4 <>\fort
  }
  \tag #'vents {
    re''4 <>^\markup\whiteout "Flûtes" re'''8( dod''') |
    re'''( dod''') re'''( dod''') re'''( dod''') re'''( dod''') |
    re'''8 r r4 r2 |
    R1*3 |
    r2 r4 <>^\markup\whiteout "Hautbois" \fortSug
  }
>> fad''4 |
mi''8( re'') dod''( si') re''( dod'') si'( lad') |
si' dod'' re'' mi'' fad'' sold'' lad'' si'' |
fad'4
<<
  \tag #'(violons violon1 violon2) {
    r4 r2 |
    r \origVersion\clef "dessus2" r4 <>^\markup\whiteout "pincé" <sol re' si' sol''>4 |
    q q q q |
    q r r2 |
    r4 q q q |
    q r r2 |
    \origVersion\clef "dessus" <>^\markup\whiteout "archet" r4
  }
  \tag #'vents {
    re''4 re''8( dod'') si'( lad') |
    si'4 r r2 |
    R1 |
    r4 <<
      { <>^\markup\whiteout "petites flûtes" si''8 la'' sol''4 si''8 la'' | sol''4 } \\
      { <>_\markup\whiteout "Hautbois" re''8 do'' si'4 re''8 do'' | si'4 }
    >> r4 r2 |
    r4 <<
      { si''8 la'' sol''4 si''8 la'' | sol''4 } \\
      { re''8 do'' si'4 re''8 do'' | si'4 }
    >>
  }
>> sol''8 fad'' mi'' re'' dod'' si' |
dod'' la' re'' la' mi'' la' fad'' la' mi'' la' la'' la' sol'' la' fad'' la' |
<<
  \tag #'(violons violon1 violon2) {
    \twoVoices #'(violon1 violon2 violons) << mi''2\trill dod''\trill >> r |
    r4 si''8(\doux la'') re'''( si'') mi'''( dod''') |
    re'''4 r r2 |
    r4 si''8( la'') re'''( si'') mi'''( dod''') |
    re'''4
  }
  \tag #'vents {
    <<
      { mi''4\trill la'' la''4.( sol''8)\trill | fad''4 } \\
      { dod''4\trill mi'' mi''2 | la'4 }
    >> r4 r2 |
    r4 <<
      { la''4 la''4.( sol''8)\trill | fad''4 } \\
      { mi''4 mi''2 | la'4 }
    >> r4 r2 |
    r4 <>^\markup\whiteout "Tous"
  }
>> la''4\fort la''4.( sol''8)\trill |
fad''8 la'' fad'' re'' mi'' sol'' mi'' dod'' |
re'' mi'' fad'' sol'' la'' si'' dod''' re''' |
la'( sol'') fad'' re'' mi''( sol'') mi'' dod'' |
re'' mi'' fad'' sol'' la'' si'' dod''' re''' |
la'( sol'') fad'' re'' mi''( sol'') mi'' dod'' |
re''4 fad'8 la' re'4 r |
