\clef "taille" re''2. fad''8 mi'' |
re''4 re'' re'' re'' |
re''2.\trill la'8 la' |
la'4 la' la' la' |
la'2.\trill fad'8(\douxSug mi') |
fad'( mi') fad'( mi') fad'( mi') fad'( mi') |
fad' r r4 r re''\fortSug |
dod''8 re'' sol'4 sol' fad' |
mi'2\trill r |
R1 |
r2 r4 <>^\markup\whiteout "pincé" <la mi' dod''>4 |
q q q q |
q r r2 |
R1 |
r2 r4 q |
q q q q |
q r4 r2 |
r <>^\markup\whiteout "archet" mi'' |
la'1 |
fad''2. re''4 |
si'2\trill r |
R1 |
r4 la'2\doux re''4 |
dod''\trill r r2 |
r4 la'2\douxSug re''4 |
dod''\trill \cesureInstr dod''\fort si'2 |
mi'4 la' fad' si\trill |
la8 si dod' re' mi' fad' sold' la' |
si'4 la' si' si'\trill |
dod''8 si' la' si' dod'' si' mi' la' |
si'4 la' si' si'\trill |
dod'' dod'8 mi' la4 r |
la'2. dod''8 si' |
la'4 la' la' la' |
la'2.\trill mi'8 mi' |
mi'4 mi' mi' mi' |
mi'2.\trill mi'8\douxSug mi' |
mi'4 mi' mi' mi' |
mi'1\trill |
mi''2\fortSug dod''\trill |
re''4 la' fad' r |
R1*5 |
r2 r4 si'\fortSug |
si' sol' fad' mi' |
re'2. si'4 |
si' fad'2 dod'4 |
si r r <>^\markup\whiteout "pincé" <sol re' re''>4 |
q q q q |
q r r2 |
r4 <sol re' si'> q q |
q r r2 |
r2 r4 <>^\markup\whiteout "archet" sol' |
sol' fad' dod'' re'' |
dod'' re'' dod'' re'' |
dod''2\trill r |
r4 re'2\doux sol'4 |
fad'\trill r r2 |
r4 re'2 sol'4 |
fad'\trill re''\fort mi''2 |
la' si'4 la' |
la'1 |
la'4 la' si' la' |
la'1 |
la'4 la' si' la' |
la'4 fad'8 la' re'4 r |
