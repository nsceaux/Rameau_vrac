\piecePartSpecs
#`((flutes #:instrument ,#{\markup\center-column { Petites flûtes }#}
           #:notes "dessus" #:tag-notes vents)
   (hautbois #:notes "dessus" #:tag-notes vents)
   (cors #:notes "cor" #:instrument "Cors en ré")
   (dessus #:notes "dessus" #:tag-notes violons
           #:score-template "score")
   (violon1 #:notes "dessus" #:tag-notes violon1)
   (violon2 #:notes "dessus" #:tag-notes violon2)
   (parties #:score-template "score")
   (haute-contre)
   (taille)
   (bassons #:notes "basses" #:tag-notes basson #:clef "tenor")
   (basses))
