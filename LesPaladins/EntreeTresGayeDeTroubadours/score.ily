\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with {
        instrumentName = \markup\center-column { Petites flûtes Hautbois }
      } <<
        \global \keepWithTag #'vents \includeNotes "dessus"
      >>
      \new Staff \with { instrumentName = "Bassons" } <<
        \global \keepWithTag #'basson \includeNotes "basses"
      >>
    >>
    \new Staff \with { instrumentName = "Cors" } <<
      \keepWithTag #'() \global \includeNotes "cor"
    >>
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Violons" } <<
        \global \keepWithTag #'violons \includeNotes "dessus"
      >>
      \new Staff \with { instrumentName = "Parties" } <<
        \global \includeNotes "parties"
      >>
      \new Staff \with { instrumentName = "Basses" } <<
        \global \keepWithTag #'basse \includeNotes "basses"
        \origLayout {
          s1*10\break s1*9\pageBreak
          s1*8\break s1*9\pageBreak
          s1*9\break s1*8\pageBreak
          s1*10\break
        }
        \modVersion { s1*32\break }
      >>
    >>
  >>
  \layout { short-indent = 0 }
  \midi { }
}
