\tag #'basson \clef "petrucci-f/tenor"
\tag #'basse \clef "basse"
<>^"Tous" re'2. fad'8 mi' |
re'4 re' re' re' |
re'2. <<
  \tag #'basson {
    la'8 la' |
    la'4 la' la' la' |
    la'2.\trill fad'8(\douxSug mi') |
    fad'( mi') fad'( mi') fad'( mi') fad'( mi') |
    fad' r r4
  }
  \tag #'basse {
    re8 re |
    re4 re re re |
    re2. re,8\douxSug re, |
    re,4 re, re, re, |
    re,2
  }
>> r4 fad'\fortSug |
mi'8 re' dod' si la4 <<
  \tag #'basson { re'4 | dod'2\trill }
  \tag #'basse { re4 | la,2 }
>> r2 |
<<
  \tag #'basson {
    R1*7 |
    r4 la8 si dod' re' mi' fad' |
    sol'4 r \clef "bass"
  }
  \tag #'basse {
    R1 |
    r2 r4 <>^\markup\whiteout "pincé" <la, mi dod'>4 |
    q q q q |
    q r r2 |
    R1 |
    r2 r4 q |
    q q q q |
    q r4 r2 |
    r <>^\markup\whiteout "archet"
  }
>> dod2 |
re1 |
si, |
mi |
<<
  \tag #'basson {
    sold, |
    la,4 r r2 |
    r4 la, sold,2\trill |
    la,4 r r2 |
    r4
  }
  \tag #'basse {
    R1 |
    r4 dod'\doux re' mi' |
    la r r2 |
    r4 dod'\doux re' mi' |
    la
  }
>> la4\fort sold2\trill |
la4 fad re mi |
la,8 si, dod re mi fad sold la |
mi4 fad re mi |
la,8 si, dod re mi fad sold la |
mi,4 fad, re, mi, |
la, dod8 mi la,4 r |
la2. dod'8 si |
la4 la la la |
la2.\trill <<
  \tag #'basson {
    mi'8 mi' |
    mi'4 mi' mi' mi' |
    mi'2.\trill dod'8(\douxSug si) |
    dod'( si) dod'( si) dod'( si) dod'( si) |
    dod'4 r r2 |
    \clef "tenor" mi'\fortSug dod'\trill |
    re' re4 fad'8( mi') |
    fad'( mi') fad'( mi') fad'( mi') fad'( mi') |
    fad'4 r r re'\doux |
    re'( dod'8) r r2 |
    dod'4( si8) r r2 |
    r dod'4( si8) r |
    fad4( mi8) r \clef "bass"
  }
  \tag #'basse {
    la,8 la, |
    la,4 la, la, la, |
    la,2.\trill la,8\douxSug la, |
    la,4 la, la, la, |
    la,1 |
    dod'2\fortSug la |
    re' re4 r |
    R1*5 |
    r2
  }
>> r4 re\fort |
mi2 fad |
sol1 |
re4 r fad2 |
si,
<<
  \tag #'basson {
    r2 |
    R1 |
    r4 sol, sol,2~ |
    sol,4 r r2 |
    r4 sol, sol,2 |
    sol, r4
  }
  \tag #'basse {
    r4 <>^\markup\whiteout "pincé" <sol, re si>4 |
    q q q q |
    q r r2 |
    r4 q q q |
    q r r2 |
    r r4 <>^\markup\whiteout "archet"
  }
>> mi |
la fad mi re |
la, fad mi re |
la <<
  \tag #'basson {
    \clef "tenor" la'4 dod'2\trill |
    re'4 r r2 |
    r4 re' dod'2\trill |
    re'4 r r2 |
    r4 \clef "bass"
  }
  \tag #'basse {
    r4 r2 |
    r4 fad\doux sol la |
    re r r2 |
    r4 fad\doux sol la |
    re
  }
>> re'4\fort dod'2\trill |
re'4 fad sol la |
re8 mi fad sol la si dod' re' |
la,4 fad, sol, la, |
re8 mi fad sol la si dod' re' |
la,4 fad sol la |
re fad,8 la, re,4 r |
