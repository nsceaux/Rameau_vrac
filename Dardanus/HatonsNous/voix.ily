\clef "vhaute-contre" r4 |
R1*4 |
r2 <>^\markup\character Dardanus r4 la8 la |
re'4. re'8 fad'4 fad'8 re' |
la'1 |
la'4 la8 la si4 dod'8 re' |
dod'2\trill r4 r8 re' |
mi'2. fad'8 sol' |
fad'2\trill \appoggiatura mi'16 re'2 |
R1 |
r2 r4 la' |
la'( si'8)[ la'] sol'[ fad'] mi'[ re'] |
\appoggiatura re'8 mi'2 dod'\trill |
r2 r4 la |
re'4 si mi' dod' |
fad' fad'8 la' re'4 re'8 dod' |
si2\trill r |
r4 r8 mi' mi'4 mi'8 mi' |
mi'1~ |
mi' |
mi'4 mi'8 re' dod'4\trill re'8 mi' |
fad'2 r4 r8 si' |
sold'2.\trill fad'8 mi' |
la'1~ |
la'8[\melisma sol' fad' mi'] re'[ mi' dod' re'] |
mi'2( mi)\melismaEnd |
la1 |
R1*4 |
mi'2.~ mi'16[\melisma fad' mi' red'] |
mi'2.~ mi'16[ sol' fad' la'] |
sol'2.( la'16)[ sol']\melismaEnd fad'[ mi'] |
red'2.\trill si8 si |
fad'2~ fad'4. la'8 |
sol'2 sol'4 fad'8 mi' |
si'1~ |
si'~ |
si'2 si'4 r |
R1*2 |
si2.~ si16[\melisma dod' si re'] |
dod'2.~ dod'16[ re' dod' red']( |
red'2.)\trill\melismaEnd dod'8[ si] |
fad'1 |
fad'4 r r r8 la' |
sol'2. fad'8[ mi'] |
mi'2( red'4.\trill) mi'8 |
mi'1 |
r4 sol'8 fad' mi'4 fad'8 sol' |
dod'2.\trill r8 mi' |
mi'2.\trill re'8 mi' |
\appoggiatura mi'8 fad'2 fad'4 r |
r2 r4 la' |
fad' re' la4. re'8 |
sol2 r4 si' |
sol' mi' si4. mi'8 |
la2. si8 dod' |
re'2~ re'8[\melisma mi' dod' re'] |
mi'[ re' dod' re'] mi'[ fad' re' mi']( |
fad'2)\melismaEnd sol'8[ fad'] mi'[ re'] |
la'1~ |
la' |
R1*2 |
r2 r4 la |
re' si mi' dod' |
fad' mi'8 re' sol'4( fad'8)[\trill\melisma mi'] |
la'2. sol'8[ fad'] |
si'[ la' sol' fad'] mi'[ re' dod' si]( |
la1)~ |
la2\melismaEnd la4. re'8 |
re'1 |
R1 |
r2 r4 la |
re' re' fad'4. re'8 |
sol'2. sol'8 la' |
si'1~ |
si'~ |
si'8[\melisma la' sol' fad'] mi'[ re' dod' re']( |
la'1)\melismaEnd |
la2~ la4. re'8 |
re'1 |
R1*4 |
