\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes violon1
            #:score-template "score-voix")
   (violon2 #:notes "dessus" #:tag-notes violon2
            #:score-template "score-voix")
   (basses #:notes "basse"
           #:score-template "score-basse-continue-voix")
   (silence #:on-the-fly-markup ,#{ \markup\null #}))
