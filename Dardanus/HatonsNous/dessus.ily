<<
  \tag #'violon1 {
    \clef "dessus" <>^\markup { \concat { P \super er } violons } la'8 la' |
    re''4. re''8 fad''4 fad''8 re'' |
    la''4 la''2 sol''8\trill fad'' |
    sol'' sol'' mi'' mi'' dod'' dod'' la' la' |
    mi'4 sol''2 fad''8\trill mi'' |
    fad'' fad'' re'' re'' la' la' fad' fad' |
    re'4 fad''2 mi''8\trill re'' |
    mi''8 mi'' dod'' dod'' la' la' mi' mi' |
    dod'4 mi'8\doux la' fad'4\trill mi' |
    mi' la''8 fad'' sol'' mi'' fad'' re'' |
    \appoggiatura dod''8 si'2 la' |
    la' r4 la''\fort |
    si''8 si'' sol'' sol'' re'' re'' si'' si'' |
    la'' la'' fad'' fad'' re''4 fad''\doux |
    mi'' la''2 re''4 |
    dod''2\trill r4 la'\fort |
    re'' si' mi'' dod''\doux |
    \appoggiatura si'8 la'4 re'' si' mi'' |
    dod''\trill fad'' mi'' mi' |
    mi' re'''2\fort dod'''8\trill si'' |
    dod''' dod''' la'' la'' mi'' mi'' dod'' dod'' |
    la'4 mi'''2 re'''8 dod''' |
    si'' si'' sold'' sold'' mi'' mi'' si' si' |
    sold' mi'\doux fad' sold' la' la' si' dod'' |
    re''4. fad''8 si''2 |
    mi''1~ |
    mi''8 re'' dod'' si' la' sol' fad' mi' |
    fad'2~ fad'8 sold' la'4~ |
    la'2( sold'4.)\trill la'8 |
    la'2 r4 mi''\fort |
    fad'' re'' sold'' mi'' |
    la'' sold''8\trill fad'' mi'' fad'' re'' mi'' |
    dod''\trill la' dod'' re'' mi''4 mi' |
    la'2 r |
    si'\doux~ si'4. la'8 |
    sol'2. fad'4 |
    mi'2. sol'4 |
    fad'2\trill r |
    r2 r4 si' |
    si'2~ si'4. mi''8 |
    red''4\trill la''2 sol''8\trill fad'' |
    sol'' sol'' mi'' mi'' si' si' sol' sol' |
    mi'4 si''2 la''8 sol'' |
    fad'' fad'' red'' red'' si' si' fad' fad' |
    red'4 r r2 |
    r2 si'\doux~ |
    si'4.( la'8) la'2~ |
    la' r |
    r4 si'8 dod'' red'' mi'' fad'' sol'' |
    la''2~ la''4.( si'8) |
    si'2 do'' |
    si' r4 si'\fort |
    mi'' si' sol'' mi'' |
    mi'''2 r4 mi'\doux |
    la' mi' dod'' la' |
    la''2 r4 la'\fort |
    re'' la' fad'' re'' |
    re'''2 r |
    r r4 re'\doux |
    sol' re' si' sol' |
    sol''2 r4 mi' |
    la' mi' dod'' la' |
    la''2. si''8 la'' |
    sol'' fad'' mi'' fad'' sol'' si'' la'' sol'' |
    fad''4 la'' re'''4. re''8 |
    dod''4\trill sol''2\fort fad''8\trill mi'' |
    fad'' fad'' re'' re'' la' la' fad' fad' |
    re'4 la''2 sol''8\trill fad'' |
    mi'' mi'' dod'' dod'' la' la' mi' mi' |
    dod'4 r r2 |
    R1*6 |
    r4 la''2 sol''8\trill fad'' |
    sol'' sol'' mi'' mi'' dod'' dod'' la' la' |
    mi'4 sol''2\doux fad''8\trill mi'' |
    fad'' fad'' re'' re'' la' la' fad' fad' |
    re'4 sol' si' re'' |
    sol''1~ |
    sol''~ |
    sol''8 fad'' mi'' re'' si''4.( mi''8) |
    \appoggiatura mi''16 fad''2~ fad''4. sol''8 |
    fad''2( mi''4.\trill) re''8 |
    re''2.
  }
  \tag #'violon2 {
    \clef "dessus2" <>^\markup\whiteout { \concat { 2 \super e } violons } r4 |
    r la'8 la' re''4. re''8 |
    fad''4 fad''2 mi''8\trill re'' |
    mi'' mi'' dod'' dod'' la' la' mi' mi' |
    dod'4 mi''2 re''8\trill dod'' |
    re'' re'' la' la' fad' fad' re' re' |
    la4 re''2 dod''8\trill si' |
    dod''8 dod'' la' la' mi' mi' dod' dod' |
    la4 dod'\doux re' si\trill |
    la dod''8 la' mi'' dod'' re'' re' |
    re'4 re''2 dod''4\trill |
    \appoggiatura dod''8 re''2 r4 fad''\fort |
    sol''8 sol'' re'' re'' si' si' sol'' sol'' |
    fad'' fad'' re'' re'' fad'4 la'\doux |
    la r r2 |
    r r4 la'\fort |
    la' sol'2 sol'4\doux |
    fad' si' sold' dod'' |
    \appoggiatura si'8 la'4 re''4 \appoggiatura dod''8 si'4. la'8 |
    sold'4\trill si''2\fort la''8\trill sold'' |
    la'' la'' mi'' mi'' dod'' dod'' la' la' |
    mi'4 dod'''2 si''8 la'' |
    sold'' sold'' mi'' mi'' si' si' sold' sold' |
    mi' re'\doux dod' si la4 la' |
    la'2 r4 r8 re'' |
    \appoggiatura dod''16 si'2\trill~ si'4. la'16 si' |
    dod''8 si' la' sold' fad' mi' re' dod' |
    si2. mi'8 fad' |
    dod'2( si4.) la8 |
    la2 r4 mi''\fort |
    mi''( re'') re''( mi'') |
    dod''2\trill si' |
    mi'8 la' dod'' re'' mi''4 mi' |
    la'2 r |
    mi'2\doux~ mi'4. fad'8 |
    si2. si4 |
    si1 |
    si2 r |
    r2 r4 si' |
    si'2.( la'8)\trill sol' |
    fad'4\trill fad''2 mi''8\trill red'' |
    mi'' mi'' si' si' sol' sol' mi' mi' |
    si4 sol''2 fad''8 mi'' |
    red'' red'' si' si' fad' fad' red' red' |
    si4 r r2 |
    r2 mi'\doux~ |
    mi'2. fad'4 |
    fad'2 r |
    r si'8 dod'' red'' mi'' |
    si' dod'' red'' mi'' red'' mi'' fad''4~ |
    fad''4( mi''8 red'') \appoggiatura red''8 mi''4. sol'8 |
    sol'2( fad'4)\trill si'\fort |
    mi'' si' sol'' mi'' |
    si'2 r4 mi'\doux |
    la' mi' dod'' la' |
    mi'4. dod''8 dod''4.(\trill si'16 dod'') |
    re''4 la' fad'' re'' |
    la'2 r |
    r r4 re'\doux |
    re'2. re'4 |
    si'2 r4 mi' |
    mi'2. mi'4 |
    fad'2 re''~ |
    re''2. dod''4\trill |
    re''4 la'2( sol'8)\trill fad' |
    mi'4\trill mi''2\fort re''8\trill dod'' |
    re'' re'' la' la' fad' fad' re' re' |
    la4 fad''2 mi''8\trill re'' |
    dod'' dod'' la' la' mi' mi' dod' dod' |
    la4 r r2 |
    R1*6 |
    r4 fad''2 mi''8\trill re'' |
    mi'' mi'' dod'' dod'' la' la' mi' mi' |
    dod'4 mi''2\doux re''8\trill dod'' |
    re'' re'' la' la' fad' fad' re' re' |
    \appoggiatura dod'8 si4 re' sol' si' |
    re''1~ |
    re''~ |
    re''2 sol''8 fad'' mi'' re'' |
    re''1~ |
    re''2( dod''4.)\trill re''8 |
    re''2.
  }
>> re''8\fort re'' |
sol''4. sol''8 la''4.(\trill sol''16 la'') |
si''8 la'' sol'' fad'' mi'' re'' dod'' re'' |
la'2 mi''\trill |
re''2.
