\clef "basse" r4 |
R1*2 |
r2 r4 la,8 la, |
la4. la8 dod'4 dod'8 la |
re'2 r |
r2 r4 re |
la,1 |
r4 la2^\doux sold4\trill |
la2 r4 r8 fad |
sol2 la |
re r4 re'^\fort |
sol2 r4 sol, |
re,2 r4 re'^\doux |
dod' la re' re |
la2 r4 la^\fort |
fad sol8 fad mi4 la^\doux |
re'4 si mi' dod' |
fad' fad sold4. la8 |
mi4 mi\fort mi' mi |
la2 r |
r2 r4 la, |
mi,2 r |
r4 mi^\doux fad mi |
re2 r |
re'1 |
dod'2 dod |
re~ re8 mi dod re |
mi2 mi, |
la, r4 la\fort |
re' si mi' dod' |
fad' fad sold mi |
la8 la, dod re mi4 mi, |
la,2 r |
sol2^\doux~ sol4. fad8 |
mi2. red4 |
mi2 mi, |
si,2. si4 |
red1 |
mi |
si,2 r4 si |
mi'2 r |
r r4 mi |
si1 |
r2 la^\doux |
sold2. mi4 |
la2. fad4 |
si1 |
red~ |
red2 r4 r8 si, |
mi2 la, |
si,1 |
mi,2 r4 mi\fort |
sol mi mi' mi\doux |
la2 r4 la, |
dod la, la la, |
re,2 r4 re\fort |
fad re la re |
re'2 r4 re'\doux |
si re' sol si |
mi2 r4 mi' |
dod' mi' la dod' |
fad2. sol8 fad |
mi2 la |
re2. re4 |
la,2 r4 la\fort |
re'2 r |
r r4 re |
la,2 r |
r r4 la8\doux sol |
fad4 sol8 fad mi4 la |
re2 mi |
fad2. re4 |
sol2~ sol8 la fad sol |
la4 sol8 fad mi re dod si, |
la,1 |
re, |
R1 |
r4 la\fort la, la\doux |
re' re fad4. re8 |
sol1 |
sol,~ |
sol, |
sol2~ sol4. si8 |
la4 sol8 fad mi re dod si, |
la,1 |
re,2. re'8\fort do' |
si4 do'8 si la4 re' |
sol2~ sol4. si8 |
la2 la, |
re2.
