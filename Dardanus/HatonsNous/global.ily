\key re \major
\tempo "Vivement" \midiTempo#160
\digitTime\time 2/2 \partial 4 s4 s1*33
\tempo "Lent" s1*6 s4
\tempo "Vivement" s2. s1*4
\tempo "Lent" s1*7
\tempo "Vivement" s1*39 \bar "|."
