\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new GrandStaff <<
      \new Staff << \global \keepWithTag #'violon1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'violon2 \includeNotes "dessus" >>
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s1*6\pageBreak
        s1*5\break s1*6\break \grace s8 s1*5\pageBreak
        s1*7\break s1*8\break s1*7\pageBreak
        s1*8\break s1*5 s2 \bar "" \break s2 s1*5\pageBreak
        s1*7 s2 \bar "" \break s2 s1*8\break \grace s8
      }
    >>
  >>
  \layout { indent = \smallindent }
  \midi { }
}
