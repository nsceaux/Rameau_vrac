<<
  \tag #'songe1 {
    \clef "vdessus" R1*34 |
    r2 r4 <>^\markup\character Trois Songes re''8 re'' |
    re''4 re''8 re'' re''4 re'' |
    sol''1~ |
    sol'' |
    sol''4 r r sol'' |
    fad''2\trill fad''4 sol'' |
    mi''2\trill mi''4 fad''8 la'' |
    re''4 re''8 re'' re''4. do''16[ si'] |
    la'2\trill la'4 re'' |
    re''1~ |
    re''~ |
    re''2 re''4 r8 si' |
    mi''1~ |
    mi''2 mi''4 la'8 si' |
    dod''4 la'8 si' dod''[ re''] mi''[ fad''] |
    sol''4 mi''8 mi'' mi''4\trill re''8 mi'' |
    fad''2 r4 fad'' |
    fad'' mi''8 re'' re''4.(\trill dod''8) |
    \appoggiatura dod''8 re''1 |
    R1*8 |
    r2 r4 la' |
    re''1~ |
    re'' |
    re''4 r r2 |
    r2 r4 sol'8 sol' |
    re''4 re''8 re'' mi''4 fa'' |
    mi''1\trill |
    r2 r4 la'8 la' |
    mi''4 mi''8 mi'' fad''4 sol'' |
    fad''1\trill |
    R1*2 |
    r2 r4 si' |
    mi'' mi''8 sol'' re''2 |
    si'1\trill |
    R1*2 |
    r2 r4 r8 si' |
    si'4 si'8 do'' la'4\trill la'8 sold' |
    sold'2 sold'4 sold' |
    la'4. la'8 la'4 la' |
    la'2( sold'4.)\trill la'8 |
    la'2 r |
    R1*5 |
    r2 r4 si'8 si' |
    si'4 si'8 si' si'4 si' |
    fad''1 |
    fad''4 sol''8 red'' mi''4 si'8 si' |
    si'4 la' la' fad'' |
    red''\trill red''8 mi'' mi''4.( red''8) |
    \appoggiatura red''8 mi''2 r4 mi'' |
    mi''1~ |
    mi''~ |
    mi''~ |
    mi''2 mi''4 sol'' |
    fad''1~ |
    fad''2 fad''4 fad'' |
    sol''1~ |
    sol''2 sol''4 mi'' |
    la''1~ |
    la'' |
    la''4 r r2 |
    R1 |
    r2 r4 si' |
    mi''4 mi''8 sol'' re''2 |
    si'1\trill |
    r2 r4 si' |
    mi'' mi''8 sol'' re''2 |
    si'1\trill |
  }
  \tag #'songe2 {
    \clef "vhaute-contre" R1*34 |
    r2 r |
    R1 |
    r2 r4 sol8 sol |
    sol4 sol8 sol sol4 sol |
    re'1~ |
    re' |
    re'4 do'8 si do'2~ |
    do'4 re'8 do' si4 sol' |
    fad'2\trill fad'4 r |
    R1 |
    r2 r4 re'8 re' |
    sol'4 fad'8 mi' re'[ do'] si[ la] |
    sol2 r4 mi'8 mi' |
    la'4 sol'8 fad' mi'[ re'] dod'[ si] |
    la4 la' la'2~ |
    la'1~ |
    la'~ |
    la'4 sol'8 fad' mi'2\trill |
    re'1 |
    R1*9 |
    r2 r4 la'8 la' |
    fad'4\trill fad'8 fad' fad'4 la' |
    re'2 r4 sol' |
    sol'1~ |
    sol' |
    sol'4 do' mi' mi'8 mi' |
    la'1~ |
    la' |
    la'4 r r2 |
    R1*2 |
    r2 r4 re' |
    sol' sol'8 sol' sol'4.( fad'8) |
    \appoggiatura fad'16 sol'1 |
    R1*2 |
    r4 r2 r8 re' |
    re'4 re'8 re' red'4\trill red'8 mi' |
    mi'2 mi'4 re'! |
    do'4. do'8 do'4 fa' |
    si2.\trill la4 |
    la2 r |
    R1*6 |
    r2 r4 si8 si |
    si4 si8 si si4 si |
    sol'1~ |
    sol'2 sol'4 fad' |
    fad' fad'8 sol' fad'2\trill |
    mi' r4 sol' |
    sol'1~ |
    sol'~ |
    sol' |
    sol'4 mi' la'2~ |
    la'1~ |
    la'2 la'4 la' |
    re'2 re'4 sol'8 sol' |
    mi'4 mi'8 mi' mi'4 sol' |
    do'2. la'8 la' |
    fad'4\trill fad'8 fad' fad'4 la' |
    re'1 |
    R1 |
    r2 r4 re' |
    sol' sol'8 sol' sol'4.( fad'8) |
    \appoggiatura fad'8 sol'1 |
    r2 r4 re' |
    sol' sol'8 sol' sol'4.( fad'8) |
    \appoggiatura fad'8 sol'1 |
  }
  \tag #'songe3 {
    \clef "vbasse" R1*34 |
    r2 r |
    R1*3 |
    r2 r4 re8 re |
    re4 re8 re re4 re |
    la2 la4 fad |
    sol2 sol4 sol, |
    re1 |
    re4 r r fad8 sol |
    la4 la8 la si4 do' |
    si2\trill r4 si8 si |
    si4 si8 si dod'4 re' |
    dod'2\trill r |
    R1 |
    r4 la8 si dod'4\trill si8 dod' |
    re'2. fad4 |
    sol sol8 sol la2 |
    re1 |
    R1*9 |
    r2 r4 re8 re |
    la4 la8 la si4 do' |
    si1\trill |
    r2 r4 re'8 re' |
    si4\trill si8 si si4 re' |
    sol1 |
    r2 r4 mi'8 mi' |
    dod'4\trill dod'8 dod' dod'4 mi' |
    la2. la4 |
    re'1~ |
    re'~ |
    re'2 re'4 re' |
    re' do'8 si la2\trill |
    sol1 |
    R1*2 |
    r2 r4 r8 sol |
    sol4 sol8 sol fad4 fad8 si |
    mi2 mi4 mi |
    fa4. fa8 fa4 re |
    mi2. mi4 |
    la,2 r |
    R1*7 |
    r2 r4 mi8 mi |
    mi4 mi8 mi mi4 mi |
    do'2 do'4 fad |
    si si8 mi si,2 |
    mi r4 do' |
    do'1~ |
    do'~ |
    do'~ |
    do'2 do'4 la |
    re'1~ |
    re'2 re'4 re'8 re' |
    si4\trill si8 si si4 re' |
    sol2. mi'8 mi' |
    mi'4 do'8 do' do'4 mi' |
    la2. si8 do' |
    fad2\trill r4 la |
    re'1~ |
    re'2 re'4 re' |
    re' do'8 si la2\trill |
    sol1 |
    r2 r4 re' |
    re' do'8 si la2\trill |
    sol1 |
  }
  \tag #'vdessus {
    \clef "vdessus" R1*52 |
    r2 r4 <>^\markup\character Chœur re''8 re'' |
    re''4 re''8 re'' re''4 re'' |
    sol''1~ |
    sol'' |
    sol''4 r r sol'' |
    fad''2\trill fad''4 sol'' |
    mi''2\trill mi''4 fad''8 la'' |
    re''4 re''8 re'' re''4. do''16[ si'] |
    la'2\trill la'4 r |
    R1*2 |
    r2 r4 re'' |
    re''2 re''4 r |
    R1 |
    r2 r4 re'' |
    mi''2 mi''4 r |
    R1 |
    r2 r4 mi'' |
    fad''2 fad''4 r |
    R1 |
    r4 la'8 la' si'4 do''8 si' |
    do''2. si'4 |
    mi'' mi''8 sol'' re''2 |
    si'1 |
    R1*7 |
    r4 r8 do'' do''4. re''8 |
    si'4.\trill <>^"Trio" <<
      \new Voice \with { autoBeaming = ##f } {
        \voiceOne sol''8 sol''2~ |
        sol''4 sol''8 sol'' fad''4 fad''8 fad'' |
        si'2 si'4
      }
      { \voiceTwo \hideNotes sol''8 sol''4. \unHideNotes si'8 |
        la'4 la'8 la' la'4 si'8 fad' |
        sol'2 sol'4 \oneVoice }
      \new Voice { \voiceFour sol'8\rest sol'4\rest sol'8\rest }
    >> <>^"Tous" si'4 |
    mi'' re'' do'' si' |
    la'2.\trill sol'4 |
    fad'1\trill |
    R1*3 |
    r2 r4 fad'' |
    red''4\trill red''8 mi'' mi''4.( red''8) |
    \appoggiatura red''8 mi''1 |
    R1 |
    r2 r4 do'' |
    do''1~ |
    do''2 do''4 r |
    R1*2 |
    r2 r4 sol' |
    sol'2 sol'4 r |
    r2 r4 la' |
    la'2 la'4 r |
    R1 |
    r4 la'8 la' si'4 do''8 si' |
    do''2. si'4 |
    mi'' mi''8 sol'' re''2 |
    si'1\trill |
    r2 r4 si' |
    mi'' mi''8 sol'' re''2 |
    si'1\trill |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R1*54 |
    r2 r4 sol8 sol |
    sol4 sol8 sol sol4 sol |
    re'2 re'4 la' |
    la'1~ |
    la'2 la'4 la' |
    sol'2 sol'4 sol' |
    fad'2\trill fad'4 r |
    R1*2 |
    r2 r4 fad' |
    sol'2 sol'4 r |
    R1 |
    r2 r4 sol' |
    sol'2 sol'4 r |
    R1 |
    r2 r4 la' |
    la'2 la'4 r |
    r re'8 mi' fad'4 sol'8 la' |
    re'4 re'8 re' sol'4 fad'8 sol' |
    la'2. re'4 |
    sol' sol'8 sol' sol'4.( fad'8) |
    \appoggiatura fad'16 sol'1 |
    R1*7 |
    r4 r8 la' la'4. la'8 |
    re'2 r4 r8 sol |
    re'4 re'8 re' red'4\trill red'8 mi' |
    mi'2 mi'4 sol' |
    la' si' fad' sol' |
    red'2. mi'4 red'1\trill |
    R1*3 |
    r2 r4 la' |
    la' la'8 sol' fad'2\trill |
    mi'1 |
    R1 |
    r2 r4 la' |
    la'1~ |
    la'2 la'4 r |
    R1*2 |
    r2 r4 re' |
    mi'2 mi'4 r |
    r2 r4 mi' |
    fad'2 fad'4 r |
    r4 re'8 mi' fad'4 sol'8 la' |
    re'4 re'8 re' sol'4 fad'8 sol' |
    la'2. re'4 |
    sol' sol'8 sol' sol'4.( fad'8) |
    \appoggiatura fad'8 sol'1 |
    r2 r4 re' |
    sol' sol'8 sol' sol'4.( fad'8) |
    \appoggiatura fad' sol'1 |
  }
  \tag #'vtaille {
    \clef "vtaille" R1*54 |
    r2 r4 sol8 sol |
    sol4 sol8 sol sol4 sol |
    re'1~ |
    re' |
    re'4 do'8 si do'2~ |
    do'4 si8 la si4 re' |
    re'2 re'4 r |
    R1*2 |
    r2 r4 la |
    si2 si4 r |
    R1 |
    r2 r4 si |
    do'2 do'4 r |
    R1 |
    r2 r4 dod' |
    re'2 re'4 r |
    R1 |
    r4 fad'8 la' re'4 re'8 re' |
    re'2. re'4 |
    sol sol8 mi' re'2 |
    re'1 |
    R1*7 |
    r4 r8 mi' la4. re'8 |
    re'2 r4 r8 sol |
    re'4 re'8 re' red'4\trill red'8 mi' |
    mi'2 mi'4 mi' |
    fad' sol' red' mi' |
    si2. si4 |
    si1 |
    R1*3 |
    r2 r4 do' |
    si si8 si si2 |
    si1 |
    R1 |
    r2 r4 mi' |
    mi'1~ |
    mi'2 mi'4 r |
    R1*2 |
    r2 r4 si |
    do'2 do'4 r |
    r2 r4 do' |
    do'2 do'4 r |
    R1 |
    r4 fad'8 la' re'4 re'8 re' |
    re'2. re'4 |
    sol sol8 mi' re'2 |
    re'1 |
    r2 r4 re' |
    sol sol8 mi' re'2 |
    re'1 |
  }
  \tag #'vbasse {
    \clef "vbasse" R1*56 |
    r2 r4 re8 re |
    re4 re8 re re4 re |
    la2 la4 fad |
    sol2 sol4 sol, |
    re2 re4 r |
    R1*2 |
    r2 r4 re |
    sol2 sol4 r |
    R1 |
    r2 r4 sol, |
    do2 do4 r |
    R1 |
    r2 r4 la, |
    re2 re4 r |
    R1 |
    r4 re'8 do' si4 la8 sol |
    fad2.\trill sol4 |
    mi4 mi8 do re2 |
    sol,1 |
    R1*7 |
    r4 r8 la fad4.\trill re8 |
    sol2 r |
    R1 |
    r4 r8 mi' mi'4 re' |
    do' si la sol |
    fad2.\trill mi4 |
    si1 |
    R1*3 |
    r2 r4 fad |
    si si8 mi si,2 |
    mi1 |
    R1 |
    r2 r4 la |
    la1~ |
    la2 la4 r |
    R1 |
    r2 r4 re8 re |
    sol2. sol,4 |
    do2 do4 do'8 do' |
    la2.\trill la,4 |
    re2 re4 r |
    R1 |
    r4 re'8 do' si4 la8 sol |
    fad2.\trill sol4 |
    mi mi8 do re2 |
    sol1 |
    r2 r4 sol |
    mi mi8 do re2 |
    sol1 |
  }
>>
