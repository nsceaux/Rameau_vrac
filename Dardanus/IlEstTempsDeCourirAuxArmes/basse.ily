\clef "basse" <>^"Tous" sol,2 r |
r sol4 fad |
mi do re re, |
sol,2 r4 sol |
fad2 r |
re, r4 fad |
mi2 r |
la, r4 la |
re'2 r4 fad |
sol2 r4 mi |
la2 r4 dod |
re sol, la, la, |
re,8 re16 mi fad8. fad16 la8. la16 re'8. re'16 |
re'2 r |
r re4 dod |
si, sol, la, la, |
re,2 r4 re |
sol2 r |
si,2 r4 sol, |
do2 r |
la, r4 la, |
si,2 r4 red |
mi la, si, si, |
mi,2 r |
sold2 r4 mi |
la2 r |
fad r4 re |
sol2 r |
si, r4 sol, |
do2 r4 la, |
re2 r4 fad |
sol do re re, |
sol,1~ |
sol,~ |
sol,2~ sol,_"B.C." |
R1 |
r2 r4 sol8.\doux sol16 |
sol4 sol8. sol16 sol4 sol |
re'2. re8 re |
re4 re8 re re4 re |
la2 la4 fad |
sol2 sol4 sol, |
re1 |
R1 |
r2 re |
sol,1 |
mi,2 mi |
la, r |
R1 |
r4 la8 si dod'4\trill si8 dod' |
re'2. fad4 |
sol sol8 sol la4 la, |
re8 <>^"[Tous]" re16\fort mi fad8 fad16 sol la8. la16 re'8. re'16 |
re'8 re16 mi fad8 fad16 sol la8. la16 re'8. re'16 |
si2\trill r4 sol8. sol16 |
sol4 sol8. sol16 sol4 sol |
re'2. re8 re |
re4 re8 re re4 re |
la2 la4 fad |
sol2 sol4 sol, |
re8 re16 mi fad8 fad16 sol la8. la16 re'8. re'16 |
si4 sol la la, |
re,1 |
r2 r4 re |
sol2 sol, |
R1 |
r2 r4 sol, |
do2 do, |
R1 |
r2 r4 la, |
re2 re, |
R1 |
r4 re'8 do' si4 la8 sol |
fad2.\trill sol4 |
mi mi8 do re2 |
sol,1 |
\clef "tenor" <>^"Bassons" r8 sol16 la si8. si16 re'8. re'16 sol'8. sol'16 |
mi'4 do' re' re |
sol2 r4 \clef "bass" <>^"[B.C.]" r8 sol |
sol4 sol8 sol fad4 fad8 si |
mi2 mi4 mi |
fa4. fa8 fa4 re |
mi2. mi4 |
la,4. <>^"[Tous]" la8 fad4.\trill re8 |
sol2 r4 r8 sol |
re'4 re'8. re'16 red'4 red'8. mi'16 |
mi'4 r8 mi' mi'4 re' |
do' si la sol |
fad2.\trill mi4 |
si1 |
r2 r4 <>^"[B.C.]" si |
red fad si, mi8 mi |
mi,2. mi,4 la,2. <>^"[Tous]" fad,4 |
si,4. mi8 si,2 |
mi8 sol16 fad mi8. mi16 mi8. mi16 do8. do16 |
do,1~ |
do,2 r4 la, |
la,1~ |
la,2. la,4 |
re,1 |
r2 r4 re8 re |
sol2. sol,4 |
do2 do4 do'8 do' |
la2.\trill la,4 |
re2 re4 r |
R1 |
r4 re'8 do' si4 la8 sol |
fad2.\trill sol4 |
mi mi8 do re2 |
sol,1 |
r8 sol,16 la, si,8. si,16 re8. re16 sol8. sol16 |
mi4 mi8 do re2 |
sol,1 |
