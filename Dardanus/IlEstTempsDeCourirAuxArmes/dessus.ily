\clef "dessus" r8 si'16 do'' re''8. re''16 si'8. si'16 sol'8. sol'16 |
re'8 sol'16 la' si'8. si'16 re''8. re''16 sol''8. sol''16 |
sol''4. la''8 la''4.(\trill sol''16 la'') |
si''8 sol''16 la'' si''8. sol''16 re''8. si''16 la''8.\trill sol''16 |
la''4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { fad'4 sol' la' | }
  { re'4 mi' fad' | }
>>
r8 fad''16 sol'' la''8. la''16 re''8. la''16 sol''8.\trill fad''16 |
sol''4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { mi'4 fad' sol' | }
  { dod' re' mi' | }
>>
r8 mi''16 fad'' sol''8. sol''16 la'8. sol''16 fad''8.\trill mi''16 |
fad''8 fad''16 sol'' la''8. la''16 fad''8. fad''16 re''8. re''16 |
si'8\trill sol''16 la'' si''8. si''16 sol''8. sol''16 mi''8. mi''16 |
dod''8\trill dod''16 re'' mi''8. mi''16 dod''8. dod''16 la'8. la'16 |
fad'4. mi'8 mi'4.\trill re'8 |
re'1 |
r8 fad'16 sol' la'8. la'16 fad'8. fad'16 re'8. re'16 |
la8 re'16 mi' fad'8. fad'16 la'8. la'16 re''8. re''16 |
re''4. mi''8 mi''4.(\trill re''16 mi'') |
fad''8 fad''16 sol'' la''8. la''16 fad''8. fad''16 re''8. re''16 |
si'4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { si4 do' re' | }
  { sol4 la si | }
>>
r8 sol''16 la'' si''8. si''16 sol''8. sol''16 si'8. si'16 |
mi''4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { do'4 re' mi' | }
  { la si do' | }
>>
r8 la''16 si'' do'''8. do'''16 la''8. la''16 fad''8. fad''16 |
red''8\trill red''16 mi'' fad''8. fad''16 red''8. red''16 si'8. si'16 |
sol'4.\trill fad'8 fad'4.\trill mi'8 |
mi'4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { sold'4 la' si' | }
  { si do' re' | }
>>
r8 si'16 do'' re''8. re''16 mi'8. re''16 do''8.\trill si'16 |
do''4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { fad' sol' la' | }
  { la si do' }
>>
r8 la'16 si' do''8. do''16 re'8. do''16 si'8.\trill la'16 |
si'4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { si4 do' re' | }
  { sol la si }
>>
r8 re''16 mi'' fa''8. fa''16 sol'8. fa''16 mi''8.\trill re''16 |
mi''8 mi''16 fa'' sol''8. sol''16 la'8. sol''16 \ficta fad''8. mi''16 |
fad''8 fad''16 sol'' la''8. la''16 fad''8. fad''16 re''8. re''16 |
si'4.\trill la'8 la'4.\trill sol'8 |
sol'8 do''16 si' si'8. si'16 sol'8. sol'16 re'8. re'16 |
si8 si'16 la' sol'8. sol'16 re'8. re'16 si8. si16 |
si2
%%
r2 |
R1 |
r8 <>\doux \twoVoices #'(dessus1 dessus2 dessus) <<
  { <>-\markup { \concat { P \super rs } violons }
    sol''16 la'' si''8. si''16 sol''8. sol''16 re''8. re''16 |
    si'8\trill si'16 do'' re''8. re''16 si'8. si'16
    \tag #'dessus { \voiceTwo <>_\markup\concat { 1 \super rs } }
    sol'8. sol'16 |
    re'2 }
  { <>-\markup { \concat { 2 \super es } violons }
    si'16 do'' re''8. re''16 si'8. si'16 sol'8. sol'16 |
    re'8 sol'16 la' si'8. si'16 sol'8. sol'16
    \tag #'dessus { \voiceOne <>^\markup\concat { 2 \super ds } }
    si'8. si'16 |
    la'2\trill }
>> r2 |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { \tag #'dessus <>^\markup\concat { 1 \super rs }
    r8 fad''16 sol'' la''8. la''16 fad''8. fad''16 re''8. re''16 |
    mi''8 fad''16 sol'' la''8. la''16 la''4~ la''8. la''16 |
    \tag #'dessus { \voiceTwo <>_\markup\concat { 1 \super rs } }
    re''2~ re''4. do''16 si' |
    la'8\trill re''16 mi'' fad''8. fad''16 re''8. re''16 la'8. la'16 |
    fad'8\trill re'16 mi' fad'8. fad'16 fad'8. fad'16 re'8. re'16 |
    la2 }
  { \tag #'dessus <>_\markup\concat { 2 \super ds }
    r2 r4 re'' |
    re'' do''8.\trill si'16 \appoggiatura si'8 do''4 la''4~ |
    \tag #'dessus { \voiceOne <>^\markup\concat { 2 \super ds } }
    la'' sol''8.\trill fad''16 \appoggiatura fad''8 sol''4. la''8 |
    fad''8\trill fad''16 sol'' la''8. la''16 fad''8. fad''16 re''8. re''16 |
    la'8 si'16 dod'' re''8. re''16 re''8. re''16 la'8. la'16 |
    fad'2\trill }
>> r2 |
R1*5 | \allowPageTurn
r8 \twoVoices #'(dessus1 dessus2 dessus) <<
  { <>-\markup { \concat { P \super rs } violons }
    fad''16 sol'' la''8. la''16 fad''8. fad''16 re''8. re''16 |
    \tag #'dessus { \voiceTwo <>_\markup\concat { 1 \super rs } }
    re'4 sol'8. fad'16 mi'4
    \tag #'dessus { \voiceOne <>^\markup\concat { 1 \super rs } }
    sol' |
    fad'8\trill }
  { <>-\markup { \concat { 2 \super es } violons }
    re''16 mi'' fad''8. fad''16 re''8. re''16 la'8. la'16 |
    \tag #'dessus { \voiceOne <>^\markup\concat { 2 \super ds } }
    si'4 dod''8. re''16 la'4 
    \tag #'dessus { \voiceTwo <>_\markup\concat { 2 \super ds } }
    la |
    la8 }
>> <>^"[Tous]" re'16\fort mi' fad'8 fad'16 sol' la'8. la'16 re''8. re''16 |
fad''8 re'16 mi' fad'8 fad'16 sol' la'8. la'16 re''8. re''16 |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { re''8 sol''16 la'' si''8. si''16 sol''8. sol''16 re''8. re''16 |
    si'8\trill si'16 do'' re''8. re''16 si'8. si'16 si'8.\trill la'16 |
    la'8 }
  { si'8\trill si'16 do'' re''8. re''16 si'8. si'16 sol'8. sol'16 |
    re'8 sol'16 la' si'8. si'16 sol'8. sol'16 sol'8. sol'16 |
    re'8 }
>> si'16 do'' re''8. re''16 re''8. re''16 sol''8. la''16 |
fad''8\trill fad''16 sol'' la''8. la''16 fad''8. fad''16 re''8. re''16 |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''2. fad''8. la''16 |
    re''4 re''8. re''16 re''4. do''16 si' |
    la'8\trill }
  { re''4 do''8.\trill si'16 do''2~ |
    do''4 si'8.\trill la'16 si'4. la'16 sol' |
    fad'8\trill }
>> re'16 mi' fad'8 fad'16 sol' la'8. la'16 re''8. re''16 |
re''4. mi''8 mi''4.(\trill re''16 mi'') |
fad''8 fad''16 sol'' la''8. la''16 fad''8. fad''16 re''8. re''16 |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { la'2 }
  { fad' }
>> r2 |
r8 \twoVoices #'(dessus1 dessus2 dessus) <<
  { si''16 do''' re'''8. re'''16 si''8. si''16 sol''8. sol''16 |
    re''8 mi''16 fad'' sol''8. sol''16 re''8. re''16 si'8. si'16 |
    sol'2 }
  { sol''16 la'' si''8. si''16 sol''8. sol''16 re''8. re''16 |
    si'8 sol'16 la' si'8. si'16 si'8. si'16 sol'8. sol'16 |
    re'2 }
>> r2 |
r8 \twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''16 fa'' sol''8. sol''16 mi''8. mi''16 do''8. do''16 |
    do''8 do''16 re'' mi''8. mi''16 do''8. do''16 la'8. la'16 |
    mi'2 }
  { do''16 re'' mi''8. mi''16 do''8. do''16 la'8. la'16 |
    la'8 la'16 si' do''8. do''16 la'8. la'16 mi'8. mi'16 |
    la2 }
>> r2 |
r8 \twoVoices #'(dessus1 dessus2 dessus) <<
  { fad''16 sol'' la''8. la''16 fad''8. fad''16 re''8. re''16 |
    la'2 }
  { re''16 mi'' fad''8. fad''16 re''8. re''16 la'8. la'16 |
    fad'2 }
>> r2 |
r4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { la''4 si'' do''' | }
  { fad''4 sol'' la'' | }
>>
r8 re'16 mi' fad'8 fad'16 sol' la'8. la'16 re''8. re''16 |
sol''4 sol''8. sol''16 sol''4.( fad''8) |
\appoggiatura fad''16 sol''8 si'16 do'' re''8. re''16 si'8. si'16 sol'8. sol'16 |
re'8 sol'16 la' si'8. si'16 re''8. re''16 sol''8. sol''16 |
sol''4. la''8 la''4.(\trill sol''16 la'') |
si''4 r r2 |
R1*4 |
r8 la'16 si' do''8 do''16 re'' mi''8. mi''16 la''8. la''16 |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { la''4. re''8 sol''2~ |
    sol''4 sol''8. sol''16 fad''4 fad''8. fad''16 |
    si'8 }
  { la''4. re''8 sol''4. si'8 |
    la'4 la'8. la'16 la'4 si'8. fad'16 |
    sol'8 }
>> mi'16 fad' sol'8 sol'16 la' si'8. si'16 mi''8. mi''16 |
mi''4 re'' do'' si' |
la'2.\trill sol'4 |
fad'8\trill si16 dod' red'8 red'16 mi' fad'8. fad'16 si'8. si'16 |
red''4 <>\doux \twoVoices #'(dessus1 dessus2 dessus) <<
  { fad'4 sol' la' | }
  { red' mi' fad' | }
>>
r8 si'16 dod'' red''8 red''16 mi'' fad''8. fad''16 si''8. si''16 |
si''1 |
r8 la'16 si' do''8 do''16 re'' mi''8. mi''16 la''8. la''16 |
la''4. sol''8 fad''4.\trill( mi''16 fad'') |
sol''8 mi'16 fad' sol'8 sol'16 la' si'8. si'16 mi''8. mi''16 |
mi''8 mi'16\doux fa' sol'8 la'16 si' do''8. do''16 mi''8. mi''16 |
sol''8 do''16 re'' mi''8 mi''16 fa'' sol''8. sol''16 do'''8. do'''16 |
do'''8 la'16\fort si' do''8 do''16 re'' mi''8. mi''16 la''8. la''16 |
la''8 do''16\fort re'' mi''8 fad''16 sold'' la''8. la''16 do'''8. do'''16 |
fad''8 re''16\doux mi'' fad''8 fad''16 sol'' la''8. la''16 re'''8. re'''16 |
re'''2 r |
r8 si'16 do'' re''8. re''16 si'8. si'16 sol'8. sol'16 |
mi'4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { do''4 re'' mi'' | }
  { sol'4 sol' do'' | }
>>
r8 fad''16 sol'' la''8. la''16 do'''8. do'''16 mi''8. la''16 |
fad''4\trill \twoVoices #'(dessus1 dessus2 dessus) <<
  { la' si' do'' | }
  { fad' sol' la' | }
>>
R1*2 |
r8 re'16 mi' fad'8 fad'16 sol' la'8. la'16 re''8. re''16 |
sol''4 sol''8. sol''16 sol''4.( fad''8) |
\appoggiatura fad''8 sol'' si'16 do'' re''8. re''16 si'8. si'16 sol'8. sol'16 |
re'8 sol'16 la' si'8. si'16 re''8. re''16 sol''8. sol''16 |
sol''4 sol''8. sol''16 sol''4.( fad''8) |
\appoggiatura fad''8 sol''1 |
