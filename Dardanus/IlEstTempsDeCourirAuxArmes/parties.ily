\tag #'(haute-contre parties) \clef "haute-contre"
\tag #'taille \clef "taille"
R1 |
r2 \twoVoices #'(haute-contre taille parties) <<
  { si'4 si' |
    si' mi'' re''2 |
    re'' }
  { re'4 re' |
    mi' sol' sol' fad'\trill |
    sol'2 }
>> r4 re' |
re'2 r |
\twoVoices #'(haute-contre taille parties) <<
  { fad'2 s4 re' |
    dod'2 s |
    mi' s4 dod'' |
    re''2 }
  { la2 s4 la |
    la2 s |
    dod' s4 la' |
    la'2 }
  { s2 r4 s |
    s2 r |
    s r4 s | }
>> r4 re' |
re'2 r4 \twoVoices #'(haute-contre taille parties) <<
  { sol'4 |
    sol'2 s4 mi' |
    re' re' re' dod'\trill |
    re'1 | }
  { mi'4 |
    mi'2 s4 la |
    la si la sol |
    fad1 | }
  { s4 | s2 r4 s | }
>>
R1 |
r2 \twoVoices #'(haute-contre taille parties) <<
  { fad'4 fad' |
    si'2 la' |
    la' s4 fad' |
    sol'2 s |
    sol'2 s4 re' |
    do'2 s |
    mi' s4 la' |
    fad'2 s4 fad' |
    mi' mi' mi' red'\trill |
    mi'2 s |
    si'2 s4 sold' |
    la'2 s |
    la' s4 fad'\trill |
    sol'2 s |
    re'' s4 re'' |
    re''2 s4 do'' |
    do''2 s4 la' |
    sol' sol' sol' fad'\trill |
    sol'1~ |
    sol' |
    re'2 }
  { la4 la |
    si re' re' dod'\trill |
    re'2 s4 re' |
    re'2 s |
    re'2 s4 sol |
    sol2 s |
    do'2 s4 do' |
    si2 s4 si |
    si do' si la |
    sold2 s |
    mi'2 s4 mi' |
    mi'2 s |
    re' s4 re' |
    re'2 s |
    sol'2 s4 sol' |
    sol'2 s4 la' |
    la'2 s4 re' |
    re' mi' re' do' |
    si1 |
    re' |
    si2 }
  { s2 |
    s1 |
    s2 r4 s |
    s2 r |
    s2 r4 s |
    s2 r |
    s r4 s |
    s2 r4 s |
    s1 |
    s2 r |
    s r4 s |
    s2 r |
    s r4 s |
    s2 r |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s | }
>> r2 |
R1*17 | \allowPageTurn
r8 re'16 mi' fad'8 fad'16 sol' la'8. la'16 re''8. re''16 |
re''2 r |
R1 |
r8 sol'16 la' si'8. si'16 sol'8 sol'16 la' si'8. si'16 |
la'2. la'4 |
la'1~ |
la'2. la'4 |
sol'2. re''4 |
re''8 re'16 mi' fad'8 fad'16 sol' la'8. la'16 re''8. re''16 |
re''4. mi''8 dod''4.(\trill si'16 dod'') |
re''2 r |
r2 r4 re'' |
re''1 |
R1 |
r2 r4 sol' |
sol'1 |
R1 |
r2 r4 la' |
la'1 |
r4 re'8 mi' fad'4 sol'8 la' |
re'4 re'8 re' sol'4 fad'8 sol' |
la'2. si'4 |
mi''4 mi''8 mi'' re''2 |
re''8 sol'16 la' si'8. si'16 sol'8. sol'16 re'8. re'16 |
si8 sol16 la si8. si16 re'8. re'16 sol'8. sol'16 |
si'4 mi' fad'4.(\trill mi'16 fad') |
sol'4 r r2 |
R1*4 | \allowPageTurn
r4 r8 mi'' la'4. re''8 |
re''2 r4 r8 sol |
re'4 re'8 re' red'4\trill red'8 mi' |
mi'2. mi'4 |
fad' sol' red' mi' |
red'2.\trill mi'4 |
red'4.\trill si16 dod' red'8 red'16 mi' fad'8. fad'16 |
fad'2 r |
R1*2 |
r2 r4 la' |
la' la'8 sol' fad'2\trill |
mi'1 |
R1 |
r2 r4 mi'' |
mi''1~ |
mi''2. do''4 |
la'1\trill |
r8 re'16 mi' fad'8 fad'16 sol' la'8. la'16 re''8. re''16 |
re''2. sol'4 |
sol'2. sol'8 sol' |
do''2. do''4 |
do''2 r |
r4 re'8 mi' fad'4 sol'8 la' |
re'4 re'8 re' sol'4 fad'8 sol' |
la'2. si'4 |
mi''4 mi''8 mi'' re''2 |
re''8 sol'16 la' si'8. si'16 sol'8. sol'16 re'8. re'16 |
si8\trill sol16 la si8. si16 re'8. re'16 sol'8. sol'16 |
mi''4 mi''8 mi'' re''2 |
re''1 |
