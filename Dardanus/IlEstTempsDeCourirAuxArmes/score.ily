\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with {
        instrumentName = \markup\center-column { Hautbois Violons }
      } <<
        \new Staff <<
          { s1*52 s2 \noHaraKiri }
          \global \keepWithTag #'dessus1 \includeNotes "dessus"
        >>
        \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
      >>
      \new Staff \with { instrumentName = "Hautes-contre" } <<
        { s1*52 s2 \noHaraKiri }
        \global \keepWithTag #'haute-contre \includeNotes "parties"
      >>
      \new Staff \with { instrumentName = "Tailles" } <<
        { s1*35 \startHaraKiri }
        \global \keepWithTag #'taille \includeNotes "parties"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        { s1*52 s2 \noHaraKiri }
        \global \keepWithTag #'songe1 \includeNotes "voix"
      >> \keepWithTag #'songe1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*52 s2 \noHaraKiri }
        \global \keepWithTag #'songe2 \includeNotes "voix"
      >> \keepWithTag #'songe2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*52 s2 \noHaraKiri }
        \global \keepWithTag #'songe3 \includeNotes "voix"
      >> \keepWithTag #'songe3 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        { s1*52 s2 \noHaraKiri }
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*52 s2 \noHaraKiri }
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*52 s2 \noHaraKiri }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s1*52 s2 \noHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s1*34 s2\break s2
      }
      \origLayout {
        s1*6\break s1*6\break s1*6\pageBreak
        s1*7\break s1*6\break s1*3 s2\pageBreak
        s2 s1*4\break s1*4\pageBreak
        s1*4\break s1*3\pageBreak
        s1*4\pageBreak
        s1*4\pageBreak
        s1*5\pageBreak
        s1*5\pageBreak
        s1*5\pageBreak
        s1*4\pageBreak
        s1*6\pageBreak
        s1*4 s2 \bar "" \pageBreak
        s2 s1*4\pageBreak
        s1*4\pageBreak
        s1*6\pageBreak
        s1*4\pageBreak
        s1*4\pageBreak \grace s8
      }
    >>
  >>
  \layout { }
  \midi { }
}
