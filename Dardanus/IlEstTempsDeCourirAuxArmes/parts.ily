\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes dessus1)
   (violon2 #:notes "dessus" #:tag-notes dessus2)
   (hautbois #:notes "dessus" #:tag-notes dessus)
   (haute-contre #:notes "parties" #:tag-notes haute-contre)
   (taille #:notes "parties" #:tag-notes haute-contre)
   (bassons #:notes "basse")
   (basses #:notes "basse" #:score-template "score-basse-continue"))
