\tag #'songe1 {
  Il est temps de cou -- rir aux ar -- mes, aux ar -- mes, aux ar -- mes,
  il est temps de cou -- rir aux ar -- mes, aux ar -- mes, aux ar -- mes,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  hâ -- tez- vous, hâ -- tez- vous, aux ar -- mes, aux ar -- mes.

  Aux ar -- mes,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  aux ar -- mes, aux ar -- mes.

  Al -- lez, au mi -- lieu des a -- lar -- mes,
  cueil -- lir un im -- mor -- tel lau -- rier.

  Il est temps de cou -- rir aux ar -- mes,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  aux ar -- mes, aux ar -- mes, aux ar -- mes,
  aux ar -- mes, aux ar -- mes, aux ar -- mes,
  aux ar -- mes, aux ar -- mes, aux ar -- mes, aux ar -- mes.
}
\tag #'songe2 {
  Il est temps de cou -- rir aux ar -- mes,
  il est temps de cou -- rir aux ar -- mes,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  aux ar -- mes, aux ar -- mes.

  Hâ -- tez- vous gé -- né -- reux guer -- rier,
  aux ar -- mes, aux ar -- mes, aux ar -- mes,
  aux ar -- mes, aux ar -- mes.

  Al -- lez, au mi -- lieu des a -- lar -- mes,
  cueil -- lir un im -- mor -- tel lau -- rier.

  Il est temps de cou -- rir aux ar -- mes,
  aux ar -- mes, aux ar -- mes, aux ar -- mes, aux ar -- mes, aux ar -- mes,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  aux ar -- mes, aux ar -- mes, aux ar -- mes, aux ar -- mes.
}
\tag #'songe3 {
  Il est temps de cou -- rir aux ar -- mes, aux ar -- mes, aux ar -- mes,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  hâ -- tez- vous, hâ -- tez- vous,
  aux ar -- mes, aux ar -- mes.

  Hâ -- tez- vous gé -- né -- reux guer -- rier,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  aux ar -- mes, aux ar -- mes, aux ar -- mes.

  Al -- lez, au mi -- lieu des a -- lar -- mes,
  cueil -- lir un im -- mor -- tel lau -- rier.

  Il est temps de cou -- rir aux ar -- mes,
  aux ar -- mes, aux ar -- mes, aux ar -- mes, aux ar -- mes,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  hâ -- tez- vous gé -- né -- reux guer -- rier,
  hâ -- tez- vous,
  aux ar -- mes, aux ar -- mes, aux ar -- mes, aux ar -- mes, aux ar -- mes.
}

\tag #'vdessus {
  Il est temps de cou -- rir aux ar -- mes, aux ar -- mes, aux ar -- mes,
  il est temps de cou -- rir aux ar -- mes.

  Aux ar -- mes, aux ar -- mes, aux ar -- mes,
  hâ -- tez- vous, hâ -- tez- vous,
  aux ar -- mes, aux ar -- mes.

  Al -- lez, al -- lez, al -- lez, al -- lez, au mi -- lieu des a -- lar -- mes,
  cueil -- lir un im -- mor -- tel lau -- rier.

  Aux ar -- mes, aux ar -- mes, aux ar -- mes,
  aux ar -- mes, aux ar -- mes,
  hâ -- tez- vous, hâ -- tez- vous,
  aux ar -- mes, aux ar -- mes, aux ar -- mes, aux ar -- mes.
}
\tag #'vhaute-contre {
  Il est temps de cou -- rir aux ar -- mes, aux ar -- mes,
  aux ar -- mes, aux ar -- mes.

  Aux ar -- mes, aux ar -- mes, aux ar -- mes,
  hâ -- tez- vous, hâ -- tez- vous,
  hâ -- tez- vous, hâ -- tez- vous,
  aux ar -- mes, aux ar -- mes.

  Al -- lez, al -- lez, al -- lez, au mi -- lieu des a -- lar -- mes,
  cueil -- lir un im -- mor -- tel lau -- rier.

  Aux ar -- mes, aux ar -- mes, aux ar -- mes,
  aux ar -- mes, aux ar -- mes,
  hâ -- tez- vous, hâ -- tez- vous,
  hâ -- tez- vous, hâ -- tez- vous,
  aux ar -- mes, aux ar -- mes, aux ar -- mes, aux ar -- mes.
}
\tag #'vtaille {
  Il est temps de cou -- rir aux ar -- mes,
  il est temps de cou -- rir aux ar -- mes.

  Aux ar -- mes, aux ar -- mes, aux ar -- mes,
  hâ -- tez- vous, hâ -- tez- vous,
  aux ar -- mes, aux ar -- mes.

  Al -- lez, al -- lez, al -- lez, au mi -- lieu des a -- lar -- mes,
  cueil -- lir un im -- mor -- tel lau -- rier.

  Aux ar -- mes, aux ar -- mes, aux ar -- mes,
  aux ar -- mes, aux ar -- mes,
  hâ -- tez- vous, hâ -- tez- vous,
  aux ar -- mes, aux ar -- mes, aux ar -- mes, aux ar -- mes.
}
\tag #'vbasse {
  Il est temps de cou -- rir aux ar -- mes, aux ar -- mes, aux ar -- mes.

  Aux ar -- mes, aux ar -- mes, aux ar -- mes,
  hâ -- tez- vous, hâ -- tez- vous,
  aux ar -- mes, aux ar -- mes.

  Al -- lez, al -- lez,
  al -- lez cueil -- lir un im -- mor -- tel lau -- rier.

  Aux ar -- mes, aux ar -- mes,
  aux ar -- mes, hâ -- tez- vous,
  aux ar -- mes, hâ -- tez- vous,
  aux ar -- mes,
  hâ -- tez- vous, hâ -- tez- vous,
  aux ar -- mes, aux ar -- mes, aux ar -- mes, aux ar -- mes.
}
