<<
  \tag #'haute-contre {
    \clef "haute-contre"
    r4 sol'2~ |
    sol' fad'4 |
    sol'2 sol'4 |
    sol'2 fad'4 |
    sol' sol'2~ |
    sol' fad'4 |
    sol'2 sol'4 |
    sol' fad'4. sol'8 |
    sol'4 si'8 do'' re'' mi'' |
    re''4 la'2 |
    sol'4. la'8 si' do'' |
    si'4 fad'2 |
    mi'4. fad'8 sol' la' |
    si'2 re'4 |
  }
  \tag #'taille {
    \clef "taille"
    r4 re'2~ |
    re' do'4 |
    si2 si4 |
    la2 re'4 |
    re' re'2~ |
    re' do'4 |
    si2 si4 |
    la do'2 |
    si4 si'8 do'' re'' sol' |
    sol' si' la'4 fad' |
    sol'4. la'8 si' mi' |
    mi' sol' fad'4 si'~ |
    si'2 la'4~ |
    la'2 sol'4 |
  }
>>
mi'8 re' do'4 si |
la4 si'8 do''
<<
  \tag #'haute-contre {
    re''8 mi'' |
    re''4 la'2 |
    sol' sol'4 |
    fad'2 fad'4 |
    mi'2 mi'4 |
    re'2 re'4 |
  }
  \tag #'taille {
    re''8 sol' |
    sol'4 fad'2 |
    fad' mi'4 |
    mi'2 re'4 |
    re'2 do'4 |
    do'2 si4 |
  }
>>
mi'8 re' do'4 si |
la2 r4 |
re'4 la la' |
mi'2 mi''4 |
re''2~ re''8. si'16 |
\appoggiatura si'8 do''2( sol'4) |
R2. |
r4 r mi'' |
mi''2 mi''4 |
la'2 re'8. si16 |
do'8. sol'16 fad'4.\trill sol'8 |
sol'2 r4 |
re' la la' |
mi'2 mi''4 |
re''2~ re''8. si'16 |
do''2 sol'4 |
R2. |
r4 r mi'' |
mi''2 mi''4 |
la'2 re'8. si16 |
do'8. sol'16 fad'4.\trill sol'8 |
sol'4 fad' mi' |
fad'2 fad'4 |
mi'2.~ |
mi' |
mi' |
mi' |
mi'2 fad'4 |
si2 mi'4 |
red'\trill \appoggiatura dod'8 si2 |
R2.*4 | \allowPageTurn
r8 r16 si' si'4. si'8 |
mi''8. re''16 do'' re'' do'' si' la'8. la'16 |
la'8. la'16 la'4. la'8 |
re''8. do''16 si' do'' si' la' sol'8. sol'16 |
sol'2~ sol'16 si' la' sol' |
fad'8. sol'16 fad'4.\trill sol'8 |
sol'2 r4 |
R2. |
r8 r16 dod'' dod''4. dod''8 |
fad'4 re'' dod'' |
si' lad'4.\trill si'8 |
si'4 <>\tresfort <<
  \tag #'haute-contre {
    sol'2~ |
    sol'2 fad'4 |
    sol'2 sol'4 |
    sol'2 fad'4 |
    sol'4 sol'2~ |
    sol' fad'4 |
    sol'2 sol'4 |
    sol' fad'4. sol'8 |
    sol'4 si'8 do'' re'' mi'' |
    re''4 la'2 |
    sol'4. la'8 si' do'' |
    si'4 fad'2 |
    mi'4. fad'8 sol' la' |
    si'2 re'4 |
    mi'8 re' do'4 si |
    la si'8 do'' re'' mi'' |
    re''4 la'2 |
    sol' sol'4 |
    fad'2 fad'4 |
    mi'2 mi'4 |
    re'2 re'4 |
  }
  \tag #'taille {
    re'2~ |
    re' do'4 |
    si2 si4 |
    la2 re'4 |
    re' re'2 |
    re' do'4 |
    si2 si4 |
    la do'2 |
    si4 si'8 do'' re'' sol' |
    sol' si' la'4 fad' |
    sol'4. la'8 si' mi' |
    mi' sol' fad'4 si'~ |
    si'2 la'4 |
    la'2 sol'4 |
    mi'8 re' do'4 si |
    re'4 si'8 do'' re'' sol' |
    sol'4 fad'2 |
    fad' mi'4 |
    mi'2 re'4 |
    re'2 do'4 |
    do'2 si4 |
  }
>>
mi'8 re' do'4 si |
la2 r4 |
re' la la' |
mi'2 mi''4 |
re''2~ re''8. si'16 |
\appoggiatura si'8 do''2 sol'4 |
R2. |
r4 r mi'' |
mi''2 mi''4 |
la'2 re'8. si16 |
do'8. sol'16 fad'4.\trill sol'8 |
sol'4 r r |
re'4 la la' |
mi'2 mi''4 |
re''2~ re''8. si'16 |
do''2 sol'4 |
R2. |
r4 r mi'' |
mi''2 mi''4 |
la'2 re'8. si16 |
do'8. sol'16 fad'4.\trill sol'8 |
<<
  \tag #'haute-contre {
    sol'2 r8*4/3 <>\fortSug re''8*2/3 |
    re''2 r8*4/3 re''8*2/3 |
    re''2 r8*4/3 dod''8*2/3 |
    re''2 r8*4/3 fad'8*2/3 |
    sol'2 r8*4/3 re''8*2/3 |
    re''2 r8*4/3 re''8*2/3 |
    re''2 r8*4/3 dod''8*2/3 |
    \smallNotes re''2 r8*4/3 fad'8*2/3 |
    sol'2
  }
  \tag #'taille {
    re'2 r8*4/3 <>\fortSug  re'8*2/3 |
    re'2 r8*4/3 la'8*2/3 |
    si'2 r8*4/3 la'8*2/3 |
    la'2 r8*4/3 re'8*2/3 |
    re'2 r8*4/3 re'8*2/3 |
    re'2 r8*4/3 la'8*2/3 |
    si'2 r8*4/3 la'8*2/3 |
    la'2 r8*4/3 re'8*2/3 |
    re'2
  }
>> r8 sol' |
sol'2 r8*4/3 do''8*2/3 |
la'2\trill r8*4/3 do''8*2/3 |
si'8. do''16 la'4.\trill sol'8 |
sol'2 r8*4/3 sol'8*2/3 |
sol'2 r8*4/3 do''8*2/3 |
la'2\trill r8*4/3 fad'8*2/3 |
sol'8. la'16 fad'4.\trill sol'8 |
\new CueVoice {
  sol'4 si' la' |
  sol' do' re'8 mi' |
  re'2 sol'4 |
  sol'2 fad'4\trill |
  sol' si' la' |
  sol' do' re'8 mi' |
  re'2 sol'4 |
  sol'2 fad'4\trill |
  sol'2~ sol'8. re''16 |
  re''4. la'8 fad'8. la'16 |
  \appoggiatura la'8 si'2~ si'8. la'16 |
  la'4 dod''4.( si'16 dod'') |
  re''2~ re''8. re''16 |
  re''4. la'8 fad'8. la'16 |
  \appoggiatura la'8 si'2~ si'8. la'16 |
  la'4 dod''4.( si'16 dod'') |
  re''2 r4 |
  r r fa' |
  mi'2 la'4 |
  re'8. sol'16 fad'4.\trill sol'8 |
  sol'4 r16 re' do' si la re' sol re' |
  la4 la'8 si' fad' fa' |
  mi'2 la'4 |
  re'8 sol' fad'!4.\trill sol'8 |
}
sol'4 %{ \afterGrace sol'2( re'8) |
re'4 r r |
r \afterGrace sol'2( re'8) |
re'4 r r |
r r sol'8. fa'16 |
mib'4 re' do' |
sib2 do'4 |
re'2 re4 |
sol \afterGrace sol'2( re'8) |
re'4 r r |
r \afterGrace sol'2( re'8) |
re'4 r r |
r r sol'8 fa' |
mib'4 re' do' |
sib2 do'4 |
re'2 re4 |
sol r r |
R2.*8 | \allowPageTurn
\new CueVoice {
  r8 r16 la'\fort la'4. la'8 |
  do''2 do''8. do''16 |
  fa'2~ fa'8 sib' |
  sib'4 la'4.\trill sib'8 |
  sib'4 r r |
  r r la'\doux |
  la' r r |
  r r sol' |
  sol'2~ sol'8. fa'16 |
  mib'8. do'16 si4.\trill do'8 |
  do'4
} do''4 sib'! |
la'( sol')\trill fad' |
r sol'\trill fad' |
r sol'\trill fad' |
r sol'\trill fad' |
mib'2 mib'4\trill |
%%
re' <>\fort <<
  \tag #'haute-contre {
    sol'2~ |
    sol'2 fad'4 |
    sol'2 sol'4 |
    sol'2 fad'4 |
    sol' sol'2~ |
    sol' fad'4 |
    sol'2 sol'4 |
    sol' fad'4. sol'8 |
    sol'16 re' mi' fad' sol' la' si' do'' re''8 la' |
    si'16 si si si
  }
  \tag #'taille {
    re'2~ |
    re' do'4 |
    si2 si4 |
    la2 re'4 |
    re' re'2~ |
    re' do'4 |
    si2 si4 |
    la do'2 |
    si16 re' mi' fad' sol' la' si' do'' re''8 fad' |
    sol'16 si si si
  }
>> si8 do' re' mi' |
re'16 re' re' re' re'8 sol' fad' sol' |
sol'2 re'4 |
re'16 re' mi' fad' sol' la' si' do'' re''8 la' |
si'16 si si si si8 do' re' mi' |
re'16 re' re' re' re'8 sol' fad' sol' |
sol'2 re'4 |
re'4~ re'8. re''16 re''8. re''16 |
re''8. re''16 fad'4~ fad'8. la'16 |
si'2~ si'8. si'16 |
mi'2 dod''4\trill |
re''8. fad'16 sol'4. si'8 |
re''8. re''16 fad'4~ fad'8. la'16 |
si'2~ si'8. si'16 |
mi'2 dod''4\trill |
re'' %} r4 r |
R2.*6 |
r8 r16 re'\fort fad'8. la'16 re''4 |
r8 r16 re' sol'8. si'16 re''4~ |
re''4 do''16 si' la' sol' do'' si' la'8~ |
la'8. sol'16 fad'4.\trill sol'8 |
sol'4 r r |
R2.*6 |
r8 r16 re' fad'8. la'16 re''4 |
r8 r16 re' sol'8. si'16 re''4~ |
re'' do''16 si' la' sol' do'' si' la'8~ |
la' sol'4 fad' sol'8 |
sol'2. |
