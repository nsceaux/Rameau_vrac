\score {
  \new StaffGroup <<
    %\new Staff \with {
    %  \haraKiriFirst
    %  shortInstrumentName = "Fl."
    %} << \global \includeNotes "flute" >>
    \new Staff \with {
      instrumentName = \markup\center-column { Flûtes Hautbois }
      shortInstrumentName = \markup\center-column { Fl. Hb. }
    } <<
      \global \keepWithTag #'hautbois \includeNotes "dessus"
      \origLayout {
        s2.*8\break s2.*7\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*9\break s2.*6\pageBreak
        s2.*5\break s2.*7\pageBreak
        s2.*8\break s2.*7\pageBreak
        s2.*7\break \grace s8 s2.*7\pageBreak
        s2.*7\break s2.*6\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*5\break s2.*5\break \grace s8 s2.*5\pageBreak
        s2.*4\break s2.*5\break s2.*10\pageBreak
        s2.*10\break s2.*8\pageBreak
        s2.*8\break s2.*8\pageBreak
        s2.*7\break s2.*5\pageBreak
        s2.*5\break s2.*7\pageBreak
        s2.*7\break s2.*8\pageBreak
      }
    >>
    \new GrandStaff \with {
      instrumentName = "Violons"
      shortInstrumentName = "Vn."
    } <<
      \new Staff << \global \keepWithTag #'violon1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'violon2 \includeNotes "dessus" >>
    >>
    \new Staff \with {
      instrumentName = "Hautes-contre"
      shortInstrumentName = "Hc."
    } << \global \keepWithTag #'haute-contre \includeNotes "parties" >>
    \new Staff \with {
      instrumentName = "Tailles"
      shortInstrumentName = "T."
    } << \global \keepWithTag #'taille \includeNotes "parties" >>
    \new Staff \with {
      instrumentName = "Bassons"
      shortInstrumentName = "Bn."
    } <<
     %% { s2.*214 <>^\markup\whiteout\italic { Recommandez la douceur aux bassons } }
      \global \keepWithTag #'basson \includeNotes "basse"
    >>
    \new Staff \with {
      instrumentName = "Basses"
      shortInstrumentName = "B."
    } << \global \keepWithTag #'basse \includeNotes "basse" >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 8\mm
  }
  \midi { }
}
