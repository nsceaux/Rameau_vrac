\clef "dessus" r4 si'2~ |
si'8 la'16( si') si'4.\trill la'8 |
sol' la' si' do'' re'' si' |
mi''4 do''4.(\trill si'16 do'') |
si'4\trill si'2~ |
si'8 la'16( si') si'4.\trill la'8 |
sol' la' si' do'' re'' si' |
mi''4 la'4.\trill sol'8 |
sol'4 sol''4. si'8 |
la' sol'' fad'' mi'' re'' do'' |
si'\trill la' sol' si' mi'' sol' |
fad' mi'' re'' do'' si' la' |
sol'\trill fad' mi' sol' do'' mi' |
re' do'' si' la' sol' fad' |
mi'4 fad' sol' |
fad'4\trill sol''4. si'8 |
la' sol'' fad'' mi'' re'' do'' |
si'\trill la' sol' si' mi'' sol' |
fad' mi'' re'' do'' si' la' |
sol'\trill fad' mi' sol' do'' mi' |
re' do'' si' la' sol' fad' |
mi'4 fad' sol' |
fad'4\trill <<
  \tag #'(hautbois1 hautbois2 hautbois) {
    r4 r |
    R2.*27 |
    r4 <>\doux \twoVoices #'(hautbois1 hautbois2 hautbois) <<
      { fad''4 sol'' |
        la'' fad'' sol'' |
        fad''\trill fad' sol' |
        la' fad' sol' |
        fad'8.\trill }
      { red''4 mi'' |
        fad'' red'' mi'' |
        red''\trill red' mi' |
        fad' red' mi' |
        red'8. }
    >>
  }
  \tag #'(violon1 violon2) {
    <<
      \tag #'violon1 {
        re'8. mi'16 fad'8. sol'16 |
        la'8. si'16 do''8. re''16 mi''8. fad''16 |
        sol''2.~ |
        sol''~ |
        sol''8. sol'16 do''8. sol'16 re''8. sol'16 |
      }
      \tag #'violon2 {
        r4 re'8. mi'16 |
        fad'8. sol'16 la'8. si'16 do''8. re''16 |
        \appoggiatura do''8 si'2~ si'8. la'16 |
        sol'2.~ |
        sol'8. sol'16 sol'8. do''16 re''8. sol'16 |
      }
    >>
    mi''8. sol'16 do''8. sol'16 re''8. sol'16 |
    mi''8. do''16 mi''8. do''16 sol''8. mi''16 |
    do'''8. la''16 mi''8. fad''16 sol''8. la''16 |
    fad''8.\trill mi''16 re''8. do''16 si'8.\trill la'16 |
    sol'8. do''16 la'4.\trill sol'8 |
    sol'4 <<
      \tag #'violon1 {
        re'8. mi'16 fad'8. sol'16 |
        la'8. si'16 do''8. re''16 mi''8. fad''16 |
        sol''2.~ |
        sol''~ |
        sol''8. sol'16 do''8. sol'16 re''8. sol'16 |
      }
      \tag #'violon2 {
        r4 re'8. mi'16 |
        fad'8. sol'16 la'8. si'16 do''8. re''16 |
        \appoggiatura do''8 si'2~ si'8. la'16 |
        sol'2.~ |
        sol'8. sol'16 do''8. sol'16 re''8. sol'16 |
      }
    >>
    mi''8. sol'16 do''8. sol'16 re''8. sol'16 |
    mi''8. do''16 mi''8. do''16 sol''8. mi''16 |
    do'''8. la''16 mi''8. fad''16 sol''8. la''16 |
    fad''8.\trill mi''16 re''8. do''16 si'8.\trill la'16 |
    sol'8. do''16 la'4.\trill sol'8 |
    sol'4 si'\doux mi'' |
    mi''4. fad''16 mi'' red''4 sol'' do'' si'\trill |
    \appoggiatura si'8 do''4.( re''16 do'' si'4) |
    mi'' la' sold'\trill |
    \appoggiatura sold'8 la'4.( si'16 la' sold'4) |
    do''4 si' la' |
    sol'! fad' mi' |
    red'\trill r r |
    r si'\doux si' |
    si'2.~ |
    si'~ |
    si'8.
  }
>> si''16\fort si''4. si''8 |
sold''16 la'' si'' la'' sold'' fad'' mi'' re''! mi'' re'' do'' si' |
do''8. la''16 la''4. la''8 |
fad''16 sol''! la'' sol'' fad'' mi'' re'' do'' re'' do'' si' la' |
si'8. sol''16 sol''4. sol''8 |
mi''16 fad'' sol'' fad'' mi'' re'' do'' si' do'' re'' do'' si' |
la'8. si'16 la'4.\trill sol'8 |
sol'2 r4 |
r8 r16 mi'' mi''4. mi''8 |
mi''4~ mi''16 sol'' fad'' mi'' fad'' mi'' re'' dod'' |
re'' dod'' si' dod'' si' dod'' re'' mi'' fad'' sol'' la'' fad'' |
si''8. re''16 dod''4.\trill si'8 |
si'4 si'2\tresfort ~ |
si'8 la'16( si') si'4.\trill la'8 |
sol' la'[ si' do'' re'' si'] |
mi''4 do''4.(\trill si'16 do'') |
si'4\trill si'2~ |
si'8 la'16( si') si'4.\trill la'8 |
sol' la' si' do'' re'' si' |
mi''4 la'4.\trill sol'8 |
sol'4 sol''4. si'8 |
la' sol'' fad'' mi'' re'' do'' |
si'\trill la' sol' si' mi'' sol' |
fad' mi'' re'' do'' si' la' |
sol'\trill fad' mi' sol' do'' mi' |
re' do'' si' la' sol' fad' |
mi'4 fad' sol' |
fad'4\trill sol''4. si'8 |
la' sol'' fad'' mi'' re'' do'' |
si'\trill la' sol' si' mi'' sol' |
fad' mi'' re'' do'' si' la' |
sol'\trill fad' mi' sol' do'' mi' |
re' do'' si' la' sol' fad' |
mi'4 fad' sol' |
fad'\trill <<
  \tag #'(hautbois1 hautbois2 hautbois) { r4 r | R2.*19 | \allowPageTurn r8*2/3 }
  \tag #'(violon1 violon2) {
    <<
      \tag #'violon1 {
        re'8. mi'16 fad'8. sol'16 |
        la'8. si'16 do''8. re''16 mi''8. fad''16 |
        sol''2.~ |
        sol''~ |
        sol''8 sol' do'' sol' re'' sol' |
      }
      \tag #'violon2 {
        r4 re'8. mi'16 |
        fad'8. sol'16 la'8. si'16 do''8. re''16 |
        \appoggiatura do''8 si'2~ si'8. la'16 |
        sol'2.~ |
        sol'8 sol' do'' sol' re'' sol' |
      }
    >>
    mi'' sol' do'' sol' re'' sol' |
    mi'' do'' mi'' do'' sol'' mi'' |
    do''' la'' mi'' fad'' sol'' la'' |
    fad''\trill mi'' re'' do'' si'\trill la' |
    sol'8. do''16 la'4.\trill sol'8 |
    sol'4 <<
      \tag #'violon1 {
        re'8. mi'16 fad'8. sol'16 |
        la'8. si'16 do''8. re''16 mi''8. fad''16 |
        sol''2.~ |
        sol''~ |
        sol''8 sol' do'' sol' re'' sol' |
      }
      \tag #'violon2 {
        r4 re'8. mi'16 |
        fad'8. sol'16 la'8. si'16 do''8. re''16 |
        \appoggiatura do''8 si'2~ si'8. la'16 |
        sol'2.~ |
        sol'8 sol' do'' sol' re'' sol' |
      }
    >>
    mi'' sol' do'' sol' re'' sol' |
    mi'' do'' mi'' do'' sol'' mi'' |
    do''' la'' mi'' fad'' sol'' la'' |
    fad''\trill mi'' re'' do'' si'\trill la' |
    sol'8. do''16 la'4.\trill sol'8 |
    sol'8*2/3
  }
>> si''8*2/3\fort sol'' re'' si' sol' re' sol' si' |
la' la'' fad'' re'' la' fad' re' fad' la' |
sol' sol'' mi'' si' sol' mi' dod' mi' sol' |
fad' fad'' re'' la' fad' re' la la' do''! |
si' si'' sol'' re'' si' sol' re' sol' si' |
la' la'' fad'' re'' la' fad' re' fad' la' |
sol' sol'' mi'' si' sol' mi' dod' mi' sol' |
fad' fad'' re'' la' fad' re' la la' do''! |
si'4 r r8*2/3 re''( si') |
mi''2 r8*2/3 mi''( la'') |
fad''2\trill r8*4/3 la''8*2/3 |
re''8. sol''16 fad''4.\trill sol''8 |
sol''2 r8*2/3 re'' si' |
mi''2 r8*2/3 mi'' la'' |
fad''4\trill la'' r8*4/3 re''8*2/3 |
si'8. do''16 la'4.\trill sol'8 |
sol'16 re' mi' fad' sol' la' si' do'' re'' mi'' fad'' re'' |
sol'' sol' la' sol' la' sol' la' sol' si' sol' do'' sol' |
re'' sol' la' sol' la' sol' la' sol' do'' sol' re'' sol' |
mi'' sol' re'' sol' do'' sol' si' sol' la' fad' re''8 |
si'16\trill re' mi' fad' sol' la' si' do'' re'' mi'' fad'' re'' |
sol'' sol' la' sol' la' sol' la' sol' si' sol' do'' sol' |
re'' sol' la' sol' la' sol' la' sol' do'' sol' re'' sol' |
mi'' sol' re'' sol' do'' sol' si' sol' la' fad' re''8 |
si'8.\trill re''16 si''4~ si''8. sol''16 |
\appoggiatura sol''8 la''4~ la''16 sol'' fad'' mi'' re'' fad'' mi'' fad'' |
sol''2.~ |
sol''16 fad'' mi'' re'' mi'' re'' dod'' si' la'8. la''16 |
fad''8.\trill re''16 si''4~ si''8. sol''16 |
\appoggiatura sol''8 la''4~ la''16 sol'' fad'' mi'' re'' fad'' mi'' fad'' |
sol''2.~ |
sol''16 fad'' mi'' re'' mi'' re'' dod'' si' la'8. la''16 |
fad'' re'' do''! si' la' re'' sol' re'' fad' re'' mi' re'' |
re' re'' mi' re'' fad' re'' sol' re'' la' re'' si' re'' |
do'' si' la' si' do'' re'' do'' re'' mi'' \smallNotes fad'' mi'' fad'' |
sol''8. si'16 la'4.\trill sol'8 |
sol'16 re'' do'' si' la' re'' sol' re'' fad' re'' mi' re'' |
re' re'' mi' re'' fad' re'' sol' re'' la' re'' si' re'' |
do'' si' la' si' do'' re'' do'' re'' mi'' fad'' mi'' fad'' |
sol''8. si'16 la'4.\trill sol'8 |
%%%
sol'4 %{<<
  \tag #'(hautbois1 hautbois2 hautbois) { r4 r | R2.*24 | }
  \tag #'(violon1 violon2) {
    <<
      \tag #'violon1 {
        \afterGrace sib'2( la'8) |
        la'4 r r |
        sol'4 \afterGrace sib'2( la'8) |
        la'4 r r |
      }
      \tag #'violon2 {
        \afterGrace sol'2( fad'8) |
        fad'4 r r |
        sol' \afterGrace sol'2( fad'8) |
        fad'4 r r |
      }
    >>
    r4 r sib'8. la'16 |
    do''4 sib'\trill la' |
    r re''4. sol'8 |
    fad'4\trill sol' la'\trill |
    \appoggiatura la'8 sib'4 <<
      \tag #'violon1 {
        \afterGrace sib'2( la'8) |
        la'4 r r |
        r \afterGrace sib'2( la'8) |
        la'4 r r |
      }
      \tag #'violon2 {
        \afterGrace sol'2( fad'8) |
        fad'4 r r |
        r \afterGrace sol'2( fad'8) |
        fad'4 r r |
      }
    >>
    r4 r sib'8 la' |
    do''4 sib'\trill la' |
    r re''4. sol'8 |
    fad'4\trill sol' la'\trill |
    \appoggiatura la'8 sib'4 <<
      \tag #'violon1 {
        mib''4 re'' |
        do''4.\trill re''8 si'4\trill |
        r do'' si'\trill |
        r do'' si'\trill |
        r mib'' re'' |
        do''4.\trill( re''8) sib'!4\trill |
        r la' sib' |
        r la' sib' |
        la'2 r4 |
      }
      \tag #'violon2 {
        sol'4 fa'! |
        mib'4.(\trill fa'8) re'4\trill |
        r mib'\trill re' |
        r mib'\trill re' |
        r sol' fa' |
        mib'4.(\trill fa'8) re'4\trill |
        r do' re' |
        r do' re' |
        do'2 r4 |
      }
    >>
  }
>>
r8 r16 <>^"Tous" do''\fort fa''4. fa''8 |
la''4~ la''16 sib'' la'' sol'' fa'' la'' sol'' la'' |
sib''4~ sib''16 la'' sol'' fa'' mib'' re'' do'' sib' |
sol''4 do''4.\trill sib'8 |
sib'4 <<
  \tag #'(hautbois1 hautbois2 hautbois) { r4 r | R2.*11 | r4 }
  \tag #'(violon1 violon2) {
    <<
      \tag #'violon1 {
        re''4.\doux( dod''8) |
        dod''4 mi'' sol'' |
        sol''8.( fad''16)\trill do''4( si') |
        si' re'' fa''~ |
        fa''8.( mib''16)\trill mib'' fa'' mib'' re'' mib'' re'' do'' si' |
        do''8. re''16 re''4.\trill do''8 |
        do''4 r r |
        R2. |
        re'' |
        re'' |
        re''2~ re''8. re''16 |
        sol''2~ sol''8 fad''16 sol'' |
        fad''4\trill
      }
      \tag #'violon2 {
        r4 sib'\doux |
        sib'8.( la'16) la'4 dod''\trill |
        \appoggiatura dod''8 re''4 r lab' |
        lab'8.( sol'16) sol'4 si'\trill |
        \appoggiatura si'8 do''2 sol'4~ |
        sol'8 fa'16 lab' sol'4 fa' |
        mib' mib'' re'' |
        do'' sib'\trill la' |
        r sib'\trill la' |
        r sib'\trill la' |
        r sib'\trill la' |
        sib'4. sib'8 do''4\trill |
        \appoggiatura do''8 re''4
      }
    >>
  }
>> %%
si'2\fort~ |
si'8 la'16( si') si'4.\trill la'8 |
sol' la' si' do'' re'' si' |
mi''4 do''4.(\trill si'16 do'') |
si'4\trill si'2~ |
si'8 la'16( si') si'4.\trill la'8 |
sol' la' si' do'' re'' si' |
mi''4 la'4.\trill sol'8 |
sol'16 re' mi' fad' sol' la' si' do'' re'' mi'' fad'' re'' |
sol'' sol' la' sol' la' sol' la' sol' si' sol' do'' sol' |
re'' sol' la' sol' la' sol' la' sol' do'' sol' re'' sol' |
mi'' sol' re'' sol' do'' sol' si' sol' la' fad' re''8 |
si'16\trill re' mi' fad' sol' la' si' do'' re'' mi'' fad'' re'' |
sol'' sol' la' sol' la' sol' la' sol' si' sol' do'' sol' |
re'' sol' la' sol' la' sol' la' sol' do'' sol' re'' sol' |
mi'' sol' re'' sol' do'' sol' si' sol' la' fad' re''8 |
si'8.\trill re''16 si''4~ si''8. sol''16 |
la''4~ la''16 sol'' fad'' mi'' re'' fad'' mi'' fad'' |
sol''2.~ |
sol''16 fad'' mi'' re'' mi'' re'' dod'' si' la'8. la''16 |
fad''8.\trill re''16 si''4~ si''8. sol''16 |
la''4~ la''16 sol'' fad'' mi'' re'' fad'' mi'' fad'' |
sol''2.~ |
sol''16 fad'' mi'' re'' mi'' re'' dod'' si' la'8. la''16 |
fad''4\trill %} <<
  \tag #'(hautbois1 hautbois2 hautbois) { r4 r | R2.*6 }
  \tag #'(violon1 violon2) {
    re''4\doux do''! |
    si'( la')\trill sol' |
    sol'( fad')\trill mi' |
    mi'( dod') re' |
    re'( dod') re' |
    re'( si) do'! |
    do'2. |
  }
>>
la''2\fort ~ la''16*4/6 sol'' fad'' mi'' re'' do'' |
si'4 sol''2~ |
sol''8. fad''16 mi'' re'' do'' si' la' sol' fad' mi' |
re'8. si'16 la'4.\trill sol'8 |
sol'4 <<
  \tag #'(hautbois1 hautbois2 hautbois) { r4 r | R2.*6 }
  \tag #'(violon1 violon2) {
    re''4\doux do'' |
    si'( la')\trill sol' |
    sol'( fad')\trill mi' |
    mi'( dod') re' |
    re'( dod') re' |
    re'( si) do'! |
    do'2. |
  }
>>
la''2\fort ~ la''16*4/6 sol'' fad'' mi'' re'' do'' |
si'4 sol''2~ |
sol''8. fad''16 mi'' re'' do'' si' la' sol' fad' mi' |
re'8. si'16 la'4.\trill sol'8 |
sol'2. |
