\clef "basse" r4 r sol |
re re, re |
mi mi, re |
do re do, |
sol,2 sol4 |
re re, re |
mi mi, re |
do re re, |
sol, sol8 la si do' |
re'2 re4 |
mi4. fad8 sol la |
si2 si,4 |
do4. re8 mi fad |
sol2 si,4 |
do8 si, la,4\trill sol, |
re4 sol8 la si\trill do' |
re'2 re4 |
mi4. fad8 sol la |
si2 si,4 |
do4. re8 mi fad |
sol2 si,4 |
do8 si, la,4\trill sol, |
re2 r4 |
R2. |
<<
  \tag #'basson {
    \clef "tenor" r4 sol8. la16 si8. do'16 |
    re'8. mi'16 fa'8. mi'16 fa'8. sol'16 |
    mi'2\trill sol'4 |
    sol'2.~ |
    sol'2 do'8. si16 | \clef "basse"
  }
  \tag #'basse {
    r4 mi8. fad16 sol8. la16 |
    si8. do'16 re'8. do'16 si8. sol16 |
    do'2 si4\trill |
    \appoggiatura si8 do'2 si4\trill |
    \appoggiatura si8 do'2~ do'8. si16 |
  }
>>
la2 la,4 |
re4. mi8 fad8. re16 |
mi8. do16 re4 re, |
sol, r r |
R2. |
<<
  \tag #'basson {
    \clef "tenor" \grace s8 r4 sol8. la16 si8. do'16 |
    re'8. mi'16 fa'8. mi'16 fa'8. sol'16 |
    mi'2\trill sol'4 |
    sol'2.~ |
    sol'2 do'8. si16 | \clef "basse"
  }
  \tag #'basse {
    r4 mi8. fad16 sol8. la16 |
    si8. do'16 re'8. do'16 si8. sol16 |
    do'2 si4\trill |
    \appoggiatura si8 do'2 si4\trill |
    \appoggiatura si8 do'2~ do'8. si16 |
  }
>>
la2 la,4 |
re4. mi8 fad8. re16 |
mi8. do16 re4 re, |
sol, r <>\doux <<
  \tag #'basson {
    si4 |
    la2 do'4 |
    si la sold\trill |
    \appoggiatura sold8 la4.( si16 la sold4) |
    do'4 do' si\trill |
    \appoggiatura si8 do'4.( re'16 do') si4 |
    mi'4 re' do' |
    si la sol |
    fad\trill si si |
    si2. |
    r4 si,, si,, |
    si,,2.~ |
    si,,4 r r |
  }
  \tag #'basse {
    sol4 |
    fad2 fad4 |
    mi2.~ |
    mi~ |
    mi~ |
    mi2 mi4 |
    la,2. |
    si,4 do la, |
    si,2.~ |
    si,~ |
    si,~ |
    si,~ |
    si,4 r r |
  }
>>
r8 r16 mi'\fort mi'4. mi'8 |
la16 si do' si la si la sol fad sol fad mi |
re8. re'16 re'4. re'8 |
sol16 la si la sol la sol fad mi fad mi re |
do2 la,4 |
re8. sol,16 re4 re, |
sol,8. mi'16 mi'4. mi'8 |
dod'16 re' mi' re' dod' re' mi' re' dod' re' dod' si |
lad2 fad4 |
si2 la!4 |
sol8. mi16 fad4 fad, |
si, r sol\tresfort |
re'2 re4 |
mi2 si,4 |
do re re, |
sol,2 sol4 |
re re, re |
mi mi, re |
do re re, |
sol, sol8 la si do' |
re'2 re4 |
mi4. fad8 sol la |
si2 si,4 |
do4. re8 mi fad |
sol2 si,4 |
do8 si, la,4\trill sol, |
re sol8 la si\trill do' |
re'2 re4 |
mi4. fad8 sol la |
si2 si,4 |
do4. re8 mi fad |
sol2 sol,4 |
do8 si, la,4\trill sol, |
re2 r4 |
R2. |
<<
  \tag #'basson {
    \clef "tenor" \grace s8 r4 sol8. la16 si8. do'16 |
    re'8. mi'16 fa'8. mi'16 fa'8. sol'16 |
    mi'2\trill sol'4 |
    sol'2.~ |
    sol'2 do'8. si16 | \clef "basse"
  }
  \tag #'basse {
    r4 mi8. fad16 sol8. la16 |
    si8. do'16 re'8. do'16 si8. sol16 |
    do'2 si4\trill |
    \appoggiatura si8 do'2 si4\trill |
    \appoggiatura si8 do'2~ do'8. si16 |
  }
>>
la2 la,4 |
re4. mi8 fad8. re16 |
mi8. do16 re4 re, |
sol, r r |
R2. |
<<
  \tag #'basson {
    \clef "tenor" \grace s8 r4 sol8. la16 si8. do'16 |
    re'8. mi'16 fa'8. mi'16 fa'8. sol'16 |
    mi'2\trill sol'4 |
    sol'2.~ |
    sol'2 do'8. si16 | \clef "basse"
  }
  \tag #'basse {
    r4 mi8. fad16 sol8. la16 |
    si8. do'16 re'8. do'16 si8. sol16 |
    do'2 si4\trill |
    \appoggiatura si8 do'2 si4\trill |
    \appoggiatura si8 do'2~ do'8. si16 |
  }
>>
la2 la,4 |
re4. mi8 fad8. re16 |
mi8. do16 re4 re, |
sol,2 r8*4/3 sol8*2/3\fortSug |
fad2 fad4 |
mi2 r8*4/3 la8*2/3 |
re2 r8*4/3 re8*2/3 |
sol,2 r8*4/3 sol8*2/3 |
fad2 r8*4/3 fad8*2/3 |
mi2 r8*4/3 la8*2/3 |
re2 r8*4/3 re8*2/3 |
sol,8*2/3 si sol re sol re si, re sol, |
do do' sol mi sol mi do mi la, |
re re' la fad la fad re fad re |
sol8. do16 re4 re, |
sol,8*2/3 si sol re sol re si, re sol, |
do do' sol mi sol mi do mi la, |
re re' la fad la fad re fad re |
sol8. do16 re4 re, |
sol,4 sol fad |
mi2 re8 do |
si,2 mi8 re |
do2 re4 |
sol, sol fad |
mi2 re8 do |
si,2 mi8 re |
do2 re4 |
sol, sol,8. \clef "tenor" sol16 re'8. sol16 |
fad8. la16 re'4~ re'8. fad16 |
mi8. si16 mi'8. sol'16 si8. re'16 |
dod'8. mi'16 la4 la, |
re sol,8. sol16 re'8. sol16 |
fad8. la16 re'4~ re'8. fad16 |
mi8. si16 mi'8. sol'16 si8. re'16 |
dod'8. mi'16 la4 \clef "basse" la, |
re2 r4 |
r r re' |
la2 do'4 |
si8. do'16 re'4 re |
sol r16 re' do' si la re' sol re' |
fad re' mi re' re4 re |
la,2 do4 |
si,8. do16 re4 re, |
sol,4 %{r r | \allowPageTurn
R2.*23 |
r8 r16 fa\fort do'4. do'8 |
fa'2.~ |
fa'4 mib'2 |
re'2. |
mib'4 fa' fa |
sib r r |
<<
  \tag #'basson { R2.*11 | }
  \tag #'basse {
    r4 r la\doux |
    re r r |
    r r sol |
    do2 r8 r16 sol |
    lab8. fa16 sol4 sol, |
    do, r r |
    R2.*5 |
  }
>>
r4 r sol\fort |
re4 re, re |
mi mi, re |
do re do, |
sol,2 sol4 |
re re, re |
mi mi, re |
do re re, |
sol, sol fad |
mi2 re8 do |
si,2 mi8-! re-! |
do2 re4 |
sol, sol fad |
mi2 re8-! do-! |
si,2 mi8-! re-! |
do2 re4 |
sol,~ sol,8. \clef "tenor" re'16 sol'8. sol16 |
fad8. la16 re'4~ re'8. fad16 |
mi8. si16 mi'8. sol'16 si8. re'16 |
dod'8. mi'16 la4 la, |
re4. r16 sol re'8. sol16 |
fad8. la16 re'4~ re'8. fad16 |
mi8. si16 mi'8. sol'16 si8. re'16 |
dod'8. mi'16 la4 \clef "basse" la, |
re %} r4 r |
<<
  \tag #'basson {
    re'(\doux do'!\trill) si |
    si( la)\trill sol |
    sol4( mi) fad |
    fad( mi) fad |
    fad fa mi |
    mi2. |
    r8 r16 re\fort fad8. la16 re'4 |
    r8 r16 re sol8. si16 re'8. si16 |
    mi'2~ mi'8. do'16 |
    fad8.\trill sol16 re4 re, |
    sol, r r |
    re'\doux( do') si |
    si( la)\trill sol |
    sol4( mi) fad |
    fad( mi) fad |
    fad fa mi |
    mi2. |
    r8 r16 re\fort fad8. la16 re'4 |
    r8 r16 re sol8. si16 re'8. si16 |
    mi'2~ mi'8. do'16 |
    fad8.\trill sol16 re4 re, |
    sol,2. |
  }
  \tag #'basse {
    R2. |
    re,2.\doux~ |
    re,~ |
    re,~ |
    re,4 re la, |
    la,2. |
    fad,2.\fort |
    sol,2 si,4 |
    do2.~ |
    do8 si,16 do re4 re, |
    sol,4 r r |
    R2. |
    re,2.\doux~ |
    re,~ |
    re, |
    re,4 re la, |
    la,2. |
    fad,2.\fort |
    sol,2 si,4 |
    do2. |
    do8 si,8*1/2 do re4 re, |
    sol,2. |
  }
>>
