\score {
  \new StaffGroup <<
    \new Staff \with { \haraKiriFirst shortInstrumentName = "Fl." }
    << \global \includeNotes "flute" >>
    \new Staff \with {
      instrumentName = "Hautbois"
      shortInstrumentName = "Hb."
    } << \global \keepWithTag #'hautbois \includeNotes "dessus" >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 8\mm
  }
}
