\piecePartSpecs
#`((flutes #:tag-notes hautbois #:notes "dessus"
           #:instrument ,#{ \markup\center-column { Hautbois Flûtes } #})
   (hautbois #:tag-notes hautbois #:notes "dessus"
             #:instrument ,#{ \markup\center-column { Hautbois Flûtes } #})
   (dessus #:score "score-dessus")
   (violon1 #:tag-notes violon1 #:notes "dessus")
   (violon2 #:tag-notes violon2 #:notes "dessus")
   (parties #:score-template "score-parties2")
   (haute-contre)
   (taille)
   (bassons #:tag-notes basson #:notes "basse")
   (basses #:tag-notes basse #:notes "basse"))
