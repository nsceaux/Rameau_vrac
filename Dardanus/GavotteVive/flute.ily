\clef "dessus" sol''8 sib'' la'' fad'' |
sol'' fad'' sol'' re'' mib'' re'' mib'' do'' |
re'' fad'' sol'' re'' mib'' re'' mib'' do'' |
re'' fad'' sol'' re'' mib'' re'' do'' sib' |
la'2\trill sol''8 sib'' la'' fad'' |
sol'' fad'' sol'' re'' mib'' re'' mib'' do'' |
re'' fad'' sol'' re'' mib'' re'' mib'' do'' |
re'' fad'' sol'' sib' la'4.\trill sol'8 |
sol'2\fermata 
r2 |
r sib''4 sib'' |
sib''1~ |
sib''2. fa''8 sib'' |
la''2\trill \twoVoices #'(flute1 flute2 flutes) <<
  { sib''8 do''' re''' sib'' |
    sol'' sib'' la'' sib'' do''' la'' do''' la'' |
    fa'' la'' sol'' la'' sib'' fa'' mib''\trill re'' |
    sol'' do'' re'' mib'' do''4.\trill sib'8 |
    sib'2 }
  { re''2 |
    re'' do'' |
    do'' sib' |
    sib' la'4.\trill sib'8 |
    sib'2 }
>>

