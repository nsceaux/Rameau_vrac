\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Flûtes" } <<
      \global \includeNotes "flute"
    >>
    \new Staff \with { instrumentName = "Violons" } <<
      \global \includeNotes "violon"
      \origLayout {
        s2 s1*3\break s1*4 s2\break
        s2 s1*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
