\clef "dessus2" sol'4 la' |
sib'2 do''4 la' |
sib'2 do''4 la' |
sib'2 do''8 sib' la' sol' |
re'8 do'\trill sib la sol4 la |
sib2 do'4 la |
sib2 do'4 la |
sib do' re' re' |
sol2
\twoVoices #'(violon1 violon2 violons) <<
  { sib'8 do'' re'' mib'' |
    fa'' mib'' fa'' mib'' sol'' fa'' sol'' mib'' |
    fa'' mib'' fa'' re'' sol'' fa'' sol'' mib'' |
    fa'' mib'' fa'' re'' sol'' fa'' mib'' re'' |
    do''2\trill fa'' |
    fa'' mib'' |
    mib'' re'' |
    do''4 sol' fa' mib' |
    re'8 do'' sib'\trill la' }
  { r4 sib'8 do'' |
    re'' do'' re'' sib' mib'' re'' mib'' do'' |
    re'' do'' re'' sib' mib'' re'' mib'' do'' |
    re'' do'' re'' sib' mib'' re'' do'' sib' |
    fa'2 re' |
    mib' do' |
    re' sib |
    mib' fa' |
    sib8 do'' sib'\trill la' }
>>
