\tag #'(vdessus basse) {
  Par un som -- meil a -- gré -- a -- ble
}
\tag #'(vdessus vhaute-contre vbasse basse) {
  que tous vos sens soient char -- mez !
}
\tag #'(vdessus basse vhaute-contre) {
  Dor -- mez, dor -- mez.
}
\tag #'(vtaille vbasse) {
  Dor -- mez. __
}
\tag #'(vdessus basse) {
  Par cet en -- chan -- teur ai -- ma -- ble,
}
\tag #'(vdessus vhaute-contre vbasse basse) {
  tous les cha -- grins sont cal -- mez,
}
\tag #'(vdessus basse vbasse) {
  Dor -- mez, dor -- mez.
}
\tag #'(vtaille vhaute-contre) {
  Dor -- mez. __
}
Les plai -- sirs sont ra -- ni -- mez.

\tag #'(vdessus basse) {
  Le tendre A -- mour est pour vous.
}
Quel sort plus doux !
\tag #'(vdessus vhaute-contre vbasse basse) {
  Bra -- vez les des -- tins ja -- loux.
}
Le tendre A -- mour est pour vous.
\tag #'(vdessus basse) {
  Par un som -- meil a -- gré -- a -- ble
}
\tag #'(vdessus vhaute-contre vbasse basse) {
  que tous vos sens soient char -- mez !
}
\tag #'(vdessus basse) {
  Par cet en -- chan -- teur ai -- ma -- ble,
}
\tag #'(vdessus vhaute-contre vbasse basse) {
  tous les cha -- grins sont cal -- mez !
}
\tag #'(vdessus basse vbasse) {
  Dor -- mez, dor -- mez.
}
\tag #'(vtaille vhaute-contre) {
  Dor -- mez. __
}
Les plai -- sirs sont ra -- ni -- mez.
\tag #'(vdessus vhaute-contre vbasse basse) {
  La Gloire et l’A -- mour vous don -- nent
  et le myrthe et le lau -- rier.
}
Heu -- reux guer -- rier,

\tag #'vhaute-contre { Ces dieux, ces dieux }
\tag #'(vdessus basse) { Ces dieux }
\tag #'(vdessus vhaute-contre basse) {
  vous cou -- ron -- nent :
  Ils tri -- om -- phent tour à tour.
}
\tag #'vbasse {
  Ils tri -- om -- phent,
  ils tri -- om -- phent tour à tour.
}
Mais le grand jour
est ce -- lui du tendre A -- mour.
%%
\tag #'(vdessus basse) {
  Par un som -- meil a -- gré -- a -- ble
}
\tag #'(vdessus vhaute-contre vbasse basse) {
  que tous vos sens soient char -- mez !
}
\tag #'(vdessus basse) {
  Par cet en -- chan -- teur ai -- ma -- ble,
}
\tag #'(vdessus vhaute-contre vbasse basse) {
  tous les cha -- grins sont cal -- mez !
}
\tag #'(vdessus basse vbasse) {
  Dor -- mez, dor -- mez.
}
\tag #'(vtaille vhaute-contre) {
  Dor -- mez. __
}
Les plai -- sirs sont ra -- ni -- mez.