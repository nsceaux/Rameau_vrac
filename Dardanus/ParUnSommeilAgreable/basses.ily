\clef "basse" <>^"[B.C]" r2 |
re'4(^\douxSug mib') fad( re) |
sol4 sol, sol sol |
fa fa mib mib |
re2 <>^"[Bn et bs]" sol,\douxSug |
re, sol, |
re, r2 |
R1 |
r2 <>^"[B.C]" sib,4 sib, |
do do re re |
sol,2 <>^"[Bn et bs]" do |
si, do |
sol, do'4 do' |
sib! do' re' re |
sol2 r |
R1 |
r2 sib,\fort |
la, sib, |
fa, <>^"[B.C]" fa4\douxSug fa |
la, la, sib,4. sib,8 |
fa,2 <>^"[Bassons et basses]" sib4 re |
sol sol mib fa |
sib,2 r |
<>^"[B.C]" re'4( mib') fad( re) |
sol sol, sol sol |
fa fa mib mib |
re2 r |
R1 |
r2 sib,4 sib, |
do do re re |
sol,2 <>^"[Bassons et basses]" do |
si, do |
sol, do'4 do' |
sib! do' re' re |
sol2 <>^"[B.C]" sol4 la |
sib sol do'4. do8 |
fa4 fa sol la |
sib la sib sol |
la2 <>^"[Bassons et basses]" re\fortSug |
dod re |
la, r |
r <>^"[B.C]" la4. la8 |
sib4. r8 sib4. sib8 |
fa4 sib sol la |
re2 <>^"[Bassons et basses]" sol |
fad sol |
fad sol4 sol |
fa! sol la la, |
re2
%%
r |
<>^"[B.C]" re'4( mib') fad( re) |
sol sol, sol sol |
fa fa mib mib |
re2 r |
R1 |
r2 sib,4 sib, |
do do re re |
sol,2 <>^"[Bassons et basses]" do |
si, do |
sol, do'4 do' |
sib! do' re' re |
sol2