\clef "dessus" r2 |
R1*3 |
r2 <>\douxSug \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''2 | fad'' sol'' | fad'' }
  { sib' | la' sib' | la' }
>> r2 |
<>^"[Violons]" re''4( mib'') fad'( re') |
sol'4 sol8 r sol''4( re'') |
\appoggiatura re''8 mib''2 re''4( la'4)\trill |
\appoggiatura la'8 sib'2 <>^"[Tous]" mib'' |
re'' mib'' |
re'' mib''4 sol'' |
re'' \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''4 sol'' fad''\trill | sol''2 }
  { mib''4 re'' do'' | sib'2 }
>> r2 |
<>^"[Violons]" \douxSug \twoVoices #'(dessus1 dessus2 dessus) <<
  { sib'4 la' \appoggiatura la'8 sib'4. do''8 | la'2\trill }
  { sib4 do' re' mib' | fa'2 }
>> <>^"[Tous]" re''2\fort |
do'' re'' |
do'' r |
R1 |
r2 re''4 fa'' |
\appoggiatura mib''8 re''4 \appoggiatura do''8 sib'4 do'' la'\trill |
sib'2 r |
R1*4 |
<>^"[Violons]" \douxSug re''4( mib'') fad'( re') |
sol'4 sol8 r sol''4( re'') |
\appoggiatura re''8 mib''2 re''4 la'\trill |
\appoggiatura la'8 sib'2 <>^"[Tous]" mib'' |
re'' mib'' |
re'' mib''4 sol'' |
re'' \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''4 sol'' fad''\trill | sol''2 }
  { mib''4 re'' do'' | sib'2 }
>> r2 |
R1*3 |
r2 fa''\fortSug |
mi'' fa'' |
mi'' r |
R1*3 |
r2 sib' |
la' sib' |
la' sib'4 \appoggiatura la'8 sol'4 |
\appoggiatura sol'8 la'4 re'' re'' dod''\trill |
re''2
%%
r |
R1*4 |
<>^"[Violons]" \douxSug re''4( mib'') fad'( re') |
sol'4 sol8 r sol''4( re'') |
\appoggiatura re''8 mib''2 re''4 la'\trill |
\appoggiatura la'8 sib'2 <>^"[Tous]" mib'' |
re'' mib'' |
re'' mib''4 sol'' |
re'' \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''4 sol'' fad''\trill | sol''2 }
  { mib''4 re'' do'' | sib'2 }
>>
