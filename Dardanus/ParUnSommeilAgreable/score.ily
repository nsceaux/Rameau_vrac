\score {
  <<
    \new StaffGroup <<
      \new Staff <<
        \global \keepWithTag #'dessus \includeNotes "dessus"
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basses"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*3\pageBreak
        s1*5\break \grace s8 s1*5 s2\pageBreak
        s2 s1*5\break \grace s8 s1*5\pageBreak
        \grace s8 s1*5\break \grace s8 s1*4 s2\pageBreak
        s2 s1*4\break s1*5\pageBreak
        s4 s2\break
      }
    >>
  >>
  \layout {
    indent = \smallindent
    short-indent = 0
    system-count = 10
  }
  \midi { }
}
