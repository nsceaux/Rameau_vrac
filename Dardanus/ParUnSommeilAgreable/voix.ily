<<
  \tag #'(vdessus basse) {
    \clef "vdessus"
    <>^\markup\character { \concat { 1 \super er } songe }
    re''4 mib'' |
    fad' la' do''4. re''8 |
    sib'4\trill \appoggiatura la'8 sol'4 sib' do'' |
    \appoggiatura sib'8 la'4 re'' sol' la' |
    fad'2\trill <>^"[Tous]" sib' |
    la' sib' |
    la' <>^\markup\character { \concat { [1 \super er } songe] } re''4 mib'' |
    fad' la' do''4. re''8 |
    sib'4\trill \appoggiatura la'8 sol'4 sib' re'' |
    do'' sib' la' sib' |
    \appoggiatura la'8 sol'2 <>^"[Tous]" mib'' |
    re'' mib'' |
    re'' mib''4 do''\trill |
    re'' do'' sib'\trill la' |
    sol'2 <>^\markup\character { \concat { [1 \super er } songe] } sib'4 do'' |
    re'' mib'' fa'' sol'' |
    do''2 <>^"[Tous]" re'' |
    do'' re'' |
    do'' <>^\markup\character { \concat { [1 \super er } songe] } do''4 mib'' |
    mib'' \appoggiatura re''8 do''4 \appoggiatura do''8 re''4 \appoggiatura do''8 sib'4 |
    \appoggiatura sib'8 do''2 <>^"[Tous]" re''4 fa'' |
    \appoggiatura mib''8 re''4 \appoggiatura do''8 sib'4 do'' la'\trill |
    sib'2 <>^\markup\character { \concat { [1 \super er } songe] } re''4 mib'' |
    fad' la' do''4. re''8 |
    sib'4\trill \appoggiatura la'8 sol'4 sib' do'' |
    \appoggiatura sib'8 la'4 re'' sol' la' |
    fad'2\trill re''4 mib'' |
    fad' la' do''4. re''8 |
    sib'4\trill \appoggiatura la'8 sol'4 sib' re'' |
    do'' sib' la' sib' |
    \appoggiatura la'8 sol'2 <>^"[Tous]" mib'' |
    re'' mib'' |
    re'' mib''4 do''\trill |
    re'' do'' sib'\trill la' |
    sol'2 <>^\markup\character { \concat { [1 \super er } songe] } re''4 do'' |
    sib' la' sol'4. do''8 |
    la'4\trill la' sib' la' |
    sol' la' sol' fa' |
    mi'2\trill <>^"[Tous]" fa'' |
    mi'' fa'' |
    mi'' r |
    <>^\markup\character { \concat { [1 \super er } songe] } la'4 si' dod''4. re''8 |
    \appoggiatura re''8 mi''4 mi''8 r sol''4 mi'' |
    fa''4 \appoggiatura mi''8 re''4 mi'' dod''\trill |
    re''2 <>^"[Tous]" sib' |
    la' sib' |
    la' sib'4 \appoggiatura la'8 sol'4 |
    \appoggiatura sol'8 la'4 re'' re'' dod''\trill |
    re''2
    %%
    <>^\markup\character { \concat { [1 \super er } songe] } re''4 mib'' |
    fad' la' do''4. re''8 |
    sib'4\trill \appoggiatura la'8 sol'4 sib' do'' |
    \appoggiatura sib'8 la'4 re'' sol' la' |
    fad'2\trill re''4 mib'' |
    fad' la' do''4. re''8 |
    sib'4\trill \appoggiatura la'8 sol'4 sib' re'' |
    do'' sib' la' sib' |
    \appoggiatura la'8 sol'2 <>^"[Tous]" mib'' |
    re'' mib'' |
    re'' mib''4 do''\trill |
    re'' do'' sib'\trill la' |
    sol'2
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r2 |
    R1 |
    r2 <>^\markup\character { \concat { 2 \super e } songe }
    re'4 mib' |
    mib' re' re' do'\trill |
    re'2 <>^"[Tous]" sol'2 |
    fad' sol' |
    fad' r |
    R1 |
    r2 <>^\markup\character { \concat { [2 \super e } songe] } sol'4 sib' |
    la' sol' sol' fad'\trill |
    sol'2 <>^"[Tous]" sol' |
    sol'1~ |
    sol'2 sol'4 sol' |
    sol' sol' sol' fad'\trill |
    sol'2 r2 |
    R1 |
    r2 sib' |
    fa' sib' |
    la'\trill <>^\markup\character { \concat { [2 \super e } songe] } fa'4 fa' |
    fa' fa' fa'4. sib'8 |
    la'2\trill <>^"[Tous]" sib'4 sib' |
    sib' sib' sol' fa' |
    fa'2 r |
    R1 |
    r2 <>^\markup\character { \concat { [2 \super e } songe] } re'4 mib' |
    mib' re' re' do'\trill |
    re'2 r |
    R1 |
    r2 sol'4 sib' |
    la' sol' sol' fad'\trill |
    sol'2 <>^"[Tous]" sol' |
    sol'1~ |
    sol'2 sol'4 sol' |
    sol' sol' sol' fad'\trill |
    sol'2 <>^\markup\character { \concat { [2 \super e } songe] } sib'4 la' |
    sol' fa' fa'4. mi'8 |
    \appoggiatura mi'8 fa'4 fa' mi' fa' |
    mi' fa' mi' re' |
    dod'2\trill <>^"[Tous]" la' |
    la' la' |
    la' <>^\markup\character { \concat { [2 \super e } songe] } la4 si |
    dod' re' mi'4. fa'8 |
    \appoggiatura fa'8 sol'4 sol'8 r sib'4 sol' |
    la' \appoggiatura sol'8 fa'4 sol' mi'\trill |
    re'2 <>^"[Tous]" re' |
    re' re' |
    re' re'4 mi' |
    fa' mi' mi'4( fad'8) sol' |
    fad'2\trill
    %%
    r |
    R1 |
    r2 <>^\markup\character { \concat { [2 \super e } songe] } re'4 mib' |
    mib' re' re' do'\trill |
    re'2 r |
    R1 |
    r2 sol'4 sib' |
    la' sol' sol' fad'\trill |
    sol'2 <>^"[Tous]" sol' |
    sol'1~ |
    sol'2 sol'4 sol' |
    sol' sol' sol' fad'\trill |
    sol'2
  }
  \tag #'vtaille {
    \clef "vtaille" r2 |
    R1*3 |
    r2 <>^"[Tous]" re' |
    re'1~ |
    re'2 r |
    R1*3 |
    r2 sol |
    sol1~ |
    sol2 do'4 mib' |
    re' mib' re' do' |
    sib2 r |
    R1 |
    r2 fa' |
    fa' fa' |
    fa' r2 |
    R1 |
    r2 fa'4 fa' |
    fa' mib' mib' do'\trill |
    re'2 r |
    R1*7 |
    r2 sol |
    sol1~ |
    sol2 do'4 mib' |
    re' mib' re' do' |
    sib2 r2 |
    R1*3 |
    r2 re' |
    sol' fa' |
    dod'\trill r |
    R1*3 |
    r2 re' |
    re' re' |
    re' re'4 re' |
    re' sib la la |
    la2
    %%
    r |
    R1*7 |
    r2 sol |
    sol1~ |
    sol2 do'4 mib' |
    re' mib' re' do' |
    sib2
  }
  \tag #'vbasse {
    \clef "vbasse" r2 |
    R1 |
    r2 <>^\markup\character { \concat { 3 \super e } songe }
    sol4 sol |
    fa fa mib mib |
    re2 <>^"[Tous]" re |
    re1~ |
    re2 r |
    R1 |
    r2 <>^\markup\character { \concat { [3 \super e } songe] } sib,4 sib, |
    do do re re |
    sol,2 <>^"[Tous]" do' |
    si do' |
    si do'4 do' |
    sib! do' re' re |
    sol2 r2 |
    R1 |
    r2 sib |
    la sib |
    fa <>^\markup\character { \concat { [3 \super e } songe] } la4 do' |
    do' \appoggiatura sib8 la4 sib4. la16[ sol] |
    fa2 <>^"[Tous]" sib4 re |
    sol sol mib fa |
    sib,2 r |
    R1 |
    r2 <>^\markup\character { \concat { [3 \super e } songe] } sol4 sol |
    fa4 fa mib mib |
    re2 r |
    R1 |
    r2 sib,4 sib, |
    do do re re |
    sol,2 <>^"[Tous]" do' |
    si do' |
    si do'4 do' |
    sib! do' re' re |
    sol2 <>^\markup\character { \concat { [3 \super e } songe] } sol4 la |
    sib sol do'4. do8 |
    fa4 fa sol la |
    sib la sib sol |
    la2 <>^"[Tous]" re' |
    dod' re' |
    la r |
    r <>^\markup\character { \concat { [3 \super e } songe] } la4. la8 |
    sib4 sib8 r sib4. sib8 |
    fa4 sib sol la |
    re2 <>^"[Tous]" sol |
    fad sol |
    fad sol4 sol |
    fa! sol la la, |
    re2
    %%
    r |
    R1 |
    r2 <>^\markup\character { \concat { [3 \super e } songe] } sol4 sol |
    fa4 fa mib mib |
    re2 r |
    R1 |
    r2 sib,4 sib, |
    do do re re |
    sol,2 <>^"[Tous]" do' |
    si do' |
    si do'4 do' |
    sib! do' re' re |
    sol2
  }
>>