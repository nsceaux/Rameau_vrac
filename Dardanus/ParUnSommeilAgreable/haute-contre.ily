\clef "haute-contre" r2 |
R1*3 |
r2 sol'\douxSug |
do' sib |
re' r |
R1*3 |
r2 sol' |
sol'1~ |
sol'2 sol'4 sol' |
sol' sol' sol' fad'\trill |
sol'2 r |
R1 |
r2 sib'\fort |
fa' sib' |
la'\trill r |
R1 |
r2 sib'4 sib' |
sib' sib' sol' fa' |
fa'2 r |
R1*7 |
r2 sol' |
sol'1~ |
sol'2 sol'4 sol' |
sol' sol' sol' fad'\trill |
sol'2 r |
R1*3 |
r2 la'\fortSug |
la' la' |
la' r |
R1*3 |
r2 sol' |
do' sib |
re' re'4 mi' |
fa' mi' mi' fad'8 sol' |
fad'2\trill
%%
r |
R1*7 |
r2 sol' |
sol'1~ |
sol'2 sol'4 sol' |
sol' sol' sol' fad'\trill |
sol'2
