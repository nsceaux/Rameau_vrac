\clef "taille" r2 |
R1*3 |
r2 re'\douxSug |
re'1 |
re2 r |
R1*3 |
r2 sol' |
fa' mib' |
si sol'4 sol' |
sol' la' sib' la' |
re'2 r |
R1 |
r2 fa'\fort |
fa' fa' |
fa' r |
R1 |
r2 fa'4 fa' |
fa' mib' mib' do'\trill |
re'2 r |
R1*7 |
r2 sol' |
fa' mib' |
si sol'4 sol' |
sol' la' sib' la' |
re'2 r |
R1*3 |
r2 re'\fortSug |
sol' fa' |
dod'\trill r |
R1*3 |
r2 re' |
re' re' |
re' re'4 re' |
re' sib la la |
la2
%%
r |
R1*7 |
r2 sol' |
fa' mib' |
si sol'4 sol' |
sol' la' sib' la' |
re'2
