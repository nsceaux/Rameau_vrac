\key sol \minor \tempo "[Tendrement]" \midiTempo#160
\digitTime\time 2/2 \partial 2
\beginMark "Trio des Songes en rondeau" s2 s1*3 s2
\beginMark Chœur s2 s1 s2
\beginMark Trio s2 s1*3 s2
\beginMark Chœur s2 s1*3 s2 \bar "||"
\beginMark\markup { \concat { 1 \super re } reprise } s2 s1 s2
\beginMark Chœur s2 s1 s2
\beginMark Trio s2 s1 s2
\beginMark Chœur s2 s1 s2
\beginMark Trio s2 s1*7 s2
\beginMark Chœur s2 s1*3 s2 \bar "||"
\beginMark Trio s2 s1*3 s2
\beginMark Chœur s2 s1 s2
\beginMark Trio s2 s1*3 s2
\beginMark Chœur s2 s1*3 s2 \bar "||"
\beginMark Trio s2 s1*7 s2
\beginMark Chœur s2 s1*3 s2 \bar "|."
