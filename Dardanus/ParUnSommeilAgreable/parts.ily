\piecePartSpecs
#`((hautbois #:notes "dessus" #:tag-notes dessus
             #:score-template "score-voix")
   (flutes #:notes "dessus" #:tag-notes dessus
           #:score-template "score-voix")
   (dessus #:score-template "score"
           #:notes "dessus" #:tag-notes dessus
           #:score-template "score-voix")
   (violon1 #:notes "dessus" #:tag-notes dessus1)
   (violon2 #:notes "dessus" #:tag-notes dessus2)
   (bassons #:notes "basses")
   (parties #:score-template "score-parties")
   (haute-contre #:notes "haute-contre")
   (taille #:notes "taille")
   (basses #:score-template "score-voix"))
