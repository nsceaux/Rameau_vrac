\clef "dessus" <>^"Violons et flûtes" r4 r8 |
r4 r8 r16 do'' sib'8.(\trill la'16) |
sol'2.~ |
sol'8.( mib''16) re''8.( do''16) sib'8.(\trill la'16) |
sol'4 sol''2~ |
sol''8.( fa''16) mib''8.( re''16) do''8.(\trill si'16) |
\appoggiatura si'8 do''4. do''8 re''8.( la'16) |
\appoggiatura la'8 sib'8.( do''32*1/2 sib' la' sib' sib'4.\trill la'8) |
la'4. r4 r8 |
r8 r16 fa' sib'8.( re''16) do''8.( mib''16) |
re''4 fa''4.( do''16 mib'') |
re''4 fa''4.( do''16 mib'') |
re''8.( mib''32 re'') re''4.(\trill do''16 re'') |
mib''8.( re''16) mib''8.( fa''16) sol''8.( la''16) |
sib''4.( la''16\trill sol'') fa''8.( sol''16) |
do''8. re''16 do''4.\trill sib'8 |
sib'2 r4 |
r4 r8 r16 do'' sib'8.(\trill la'16) |
sol'2.~ |
sol'8.( mib''16) re''8.( do''16) sib'8.(\trill la'16) |
sol'4 sol''2~ |
sol''8.( fa''16) mib''8.( re''16) do''8.(\trill si'16) |
\appoggiatura si'8 do''4 la''2~ |
la''8.( sol''16) fad''8.( mib''16) re''8.( la'16) |
\appoggiatura la'8 sib'8. do''16 la'4.\trill sol'8 |
sol'4.
