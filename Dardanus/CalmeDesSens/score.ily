\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout {
        s4. s2.*5\break \grace s8 s2.*7\break s2.*8\break \grace s8
      }
    >>
  >>
  \layout { indent = \smallindent }
  \midi { }
}
