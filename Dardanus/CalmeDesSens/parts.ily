\piecePartSpecs
#`((violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (flutes #:notes "dessus")
   (bassons #:notes "basse" #:clef "tenor")
   (basses #:notes "basse" #:clef "tenor"))
