\clef "petrucci-c4/tenor" r16 do' sib8.(\trill la16) |
sol2.~ |
sol8.( mib'16) re'8.( do'16) sib8.(\trill la16) |
sol4 sol'2~ |
sol'8.( fa'16) mib'8.( re'16) do'8.\trill( si16) |
\appoggiatura si8 do'2~ do'8. re'16 |
mib'8.( la16) sib8.( do'16) fad8.( re16) |
sol2 sol,4 |
re4. r16 mib' re'8.(\trill do'16) |
sib2 la8.( fa16) |
sib4.( re'8) la8.( do'16) |
sib4.( re'8) la8.( fa16) |
sib4 lab2 |
sol8.( lab16) sol8.( fa16) mib8.( fa16) |
re2\trill~ re8. mib16 |
fa8. sib16 fa4 fa, |
sib,4. r16 do' sib8.(\trill la16) |
sol2.~ |
sol8.( mib'16) re'8.( do'16) sib8.(\trill la16) |
sol4 sol'2~ |
sol'8.( fa'16) mib'8.( re'16) do'8.\trill( si16) |
\appoggiatura si8 do'2~ do'8. re'16 |
mib'8.( re'16) do'8.( sib16) la8.( sol16) |
\clef "bass" fad2\trill~ fad8. re16 |
sol8. do16 re4 re, |
sol,4.
