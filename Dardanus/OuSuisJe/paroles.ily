Où suis- je ? dans ces lieux, quel Dieu m’a trans -- por -- té,
m’a- t-on ren -- du la li -- ber -- té ?
Le sort cru -- el, en -- fin, va- t’il ta -- rir mes lar -- mes,
mais je n’en dou -- te plus à l’as -- pect de ces lieux,
ces son -- ges sé -- dui -- sants, qui char -- maient mes a -- lar -- mes,
é -- taient les o -- ra -- cles des Dieux.
