\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*2\break
        s1 s2. s4 \bar "" \break s2 s2. s1\break \grace s8 s1*2\break
      }
    >>
  >>
  \layout { indent = \smallindent }
  \midi { }
}
