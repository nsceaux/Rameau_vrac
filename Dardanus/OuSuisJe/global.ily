\key re \minor
\digitTime\time 3/4 \midiTempo#80 \partial 4 s4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.*3
\digitTime\time 2/2 \midiTempo#160 \grace s8 s1
\time 4/4 \midiTempo#80 \grace s8 s1
\digitTime\time 2/2 \midiTempo#160 \grace s8 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\digitTime\time 2/2 \midiTempo#160 \grace s8 s2. \bar "||"
