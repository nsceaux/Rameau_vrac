\clef "basse" r4 |
re,2. |
re4 mi2 |
fa2 sib, |
la, do4 |
sib, la,2 |
sol, sol4 |
re'2 la4. re8 |
dod2\trill r4 dod |
re2 fa |
mi8. do16 re4 mi |
la,2.
