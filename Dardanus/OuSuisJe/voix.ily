\clef "vhaute-contre" <>^\markup\character Dardanus
r8 la |
re'4 re'8 r re' fa' |
sib8 re' sib4\trill sib8 sib16 la |
la4\trill r16 fa' fa' fa' re'4\trill re'8 re'16 dod' |
dod'4\trill r16 mi' mi' mi' la'8 sib' |
\appoggiatura la'8 sol'8. sol'16 sol'8. fad'16 fad'8.\trill sol'16 |
sol'8 sol' sib'4 re'16 re' re' mi' |
\appoggiatura mi'8 fa'4 la8 re' dod'4\trill dod'8 re' |
\appoggiatura re'8 mi'4 r8 mi' la'4 sol'8 fa'16 mi' |
\appoggiatura mi'16 fa'4 fa'8 fa' \appoggiatura mi'8 re'4\trill re'8 re' |
si8\trill si16 mi' \appoggiatura mi'8 re'\trill re'16 do' si8\trill si16 do' |
\appoggiatura si8 la2.
