\piecePartSpecs
#`((violon1 #:notes "silence"
            #:score-template "score-voix")
   (violon2 #:notes "silence"
            #:score-template "score-voix")
   (basses #:notes "basse"
           #:score-template "score-basse-continue-voix"))
