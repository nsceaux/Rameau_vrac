\clef "bass"
<<
  \tag #'antenor {
    R1*3 |
    la2. la4 |
    do'1 |
    la2 la4 la8 la |
    re'2. re'4 |
    fa'2. si8 si |
    sold4\trill sold8 sold la4 la8 si |
    \appoggiatura si8 do'2 do'4 r |
    do'4. si8 la4 sol |
    \appoggiatura sol8 fa2 fa4 la |
    mi1 |
    r2 mi4. mi8 |
    si1 |
    re'2 re'4 do'8 si |
    \appoggiatura si8 do'2 do'4 r |
    fa'2. r8 si |
    mi'2 si4 re' |
    sold2 la4 do' |
    fa2 re4. mi8 |
    la,2 do' |
    fa'2. r8 si |
    mi'2 si4 re' |
    sold2 la4 do' |
    fa2 re4. mi8 |
    la,1 |
  }
  \tag #'basse { R1*27 }
>>
do4 do8 re mi fad |
sol8. sol16 do'8. do'16 do'8 si |
si\trill si mi'4 mi'16 re' do' si |
\appoggiatura si8 do'4 do'8 do'16 do' do'8 mi' la la16 la |
fad4\trill r8 si,16 dod red8\trill red16 mi |
\appoggiatura mi8 fad fad si4 si8 si16 fad |
\appoggiatura fad8 sol sol r16 si si dod' red'8.\trill red'16 red'8 mi' |
la8 la do'8. do'16 la8 do' |
fad2. sold8 la |
sold1\trill |
la2. la4 |
