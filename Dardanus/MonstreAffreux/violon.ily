\clef "treble"
<<
  \tag #'violon1 {
    la'8( do'') la'( do'') la'( do'') la'( do'') |
    si'( re'') si'( re'') si'( re'') si'( re'') |
    sold'( si') sold'( si') sold'( si') la'( sold') |
    la'( do'') la'( do'') la'( do'') la'( do'') |
    mi'( la') mi'( la') mi'( la') mi'( la') |
    la'( do'') la'( do'') la'( do'') la'( do'') |
    si'( re'') si'( re'') si'( re'') si'( re'') |
    si'( re'') si'( re'') si'( re'') si'( re'') |
    si'( re'') si'( re'') mi'4 fad'8( sold') |
    \appoggiatura sold'8 la'4 mi'8( la') mi'( la') mi'( la') |
    mi'( la') mi'( la') mi'( la') mi'( la') |
    mi'( fa') mi'( fa') re'( fa') re'( fa') |
    si( si')\fort la'( sold') la'( do'') si'( la') |
    sold'2 r |
    re'8(\doux mi') re'( mi') re'( mi') re'( mi') |
    si( re') si( re') si( re') si( re') |
    do'( mi') do'( mi') do'( mi') do'( mi') |
    do'( re') do'( re') si( re') si( re') |
    si( do') la( do') fa'( la') fa'( re') |
    re'( mi') re'( mi') do'( fa') do'( fa') |
    re'( fa') re'( fa') re'( fa') re'( mi') |
    do'( mi') do'( mi') do'( mi') do'( mi') |
    do'( re') do'( re') si( re') si( re') |
    si( do') la( do') fa'( la') fa'( re') |
    re'( mi') re'( mi') do'( fa') do'( fa') |
    re'( fa') re'( fa') re'( fa') re'( mi') |
    do'( mi') re'( si) do'4 r |
  }
  \tag #'violon2 {
    do'8( mi') do'( mi') do'( mi') do'( mi') |
    re'( fa') re'( fa') re'( fa') re'( fa') |
    si( re') si( re') si( re') do'( si) |
    do'(\doux mi') do'( mi') do'( mi') do'( mi') |
    la( do') la( do') la( do') la( do') |
    do'( mi') do'( mi') do'( mi') do'( mi') |
    re'( fa') re'( fa') re'( fa') re'( fa') |
    re'( fa') re'( fa') re'( fa') re'( fa') |
    re'( fa') re'( fa') do'( mi') si( re') |
    la( do') la( do') la( do') la( do') |
    la( do') la( do') la( do') la( do') |
    la( do') la( do') la( si) la( si) |
    sold( re')\fort do'( si) do'( mi') re'( do') |
    si2 r |
    sold8(\doux si) sold( si) sold( si) sold( si) |
    sold( si) sold( si) sold( si) sold( si) |
    la( do') la( do') la( do') la( do') |
    la( si) la( si) sol( si) sol( si) |
    sol( mi') do'( mi') re'( fa') re'( si) |
    si( do') si( do') la( do') la( do') |
    si( re') si( re') si( re') si( sold) |
    la( do') la( do') la( do') la( do') |
    la( si) la( si) sol( si) sol( si) |
    sol( mi') do'( mi') re'( fa') re'( si) |
    si( do') si( do') la( do') la( do') |
    si( re') si( re') si( re') si( sold) |
    la( do') si( sold) la4 r |
  }
>>
R2.*3 R1 R2.*2 R1 R2. R1*3 |
