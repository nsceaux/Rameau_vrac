\clef "bass" la,4 la, la, la, |
la, la, la, la, |
la, la, la, la, |
la,^\doux la, la, la, |
la, la, la, la, |
la, la, la, la, |
la, la, la, la, |
la, la, la, la, |
la, la, la, la, |
la, la, la, la, |
la, la, la, la, |
re, re, fa, fa, |
mi, mi,\fort mi, mi, |
mi,2 r |
mi,4^\doux mi, mi, mi, |
mi, mi, mi, mi, |
la, la, la, la, |
re, re, sol, sol, |
do, do, re, re, |
mi, mi, fa, fa, |
re, re, re, mi, |
la, la, la, la, |
re, re, sol, sol, |
do, do, re, re, |
mi, mi, fa, fa, |
re, re, re, mi, |
la, la, la, r |
do2. |
si,4 la,2 |
sol,4 sold,2 |
la,2. do4 |
si,2 r4 |
r red,2 |
mi,4 r8 mi fad4. sol8 |
do4 la,2 |
si,1 |
mi, |
R1 |
