\set Score.currentBarNumber = 16 \bar ""
\key la \minor \tempo "Lentement"
\digitTime\time 2/2 \midiTempo#160 s1*4
\segnoMark \bar "||" s1*23 \fineMark \bar "|."
\digitTime\time 3/4 \midiTempo#80 s2.*3
\time 4/4 \grace s8 s1
\digitTime\time 3/4  s2.*2
\time 4/4 \grace s8 s1
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1*3 \bar "|." \segnoMarkEnd
