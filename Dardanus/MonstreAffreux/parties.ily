\clef "alto" mi4 mi mi mi |
fa fa fa fa |
fa fa fa fa |
mi mi mi mi |
mi mi mi mi |
mi mi mi mi |
fa fa fa fa |
la la la la |
fa fa mi mi |
mi mi mi mi |
mi mi mi mi |
fa fa la fa |
mi mi\fort mi mi |
mi2 r |
mi4\doux mi mi mi |
mi mi mi mi |
mi mi mi mi |
fa fa fa fa |
mi la la la |
sold si la la |
la la la sold |
la \twoVoices #'(haute-contre taille parties) <<
  { mi'4 mi' mi' |
    re' re' re' re' |
    do' mi' fa' fa' |
    mi' mi' la la |
    la la re' si |
    do' do' do' }
  { do'4 do' do' |
    do' do' si si |
    si la la re' |
    si re' do' la |
    fa' fa' fa' mi' |
    mi' mi' mi' }
>> r4 |
R2.*3 R1 R2.*2 R1 R2. R1*3 |
