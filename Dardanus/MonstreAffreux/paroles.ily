\tag #'antenor {
  Monstre af -- freux, mons -- tre re -- dou -- ta -- ble,
  Ah ! Que le sort me se -- rait fa -- vo -- ra -- ble
  S’il ne m’ex -- po -- sait qu’à vos coups !
  Monstre af -- freux, mons -- tre re -- dou -- ta -- ble,
  Ah ! L’A -- mour est en -- cor plus ter -- ri -- ble que vous.
  Ah ! ah ! L’A -- mour est en -- cor plus ter -- ri -- ble que vous.
}
Con -- tre vo -- tre fu -- reur il est du moins des ar -- mes :
Mais con -- tre ses a -- lar -- mes,
Vai -- ne -- ment on cherche un ap -- pui ;
Il re -- naît des ef -- forts qu’on fait pour le dé -- trui -- re ;
Et le cœur mê -- me qu’il dé -- chi -- re
Est d’in -- tel -- li -- gence a -- vec lui.
Monstre af -
