\piecePartSpecs
#`((dessus)
   (violon1)
   (violon2)
   (parties #:score-template "score-parties2")
   (haute-contre)
   (taille)
   (basses #:notes "basse"
           #:score-template "score-basse-voix"))
