\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new GrandStaff \with {
        instrumentName = "Violons"
        shortInstrumentName = "Vln"
      } <<
        \new Staff <<
          \global \keepWithTag #'violon1 \includeNotes "violon"
        >>
        \new Staff <<
          \global \keepWithTag #'violon2 \includeNotes "violon"
        >>
      >>
      \new Staff \with {
        instrumentName = "Parties"
        shortInstrumentName = \markup\center-column { H-c Ta }
      } <<
        { s1*17\break \startHaraKiri }
        \global \keepWithTag #'parties \includeNotes "parties"
      >>
      \new Staff \with {
        shortInstrumentName = "H-c"
      } <<
        { \startHaraKiri s1*17\break s4 \stopHaraKiri }
        \global \keepWithTag #'haute-contre \includeNotes "parties"
      >>
      \new Staff \with {
        shortInstrumentName = "Ta"
      } <<
        { \startHaraKiri s1*17\break s4 \stopHaraKiri }
        \global \keepWithTag #'taille \includeNotes "parties"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Anténor
      shortInstrumentName = "A."
    } \withLyrics <<
      \global \keepWithTag #'antenor \includeNotes "voix"
    >> \keepWithTag #'antenor \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = "B.C."
      shortInstrumentName = "B.c"
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3\pageBreak
        s1*4\break s1*4\break \grace s8 s1*4\pageBreak
        s1*4\break s1*4\break s1*4\pageBreak
        s2.*3\break \grace s8 s1 s2.\break \grace s8 s2. s1\break
      }
      \modVersion {
        s1*4 s1*23\break
      }
    >>
  >>
  \layout { short-indent = 10\mm }
  \midi { }
}