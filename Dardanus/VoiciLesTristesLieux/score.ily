\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\character Anténor
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = "Basses"
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2*5\break s2 s1*3\break \grace s8 s2. s1*2\break
      }
    >>
  >>
  \layout { short-indent = \noindent }
  \midi { }
}