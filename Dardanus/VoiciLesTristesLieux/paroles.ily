Voi -- ci les tris -- tes lieux que le mons -- tre ra -- va -- ge.
Hé -- las ! Si pour moi seul je crai -- gnais sa fu -- reur,
Je l’at -- ten -- drais sur ce ri -- va -- ge
Pour ê -- tre sa vic -- time, et non pas son vain -- queur.
