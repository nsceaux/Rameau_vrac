\clef "bass" la~ la16 si do' mi |
fa4~ fa16 sol la fa |
re mi fa re si, do re si, |
mi4~ mi16 fad mi fad |
sold la sold la si do' re' si |
do8 re mi mi, |
la,1 |
re2 mi |
la,1 |
fa,2. |
re,2 sol, |
do,4 do si, la, |
sol,2 sold, |
la, si, |
mi,1 |
