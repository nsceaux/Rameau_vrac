\clef "bass" R2*6 |
r4 r8 do' do'4 do'8 re'16 mi' |
si4 si8 re' sold4.\trill sold16 la |
la2 la8 r do'4 |
\appoggiatura si8 la4 r8 la la do' |
fa4 fa8 la fa4\trill fa8 mi |
mi4\trill r16 mi mi fad sol4 la8 si16 do' |
si4\trill si16 r si8 mi'4 mi'8 mi'16 si |
\appoggiatura si8 do'4 la8 do' fad4\trill fad8 sol |
\appoggiatura fad8 mi1 |
