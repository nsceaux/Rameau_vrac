\clef "haute-contre" fa'2 fa' |
fa' fa' |
fa' fa' |
fa' fa' |
fa' la' |
fa' fa' |
sib' sol' |
la' sol' |
fa' fa' |
fa' fa' |
fa' fa' |
fa' fa' |
fa' la' |
fa' fa' |
sib' la' |
fa' fa' |
fa' fa' |
fa' fa' |
fa' sol' la' la' |
sib8 la sib do' sib la sib do' |
sib la sib do' sib la sib do' |
sib la sol fa do'4 do' |
do'2 do''8 sib' la' sol' |
fa'1 |
fa'4 sib' la' la |
sib fa' fa' mi' |
fa'1~ |
fa'~ |
fa' |

