\piecePartSpecs
#`((dessus #:score-template "score")
   (violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (flutes #:notes "dessus")
   (hautbois #:notes "dessus")
   (parties #:score-template "score-parties")
   (haute-contre #:notes "haute-contre")
   (taille #:notes "taille")
   (bassons #:notes "basse")
   (basses #:notes "basse")
   (percussions #:score-template "score-percussion"
                #:notes "dessus" #:tag-notes dessus))
