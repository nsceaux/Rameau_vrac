\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout { s1*7\pageBreak s1*7\break s1*8\break }
    >>
  >>
  \layout {
    indent = \smallindent
    short-indent = \noindent
  }
  \midi { }
}
