\clef "dessus" <>\doux fa'8 sol' la' sol' fa' mi' re' dod' |
re' mi' fa' sol' fa' mi' re' dod' |
re' mi' fa' sol' fa' mi' re' dod' |
re'4 fa'8 la' re''2 |
re'4 la'8 re'' fa''2 |
re'4 re''8 fa'' la''2~ |
la''4 sib''8 la'' sol'' fa'' mi'' re'' |
mi'' re'' dod'' si' la' sol' fa' mi' |
fa' sol' la' sol' fa' mi' re' dod' |
re' mi' fa' sol' fa' mi' re' dod' |
re' mi' fa' sol' fa' mi' re' dod' |
re'4 fa'8 la' re''2 |
re'4 la'8 re'' fa''2 |
re'4 re''8 fa'' la''2~ |
la''4 sol''8 fa'' mi'' re'' dod'' mi'' |
re''2 re' |
la'8 sol' fa' mi' fa' sol' la' sib' |
do''2 la |
re''8 do'' sib' la' sib' do'' re'' mi'' |
fa''2 fa' |
<>_"Hautbois" <<
  { sol''2. la''4 |
    sol''2.\trill la''4 |
    sib''2. la''4 |
    sol''2\trill sol'' | } \\
  { mi''2. fa''4 |
    mi''2.\trill fa''4 |
    sol''2. fa''4 |
    mi''2\trill mi'' | }
>>
<>_"Tous" do'''8 sib'' la'' sol'' fa'' mi'' re'' do'' |
re'' fa'' mi'' sol'' fa''4 la' |
sib' re'' do''8 sib' la' sol' |
fa'1~ |
\once\tieDashed fa'~ |
fa' |
