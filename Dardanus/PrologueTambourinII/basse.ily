\clef "basse" <>\doux re2 re |
re re |
re re |
re re |
re re |
re re |
sol, sib, |
la,1 |
re2 re |
re re |
re re |
re re |
re re |
re re |
sol, la, |
re, re, |
re re |
la, la, |
sib, sib, |
fa, fa, |
<<
  { <>^"Bassons" sib8 la sib do' sib la sib do' |
    sib la sib do' sib la sib do' |
    sib la sol fa mi4 fa |
    do2 do | } \\
  { do1~ |
    do |
    mi,2. fa,4 |
    do,2 do, | }
>>
<>^"Tous" la,1 |
sib,4 do re la, |
sib,2 do |
fa1~ |
fa |
fa8 sol la sol fa mi re dod |
