\clef "taille" la2 la |
la la |
la la |
la la |
la la |
la re' |
re' re' |
dod'\trill dod' |
re' la |
la la |
la la |
la la |
la la |
la re' |
re' dod'\trill |
re' la |
la la |
la do' |
sib re' |
do' do' |
sib8 la sib do' sib la sib do' |
sib la sib do' sib la sib do' |
sib la sol fa do'4 do' |
do'2 do''8 sib' la' sol' |
fa'1 |
fa'4 sib' la' la |
sib la sol2\trill |
la1~ |
la~ |
la |
