\key re \minor
\digitTime\time 2/2
\midiTempo#280
\beginMark\markup { 
  \musicglyph #"scripts.segno"
  Deuxieme tambourin
}
s1*16 \fineMark \bar ":|."
s1*14 \bar "|."
\endMark\markup\right-column {
  \musicglyph #"scripts.segno" \null
  \line { On reprend le \concat { I \super er . } }
}