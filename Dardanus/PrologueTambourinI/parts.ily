\piecePartSpecs
#`((dessus #:tag-notes dessus #:score-template "score")
   (violon1 #:notes "dessus" #:tag-notes dessus1)
   (violon2 #:notes "dessus" #:tag-notes dessus2)
   (flutes #:notes "dessus" #:tag-notes dessus1)
   (hautbois #:notes "dessus" #:tag-notes dessus1)
   (parties #:score-template "score-parties")
   (haute-contre #:notes "haute-contre")
   (taille #:notes "taille")
   (bassons #:notes "basse")
   (basses #:notes "basse")
   (percussions #:score-template "score-percussion"
                #:notes "dessus" #:tag-notes dessus))
