\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column {
        Hautbois Flûtes Violons I & II
      }
    } << \global \keepWithTag #'dessus \includeNotes "dessus" >>
    \new Staff \with { instrumentName = "Hautes-contre" } <<
      \global \includeNotes "haute-contre"
    >>
    \new Staff \with { instrumentName = "Tailles" } <<
      \global \includeNotes "taille"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column {
        Bassons Basses
      }
    } <<
      \global \includeNotes "basse"
      \origLayout { s1*8\break }
    >>
  >>
  \layout { short-indent = \noindent }
  \midi { }
  \header {
    source = "BNF Vm²-351"
    editor = "Nicolas Sceaux"
  }
}
