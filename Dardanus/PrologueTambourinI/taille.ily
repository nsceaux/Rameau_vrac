\clef "taille" fad'2 si' |
sol' si' |
sol' la' |
la' la' |
re'4 re' re' re' |
re'2\trill dod' |
la4 la la la |
la2 la |
dod'2 mi' |
re' fad' |
re' mi' |
mi' mi' |
re'8 re' re'4 re' re' |
re' re' re' re' |
re'8 re' re'4 re' re' |
re'2 si' |
mi' sol' |
fad'\trill fad' |
