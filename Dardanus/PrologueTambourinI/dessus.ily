\clef "dessus" la''2 fad''4 re'' |
si'' sol''2 mi''4 |
dod''' la''2 fad''4 |
re'''2 re'' |
fad''4 fad'' fad''4.\trill mi''16 re'' |
mi''8 re'' dod'' si' \twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''8 fad'' sol'' mi'' |
    fad''4 fad'' fad''4. mi''16\trill re'' |
    mi''2 la' | }
  { la'8 sol' fad' mi' |
    fad'4 fad' fad'4. mi'16\trill re' |
    mi'2 la | }
>>
mi''2 dod''4 la' |
fad'' re''2 si'4 |
sold'' mi''2 dod''4 |
la''2 la' |
sol'8 la' si'4 re'' sol'' |
si'' sol'' re'' si' |
sol'8 la' si'4 re'' sol'' |
si''8 la'' sol'' fad'' mi'' re'' dod'' re'' |
la'2 mi''\trill |
re'' re' |
