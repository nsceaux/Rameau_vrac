\clef "haute-contre" re''2 fad'' |
mi'' sol'' |
mi'' sol'' |
fad''\trill fad'' |
la'4 la' la' la' |
la'2 la' |
re'4 re' re' re' |
re'2\trill dod' |
la'2 dod'' |
si' re'' |
si' re'' |
dod''\trill dod'' |
si'8 la' sol'4 si' re'' |
sol' si' sol' re'' |
si'8 la' sol'4 si' re'' |
re''2 re'' |
re'' dod''\trill |
re'' la |
