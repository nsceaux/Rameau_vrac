\clef "dessus2" r8 sol16 sol do' do' mi' mi' do' do' sol sol do' do' mi' mi' |
do' sol do' do' mi' mi' sol' sol' mi' mi' do' do' mi' mi' sol' sol' |
mi' do' mi' mi' sol' sol' do'' do'' sol' sol' mi' mi' sol' sol' do'' do'' |
sol' mi' sol' sol' do'' do'' mi'' mi'' do'' do'' sol' sol' do'' do'' mi'' mi'' |
\clef "dessus" do'' sol' do'' do'' mi'' mi'' sol'' sol'' mi'' mi'' do'' do'' mi'' mi'' sol'' sol'' |
mi'' do'' mi'' mi'' sol'' sol'' do''' do''' sol'' sol'' mi'' mi'' sol'' sol'' do''' do''' |
sol'' sol'' do''' do''' do''' sol'' do''' do''' do''' sol'' do''' do''' do''' sol'' do''' do''' |
si'' si'' sol'' sol'' re'' re'' si' si' sol' sol'' re'' re'' si' si' sol' sol' |
\clef "dessus2" re' re'' si' si' sol' sol' re' re' si si' sol' sol' re' re' si si |
sol sol sol sol sol sol sol sol sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol sol sol sol sol sol sol sol sol |
sol sol' do'' do'' do'' sol' do'' do'' do'' sol' mi'' mi'' mi'' do'' sol'' sol'' |
\clef "dessus" sol''8 r r r16 sol'( mi'8) r r r16 sol' |
mi'32*4/3 do''' sol'' mi'' sol'' mi'' do'' sol'' mi'' do'' mi'' do'' sol' mi'' do'' sol' do'' sol' mi' sol' do'' mi' sol' do'' |
la'16 do'' fa'' fa'' fa'' do'' la'' la'' la'' fa'' do''' do''' do''' la'' re''' re''' |
re'''8 r r r16 la'( fad'8) r r r16 la' |
fad'32*4/3 la'' fad'' re'' fad'' re'' la' fad'' re'' la' re'' la' fad' re'' la' fad' la' fad' re' la' re'' re' la' do'' |
si'16 re' sol' sol' sol' re' si' si' si' sol' re'' re'' re'' re'' fa'' fa'' |
fa''4 r r2 |
<>^"a 2 cordes" <<
  { \repeat unfold 16 re'''16 |
    re''' re''' re''' re''' re''' re''' re''' re'''
    do''' do''' do''' do''' do''' do''' do''' do''' |
    do''' do''' do''' do''' do''' do''' do''' do'''
    si'' si'' si'' si'' si'' si'' si'' si'' |
    si'' si'' si'' si'' si'' si'' si'' si''
    la'' la'' la'' la'' la'' la'' la'' la'' |
    la'' la'' la'' la'' la'' la'' la'' la''
    sold'' sold'' sold'' sold'' sold'' sold'' sold'' sold'' |
    sold''?4 } \\
  { \repeat unfold 16 fa''16 |
    \repeat unfold 16 mi'' |
    \repeat unfold 16 re'' |
    \repeat unfold 16 do'' |
    \repeat unfold 16 si' |
    si'4 }
>> r4 r2 |
do''16 si' la' si' do'' si' la' si' do'' si' la' si' do'' si' la' do'' |
sib' la' sol' la' sib' la' sol' la' sib' la' sol' la' sib' la' sol' la' |
sib' la' sol' la' sib' la' sol' la' sib' la' sol' la' sib' la' sol' la' |
fad'8 re'16 re' fad' fad' la' la' re'' re'' fad'' fad'' la''8 r |
r8 re'16 re' sol' sol' sib' sib' re'' re'' sol'' sol'' sib''4~ |
sib''16 la'' sol'' fa'' mib'' re'' do'' sib' la' sol' fad' mi'? re'8. la'16 |
sol'8 re''16 re'' sol''4 r8 re''16 re'' fa''4 |
r8 sol'16 sol' mi''4 r8 do''16 do'' do'4 |
r8 do''16 do'' sol''4 r8 do''16 do'' sib''4 |
r8 do''16 do'' la''4 r8 fa''16 fa'' fa'4 |
r8 do''16 do'' fa''4 r8 fa''16 fa'' la''4 |
r8 la''16 la'' do'''4 r8 la''16 la'' re'''8. re'''16 |
si'' sol'' si'' re''' si'' sol'' si'' re''' si'' sol'' si'' re''' si'' sol'' si'' re''' |
si'' sol'' si'' re''' si'' sol'' si'' re''' si'' sol'' si'' re''' si'' sol'' si'' re''' |
mi'' sol'' do''' do''' do''' sol'' do''' do''' do''' sol'' do''' do''' do''' sol'' do''' do''' |
do''' si'' la'' sol'' fa'' mi'' re'' do'' la'' sol'' fa'' mi'' re'' do'' si' la' |
si'8 r r r16 re'' mib''8 r r r16 re''( |
mib''?8) r r r16 re''( mib''8) r r4 |
r r8 r16 mib''( do''8) r r4 |
r r8 r16 mib''( re''4) r8 do'' |
r16 sol' sol' sol' si' si' re'' re'' sol'' sol' sol' sol' si' si' re'' re'' |
sol'' si' si' si' re'' re'' sol'' sol'' si'' si' si' si' re'' re'' sol'' sol'' |
si'' re'' re'' re'' sol'' sol'' si'' si'' re''' re'' re'' re'' sol'' sol'' si'' si'' |
re''' re''' re''' re''' re''' re''' re''' re''' re''' do''' si'' la'' sol'' fa'' mi'' re'' |
mi'' do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' |
do''' si'' la'' sol'' fa'' mi'' re'' do'' do''' si'' la'' sol'' fa'' mi'' re'' do'' |
do''' si'' la'' sol'' fa'' mi'' re'' do'' do''' si'' la'' sol'' fa'' mi'' re'' do'' |
sol'2 re''\trill |
\clef "dessus2" <do'' sol>16 <mi' sol> q q q q q q q q q q q q q q |
q8 \new CueVoice { r r4 r2 } |
