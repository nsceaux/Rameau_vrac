\clef "basse" <>^"Tous" r8 do16 do do8 do16 do do8 do16 do do8 do16 do |
do8 do16 do do8 do16 do do8 do16 do do8 do16 do |
do8 do16 do do8 do16 do do8 do16 do do do do do |
do8.*5/6 do32 do do do8.*5/6 do32 do do do8.*5/6 do32 do do do8.*5/6 do32 do do |
do8 do16 do do do do do do8 do16 do do do do do |
do8.*5/6 do32 do do do8.*5/6 do32 do do do8.*5/6 do32 do do do8.*5/6 do32 do do |
do8 do16 do do do do do do8 do16 do do do do do |
sol8. sol32*2/3 sol sol sol8. sol32*2/3 sol sol sol8. sol32*2/3 sol sol sol8. sol32*2/3 sol sol |
sol4 r r2 |
r8 sol16 sol si si re' re' si si sol sol si si re' re' |
si si sol sol si si re' re' si si sol sol si si re' re' |
mi,2 r |
r8 r16 do'( sib8) r r r16 do'( sib8) r |
r r32 do do do do8.*5/6 do32 do do do8.*5/6 do32 do do do8.*5/6 do32 do do |
fa,4 r r2 |
r8 r16 re'( do'8) r r r16 re'( do'8) r |
r8 r32 re re re re8.*5/6 re32 re re re8.*5/6 re32 re re re8.*5/6 re32 re re |
sol,4 r r2 |
<<
  \tag #'basson {
    <>^"Bassons" \clef "tenor"
    r4 r8 r16 re'( si8) r r4 |
    r8 r16 re'( si8) r r r16 re'( si8) r |
    r r16 sol( do'8) r r r16 sol( do'8) r |
    r r16 re'( fa'8) r r r16 si( mi'8) r |
    r r16 mi'( do'8) r r r16 mi( la8) r |
    r r16 la( mi8) r r2 | \clef "bass"
  }
  \tag #'basse {
    R1 |
    r4 r8 r16 re' sol8 r r r16 sol |
    do8 r r r16 sol( do'8) r r r16 do |
    sol,8 r r r16 re'( sold8) r r r16 mi( |
    la,8) r r r16 mi( la8) r r r16 la,( |
    mi,8) r r4 r2 |
  }
>>
R1 |
r8 red16 red red8 red16 red red4 r |
r8 re!16 re re8 re16 re re4 r |
r8 dod16 dod dod8 dod16 dod dod4 r |
do!4 r r r16 do do do |
sib,4 r r r8 r16 sib, |
do2 re |
sol,4 r8 sol16 sol si4 r8 sol16 sol |
do'4 r8 do16 do do,4 \clef "tenor" r8 do'16 do' |
mi'4 r8 mi'16 mi' sol'4 r8 do'16 do' |
fa'4 \clef "basse" r8 fa16 fa fa,4 r8 fa16 fa |
la4 r8 la16 la do'4 r8 do'16 do' |
fad4 r8 la16 la re4 r8 re16 re |
<>^"a 2 cordes" <<
  { sol8 sol16 sol sol8 sol sol sol4 sol8 |
    sol8 sol16 sol sol8 sol sol sol4 sol8 | } \\
  { sol,8 sol,16 sol, sol,8 sol, sol, sol,4 sol,8 |
    sol,8 sol,16 sol, sol,8 sol, sol, sol,4 sol,8 | }
>>
la8 la mi4 r2 |
mi16 mi mi mi mi mi mi mi fa4 r8 r16 fa |
sol8 r r4 r2 |
r r4 r8 r16 sol( |
fad8) r r4 r r8 r16 sol( |
fad8) r r4 r r8 r16 re |
fa!4 r fa16 fa fa fa fa fa fa fa |
fa8 r r4 fa16 fa fa fa fa fa fa fa |
fa8 r r4 fa16 fa fa fa fa fa fa fa |
fa8 r r4 fa16 fa fa fa fa fa fa fa |
mi8 r r4 r2 |
mi16 mi mi mi mi mi mi mi mi mi mi mi mi mi mi mi |
fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa |
sol2 sol, |
do16 do do do do do do do do do do do do do do do |
do8 \new CueVoice { r r4 r2 } |
