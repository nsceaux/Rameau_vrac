\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "[Dessus]" } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "[Hautes-contre]" } <<
      \global \keepWithTag #'haute-contre \includeNotes "parties"
    >>
    \new Staff \with { instrumentName = "[Tailles]" } <<
      \global \keepWithTag #'taille \includeNotes "parties"
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'basson \includeNotes "basse"
      { \startHaraKiri s1*18 \stopHaraKiri
        s1*6 \startHaraKiri }
    >>
    \new Staff \with { instrumentName = "[Basses]" } <<
      \global \keepWithTag #'basse \includeNotes "basse"
      \origLayout {
        s1*3 s2 \bar "" \break s2 s1*3\break s1*4\pageBreak
        s1*3\break s1*3\break s1*4\pageBreak
        s1*4\break s1*4\break s1*4\pageBreak
        s1*4 s2 \bar "" \break s2 s1*3 s2 \bar "" \break s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*3\break s1*4\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
  \header {
    source = "BNF Vm²-351"
    editor = "Nicolas Sceaux"
  }
}
