<<
  \tag #'haute-contre {
    \clef "haute-contre"
    r8 mi'16 mi' mi'8 mi'16 mi' mi'8 mi'16 mi' mi'8 sol'16 sol' |
    mi'8 mi'16 mi' sol'8 sol'16 sol' sol'8 sol'16 sol' sol'8 do''16 do'' |
    sol'8 sol'16 sol' do'' do'' mi'' mi'' do'' do'' sol' sol' do'' do'' mi'' mi'' |
    do''8 sol'16 sol' sol' sol' do'' do'' mi''8 mi'16 mi' sol' sol' do'' do'' |
    mi''8 mi'16 mi' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    do''8 sol'16 sol' do'' do'' mi'' mi'' mi''8 do''16 do'' do'' do'' mi'' mi'' |
    mi''8 mi''16 mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    re''8.*5/6 re''32 re'' re'' re''8.*5/6 re''32 re'' re''
    re''8.*5/6 si'32 si' si' si'8.*5/6 si'32 si' si' |
    si'8.*5/6 si'32 si' si' si'8.*5/6 si'32 si' si'
    re'8.*5/6 re'32 re' re' re'8.*5/6 re'32 re' re' |
    si8 sol16 sol
  }
  \tag #'taille {
    \clef "taille"
    r8 sol16 sol sol8 sol16 sol sol8 sol16 sol sol8 sol16 sol |
    sol8 sol16 sol do'8 do'16 do' do'8 mi'16 mi' do' do' mi' mi' |
    do'8 sol'16 sol' sol' sol' sol' sol' mi'8 do'16 do' mi' mi' sol' sol' |
    mi'8 do'16 do' mi' mi' sol' sol' sol'8 sol16 sol sol sol sol sol |
    sol8 mi'16 mi' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    sol'8.*5/6 sol'32 sol' sol' sol'8.*5/6 sol'32 sol' sol' sol'8.*5/6 re'32 re' re' re'8.*5/6 re'32 re' re' |
    sol'8.*5/6 sol32 sol sol sol8.*5/6 sol32 sol sol sol8.*5/6 sol32 sol sol sol8.*5/6 sol32 sol sol |
    sol8 sol16 sol
  }
>> si16 si re' re' si si sol sol si si re' re' |
si si sol sol si si re' re' si si sol sol si si si si |
do'2 r |
R1 |
r16 mi' do''8~ do''16 mi' do''8~ do''16 mi' do''8~ do''16 sol' sib' sol' |
la'4 r r2 |
R1 |
r16 fad' re''8~ re''16 fad' re''8~ re''16 fad' re''8~ re''16 la' do'' la' |
si'4 \new CueVoice { r4 r2 | R1*35 }
<<
  \tag #'haute-contre {
    <do'' sol>16 <mi' sol> q q q q q q q q q q q q q q |
    q8 \new CueVoice { r r4 r2 } |
  }
  \tag #'taille {
    mi'16 do' do' do' do' do' do' do' do' do' do' do' do' do' do' do' |
    do'8 \new CueVoice { r r4 r2 } |
  }
>>
