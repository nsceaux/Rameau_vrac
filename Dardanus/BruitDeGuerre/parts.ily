\piecePartSpecs
#`((violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (parties #:score-template "score-parties2")
   (haute-contre)
   (taille)
   (bassons #:notes "basse" #:tag-notes basson)
   (basses #:notes "basse"  #:tag-notes basse)
   (silence #:on-the-fly-markup ,#{\markup TACET #}))

