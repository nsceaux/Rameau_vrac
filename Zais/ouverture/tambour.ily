re4 r re r8 r16 re |
re4 r r16 re re re re re re re |
re4 r re r |
r8 re16 re re4 r16 re re re re re re re |
re8 r re r r16 re re re re re re re |
re2 r |
r2 r4 r8 r16 dod16 |
dod4 r8 dod16 dod dod4 r |
r16 dod dod dod dod dod dod dod dod4 r8 r16 dod |
dod4 r dod r8 dod16 dod |
do4 r r2 |
R1*2 |
r4 r8 mi16 mi mi4 r |
\parenthesize mi4 r4 r2 |
R1*8 |
r4 r r r8 r16 sol |
sol4 r sol r8 sol16 sol |
sol8 sol r4 r16 sol sol sol sol sol sol sol |
sol2 r |
R1*7 |
<>^\markup\whiteout { Le tambour roule jusqu’à \musicglyph #"accordion.oldEE" [mesure 45] }
\ru#10 do1:32 |
<>^\markup\musicglyph #"accordion.oldEE" la8 r r4 r |
R2.*71