\clef "basse" R1*2 |
r16 re re4 r8 r2 |
r2 r4 r8 r16 re |
re8 r re r r4 r8 r16 re |
re2. r8 r16 re |
fad8 fad, r4 r r8 r16 dod |
dod4 r8 dod16 dod dod4 r |
r16 dod dod dod dod dod dod dod dod4 r4 |
R1*2 |
<<
  \tag #'basson {
    r8 r16 lab16 sol4 r8 sib16 sib lab4 |
    r8 lab16 lab sol4 r8 sol16 sol fa8 fa |
    mi[ mi]
  }
  \tag #'basse {
    r8 r16 fa16 mi4 r8 mi16 mi fa4 |
    r8 fa16 fa mi4 r8 mi16 mi fa8 fa, |
    do[ do]
  }
>> r4 r2 |
R1 |
<<
  \tag #'basson { r8 r16 sol16 sol8-. sol-. r4 r8 sol16 sol | fad4 }
  \tag #'basse { r8 r16 mi16 mi8-. mi-. r4 r8 mi16 mi | red4 }
>> r4 r2 |
R1*2 |
<<
  \tag #'basson {
    r4 r8 r16 fad16 fad4 r4 |
    si8-. si-. si-. si-. r2 |
    mi'8-. mi'-. mi'-. mi'-. r2 |
    \ru#8 mi'8 |
    red'4 r4 r r8 r16 sol, |
    sol,4 r sol, r8 sol,16 sol, |
    sol,8 sol, r4 r16 sol, sol, sol, sol, sol, sol, sol, |
    sol,2 r |
  }
  \tag #'basse {
    r4 r8 r16 red16 red4 r4 |
    re!8-. re-. re-. re-. r2 |
    dod8-. dod-. dod-. dod-. r2 |
    do!8 \ru#7 do8 |
    si,4 r4 r <>^\markup\whiteout "[non div]" r8 r16 <sol, sol>16 |
    q4 r q r8 q16 q |
    q8 q r4 r16 q q q q q q q |
    q2 r |
  }
>> \allowPageTurn
R1 |
<<
  \tag #'basson {
    r2 r4 r8 r16 re'16 |
    mi'8-. mi'-. r4 r r8 r16 mi' |
    mi'4 r4 r r8 r16 la, |
    re4
  }
  \tag #'basse {
    r2 r4 r8 r16 sol16 |
    mi8-. mi-. r4 r r8 r16 mi |
    la4 r4 r4 r8 r16 la, |
    re4
  }
>> r4 r2 |
R1*2 |
\ru#16 do8 |
si, si, si, si, re re re re |
dod! dod dod dod mi mi mi mi |
re8 r r4 r8 re re re |
do do do do si, si, si, si, |
la,2 r4 r8 r16 la, |
re si, si, si, \ru#12 si, |
mi4. mi32*4/3 mi mi la4. dod32*4/3 dod dod |
re2 mi |
la,8 r r4 r | \allowPageTurn
R2.*2 |
r16 re mi fad sol la si dod' re' mi' fad' re' |
la la, si, dod re mi fad sol la si dod' la |
fad4 r fad |
sol r sol |
la r la, |
re,16 re mi fad sol la si dod' re' mi' fad' re' |
si4 r si |
mi' mi re |
dod r dod |
re r re |
mi2. |
<<
  \tag #'basson {
    dod'16( re') dod'( re') mi'( re') mi'( re') mi'( re') mi'( re') |
    dod'( re') dod'( re') mi'( re') mi'( re') mi'( re') mi'( re') |
    dod'4
  }
  \tag #'basse {
    la16( si) la( si) dod'( si) dod'( si) dod'( si) dod'( si) |
    la( si) la( si) dod'( si) dod'( si) dod'( si) dod'( si) |
    la4
  }
>> r4 mi' |
\clef "tenor" la' r r |
R2. |
\clef "basse" <<
  \tag #'basson {
    fad16( sol) fad( sol) la( sol) la( sol) la( sol) la( sol) |
    fad( sol) fad( sol) la( sol) la( sol) la( sol) la( sol) |
    fad re mi fad sol la si dod' re' mi' fad' re' |
    la4 la r |
    r16 fad sold lad si dod' re' mi' fad'8 lad\trill |
    si4 si r |
    r16 si, dod re mi fad sol la si dod' re' si |
    fad'4 fad' }
  \tag #'basse {
    re16( mi) re( mi) fad( mi) fad( mi) fad( mi) fad( mi) |
    re( mi) re( mi) fad( mi) fad( mi) fad( mi) fad( mi) |
    re re, re, re, re, re, re, re, re, re, re, re, |
    la,4 la, r4 | 
    r16 \ru#11 lad, |
    si,4 si, r |
    r16 \ru#11 si,16 |
    fad,4 fad, }
>> r4 |
r r r32 fad' mi' re' dod' si lad sold |
fad8 r r4 r |
<<
  \tag #'basson {
    r8 fad si re' fad'4 |
    r8 sol si mi' sol'4 |
    r8 fad si re' fad'4 |
    r8 sol si mi' sol'4 |
    r8 fad si re' fad' re'~ |
    re'4 dod'4.\trill sol'8 |
    fad'4 fad2 |
    si16
  }
  \tag #'basse {
    re2 r4 |
    mi2 r4 |
    re2 r4 |
    mi2 r4 |
    re2 r4 |
    mi2 r4 |
    fad8. si,16 fad4 fad, |
    si,16
  }
>> si,16 dod re mi fad sold lad si dod' re' si |
fad4 r r |
R2. |
r4 r fad |
sol sol, sol |
fad fad, fad |
mi la la, |
<<
  \tag #'basson { re,4 fad fad | }
  \tag #'basse { re,16 \ru#11 fad | }
>>
sol4 sol, fad, |
<<
  \tag #'basson { mi,4 mi mi | }
  \tag #'basse { mi,16 \ru#11 mi | }
>>
la4 la, sol, |
<<
  \tag #'basson { fad,4 fad fad | }
  \tag #'basse { fad,16 \ru#11 fad | }
>>
si4 si, la, |
sol,2 mi,4 |
<<
  \tag #'basson { \ru#18 la,4 }
  \tag #'basse { \ru#72 la,16 }
  { s2.*2 <>-"en adoucissant" s2.*2 s8 <>-"en forçant" }
>>
la,8 r r4 r |
<<
  \tag #'basson {
    \ru#3 fad,4 |
    \ru#3 sol, |
    \ru#3 la, |
    \ru#3 si, |
    \ru#3 fad, |
    \ru#3 sol, |
    \ru#3 la, |
  }
  \tag #'basse {
    \ru#12 fad,16 |
    \ru#12 sol, |
    \ru#12 la, |
    \ru#12 si, |
    \ru#12 fad, |
    \ru#12 sol, |
    \ru#12 la, |
  }
>>
re,2 r4 |
re,2 r4 |
re,2 r4 |
re,2 r4 |
<<
  \tag #'basson { \ru#3 re,4 | }
  \tag #'basse { \ru#12 re,16 | }
>>
re,4 re,2 |
la, r4 |
re,8 r r4 r |
