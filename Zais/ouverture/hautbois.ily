\clef "dessus" R1*26 |
r16 re' re' re' sol' sol' si' si' si' re' re' re' sol' sol' si' si' |
re'' sol' sol' sol' si' si' re'' re'' re'' sol' sol' sol' si' si' re'' re'' |
sol'' re'' re'' re'' sol'' sol'' si'' si'' si'' sol'' sol'' sol'' si'' si'' re''' re''' |
re'''2. r8 r16 re''' |
dod''' re''' mi''' re''' dod''' re''' mi''' re''' dod''' re''' mi''' re''' dod''' si'' la'' sol'' |
fad'' re''' dod''' si'' la'' si'' la'' sol'' fad'' re''' dod''' si'' la'' si'' la'' sol'' |
fad'' re''' dod''' si'' la'' sol'' fad'' mi'' re'' la'' sol'' fad'' mi'' re'' dod'' si' |
la' re'' dod'' si' la' sol' fad' mi' re'8 r r4 |
R1 |
r16 re' fad' re' fad' re' fad' re' fad' re' fad' re' fad' re' fad' re' |
sol' re' sol' re' sol' re' sol' re' sold' mi' sold' mi' sold' mi' sold' mi' |
la' mi' la' mi' la' mi' la' mi' lad' fad' lad' fad' lad' fad' lad' fad' |
si'8 r r4 r16 re'' re'' si' si' sold' sold' la' |
la' do'' do'' la' la' fad' fad' sol'! sol' si' si' sol' sol' mi' mi' dod' |
dod'8 la16 la si si dod' dod' re' re' mi' mi' fad' fad' sol' sol' |
fad' re'' re'' re'' \ru#12 re'' |
re''4. re''32*4/3 dod'' si' dod''4. dod''32*4/3 re'' mi'' |
si'2 si'\trill |
la'8 r8 r4 r | \allowPageTurn
R2.*3 |
r16 la'' sol'' fad'' mi'' re'' dod'' si' la' sol' fad' mi' |
re'4 r r |
R2.*3 |
r16 si'' la'' sol'' fad'' mi'' re'' dod'' si' la' sold' fad' |
mi' mi' fad' sold' la' si' dod'' re'' mi'' fad'' sold'' mi'' |
dod'' la' si' dod'' re'' mi'' fad'' sold'' la'' si'' dod''' mi'' |
fad'' sold'' la'' sold'' fad'' mi'' re'' dod'' si' la' sold' fad' |
mi'2 mi'4 |
mi'2 r4 |
r r r8 r32 mi' fad' sold' |
la'4 r8 mi'32 fad' sold' la' si'8.*1/2 mi'32 fad' sold' la' si' |
dod''8 r r4 r8 r32 la' si' dod'' |
re''4 r8 la'32 si' dod'' re'' mi''8.*1/2 la'32 si' dod'' re'' mi'' |
fad''8 r r4 r |
R2.*2 |
r16 la' si' dod'' re'' mi'' fad'' sol'' la'' si'' dod''' la'' |
fad''4 r r |
r16 si' dod'' re'' mi'' fad'' sold'' lad'' si'' dod''' re''' si'' |
fad''4 r r |
r r8 r32 fad'' mi'' re'' dod''8.*5/6 fad''32 mi'' re'' |
dod''8 r r4 r32 fad'' mi'' re'' dod'' si' lad' sold' |
fad'8 r r4 r |
r4 r r8 fad''16 re'' |
si'4 r r8 sol''16 mi'' |
si'4 r r8 fad''16 re'' |
si'4 r r8 sol''16 mi'' |
si'4 r r8 fad''16 re'' |
si'4 si''2 |
dod''8. re''16 dod''4.\trill si'8 |
si'2 r4 |
R2.*2 |
r16 re''' dod''' si'' la'' sol'' fad'' mi'' re'' dod'' si' la' |
si' sol' re' sol' si' sol' re' sol' si' si' la' sol' |
la' fad' re' fad' la' fad' re' fad' la' la' sol' fad' |
sol' mi' la mi' sol' mi' la mi' sol' sol' fad' mi' |
fad' re' mi' fad' sol' la' si' dod'' re'' mi'' fad'' re'' |
si' re' mi' fad' sol' la' si' dod'' re'' mi'' fad'' re'' |
sol'' mi' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' mi'' |
dod'' mi' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' mi'' |
la'' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' la'' fad'' |
re'' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' la'' fad'' |
si'' sol' la' si' dod'' re'' mi'' fad'' sol'' la'' si'' sol'' |
mi''4 r r16 r32 mi'' fad'' sol'' la'' si'' |
dod'''4 r8 r16*1/2 la''32 si'' dod''' re'''8.*5/6 fad''32 mi'' re'' |
la''4 r r16 r32 mi' fad' sol' la' si' |
dod''4 r8 r32 la' si' dod'' re''8.*5/6 fad'32 mi' re' |
la'4 r r |
r8 si'' sold'' mi'' r4 |
r32 la'' sold'' fad'' mi'' re'' dod'' si' la'8 r r4 |
r8 la' re'' fad'' la'' re''' |
r8 sol' si' re'' sol'' si'' |
r8 la' re'' fad'' la'' re''' |
r8 sol' si' re'' sol'' si'' |
r8 re' fad' la' re'' fad'' |
r8 sol' si'\noBeam re''4 sol''8 |
r8 la' dod'' mi'' la'' la' |
fad''2 r4 |
r r <<
  { la''4 | la''2.\trill~ | la''~ | la''2 } \\
  { fad''4 | fad''2.\trill~ | fad''~ | fad''2 }
>> r16 re'' fad'' la'' |
re'''8 r r4 r |
R2. |
re'''8 r r4 r |

