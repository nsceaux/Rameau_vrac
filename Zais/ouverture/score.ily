\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with {
        instrumentName = "Petites flûtes"
        shortInstrumentName = "PFl."
      } << \global \includeNotes "flute" >>
      \new Staff \with {
        instrumentName = "Hautbois"
        shortInstrumentName = "Hb."
      } << \global \includeNotes "hautbois" >>
    >>
    \new StaffGroupNoBracket <<
      \new GrandStaff \with {
        instrumentName = "Violons"
        shortInstrumentName = "Vn."
      } <<
        \new Staff <<
          \global \keepWithTag #'violon1 \includeNotes "violons"
        >>
        \new Staff <<
          \global \keepWithTag #'violon2 \includeNotes "violons"
        >>
      >>
      \new Staff \with {
        instrumentName = "Parties"
        shortInstrumentName = "Par."
      } << \global \keepWithTag #'parties \includeNotes "Parties" >>
    >>
    \new StaffGroupNoBracket <<
      \new Staff \with {
        instrumentName = "Bassons"
        shortInstrumentName = "Bn."
      } << \global \keepWithTag #'basson \includeNotes "basse" >>
      \new Staff \with {
        instrumentName = "Basses"
        shortInstrumentName = "Bs."
      } <<
        \global \keepWithTag #'basse \includeNotes "basse"
        \origLayout {
          s1*3\pageBreak s1*3\break s1*4\pageBreak
          s1*3\break s1*3\pageBreak s1*3\break s1*3\pageBreak
          s1*3\break s1*3\pageBreak s1*3\break s1*3\pageBreak
          s1*3\break s1*3\pageBreak s1*3\break s1 s2.*2\pageBreak
          s2.*3\break s2.*3\pageBreak s2.*3\break s2.*3\pageBreak
          s2.*3\break s2.*3\pageBreak s2.*3\break s2.*3\pageBreak
          s2.*3\break s2.*3\pageBreak s2.*3\break s2.*3\pageBreak
          s2.*3\break s2.*3\pageBreak s2.*3\break s2.*3\pageBreak
          s2.*3\break s2.*3\pageBreak s2.*3\break s2.*3\pageBreak
          s2.*3\break s2.*3\pageBreak
        }
      >>
      \new DrumStaff \with {
        \haraKiri
        instrumentName = "Tambour voilé"
        shortInstrumentName = "Tamb."
        \override StaffSymbol.line-positions = #'(0)
        \override BarLine.bar-extent = #'(-1.5 . 1.5)
        } <<
        { \noHaraKiri s1*44 \revertNoHaraKiri s2.\break }
        \global \includeNotes "tambour"
      >>
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 8\mm
  }
  \midi { }
  \header {
    editor = "Graham Sadler"
  }
}