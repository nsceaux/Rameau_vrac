\score {
  \new DrumStaff \with {
    \haraKiri
    instrumentName = "Tambour voilé"
    \override StaffSymbol.line-positions = #'(0)
    \override BarLine.bar-extent = #'(-1.5 . 1.5)
  } << \global \includeNotes "tambour" >>
  \layout { indent = \largeindent }
}
