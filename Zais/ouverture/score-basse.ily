\score {
  \new StaffGroup <<
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Bassons" }
      << \global \keepWithTag #'basson \includeNotes "basse" >>
      \new Staff \with { instrumentName = "Basses" }
      << \global \keepWithTag #'basse \includeNotes "basse" >>
      \new DrumStaff \with {
        \haraKiri
        instrumentName = "Tambour voilé"
        \override StaffSymbol.line-positions = #'(0)
        \override BarLine.bar-extent = #'(-1.5 . 1.5)
      } <<
        { \noHaraKiri s1*44 \revertNoHaraKiri s2.\break }
        \global \includeNotes "tambour"
      >>
    >>
  >>
  \layout { indent = \largeindent }
}