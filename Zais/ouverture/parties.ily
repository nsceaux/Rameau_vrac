\clef "taille" R1*2 |
r16 re' re'4 r8 r2 |
r2 r4 r8 r16 \twoVoices #'(haute-contre taille parties) <<
  { fad16 |
    la8 s re' s s4 s8 s16 fad |
    fad8-. la-. re'-. fad'-. la'4 }
  { fad16 |
    fad8 s la s s4 s8 s16 fad |
    fad8-. fad-. la-. re'-. fad'4 }
  { s16 | s8 r s r r4 r8 r16 s | }
>> r8 r16 fad |
fad8-. fad-. r4 r2 |
r8 r16 dod' dod'4 r8 r16 dod' dod'4 |
r2 r8 r16 dod' dod'8 dod' |
R1*2 |
r8 r16 lab sol4 r8 sol16 sol lab4 |
r8 lab16 lab sol4 r8 sib16 sib lab8 lab |
sol sol r4 r2 |
R1 |
r8 r16 sol sol8-. sol-. r4 r8 sol16 sol |
fad4 r r2 |
R1*2 |
r4 r8 r16 la la4 r |
\twoVoices #'(haute-contre taille parties) <<
  { mi'8-. mi'-. mi'-. mi'-. }
  { si8-. si-. si-. si-. }
>> r2 |
\twoVoices #'(haute-contre taille parties) <<
  { \ru#4 sol'8-. }
  { \ru#4 mi'-. }
>> r2 |
\ru#8 lad8 |
si4 r r r8-\tag #'parties ^"[non div]" r16 <sol sol'> |
q4 r q r8 q16 q |
q8 q r4 r16 q q q q q q q |
q2 r |
R1 |
r2 r4 r8 r16 \twoVoices #'(haute-contre taille parties) <<
  { si'16 | si'8[-. si']-. }
  { sol'16 | sol'8[-. sol']-. }
>>
r4 r r8 r16 \twoVoices #'(haute-contre taille parties) <<
  { si'16 | la'4 }
  { sol'16 | sol'4 }
>>
r4 r r8 r16 \twoVoices #'(haute-contre taille parties) <<
  { dod''16 | re''4 }
  { la'16 | la'4 }
>> r4 r2 |
R1*3 |
\twoVoices #'(haute-contre taille parties) <<
  { \ru#12 re'8 \ru#8 mi' \ru#4 fad' | fad'8 }
  { \ru#8 la | \ru#4 sol \ru#4 si | \ru#4 la \ru#4 dod' | si8 }
>> r8 r4 r8 fad' fa' fa' |
mi' mi' mib' mib' re' re' re' sol |
sol2 r4 r8 r16 dod' |
re' fad' fad' fad' \ru#12 fad' |
sold'4. sold'32*4/3 la' si' mi'4. mi'32*4/3 re' dod' |
fad'2 mi'4 re' |
dod'16 la si dod' re' mi' fad' sol'! la' mi' fad' sol' |
fad'4 r r |
R2. |
r16 re mi fad sol la si dod' re' mi' fad' re' |
la la' la' la' \ru8 la' |
la'4 r re' |
re' r re' |
re' r dod'\trill |
re'2 r4 |
r16 la' la' la' la' la' la' la' la' la' la' re'' |
si'2\trill si'4 |
mi' r mi'' |
mi''2 re''4 |
re''2 si'4\trill |
dod'16( re') dod'( re') mi'( re') mi'( re') mi'( re') mi'( re') |
dod'( re') dod'( re') mi'( re') mi'( re') mi'( re') mi'( re') |
dod'4 la mi |
la r r |
r re' la |
fad16( sol) fad( sol) la( sol) la( sol) la( sol) la( sol) |
fad( sol) fad( sol) la( sol) la( sol) la( sol) la( sol) |
la re mi fad sol la si dod' re' mi' fad' sol' |
la'4 la' r |
r16 fad sold lad si dod' re' mi' fad' sold' lad' si' |
fad'4 fad' r |
r16 si dod' re' mi' fad' sol' la' si' dod'' re'' si' |
fad'4 fad' r |
r r r32 fad' mi' re' dod' si lad sold |
fad8 r r4 r |
r8 fad si re' fad'4 |
r8 sol si mi' sol'4 |
r8 fad si re' fad'4 |
r8 sol si mi' sol'4 |
r8 fad si re' fad' si'~ |
si'2 sol'8. mi''16 |
mi''8. re''16 fad'4 mi' |
re'16 si dod' re' mi' fad' sold' lad' si' dod'' re'' si' |
fad'4 r r |
R2. |
la'2 re'4 |
re'2 re'4 |
re'2 la'4 |
la' la' la' |
\ru#12 la'16 |
la'4 sol' r |
r r sol'~ |
sol'~ sol'8*2/3 la' sol' fad'( sol' mi') |
fad'2 la'4 |
la'2 re''4 |
re''2 mi''4 |
\twoVoices #'(haute-contre taille parties) <<
  { dod''\trill }
  { la' }
>> r4 la16 la la la |
\ru#12 la |
la4 r la16 la la la |
\ru#12 la |
la4 r r |
r r r8 r16 mi'' |
mi''4 r r |
\ru#12 la'16 |
\ru#12 si' |
\ru#8 la' fad' fad' fad' fad' |
\ru#12 mi' |
fad' fad' fad' fad' la' la' la' la' re'' re'' re'' re'' |
\ru#8 re'' si' si' si' si' |
\ru#12 la' |
la'2 r4 |
la'2 r4 |
la2 r4 |
la2 r4 |
\ru#12 fad'16 |
fad'4\trill r r |
r16 la si dod' re' mi' fad' sol' la' mi' fad' sol' |
fad'8 r r4 r |
