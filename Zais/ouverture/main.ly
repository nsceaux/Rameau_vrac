\header {
  copyrightYear = "2014"
  composer = "Jean-Philippe Rameau"
  title = "Zaïs"
  subtitle = "Ouverture"
  poet = "Editeur : Graham Sadler"
}

%% LilyPond options:
%%  urtext  if true, then print urtext score
%%  part    if a symbol, then print the separate part score
#(ly:set-option 'ancient-style #f)
#(ly:set-option 'original-layout (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'non-incipit #t)

%% use baroque style repeats
#(ly:set-option 'baroque-repeats #f)

%% Staff size
#(set-global-staff-size
  (if (symbol? (ly:get-option 'part))
      18
      16))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

%% No key signature modification
#(ly:set-option 'forbid-key-modification #t)

%% Use rehearsal numbers
#(ly:set-option 'use-rehearsal-numbers #f)

\layout { reference-incipit-width = #(* 1/2 mm) }

\include "italiano.ly"
\include "common/common.ily"
\include "common/columns.ily"
\include "common/alterations.ily"
\include "common/toc-columns.ily"
\include "common/livret.ily"
\include "common/pygmalion.ily"

\setOpus "Pygmalion"

\paper {
  bookTitleMarkup = \shortBookTitleMarkup
  oddFooterMarkup = ##f
  evenFooterMarkup = ##f

  systems-per-page = #(if (symbol? (ly:get-option 'part))
                          #f
                          2)
}

\opusPartSpecs
#`((fl-hb "Petites flûtes, Hautbois" () ())
   (violon1 "Violon I" () (#:notes "violons" #:tag-notes violon1))
   (violon2 "Violon II" () (#:notes "violons" #:tag-notes violon2))
   (parties "Parties" () (#:notes "parties" #:clef "alto"))
   (basse "Bassons, Basses, Tambour" () ()))

\includeScore "ZaisOuverture"
