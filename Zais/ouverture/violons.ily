\clef "dessus" R1*2 |
<<
  \tag #'violon1 { r16 <re' la'>16 q4 r8 r2 | }
  \tag #'violon2 { r16 <la fad'>16 q4 r8 r2 | }
>>
r2 r4 r8 r16 la |
re'8 r fad' r r4 r8 r16 la |
la8-. re'-. fad'-. la'-. re''4 r8 r16 <<
  \tag #'violon1 {
    re'16 |
    dod'8-. dod'-. r4 r2 |
    r8 r16 sold' si'4 r8 r16 sold' la'4 |
    r2 r8 r16 fad' mid'8 sold' |
  }
  \tag #'violon2 {
    si16 |
    lad8-. lad-. r4 r2 |
    r8 r16 mid' sold'4 r8 r16 mid' fad'4 |
    r2 r8 r16 la sold8 mid' |
  }
>>
R1 |
r16 fa'' mib'' reb'' do'' sib' lab' sol' lab' lab' lab' lab' do'' do'' do'' do'' |
do'4 r8 r16 do' do'4 r8 do'16 do' |
do'4 r do' r8 do'16 do' |
<<
  \tag #'violon1 { do'8 do' }
  \tag #'violon2 { sol8 sol }
>> r4 r2 |
r4 r16 si mi' si sol' mi' si' sol' mi'' si' sol'' mi'' |
si''4 r r2 |
r16 si'' la'' sol'' fad'' mi'' red'' dod'' si' la' sol' fad' mi' red' dod' si |
la8. do'!16 do'8. red'16 red'8. fad'16 fad'8. la'16 |
la'8. do''16 do''8. red''16 red''8. fad''16 fad''8. la''16 |
la''8. do'''16 do'''2 r16 do''16 si' la' |
sold'8-. sold'-. sold'-. sold'-. r4 r16 sold' lad' si' |
lad'4 r r8 r16 lad' dod'' dod'' mi'' mi'' |
sol''2. r16 sol'' fad'' mi'' |
red'' si'' la'' sol'' fad'' mi'' red'' dod'' si' la' sol' fad' mi' red' dod' si |
R1*2 |
r16 re' re' re' sol' sol' si' si' si' re' re' re' sol' sol' si' si' |
re'' sol' sol' sol' si' si' re'' re'' re'' sol' sol' sol' si' si' re'' re'' |
sol'' re'' re'' re'' sol'' sol'' si'' si'' si'' sol'' sol'' sol'' si'' si'' re''' re''' |
re'''2. r8 r16 re''' |
dod''' re''' mi''' re''' dod''' re''' mi''' re''' dod''' re''' mi''' re''' dod''' si'' la'' sol'' |
fad'' re''' dod''' si'' la'' si'' la'' sol'' fad'' re''' dod''' si'' la'' si'' la'' sol'' |
fad'' re''' dod''' si'' la'' sol'' fad'' mi'' re'' la'' sol'' fad'' mi'' re'' dod'' si' |
la' re'' dod'' si' la' sol' fad' mi' re'8 r r4 |
R1 |
r16 re' fad' re' fad' re' fad' re' fad' re' fad' re' fad' re' fad' re' |
sol' re' sol' re' sol' re' sol' re' sold' mi' sold' mi' sold' mi' sold' mi' |
la' mi' la' mi' la' mi' la' mi' lad' fad' lad' fad' lad' fad' lad' fad' |
si'8 r r4 r16 re'' re'' si' si' sold' sold' la' |
la' do'' do'' la' la' fad' fad' sol'! sol' si' si' sol' sol' mi' mi' dod'! |
dod'8 la16 la si si dod' dod' re' re' mi' mi' fad' fad' sol' sol' |
fad' re'' re'' re'' \ru#12 re'' |
<<
  \tag #'violon1 {
    re''4. re''32*4/3 dod'' si' dod''4. dod''32*4/3 re'' mi'' |
    si'2 si'\trill |
    la'8 r8 r4 r |
    r16 re' mi' fad' sol' la' si' dod'' re'' mi'' fad'' re'' |
    la' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' la'' fad'' |
    mi''8 la'' la'' la'' la'' re''' |
    dod'''16 la'' sol'' fad'' mi'' re'' dod'' si' la' sol' fad' mi' |
    re' re''' dod''' si'' la'' sol'' fad'' mi'' re'' dod'' si' la' |
    si' dod'' re'' dod'' si' la' sol' fad' mi' re' dod' si |
    la si dod' re' mi' fad' sol' mi' la' mi' fad' sol' |
    fad'4 r r |
    r16 re''' re''' re''' re''' re''' re''' re''' re''' re''' re''' re''' |
    re'''4 r r |
    r16 dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' |
    dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' si'' si'' si'' si'' |
    si'' si'' si'' si'' si'' si'' si'' si'' re''' re''' re''' re''' |
    dod'''4 r r |
  }
  \tag #'violon2 {
    re''4. si'32*4/3 la' sold' la'4. la'32*4/3 la' la' |
    la'2 sold'?\trill |
    la'16 la si dod' re' mi' fad' sol'! la' mi' fad' sol' |
    fad'4 r r |
    r16 re' mi' fad' sol' la' si' dod'' re'' mi'' fad'' re'' |
    la' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' la'' fad'' |
    mi'' la'' sol'' fad'' mi'' re'' dod'' si' la' sol' fad' mi' |
    re'4 r la' |
    la' sol' si' |
    mi' r la |
    la2 r4 |
    r16 fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    sold''4 r r |
    r16 la'' la'' la'' la'' la'' la'' la'' la'' la'' la'' la'' |
    la'' la'' la'' la'' la'' la'' la'' la'' la'' la'' la'' la'' |
    la'' la'' la'' la'' la'' la'' la'' la'' sold'' sold'' sold'' sold'' |
    la''4 r r |
  }
>>
r4 r r8 r32 mi' fad' sold' |
la'4 r8 mi'32 fad' sold' la' si'8.*1/2 mi'32 fad' sold' la' si' |
dod''8 r r4 r8 r32 la' si' dod'' |
re''4 r8 la'32 si' dod'' re'' mi''8.*1/2 la'32 si' dod'' re'' mi'' |
fad''8 r r4 r |
R2. |
<<
  \tag #'violon1 {
    re'''16 re''' re''' re''' re''' re''' re''' re''' re''' re''' re''' re''' |
    re'''4 dod''' r |
    dod'''16 dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' |
    dod'''4 si'' r |
    si''16 si'' si'' si'' si'' si'' si'' si'' si'' si'' si'' si'' |
    si''4 lad'' r |
  }
  \tag #'violon2 {
    fad''16 fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    fad''4 mi'' r |
    mi''16 mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    mi''4 re'' r |
    re''16 re'' re'' re'' re'' re'' re'' re'' re'' re'' re'' re'' |
    re''4 dod'' r |
  }
>>
r4 r r32 fad'' mi'' re'' dod'' si' lad' sold' |
fad'8 r r4 r |
<<
  \tag #'violon1 {
    r16 fad'' si'' re''' si'' fad'' si'' re''' si'' fad'' si'' re''' |
    sol''8 mi'' dod'' si' lad' dod'' |
    si'16 fad'' si'' re''' si'' fad'' si'' re''' si'' fad'' si'' re''' |
    sol''8 mi'' dod'' si' lad' dod'' |
    si'16 fad'' si'' re''' si'' fad'' si'' re''' si'' fad'' si'' re''' |
    sol'' la'' si'' la'' sol'' la'' sol'' fad'' mi'' fad'' mi'' re'' |
    dod''8. re''16 dod''4.\trill si'8 |
  }
  \tag #'violon2 {
    R2. |
    r16 dod'' mi'' sol'' mi'' dod'' mi'' sol'' mi'' dod'' mi'' sol'' |
    fad''8 re'' si' fad' re' fad' |
    dod'16 dod'' mi'' sol'' mi'' dod'' mi'' sol'' mi'' dod'' mi'' sol'' |
    fad''8 re'' si' fad' re' fad''~ |
    fad''4 mi''16 fad'' mi'' re'' dod'' re'' dod'' si' |
    lad'8. si'16 lad'4.\trill si'8 |
  }
>>
si'16 si dod' re' mi' fad' sold' lad' si' dod'' re'' si' |
fad'4 r r |
R2. |
r16 re''' dod''' si'' la'' sol'' fad'' mi'' re'' dod'' si' la' |
si' sol' re' sol' si' sol' re' sol' si' si' la' sol' |
la' fad' re' fad' la' fad' re' fad' la' la' sol' fad' |
sol' mi' la mi' sol' mi' la mi' sol' sol' fad' mi' |
fad' re' mi' fad' sol' la' si' dod'' re'' mi'' fad'' re'' |
si' re' mi' fad' sol' la' si' dod'' re'' mi'' fad'' re'' |
sol'' mi' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' mi'' |
dod'' mi' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' mi'' |
la'' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' la'' fad'' |
re'' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' la'' fad'' |
si'' sol' la' si' dod'' re'' mi'' fad'' sol'' la'' si'' sol'' |
mi''4 r <la'' la'>16 q q q |
<<
  \tag #'violon1 {
    <la' sol''>16 q q q q q q q <fad'' la'> q q q |
    <la' mi''>4 r <la' la>16 q q q |
    <la sol'> q q q q q q q <fad' la> q q q |
    <la mi'>4 r16 r32 dod'' re'' mi'' fad'' sold'' la''8.*1/2 re''32 mi'' fad'' sold'' la'' |
    si''4 r16 r32 re'' mi'' fad'' sold'' la'' si''8.*1/2 mi''32 fad'' sold'' la'' si'' |
    dod'''8 r r4 r |
    \ru#80 re'''16 dod''' dod''' dod''' dod''' |
    re''' la'' si'' sol'' la'' fad'' sol'' mi'' fad'' re'' mi'' dod'' |
    re'' la' si' sol' la' fad' sol' mi' fad' re' mi' dod' |
  }
  \tag #'violon2 {
    <la' mi''>16 q q q q q q q <re'' la'> q q q |
    <la' dod''>4 r <fad' la>16 q q q |
    <la mi'> q q q q q q q <re' la>16 q q q |
    <la dod'>4 r r |
    r r16 r32 si' dod'' re'' mi'' fad'' sold''8.*1/2 dod''32 re'' mi'' fad'' sold'' |
    la''8 r r4 r |
    \ru#12 re''16 |
    \ru#12 mi'' |
    \ru#12 fad'' |
    \ru#12 sol'' |
    \ru#12 la'' |
    \ru#12 si'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' sol'' sol'' sol'' sol'' |
    fad'' fad'' sol'' mi'' fad'' re'' mi'' dod'' re'' fad'' sol'' mi'' |
    fad'' fad' sol' mi' fad' re' mi' dod' re' fad' sol' mi' |
  }
>>
re' la fad' re' la' fad' re'' la' fad'' re'' la'' fad'' |
re'' la fad' re' la' fad' re'' la' fad'' re'' la'' fad'' |
re'' la' re'' fad'' re'' la' fad'' la'' fad'' re'' fad'' la'' |
re''' re'' dod'' si' la' sol' fad' mi' re' re' dod' si |
<<
  \tag #'violon1 { la8 r r4 r | }
  \tag #'violon2 { la16 la si dod' re' mi' fad' sol' la' mi' fad' sol' | }
>>
re'''8 r r4 r |
