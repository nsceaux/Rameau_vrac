\midiTempo#120 \tempo "un peu lent"
\key re \major \digitTime\time 2/2 s1*10
\key fa \minor s1*4
\key sol \major s1*17
\key re \major s1*13
\digitTime\time 3/4 \tempo "Reprise vite" s2.
\bar ".|:" \segnoMark s2.*68 \alternatives s2.*2 s2. \bar "|."