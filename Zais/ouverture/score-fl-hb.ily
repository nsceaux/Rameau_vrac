\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Petites flûtes" }
    << \global \includeNotes "flute" >>
    \new Staff \with { instrumentName = "Hautbois" }
    << \global \includeNotes "hautbois" >>
  >>
  \layout { indent = \largeindent }
}
