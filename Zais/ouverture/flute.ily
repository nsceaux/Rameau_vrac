\clef "dessus" R1*44 |
R2.*11 |
la''16 si'' dod''' si'' la'' si'' dod''' si'' la'' si'' dod''' si'' |
la'' si'' dod''' si'' la'' si'' dod''' la'' si'' dod''' re''' dod''' |
si'' dod''' re''' dod''' si'' la'' sold'' fad'' mi'' re'' dod'' si' |
la'2 r4 |
R2.*2 |
r8 la''16 sold'' la'' sold'' la'' sold'' la'' mi'' fad'' sol'' |
fad''8 r r4 r |
r r r8 r32 la' si' dod'' |
re''4 r8 la'32 si' dod'' re'' mi''8.*1/2 la'32 si' dod'' re'' mi'' |
fad''8 r r4 r |
r4 << { mi'''16 re''' dod''' re''' mi'''4 }
  \\ { dod'''16 si'' la'' si'' dod'''4 } >> |
R2. |
r4 << { re'''16 dod''' si'' dod''' re'''4 }
  \\ { si''16 la'' sold'' lad'' si''4 } >> |
R2. |
r4 << { dod'''16 si'' lad'' si'' dod'''4 }
  \\ { lad''16 sold'' fad'' sold'' lad''4 } >> |
R2.*2 |
r4 r r8 fad''16 re'' |
si'4 r r8 sol''16 mi'' |
si'4 r r8 fad''16 re'' |
si'4 r r8 sol''16 mi'' |
si'4 r r8 fad''16 re'' |
si'4 si''2 |
dod''8. re''16 dod''4.\trill si'8 |
si'2 r4 |
<<
  { fad''16( sol'') fad''( sol'') la''( sol'') la''( sol'') la''( sol'') la''( sol'') |
    fad''( sol'') fad''( sol'') la''( sol'') la''( sol'') la''( sol'') la''( sol'') |
    fad''4
  } \\
  { re''16( mi'') re''( mi'') fad''( mi'') fad''( mi'') fad''( mi'') fad''( mi'') |
    re''( mi'') re''( mi'') fad''( mi'') fad''( mi'') fad''( mi'') fad''( mi'') |
    re''4
  }
>> r4 r |
r << { si''4 re''' } \\ { sol'' si'' } >> |
r4 << { la'' re''' } \\ { fad'' la'' } >> |
r4 <<
  { re'''4 dod'''\trill |
    re''' la''8*2/3( sol'' fad'') fad''( mi'' re'') |
    re''2\trill
  } \\
  { mi''4 mi''\trill |
    re'' fad''8*2/3( mi'' re'') re''( dod'' si') |
    si'2\trill
  }
>> r4 |
r <<
  { si''8*2/3( la'' sol'') sol''( fad'' mi'') |
    mi''2\trill } \\
  { sol''8*2/3( fad'' mi'') mi''( re'' dod'') |
    dod''2\trill }
>> r4 |
r <<
  { dod'''8*2/3( si'' la'') la''( sol'' fad'') |
    fad''2\trill } \\
  { la''8*2/3( sol'' fad'') fad''( mi'' re'') |
    re''2\trill }
>> r4 |
r <<
  { re'''8*2/3( dod''' si'') si''( la'' sol'') |
    sol''4\trill } \\
  { si''8*2/3( la'' sol'') sol''( fad'' mi'') |
    dod''4\trill }
>> r4 r16 r32 mi'' fad'' sol'' la'' si'' |
dod'''4 r8 r16*1/2 la''32 si'' dod''' re'''8.*5/6 fad''32 mi'' re'' |
la''4 r r16 r32 mi' fad' sol' la' si' |
dod''4 r8 r32 la' si' dod'' re''8.*5/6 fad'32 mi' re' |
la'4 r r |
r8 si'' sold'' mi'' r4 |
r32 la'' sold'' fad'' mi'' re'' dod'' si' la'8 r r4 |
r r r8 la''16 fad'' |
re''2 r8 si''16 sol'' |
re''2 r8 la''16 fad'' |
re''4. mi''16 fad'' sol'' la'' si'' dod''' |
re'''4 r <<
  { la''4~ |
    la''4. sol''16 fad'' sol''4~ |
    sol''4 sol''4.\trill( fad''16 sol'') |
    fad''2\trill } \\
  { fad''4~ |
    fad''4. mi''16 re'' mi''4~ |
    mi''4 mi''2\trill |
    re''2 }
>> r4 |
r r <<
  { la''4 | la''2.\trill~ | la''~ | la''2 } \\
  { fad''4 | fad''2.\trill~ | fad''~ | fad''2 }
>> r16 re'' fad'' la'' |
re'''8 r r4 r |
R2. |
re'''8 r r4 r |
