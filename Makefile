OUTPUT_DIR=out
DELIVERY_DIR=delivery
NENUVAR_LIB_PATH=$$(pwd)/../nenuvar-lib
LILYPOND_CMD=lilypond -I$$(pwd) -I$(NENUVAR_LIB_PATH) \
  --loglevel=WARN -ddelete-intermediate-files \
  -dno-protected-scheme-parsing
PROJECT=Rameau

LeTempleDeLaGloire/ouverture:
	mkdir -p $(DELIVERY_DIR)/$(subst /,_,$@)
	$(LILYPOND_CMD) -o $(DELIVERY_DIR)/$(subst /,_,$@)/$(PROJECT)_$(subst /,_,$@) $@/main.ly
	$(LILYPOND_CMD) -o $(DELIVERY_DIR)/$(subst /,_,$@)/$(PROJECT)_$(subst /,_,$@)-violon1 -dpart=violon1 $@/main.ly
	$(LILYPOND_CMD) -o $(DELIVERY_DIR)/$(subst /,_,$@)/$(PROJECT)_$(subst /,_,$@)-violon2 -dpart=violon2 $@/main.ly
	$(LILYPOND_CMD) -o $(DELIVERY_DIR)/$(subst /,_,$@)/$(PROJECT)_$(subst /,_,$@)-parties -dpart=parties $@/main.ly
	$(LILYPOND_CMD) -o $(DELIVERY_DIR)/$(subst /,_,$@)/$(PROJECT)_$(subst /,_,$@)-basses -dpart=basses $@/main.ly
	$(LILYPOND_CMD) -o $(DELIVERY_DIR)/$(subst /,_,$@)/$(PROJECT)_$(subst /,_,$@)-bassons -dpart=bassons $@/main.ly
	$(LILYPOND_CMD) -o $(DELIVERY_DIR)/$(subst /,_,$@)/$(PROJECT)_$(subst /,_,$@)-flutes -dpart=flutes $@/main.ly
	$(LILYPOND_CMD) -o $(DELIVERY_DIR)/$(subst /,_,$@)/$(PROJECT)_$(subst /,_,$@)-trompette -dpart=trompette $@/main.ly
	$(LILYPOND_CMD) -o $(DELIVERY_DIR)/$(subst /,_,$@)/$(PROJECT)_$(subst /,_,$@)-cors -dpart=cors $@/main.ly
	$(LILYPOND_CMD) -o $(DELIVERY_DIR)/$(subst /,_,$@)/$(PROJECT)_$(subst /,_,$@)-timbales -dpart=percussions $@/main.ly
	$(LILYPOND_CMD) -o $(DELIVERY_DIR)/$(subst /,_,$@)/$(PROJECT)_$(subst /,_,$@)-hautbois -dpart=hautbois $@/main.ly
.PHONY: LeTempleDeLaGloire/ouverture

# génération des partitions
# Conducteur
conducteur:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)  \
	main.ly
.PHONY: conducteur
# Instruments
violon1:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-violon1 -dpart=violon1  \
	part.ly
.PHONY: violon1
violon2:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-violon2 -dpart=violon2  \
	part.ly
.PHONY: violon2
flutes:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-flutes -dpart=flutes  \
	part.ly
.PHONY: flutes
hautbois:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-hautbois -dpart=hautbois  \
	part.ly
.PHONY: hautbois
bassons:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-bassons -dpart=bassons  \
	part.ly
.PHONY: bassons
percussions:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-percussions -dpart=percussions \
	part.ly
.PHONY: percussions
haute-contre:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-haute-contre -dpart=haute-contre  \
	part.ly
.PHONY: haute-contre
taille:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-taille -dpart=taille  \
	part.ly
.PHONY: taille
basses:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-basses -dpart=basses  \
	part.ly
.PHONY: basses

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e $(OUTPUT_DIR)/$(PROJECT).pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT).pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-violon1.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-violon1.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-violon2.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-violon2.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-flutes.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-flutes.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-hautbois.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-hautbois.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-bassons.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-bassons.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-percussions.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-percussions.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-haute-contre.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-haute-contre.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-taille.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-taille.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-basses.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-basses.pdf $(DELIVERY_DIR)/; fi
.PHONY: delivery

clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)-* $(OUTPUT_DIR)/$(PROJECT).*
.PHONY: clean

parts: flutes hautbois bassons \
	violon1 violon2 haute-contre taille basses \
	percussions
.PHONY: parts

all: check parts conducteur delivery clean
.PHONY: all

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in parent directory:"; \
	  echo " cd .. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: check
