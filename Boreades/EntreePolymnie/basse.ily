\clef "basse" re1 |
la, |
si, |
fad, |
sol, |
re,2 re |
la1~ |
la8 la sol\trill fad sol2~ |
sol8 sol fad\trill mi fad fad sol fad |
mi2 la |
re r8 re mi fad |
sol4 fad\trill mi8 mi fad sol |
la2 r8 la, si, dod |
re dod re mi fad4 re\trill |
mi8 re mi fad sold la fad sold |
la dod' dod re mi4 mi, |
fad,2 fad\doux |
mi1 |
re |
r2 dod4 r |
re r mi r |
fad r sold r |
la re mi mi, |
la, la8 sol fad sol fad mi |
la,2 r |
R1 |
r4 re8 mi fad sol fad sol |
la2 lad |
si si, |
fad1 |
r2 r4 fad\doux |
sol1 |
re2 mi |
fad4 si fad fad, |
si,4 si,8\fort dod re mi re mi |
fad2 fad, |
sol,4 sol,8 la, si, dod si, dod |
re2 re, |
la, la~ |
la8 la si fad sol2~ |
sol~ sol8 la mi sol |
fad mi fad sol la4 la, |
si,2 si\doux |
la1 |
sol |
r2 fad4 r |
sol r la r |
si r dod'\fortSug r |
re'8 re' fad\trill sol la4 la, |
re2
