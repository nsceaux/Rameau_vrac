\piecePartSpecs
#`((flutes #:score "score-dessus")
   (hautbois #:score "score-dessus")
   (violon1 #:notes "dessus" #:tag-notes dessus1)
   (violon2 #:notes "dessus" #:tag-notes dessus2)
   (dessus #:score "score-dessus")
   (parties)
   (haute-contre)
   (taille)
   (bassons #:notes "basson" #:clef "tenor")
   (basses #:notes "basse"))
