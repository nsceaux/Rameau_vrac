\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Flûtes Violons }
    } <<
      \global \keepWithTag #'dessus \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "Parties" } <<
      \global \includeNotes "parties"
    >>
    \new Staff \with { instrumentName = "Bassons" } <<
      \global \includeNotes "basson" \clef "tenor/bass"
    >>
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
    >>
  >>
  \layout { short-indent = \noindent }
  \midi { }
}
