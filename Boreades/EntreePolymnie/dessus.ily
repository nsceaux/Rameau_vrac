\clef "dessus" r2 re'''~ |
re'''4 dod'''8 si'' la'' sol'' fad''\trill mi'' |
re''2 si''~ |
si''4 la''8 sol'' fad'' mi'' re''\trill dod'' |
si'2 sol''~ |
sol''4 fad''8\trill mi'' fad'' mi'' fad'' sol'' |
mi''2\trill r8 mi'' la'' dod'' |
\appoggiatura dod'' re''2 r8 re'' mi'' si' |
\appoggiatura si' dod''2 r8 la' re'' fad' |
\appoggiatura fad' sol'4. sol'8 sol'4.\trill fad'8 |
\twoVoices #'(dessus1 dessus2 dessus) <<
  { fad'8 la' si' dod'' re''4 dod''\trill |
    si'8 si' dod'' re'' mi''4 re''\trill |
    dod''8 mi'' fad'' sol'' la''4 sol''\trill |
    fad'' fad''8( mi'') mi''( re'') re'' dod'' |
    si'4.\trill
  }
  { fad'2 fad'4 mi'\trill |
    re'8 re' mi' fad' sol'4 fad'\trill |
    mi'8 dod'' re'' mi'' dod''4\trill mi'' |
    la'2. si'8 la' |
    sold'4.\trill
  }
>> mi''8 re''4.\trill dod''16 si' |
dod''8 mi'' la'' dod'' si'4.\trill la'8 |
la'4 si'8\doux( la') dod''( si') re''( fad') |
\appoggiatura fad' sold'4 la'8( sold') si'( la') dod''( mi') |
\appoggiatura mi'8 fad'4 sold'8( fad') la'( sold') si'( sold') |
fad'4( mi'8)\trill r r mi' la' dod'' |
r fad' si' re'' r sold' dod'' mi'' |
r la' re'' fad'' r fad''\fort mi''\trill re'' |
dod'' mi'' la'' dod'' si'4.\trill la'8 |
la'1 |
la'2
\twoVoices #'(dessus1 dessus2 dessus) <<
  { la''2~ |
    la''4 sol''8.\trill fad''16 \appoggiatura fad''8 sol''4. la''8 |
    fad''2\trill }
  { r2 |
    r4 dod''8 re'' mi'' dod'' re'' mi'' |
    la'2 }
>> re'''2~ |
re'''4 dod'''8\trill si''
\twoVoices #'(dessus1 dessus2 dessus) <<
  { dod'''2~ |
    dod'''4 re'''8 dod''' dod'''4 fad''8 si'' |
    lad''4\trill re''8(\doux dod'') re''( dod'') mi''( dod'') |
    \appoggiatura dod''8 re''4 mi''8( re'') mi''( re'') fad''( re'') |
    \appoggiatura re''8 mi''4 fad''8( mi'') fad''( mi'') sol''( mi'') |
    \appoggiatura mi''8 fad''4 si''8 la'' sol'' fad'' mi'' re'' |
    dod''4 re'' dod''4.\trill si'8 |
    si'2 }
  { lad''8 sold'' fad'' mi'' |
    re''4 si''8 la'' sol'' fad'' mi'' re'' |
    dod''4\trill si'8(\doux lad') si'( lad') dod''( lad') |
    \appoggiatura lad'?8 si'4 dod''8( si') dod''( si') re''( si') |
    \appoggiatura si'8 dod''4 re''8( dod'') re''( dod'') mi''( dod'') |
    \appoggiatura dod''8 re''4. fad''8 mi'' re'' dod'' si' |
    lad'4 si' lad'4.\trill si'8 |
    si'2 }
>> si''\fort ~ |
si''4 la''8 sol'' fad'' mi'' re'' dod'' |
si'2 sol''~ |
sol''4 fad''8\trill mi'' fad'' mi'' fad'' sol'' |
mi''2\trill r8 mi'' la'' dod'' |
dod''4.( re''8\prall) r8 re'' mi'' si' |
dod''\trill la' dod'' re'' mi'' fad'' sol'' mi'' |
la'' dod'' re'' sol'' mi''4.\trill re''8 |
re''4 mi''8\doux( re'') fad''( mi'') sol''( si') |
\appoggiatura si'8 dod''4 re''8( dod'') mi''( re'') fad''( la') |
\appoggiatura la'8 si'4 dod''8( si') re''( dod'') mi''( dod'') |
si'4( la'8) r r la' re'' fad'' |
r si' mi'' sol'' r dod'' fad'' la'' |
r re'' sol'' si'' r si''\fort la''\trill sol'' |
fad'' la'' re''' fad'' mi''4.\trill re''8 |
re''2
