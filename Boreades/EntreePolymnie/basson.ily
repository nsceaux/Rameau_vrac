\clef "tenor/tenor" r4 re'8 dod' si la sol\trill fad |
mi2 dod'~ |
dod'4 si8 la sol fad mi re |
dod2 \clef "basse" la2~ |
la4 sol8 fad mi re dod si, |
la,2 \clef "tenor" re'2~ |
re'4 dod'8\trill si dod'4 re'8 dod' |
si2. dod'8 re' |
la2~ la4. re'8 |
re'2 dod'\trill |
re' r |
R1 |
r2 mi'~ |
mi' la4 re'~ |
re'4 si mi'2~ |
mi'4. fad'8 mi'4 re' |
dod'2\trill re'\doux ~ |
re' dod'~ |
dod' si~ |
si4 r la mi'~ |
mi' re'2 dod'4~ |
dod'2 si\fort ~ |
si8 mi la si sold4.\trill la8 |
la1 |
la2 r |
r4 la8 si dod' la si dod' |
re'2 r8 fad' sol' fad' |
mi'2. sol'4 |
fad'1 |
R1*5 |
r4 re'8 mi' fad' fad' mi'\trill re' |
dod'2\trill fad'~ |
fad'4 sol'8 fad' mi' re' dod'\trill si |
la2 re'~ |
re'4 dod'8\trill si dod'4 re'8 dod' |
si2 r8 si dod' re' |
la4. si8 dod' re' mi' dod' |
re' mi' la re' dod'4.\trill re'8 |
re'2 sol'\doux ~ |
sol' fad'~ |
fad' mi'~ |
mi'4 re' la'2~ |
la'4 sol'2 fad'4~ |
fad' mi'2 mi'4\fortSug |
la8 re' fad' mi'16 re' dod'4.\trill re'8 |
re'2
