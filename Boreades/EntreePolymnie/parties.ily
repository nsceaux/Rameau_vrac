\clef "taille" fad'2 la'~ |
la'2. sol'4 |
fad'1 |
fad'~ |
fad'2 mi'~ |
mi' re'4 la' |
la'2. sol'4 |
fad' mi'8 re' mi'2~ |
mi'4 re'8 dod' re'4 \appoggiatura dod'8 si4 |
si2 la |
la r |
R1 |
r2 dod''~ |
dod''4.( re''16 dod'') si'4 fad' |
mi'4 si'2 si'4~ |
si' la' sold'4.\trill la'8 |
la'2 la\doux |
si la |
la1 |
sold4\trill r la dod''~ |
dod'' si'2 la'4~ |
la'2 mi'\fort ~ |
mi'4 fad' mi' re' |
dod'1 |
dod'2 r |
r4 la8 si dod' la si dod' |
re'2 la'~ |
la'4 mi''8 re'' dod'' si' lad' dod'' |
fad'2 r |
R1 |
r2 r4 si'\doux ~ |
si'1~ |
si'2 dod''4 mi''~ |
mi'' fad' fad'2 |
\appoggiatura mi'8 re'4 re'8\fort mi' fad' fad' mi'\trill re' |
dod'2\trill la' |
re' re''~ |
re'' la'~ |
la' r4 mi' |
fad' sol'8 fad' mi'2~ |
mi' la'~ |
la'4. si'8 la'4 sol' |
fad'2 re'\doux |
mi' re' |
re'1 |
dod'4\trill re' fad'2~ |
fad'4 mi'2 re'4~ |
re' sol'2 la'4\fort |
la'4. si'8 la'4 sol' |
fad'2
