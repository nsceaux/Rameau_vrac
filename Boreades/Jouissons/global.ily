\key do \major
\digitTime\time 2/2 \midiTempo#160 s1*61
\digitTime\time 3/4 \midiTempo#120 \grace s8 s2 \fineMark \bar "|."
s4 s2.*15
\digitTime\time 2/2 \midiTempo#160 s1 \bar "|." \dacapoMark