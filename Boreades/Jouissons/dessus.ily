\clef "dessus" R1 |
r2 r4 fa''8 mi'' |
re''4 do'' re'' mi'' |
la'2 r |
<>^"Violons" mi''2\doux re''4\trill do'' |
si'2\trill r4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''4 |
    mi'' fa''8( mi'') fa''( mi'') re''(\trill dod'') |
    re''( do''!) si'( do'') re''( la'') sold''( la'') |
    si''4 r r mi' |
    mi'2 r4 mi''8 la'' |
    sold''4\trill }
  { do''4 |
    do'' re''8( do'') re''( do'') si'( la') |
    si'( la') sold'( la') si'2~ |
    si'8( re'') do''( si') do''( si') la'( sold') |
    la'4 si'8 re'' do''4 re''8 do'' |
    si'4\trill }
>> \cesureInstr <>^"Tous" \twoVoices #'(dessus1 dessus2 dessus) <<
  { sold''8 la'' si''4 la''8 sold'' |
    la''2. la''8 la'' |
    la''4 la'' si'' do''' |
    si''2\trill }
  { si'8 do'' re''4 do''8 si' |
    do''2. do''8 do'' |
    re''4 mi'' fa'' mi'' |
    re''2\trill }
>> \cesureInstr <>^"Violons" \doux \twoVoices #'(dessus1 dessus2 dessus) <<
  { do'''2~ |
    do'''2. si''4\trill |
    \appoggiatura si''8 do'''2 si''4\trill mi'' | }
  { do''2 |
    do''2. re''4 |
    sol'2 r4 mi'' | }
>>
mi''8 re'' do'' re'' mi'' fa'' mi'' fa'' |
mi'' re'' do'' re'' mi'' fa'' mi'' fa'' |
mi''2 r4 <>^"Tous" \fort \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''4 |
    sol''8 fa'' mi'' fa'' sol'' la'' sol'' la'' |
    sol'' fa'' mi'' fa'' sol'' la'' sol'' la'' |
    sol''2. sol''4 |
    sol'' fa''8 mi'' re''4\trill re''8 mi'' |
    \appoggiatura re''8 do''2 }
  { mi''4 |
    mi''8 re'' do'' re'' mi'' fa'' mi'' fa'' |
    mi'' re'' do'' re'' mi'' fa'' mi'' fa'' |
    mi''2. mi''4 |
    mi'' re''8 do'' si'4\trill si'8 do'' |
    do''2 }
>> r2 |
R1*4 |
r2 r4 do''\doux |
do''8 si' la' si' do'' re'' do'' re'' |
do'' si' la' si' do'' re'' do'' re'' |
do''2 r4 <>^"Tous" <>\fort \twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''4 |
    mi''8 re'' do'' re'' mi'' fa'' mi'' fa'' |
    mi'' re'' do'' re'' mi'' fa'' mi'' fa'' |
    mi''4 mi''8 fa'' mi''4 re''8 do'' |
    si'2\trill }
  { do''4 |
    do''8 si' la' si' do'' re'' do'' re'' |
    do'' si' la' si' do'' re'' do'' re'' |
    do''4 do''8 re'' do''4 si'8 la' |
    sold'2\trill }
>> r4 <>^\markup\whiteout Violons \doux \twoVoices #'(dessus1 dessus2 dessus) <<
  { do''4 |
    do''2 si'\trill |
    do''2. mi''4 |
    mi''2( la'8) r re''4 |
    si'2.\trill }
  { mi'4 |
    fa'2 sol' |
    do'2. la4 |
    re'2. si4 |
    mi'2. }
>> <>^"Tous" mi''4 |
do''2\trill \appoggiatura si'8 la' r \twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''4 |
    mi'' re''8 do'' si'4\trill si'8 do'' |
    la'2. r4 |
    r2 r4 la'' |
    si''8 la'' sol'' la'' si''2~ |
    si''~ si''8 do''' sold'' si'' |
    la'' sol''! fad'' sold'' la''2~ |
    la''~ la''8 si'' fad'' sold'' |
    sold''4\trill }
  { do''4 |
    do'' si'8 la' sold'4\trill sold'8 la' |
    la'2. mi''4 |
    fa''8 mi'' re'' mi'' fa''2~ |
    fa''~ fa''8 sol'' re'' fa'' |
    mi'' re'' do'' re'' mi''2~ |
    mi''~ mi''8 fa'' dod'' mi'' |
    re'' do''! si' do'' re''2~ |
    re''4 }
>> re''8 do'' si' la' sold' fad' |
mi'4 fad'8 sold' la' si' do'' re'' |
mi''4 re''8 do'' si' la' sold' fad' |
mi'4 mi''8 fad'' sold'' la'' si'' do''' |
re'''4. do'''8 si'' la'' sold'' fad'' |
mi''4 fad''8 sold'' la'' si'' do''' mi'' |
mi''4 re''8 do'' si'4\trill si'8 do'' |
\appoggiatura si'8 la'2. mi''4 |
fad''2\trill fad''4 sold'' |
la''2 \twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''2~ |
    mi''4 fa''8 mi'' re'' mi'' do'' re'' |
    mi''2 r4 la'' |
    la''2.\trill la''8 sold'' |
    \appoggiatura sold''8 la''2 }
  { do''2~ |
    do''4 re''8 do'' si' do'' la' si' |
    do''2 r4 fa'' |
    si'2. si'8 do'' |
    \appoggiatura si'8 la'2 }
>> r4 |
R2.*15 R1 |
