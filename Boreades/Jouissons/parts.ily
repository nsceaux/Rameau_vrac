\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes dessus1 #:score-template "score-voix")
   (violon2 #:notes "dessus" #:tag-notes dessus2 #:score-template "score-voix")
   (haute-contre #:score-template "score-voix")
   (taille #:score-template "score-voix")
   (hautbois #:score "score-dessus")
   (bassons #:notes "basse" #:score-template "score-voix")
   (basses #:notes "basse" #:score-template "score-basse-continue-voix"))
