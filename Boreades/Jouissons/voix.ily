<<
  \tag #'basse { \clef "vhaute-contre" R1*61 r4 r }
  \tag #'calisis {
    \clef "vhaute-contre"
    r4 mi'8 mi' mi'4 la'8 la' |
    la'2. fa'8 mi' |
    re'4 do' re' mi' |
    la2 do'4 \appoggiatura si8 la4 |
    mi' fad' \appoggiatura fad'8 sold'4\prall la' |
    sold'2\trill sold'8 r mi'4 |
    mi'4\melisma fa'8[ mi'] fa'[ mi' re' dod'] |
    re'[ do'! si do'] re'2~ |
    re'8[ fa' mi' re'] mi'[ re' do' si]( |
    do'4)\melismaEnd si8 re' do'4 si8 la |
    mi'1~ |
    mi'2 r4 la'8 la' |
    la'4 la' si' do'' |
    si'2\trill sol'4 la' |
    sol'4 la' sol' fa' |
    mi'2\trill re'8 r sol'4 |
    sol'8[\melisma fa' mi' fa'] sol'[ la' sol' la'] |
    sol'[ fa' mi' fa'] sol'[ la' sol' la']( |
    sol'2)\melismaEnd sol'8 r do''4 |
    do''1~ |
    do''~ |
    do''2 do''8 r sol'4 |
    sol' fa'8 mi' re'4\trill mi'8 fa' |
    \appoggiatura re'8 do'4 mi'8 fa' sol'4 fa'8 mi' |
    re'2.\trill <<
      { \voiceOne sol'8 sol' \oneVoice }
      \new Voice \with { autoBeaming = ##f } { \voiceTwo sol8 la }
    >> si4 do' re' mi' |
    fa'2 la4 si\trill |
    do' re' mi' la' |
    sold'2\trill sold'8 r mi'4 |
    mi'8[\melisma re' do' re'] mi'[ fa' mi' fa'] |
    mi'[ re' do' re'] mi'[ fa' mi' fa']( |
    mi'2)\melismaEnd mi'8 r la'4 |
    la'1~ |
    la' |
    la'4 r8 fa' mi'4 re'8 do' |
    si2\trill r4 do'4 |
    re'8[\melisma do' si do'] re'[ mi' re' fa'] |
    mi'[ re' do' re'] mi'[ fa' mi' sol'] |
    fad'[ mi' re' mi'] fad'[ sol' fad' la'] |
    sold'4\trill fad'8[ mi'] fad'[ sold' la' si']( |
    do''2)\melismaEnd do''8 r do''4 |
    do'' si'8 la' sold'4\trill sold'8 la' |
    la'1 |
    r2 r4 la' |
    si'8[\melisma la' sol' la'] si'2~ |
    si'~ si'8[ do'' sold' si'] |
    la'[ sol'! fad' sold'] la'2~ |
    la'~ la'8[ si' fad' sold'] |
    sold'1\trill~ |
    sold'\melismaEnd |
    \appoggiatura fad'8 mi'2 r |
    r2 r4 mi' |
    mi'2\melisma fad'8[ sold' la' si']( |
    do''2)\melismaEnd do''8 r mi'4 |
    la' la'8 si' sold'4\trill sold'8 la' |
    la'1 |
    R1 |
    r2 r4 la' |
    la'8[\melisma sol' fa' mi'] re'[ do' si la]( |
    la'2)\melismaEnd la'8 r fa'4 |
    mi'2. fa'8 si |
    \appoggiatura si8 do'2
  }
  \tag #'vdessus {
    \clef "vdessus" R1 |
    r2 r4 fa''8 mi'' |
    re''4 do'' re'' mi'' |
    la'2 r |
    R1*6 |
    r4 si'8^\doux do'' re''4 do''8 si' |
    do''2. do''8 do'' |
    re''4 mi'' fa'' mi'' |
    re''2\trill r |
    R1*2 |
    r2 do''4^\doux sol' |
    do'' mi'' do'' sol' |
    do''2 do''8 r mi''4^\fort |
    mi''8[\melisma re'' do'' re''] mi''[ fa'' mi'' fa''] |
    mi''[ re'' do'' re''] mi''[ fa'' mi'' fa'']( |
    mi''2)\melismaEnd mi''8 r <<
      { \voiceOne sol''4 |
        sol'' fa''8 mi'' re''4\trill re''8 mi'' |
        \appoggiatura re''8 do''2 \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo mi''4 |
        mi''4 re''8 do'' si'4\trill si'8 do'' |
        do''2
      }
    >> <>^\markup\center-align "Tous" r4 do''8^\doux do'' |
    si'2\trill r |
    r r4 sib'8 do'' |
    la'2\trill r |
    r r4 do''8 re'' |
    si'2\trill r |
    r la'4^\fort mi' |
    la' do'' la' mi' |
    la'2 la'8 r do''4 |
    do''8[\melisma si' la' si'] do''[ re'' do'' re''] |
    do''[ si' la' si'] do''[ re'' do'' re'']( |
    do''4)\melismaEnd do''8 re'' do''4 si'8 la' |
    sold'2\trill r |
    R1*3 |
    r2 r4 mi'' |
    do''2\trill \appoggiatura si'8 la' r mi''4 |
    mi'' re''8 do'' si'4\trill si'8 do'' |
    \appoggiatura si' la'2. mi''4 |
    fa''8[\melisma mi'' re'' mi''] fa''2~ |
    fa''~ fa''8[ sol'' re'' fa''] |
    mi''[ re'' do'' re''] mi''2~ |
    mi''~ mi''8[ fa'' dod'' mi''] |
    re''[ do''! si' do''] re''2~ |
    re''1~ |
    re''~ |
    re''2\melismaEnd re''4 r |
    R1 |
    r2 r4 mi'' |
    mi''1~ |
    mi''4 re''8 do'' si'4\trill si'8 do'' |
    \appoggiatura si'8 la'2 r4 mi'' |
    fad''2\trill fad''4 sold'' |
    la''1~ |
    la''2.\melisma sol''8[\trill fa'']( |
    mi''2)\melismaEnd mi''8 r la''4 |
    la''2.\trill la''8 sold'' |
    \appoggiatura sold''8 la''2\prall
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R1 |
    r2 r4 fa'8 mi' |
    re'4 do' re' mi' |
    la2 r |
    R1*6 |
    r4 sold'8^\doux la' si'4 la'8 sold' |
    la'2. mi'8 mi' |
    fa'4 sol' sol' sol |
    sol2 r |
    R1*4 |
    r2 r4 sol'^\fort |
    sol'8[\melisma fa' mi' fa'] sol'[ la' sol' la'] |
    sol'[ fa' mi' fa'] sol'[ la' sol' la']( |
    sol'2)\melismaEnd sol'8 r do''4 |
    \appoggiatura si'8 la'4 la'8 fa' fa'4 fa'8 sol' |
    mi'2\trill r4 sol'8^\doux sol' |
    sol'2 r |
    r r4 re'8 mi' |
    fa'2 r |
    r r4 mi'8 la' |
    sold'2\trill r |
    R1*2 |
    r2 r4 mi'4^\fort |
    mi'8[\melisma re' do' re'] mi'[ fa' mi' fa'] |
    mi'[ re' do' re'] mi'[ fa' mi' fa']( |
    mi'4)\melismaEnd mi'8 fa' mi'4 mi'8 mi' |
    mi'2 r |
    R1*3 |
    r2 r4 sold' |
    la'2 la'8 r la'4 |
    la' la'8 la' sold'4\trill sold'8 la' |
    la'2. la'4 |
    la'2.\melisma si'8[ la'] |
    sol'1~ |
    sol'2. la'8[ sol'] |
    fa'1~ |
    fa'2\melismaEnd fa'4 fa' |
    si1~ |
    si |
    si4 r r2 |
    R1 |
    r2 r4 mi' |
    la'2 la'8 r la'4 |
    do'' si'8 la' sold'4\trill sold'8 la' |
    la'1 |
    R1 |
    r2 r4 mi' |
    mi'\melisma fa'8[ mi'] re'[ mi' do' re']( |
    mi'2)\melismaEnd mi'4 fa' |
    si2. fa'8 mi' |
    mi'2
  }
  \tag #'vtaille {
    \clef "vtaille" R1 |
    r2 r4 fa'8 mi' |
    re'4 do' re' mi' |
    la2 r |
    R1*6 |
    r2 r4 mi'8^\doux mi' |
    mi'2. do'8 do' |
    do'4 do' re' mi' |
    re'2\trill r |
    R1*4 |
    r2 r4 do'^\fort |
    do'1~ |
    do' |
    sol4 r r do' |
    do' do'8 do' sol4 sol8 sol |
    sol2 r4 fa'8^\doux mi' |
    re'2\trill r |
    r r4 re'8 re' |
    re'2 r |
    r r4 mi'8 mi' |
    mi'2 r |
    R1*2 |
    r2 r4 mi'^\fort |
    mi'8[\melisma re' do' re'] mi'[ fa' mi' fa'] |
    mi'[ re' do' re'] mi'[ fa' mi' fa']( |
    mi'4)\melismaEnd mi'8 re' do'4 re'8 do' |
    si2\trill r |
    R1*3 |
    r2 r4 mi' |
    mi'2 mi'8 r mi'4 |
    fa' fa'8 fa' mi'4 mi'8 si |
    \appoggiatura si8 do'2. do'4 |
    do'2 do'4 re' |
    re'1~ |
    re'4\melisma mi'8[ re'] do'2~ |
    do'1~ |
    do'4 re'8[ do'] si2~ |
    si1~ |
    si\melismaEnd |
    si4 r r2 |
    R1*2 |
    r2 r4 la |
    la la8 fa' mi'4 mi'8 si |
    do'1 |
    R1 |
    r2 r4 do' |
    do'\melisma re'8[ do'] si[ do' la si]( |
    do'2)\melismaEnd do'4 si |
    si2. re'8 si |
    \appoggiatura si8 do'2
  }
  \tag #'vbasse {
    \clef "vbasse" R1 |
    r2 r4 fa'8 mi' |
    re'4 do' re' mi' |
    la2 r |
    R1*6 |
    r4 mi8^\doux mi mi4 mi8 mi |
    la2. la8 sol |
    fa4 mi re\trill do |
    sol2 r |
    R1*5 |
    r2 do'4^\fort sol |
    do' mi' do' sol |
    do'2 do'8 r mi4 |
    fa4 fa8 fa sol4 sol8 sol |
    do2 r4 do8^\doux do |
    sol2 r |
    r r4 sol8 sol |
    re2 r |
    r r4 la8 la |
    mi2 r |
    R1*3 |
    r2 la4^\fort mi |
    la do' la mi |
    la2 la8 r la,4 |
    mi2 mi8 r r4 |
    R1*3 |
    r2 r4 mi |
    la2 la8 r do4 |
    re re8 re mi4 mi8 mi |
    la,2. la4 |
    re2~ re8[ fa mi fa]( |
    sol2) sol4 sol |
    do2~ do8[ mi re mi]( |
    fa2) fa4 fa |
    si,2 si,4 si |
    mi'\melisma re'8[ do'] si[ la sold fad] |
    mi4 fad8[ sold] la[ si do' re'] |
    mi'4 re'8[ do'] si[ la sold fad]( |
    mi2)\melismaEnd mi4 r |
    R1 |
    r2 r4 do' |
    fa fa8 re mi4 mi8 la |
    la1 |
    r2 r4 mi' |
    do'8[\melisma re' do' si] la[ sol fa mi] |
    fa2. mi8\trill[ re]( |
    do2)\melismaEnd do4 re |
    mi2. re8 mi |
    la,2
  }
>>
<<
  \tag #'(vdessus vhaute-contre vtaille vbasse) { r4 | R2.*15 R1 | }
  \tag #'(calisis basse) {
     r8 <>^\markup\italic gracieux do'8 |
    do'4.( si8)\trill do' si |
    si4.( la8\trill) sold la |
    sold4.\trill do'8 si mi' |
    mi'4.( re'16)\trill do' si8 do' |
    do'2\trill \appoggiatura si8 la4~ |
    la r8 do' si do' |
    re' mi' mi'4.( fa'16)\prall mi' |
    \appoggiatura mi'8 re'2\trill r8 sol' |
    sol'2~ sol'8 fa'16[ mi'] |
    re'4.( mi'16[ fa']) mi'[ re'] do'32[ si] do'16 |
    do'8.([ re'16] re'2\trill) |
    do'2. |
    mi'2 mi'8 re' |
    fa'2. |
    la2 sold8 la |
    si1\trill |
  }
>>