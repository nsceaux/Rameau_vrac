\clef "taille" R1 |
r2 r4 fa''8 mi'' |
re''4 do'' re'' mi'' |
la'2 r |
R1*6 |
r2 r4 mi''8 mi'' |
mi''2. mi'8 mi' |
fa'4 sol' sol' sol' |
sol'2 mi'4\doux fa' |
mi' fa' mi'\trill re' |
do'2 sol8 r r4 |
r2 do''4 sol' do'' mi'' do'' sol' |
do''2 do'8 r do''4\fort |
do''1~ |
do''~ |
do''4. sol'8 sol'4 do'' |
\appoggiatura si'8 la'4 la'8 la' sol'4 sol'8 sol' |
sol'2 r |
R1*3 |
la4 si do' la |
mi'1 |
r2 la'4 mi' |
la' do'' la' mi' |
la'2 r4 la' |
la'1~ |
la'~ |
la'4. la'8 la'4 mi' |
mi'2 r |
R1*3 |
r2 r4 mi' |
mi'2. mi'4 |
fa' fa'8 fa' mi'4 mi'8 mi' |
mi'2. do''4 |
do''2. re''8 do'' |
si'2 re''~ |
re''4 mi''8 re'' do''2~ |
do'' la' |
fa'' re'' |
re''1~ |
re''~ |
re''~ |
re''2 r |
r2 r4 mi' |
la'2. do''4 |
do'' si'8 la' mi'4 mi'8 mi' |
mi'1 |
R1 |
r2 r4 la' |
la'8 sol' fa' mi' re' do' si la |
la'2. fa'4 |
mi'2. fa'8 mi' |
mi'2 r4 |
R2.*15 R1 |
