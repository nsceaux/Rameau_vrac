\score {
  \new StaffGroupNoBar \with { \haraKiri } <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with {
        instrumentName = \markup\center-column { Hautbois Violons }
      } <<
        \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
        \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
      >>
      \new Staff \with { instrumentName = "Parties" } <<
        \global \keepWithTag #'parties \includeNotes "parties"
      >>
    >>
    \new ChoirStaff \with { instrumentName = \markup\character Chœur } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with { instrumentName = \markup\character Calisis } \withLyrics <<
      \global \keepWithTag #'calisis \includeNotes "voix"
    >> \keepWithTag #'calisis \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion { s1*61 s2 \break }
      \origLayout {
        s1*7\break s1*7\pageBreak
        s1*8\break \grace s8  s1*6\pageBreak
        s1*7\break s1*7\pageBreak
        \grace s8 s1*9\break s1*8\pageBreak
        s1*2 s2.*5\break s2.*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
