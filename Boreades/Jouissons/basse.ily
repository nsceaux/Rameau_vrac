\clef "basse" R1 |
r2 r4 fa'8 mi' |
re'4 do' re' mi' |
la2 r |
<>^"Basses" do'2\doux si4\trill la |
mi'2 r |
la r |
la r |
sold r4 mi |
la sold\trill la sold8 la |
mi4 \cesureInstrDown <>^"Tous" mi8\doux mi mi4 mi8 mi |
la2. la8 sol |
fa4 mi re\trill do |
sol2 r |
R1*4 |
r2 r4 <>^"a 2 c." <do do,>4 |
q1~ |
q~ |
q2. mi4 |
fa fa8 fa sol4 sol8 sol |
do2 r4 do8\doux do |
sol2 r |
r r4 sol8 sol |
re2 r |
r r4 la8 la |
mi2 r |
R1*2 |
r2 r4 la, |
la,1~ |
la,~ |
la,2. la,4 |
mi,2. r4 |
R1*3 |
r2 r4 mi |
la2. do4 |
re re8 re mi4 mi8 mi, |
la,2. la4 |
re2~ re8 fa mi fa |
sol2. sol4 |
do2~ do8 mi re mi |
fa2. fa4 |
si,2. si4 |
mi' re'8 do' si la sold fad |
mi4 fad8 sold la si do' re' |
mi'4 re'8 do' si la sold fad |
mi4 re8 do si, la, sold, fad, |
mi,2 r |
r r4 do' |
fa4 fa8 re mi4 mi8 la |
la1 |
r2 r4 mi' |
do'8 re' do' si la sol fa mi |
fa2.( mi8\trill re) |
do2 do4 re |
mi2. re8 mi |
la,2 r8 <>^"B.C." la8 |
mi2. |
fa2~ fa4\trill |
mi4. mi8 re do |
re2 mi4 |
la,2. r4 r la |
si2~ si8 do' |
sol2~ sol8 mi |
fa2.~ |
fa |
mi8. fa16 sol4 sol, |
do2. |
la, |
re |
fa |
mi1 |
