\tag #'calisis {
  Jou -- ïs -- sons, jou -- ïs -- sons, jou -- ïs -- sons de nos beaux ans,
  les jours faits pour la ten -- dres -- se
  s’en -- vo -- lent sur l’ai -- le du temps. __
  Jou -- ïs -- sons de nos beaux ans,
  les jours faits pour la ten -- dres -- se
  s’en -- vo -- lent,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps.
  Jou -- ïs -- sons, jou -- ïs -- sons, jou -- ïs -- sons de nos beaux ans,
  les jours faits pour la ten -- dres -- se
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent sur l’ai -- le du temps.
}
\tag #'vdessus {
  Jou -- ïs -- sons de nos beaux ans,
  Jou -- ïs -- sons, jou -- ïs -- sons, jou -- ïs -- sons de nos beaux ans,
  Les jours faits pour la ten -- dres -- se
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps.
  Jou -- ïs -- sons, jou -- ïs -- sons, jou -- ïs -- sons,
  les jours faits pour la ten -- dres -- se
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps.
}
\tag #'vhaute-contre {
  Jou -- ïs -- sons de nos beaux ans,
  Jou -- ïs -- sons, jou -- ïs -- sons, jou -- ïs -- sons de nos beaux ans,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps.
  Jou -- ïs -- sons, jou -- ïs -- sons, jou -- ïs -- sons,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent,
  s’en -- vo -- lent,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent sur l’ai -- le du temps.
}
\tag #'vtaille {
  Jou -- ïs -- sons de nos beaux ans,
  Jou -- ïs -- sons, jou -- ïs -- sons de nos beaux ans,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps.
  Jou -- ïs -- sons, jou -- ïs -- sons, jou -- ïs -- sons,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent sur l’ai -- le du temps.
}
\tag #'vbasse {
  Jou -- ïs -- sons de nos beaux ans,
  Jou -- ïs -- sons, jou -- ïs -- sons, jou -- ïs -- sons de nos beaux ans,
  Les jours faits pour la ten -- dres -- se
  s’en -- vo -- lent sur l’ai -- le du temps.
  Jou -- ïs -- sons, jou -- ïs -- sons, jou -- ïs -- sons,
  les jours faits pour la ten -- dres -- se
  s’en -- vo -- lent,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent,
  s’en -- vo -- lent,
  s’en -- vo -- lent,
  s’en -- vo -- lent,
  s’en -- vo -- lent sur l’ai -- le du temps,
  s’en -- vo -- lent sur l’ai -- le du temps.
}
\tag #'(calisis basse) {
  Zé -- phire em -- bel -- lit dans nos champs
  les fleurs nais -- san -- tes qu’il ca -- res -- se,
  et c’est l’a -- mour qui rend tou -- chants
  les traits dont bril -- le la jeu -- nes -- se.
  Mais leur é -- clat n’a qu’un prin -- temps.
}
