\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with {
        instrumentName = \markup\center-column { Violons Hautbois }
      } << \global \includeNotes "violon" >>
      \new Staff \with {
        instrumentName = \markup\center-column {
          Hautes-contre
        }
      } << \global \includeNotes "haute-contre" >>
      \new Staff \with {
        instrumentName = \markup\center-column {
          Tailles
        }
      } << \global \includeNotes "taille" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character La Folie
      \consists "Metronome_mark_engraver"
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { Bassons Basses }
    } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s16 s4 s2.*2 s1*2\break s2. s1*2\break s1*3\break
        s2 s1*6 s2 \pageBreak
        s2.*7\break s2.*8\break s2.*7\pageBreak
        s2.*7\break s2.*6 s4. \bar "" \pageBreak
        s4. s2.*5 s4. \bar "" \break s4. s2.*6\pageBreak
        \grace s16 s2.*7 s4. \bar "" \break
        s4. s2.*6\break s2.*5 s4. \bar "" \pageBreak
        s4. s2.*6 s4. \bar "" \break s4. s2.*6\pageBreak
        s2.*7\break \grace s16 s2.*8 s4. \bar "" \pageBreak
        s4. s2.*6 s4. \bar "" \break s4. s2.*6 s4. \bar "" \pageBreak
        s4. s2.*7\break s2.*3\break s2.*5 s1\pageBreak
        s1*7\break s1*5\break
        s2.*6\break s2.*6\pageBreak
      }
    >>
  >>
  \layout { short-indent = \noindent }
  \midi { }
}
