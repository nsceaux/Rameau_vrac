\key re \major
\digitTime\time 3/4 \midiTempo#80 \partial 16*5 s16*5 s2.*2
\digitTime\time 2/2 \midiTempo#160 \grace s16 s1
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 \grace s16 s1
\digitTime\time 2/2 \grace s16 s1
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 2/2 \midiTempo#160 s1 \bar "|."
\measure 1/2 s2
\measure 2/2 s1*6
\measure 1/2 s2 \bar "|."
\tempo "Gay" \time 6/8 \customTime\markup\vcenter\number 2 s2.*126
\digitTime\time 2/2 \tempo "moins vite" s1*3
\tempo "Lent" s1*4 s2
\tempo "Vite" s2 s1*2 s2
\tempo "Lent" s2 s1*2
\time 6/8 s2.*65 \bar "|."
