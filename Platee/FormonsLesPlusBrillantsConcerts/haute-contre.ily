\clef "haute-contre" r16 r4 |
R2.*2 R1*2 R2. R1*5 R1*7 |
la4 r8 la4 r8 |
la4 r8 la'4 r8 |
fad''4 r8 fad''4 r8 |
fad''4 r8 r4 re''8 |
re''4. r4 re''8 |
la'4. r4 re''8 |
re''4. r4 re''8 |
la'4. r4 re''8 |
re''4. re' |
dod' re' |
mi' fad' |
sol' fad' |
mi' re' |
mi' r4 re'8 |
re'4. la |
la re'~ |
re' dod'\trill |
re'8 fad' la' re'' la' re' |
re'4. dod'\trill |
re'2. |
r4 la'8 la'4 mi'8 |
mi'4.~ mi'4 fad'8 |
si4. la |
la2. |
r2*3/4 r4 la'8 |
la'4. mi' |
fad' re'' |
dod''4 r8 re''4 r8 |
sol'4 r8 fad'4 r8 |
mi'4 r8 re'4 r8 |
dod'4 r8 re'4 r8 |
la'4 r8 r2*3/4 |
r la4.~ |
la r2*3/4 |
r r4 fad'8 |
mi'4. mi' |
si r2*3/4 |
mi'4. r2*3/4 |
mi'4. re'4 dod'8 |
si4\trill r8 dod'4 r8 |
si4 r8 la'4 r8 |
sold'4 r8 la'4 r8 |
re''4 r8 dod''4 r8 |
si'4.\trill r4 dod''8 |
dod''4 si'8 si'4. |
mi' r2*3/4 |
r r8 r fad' |
mi'4. re' |
dod' r2*3/4 |
fad'4\tresdouxSug mi'8 red' mi' fad' |
si4. r4 mi'8 |
\appoggiatura re'16 dod'4. re'8 si mi' |
si4. r4 mi'8 |
\appoggiatura re'16 dod'4. re'8 si mi' |
mi'2. |
r4 la'8 la'4 si'8 |
mi'4. re' |
dod'4 r8 la' la' re'' |
si'4. mi'' |
dod''\fortSug\trill mi''~ |
mi'' re''~ |
re''2. |
si' |
mi'4. r4 la'8 |
la'4. sold'\trill |
la'8 dod'' mi'' dod'' la' la' |
mi'4. sold'\trill |
la' r2*3/4 |
R2.*7 |
dod'4\douxSug r8 re'4 r8 |
mi'4 r8 fad'4 r8 |
sol'4 r8 fad'4 r8 |
mi'4 r8 re'4 r8 |
mi'8 sol' fad' mi' re' dod' |
dod'4. mi' |
la4. la |
la la |
la8 la' sol' fad'4. |
mi'2. |
fad' |
la'4. re'4 dod'8 |
re'4 mi'8 fad'4 re'8 |
re'4. sol' |
fad'4 r8 r2*3/4 |
mi'2.~ |
mi'8 r r r2*3/4 |
mi'2.~ |
mi'8 r r r2*3/4 |
r r4 fad'8 |
fad'4 mi'8 mi'4.\trill |
re'8 r r re' re' la' |
la'4. sol' |
fad'\fortSug la4 r8 |
la4 r8 la'4 r8 |
fad''4 r8 fad''4 r8 |
fad''4 r8 r4 re''8 |
re''4. r4 re''8 |
la'4. r4 re''8 |
re''4. r4 re''8 |
la'4. r4 re''8 |
re''4. re' |
dod' re' |
mi' fad' |
sol' fad' |
mi' re' |
mi'4. r8 r re' |
re'4. mi' |
la re'~ |
re' dod'\trill |
re'8 fad' la' re'' la' re' |
re'4. dod'\trill |
re' r4 r8 |
R2.*3 |
lad'4\douxSug r8 si'4 r8 |
dod''4 r8 re''4 r8 |
mi''4 r8 re''4 r8 |
dod''4 r8 si'4. |
lad'4 dod''8 fad'4 r8 | \allowPageTurn
R1*13 R2.*7 |
r2*3/4 r4 mi'8 |
fad'4. mi' |
mi' re' |
mi' fad' |
sol' fad' |
mi' re' |
dod'4 mi'8 la4 r8 |
R2.*7 |
dod'4\douxSug r8 re'4 r8 |
mi'4 r8 fad'4 r8 |
sol'4 r8 fad'4 r8 |
mi'4 r8 re'4 r8 |
mi'8 sol' fad' mi' re' dod' |
dod'4. mi' |
la4. la |
la la |
la8 la' sol' fad'4. |
mi'2. |
fad' |
la'4. re'4 dod'8 |
re'4 mi'8 fad'4 re'8 |
re'4. sol' |
fad'4 r8 r2*3/4 |
mi'2.~ |
mi'8 r r r2*3/4 |
mi'2.~ |
mi'8 r r r2*3/4 |
r r4 fad'8 |
fad'4 mi'8 mi'4.\trill |
re'8 r r re' re' la' |
la'4. sol' |
fad'\fortSug la4 r8 |
la4 r8 la'4 r8 |
fad''4 r8 fad''4 r8 |
fad''4 r8 r4 re''8 |
re''4. r4 re''8 |
la'4. r4 re''8 |
re''4. r4 re''8 |
la'4. r4 re''8 |
re''4. re' |
dod' re' |
mi' fad' |
sol' fad' |
mi' re' |
mi'4. r8 r re' |
re'4. mi' |
la re'~ |
re' dod'\trill |
re'8 fad' la' re'' la' re' |
re'4. dod'\trill |
re' r4 r8 |
R2. |
