For -- mons les plus bril -- lants con -- certs ;
Quand Ju -- pi -- ter por -- te les fers
de l'in -- com -- pa -- ra -- ble Pla -- té -- e,
je veux que les trans -- ports de son âme en -- chan -- té -- e,
s'ex -- pri -- ment par mes chants di -- vers.

Es -- say -- ons du bril -- lant,
don -- nons dans la sail -- li -- e !

Aux lan -- gueurs d’A -- pol -- lon, Daph -- né se re -- fu -- sa __
Daph -- né, Daph -- né se re -- fu -- sa :
L’A -- mour sur son tom -- beau, __
é -- tei -- gnit son flam -- beau,
la mé -- ta -- mor -- pho -- sa, __
la mé -- ta -- mor -- pho -- sa,
la mé -- ta -- mor -- pho -- sa, __
la mé -- ta -- mor -- pho -- sa,
la mé -- ta -- mor -- pho -- sa.

L’A -- mour sur son tom -- beau, __
é -- tei -- gnit son flam -- beau,
la mé -- ta -- mor -- pho -- sa, __
la mé -- ta -- mor -- pho -- sa, __
la mé -- ta -- mor -- pho -- sa, __
la mé -- ta -- mor -- pho -- sa,
la mé -- ta -- mor -- pho -- sa.

C'est ain -- si que l’A -- mour de tout temps s'est ven -- gé
de tout temps s'est ven -- gé : __
Que l’A -- mour est cru -- el, quand il est ou -- tra -- gé,
quand il est ou -- tra -- gé !

Aux lan -- gueurs d’A -- pol -- lon, Daph -- né se re -- fu -- sa __
L’A -- mour sur son tom -- beau, __
é -- tei -- gnit son flam -- beau,
la mé -- ta -- mor -- pho -- sa, __
la mé -- ta -- mor -- pho -- sa, __
la mé -- ta -- mor -- pho -- sa, __
la mé -- ta -- mor -- pho -- sa,
la mé -- ta -- mor -- pho -- sa.
