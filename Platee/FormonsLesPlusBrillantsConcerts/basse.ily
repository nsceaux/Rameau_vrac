\clef "basse" <>^"Basses" sol16 fad8.\trill mi16 |
re2 r8 re |
sol4. mi8 la8. re16 |
la,1 |
r2 re4 do |
si,~ si,16 la, sol,8 fad,8. mi,16 |
re,2 r4 re |
\appoggiatura mi16 red1 |
mi2 dod~ |
dod4 si,8 la, mi4 mi, |
la,1 |
<>_"pincez" <re la>4 q |
q2 <re re'>4 q |
q2 <re la>4 q |
q2 r |
r <la, mi la>4 q |
q2 r |
r2 re,4 re, |
re,2 |
<>^"Basses & bassons" re4 r8 re4 r8 |
re4 r8 re4 r8 |
re8 la, fad re la fad |
re' la re' la fad re |
sol4. r4 sol8 |
fad4. r4 re8 |
sol4. r4 sol,8 |
fad,4. r4 re,8 |
sol,4 r8 si,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la4 r8 |
si4 r8 dod'4 r8 |
re'4 r8 fad4 sol8 |
la4 r8 la,4 r8 |
re8 fad la re' la re |
la,4. la, |
re,2. |
r4 re'8 dod'4 si8 |
la4.~ la4 fad8 |
sol4. la |
re2. |
r2*3/4 r4 re8 |
dod2. |
si,4. mi |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4. r4 la8 |
dod4.~ dod4 la,8 |
re4. r2*3/4 |
r r4 re'8 |
si4 la8 sold4 fad8 |
mi4.~ mi8 mi fad |
sold mi fad sold mi sold |
la4. si4 dod'8 |
sold4. la4 r8 |
sold4. la4 r8 |
mi4 r8 mi,4 r8 |
mi,4 r8 mi,4 r8 |
mi,4 r8 r4 dod8 |
re2.~ |
re4. dod8 r4 |
r2*3/4 r8 r re |
mi4. mi, |
fad,4. r2*3/4 |
la2.^\tresdouxSug |
sold |
fad |
mi |
fad |
sold |
la4 re'8 dod'4 re'8 |
mi'4. mi |
la4 r8 re dod re |
mi4. mi, |
la,\fortSug dod' |
re'~ re'4 si8 |
mi'2. |
sold |
la4. dod4 re8 |
mi4. mi, |
la,8 dod mi la dod' mi' |
mi,2. |
la,4. r8 r la |
fad4. re |
sol~ sol8 si la |
sold4. mi |
la~ la8 dod' si |
lad4. fad |
si~ si8 la sol |
la4. re |
la,4\douxSug r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,2.~ |
la,4. la,4 sol,8 |
fad,2.~ |
fad,~ |
fad, |
sol, |
fad,~ |
fad,4. sol,4 la,8 |
si,4 sol,8 fad,4. |
sol, la, |
re,4 r8 r2*3/4 |
si2.( |
la8) r r r2*3/4 |
si,2.( |
la,8) r r r2*3/4 |
r dod'8 re' re |
sol4. la |
si8 r r sol fad mi |
re4. la, |
re, r2*3/4 |
R2. |
re8\fort la, fad re la fad |
re' la re' la fad re |
sol4. r8 r sol |
fad4. r4 re8 |
sol4. r8 r sol, |
fad,4. r8 r re, |
sol,4. si, |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la4 r8 |
si4 r8 dod'4 r8 |
re'4 r8 fad4 sol8 |
la4. la, |
re8 fad la re' la re |
la,4. la, |
re,2. |
r2*3/4 si4. |
la sol |
fad4. si,8 la, sol, |
fad,4^\douxSug r8 fad,4 r8 |
fad,4 r8 fad,4 r8 |
fad,4 r8 fad,4 r8 |
fad,4 r8 fad,4 r8 |
fad,4 <>^"Basses seules" fad8 si4 sold8 |
dod'2 fad4. mi8 |
re1 |
dod |
r2 lad, |
si,1 |
dod~ |
dod~ |
dod~ |
dod~ |
dod~ |
dod~ |
dod |
fad, | \allowPageTurn
R2.*2 |
r8^"Tous" r re' dod'4 si8 |
la4.~ la4 fad8 |
sol4. la |
re2. |
r2*3/4 r8 r re |
dod2. |
si, |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 r4 la8 |
fad4. re |
sol~ sol8 si la |
sold4. mi |
la~ la8 dod' si |
lad4. fad |
si~ si8 la sol |
la4. re |
la,4\douxSug r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,2.~ |
la,4. la,4 sol,8 |
fad,2.~ |
fad,~ |
fad, |
sol, |
fad,~ |
fad,4. sol,4 la,8 |
si,4 sol,8 fad,4. |
sol, la, |
re,4 r8 r2*3/4 |
si2.( |
la8) r r r2*3/4 |
si,2.( |
la,8) r r r2*3/4 |
r dod'8 re' re |
sol4. la |
si8 r r sol fad mi |
re4. la, |
re, r2*3/4 |
R2. |
re8\fort la, fad re la fad |
re' la re' la fad re |
sol4. r8 r sol |
fad4. r4 re8 |
sol4. r8 r sol, |
fad,4. r8 r re, |
sol,4. si, |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la,4 r8 |
la,4 r8 la4 r8 |
si4 r8 dod'4 r8 |
re'4 r8 fad4 sol8 |
la4. la, |
re8 fad la re' la re |
la,4. la, |
re,2. |
R2. |
