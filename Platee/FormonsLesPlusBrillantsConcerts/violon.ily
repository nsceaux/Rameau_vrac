\clef "dessus" r16 r4 |
R2.*2 R1*2 R2. R1*5 |
<>_"pincez" ^"Violons" <re' la' fad''>4 q |
q2 <re' si' sol''>4 q |
q2 <re' la' fad''>4 q |
q2 r |
r <la mi' dod'' la''>4 q |
q2 r |
r2 <re' la' fad''>4 q |
q2 |
<>_"avec l’archet" ^"Violons & hautbois" re'8 la fad' re' la' fad' |
re'' la' fad'' re'' la'' fad'' |
re'''4 r8 re'''4 r8 |
re'''4 r8 r r la'' |
si'' sol'' re'' si'' dod''' re''' |
re''' la'' fad'' re'' re''' la'' |
si'' sol'' re'' si'' dod''' re''' |
re''' la'' fad'' re'' re''' la'' |
si'' la'' sol'' fad'' mi'' re'' |
la' dod'' mi'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
la' dod'' mi'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
dod'' mi'' re'' dod'' si' la' |
sol' si' la' sol' fad' mi' |
fad' la' re'' la' fad' re' |
la4 r8 mi'4\trill r8 |
re'8 fad' la' re'' fad'' la'' |
la4 r8 mi'4\trill r8 |
re'8 fad' la' re'' fad'' la'' |
re'''4 fad''8\doux mi''4 sold'8 |
\appoggiatura sold'16 la'8 mi' dod'' la' re'' re' |
re'4.~ re'4 dod'8 |
\appoggiatura dod'16 re'8 la fad' re' la' fad' |
re''4 r8 r2*3/4 |
r la''4.~ |
la'' sold''\trill |
la'8 dod'' mi'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
la' dod'' mi'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
<< mi''4 \\ dod'' >> r8 r2*3/4 |
R2. |
re'8 la fad' re' la' fad' |
re'' la' fad'' re'' la'' la' |
la'([ sold']) la' \appoggiatura la'16 si'4 la'8 |
sold'4.\trill~ sold'8 sold' la' |
si' sold' la' si' sold' si' |
mi'4. r2*3/4 |
mi'8 sold' si' mi' la' dod'' |
mi' si' re'' mi' dod'' mi'' |
mi' sold' si' mi' la' dod'' |
mi' si' re'' mi' dod'' mi'' |
sold' si' la' sold' fad' mi' |
la2. |
si4.( dod'8) r r |
r2*3/4 r8 r la' |
la'4. sold'\trill |
\appoggiatura sold'16 la'4. r2*3/4 |
R2.*2 |
la''2.\tresdoux |
sold''4. r8 r sold' |
la'2. |
si' |
mi'4 re'8 dod'4 la'8 |
la'4. sold'\trill |
la'4 r8 fad'' mi'' la'' |
la''4. sold''\trill |
la''8 dod''\fort re'' mi'' fad'' sold'' |
r fad'' mi'' fad'' sold'' la'' |
sold'' si'' la'' sold'' fad'' mi'' |
r fad'' mi'' re'' dod'' si' |
dod'' mi'' la'' mi'' dod'' la' |
mi'4. si'4(\trill la'16 si') |
dod''8 mi'' la'' mi'' dod'' la' |
mi'4. si'\trill |
la' r2*3/4 |
R2.*7 | \allowPageTurn
la'8\doux dod'' fad'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
la' dod'' mi'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
dod'' mi'' re'' dod'' si' la' |
sol' si' la' sol' fad' mi' |
fad' la' sol' fad' mi' re' |
re' fad' mi' re' dod' si |
la2. |
si4.~ si4 dod'8 |
re'2. |
re'8 re'' dod'' si' la' sol' |
fad'4 re''8 re'4 la'8 |
si'4. mi' |
fad'4 r8 r2*3/4 |
re''2.( |
dod''8) r r r2*3/4 |
re'2.( |
dod'8) r r r2*3/4 |
r mi''4 la'8~ |
la'4 sol'8 sol'4. |
fad'8 r r si'8 la' dod'' |
re''4. dod''\trill |
re'8\fort la fad' re' la' fad' |
re'' la' fad'' re'' la'' fad'' |
re'''4 r8 re'''4 r8 |
re'''4. r4 la''8 |
si'' sol'' mi'' si'' dod''' re''' |
re''' la'' fad'' re'' re''' la'' |
si'' sol'' re'' si'' dod''' re''' |
re''' la'' fad'' re'' re''' la'' |
si'' la'' sol'' fad'' mi'' re'' |
la' dod'' mi'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
la' dod'' mi'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
dod'' mi'' re'' dod'' si' la' |
sol' si' la' sol' fad' mi' |
fad' la' re'' la' fad' re' |
la4 r8 mi'4.\trill |
re'8 fad' la' re'' fad'' la'' |
la4. mi'\trill |
re'8 fad' la' re'' fad'' la'' |
re'''4 r8 r2*3/4 |
R2.*2 |
fad'8\doux lad' dod'' fad' si' re'' |
fad' dod'' mi'' fad' re'' fad'' |
fad' lad' dod'' fad' si' re'' |
fad' dod'' mi'' fad' re'' fad'' |
fad'4 r8 r2*3/4 | \allowPageTurn
R1*13 | \allowPageTurn
re'8\fort la fad' re' la' fad' |
re'' la' fad'' re'' la'' fad'' |
re''' r r r2*3/4 |
R2.*2 |
re''8 la' fad'' re'' la'' fad'' |
re'''4 r8 r2*3/4 |
r la''4.~ |
la'' sold''\trill |
la'8 dod'' mi'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
la' dod'' mi'' la' re'' fad'' |
la'8 mi'' sol'' la' fad'' la'' |
la' dod'' mi'' r2*3/4 |
R2.*7 |
la'8\doux dod'' fad'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
la' dod'' mi'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
dod'' mi'' re'' dod'' si' la' |
sol' si' la' sol' fad' mi' |
fad' la' sol' fad' mi' re' |
re' fad' mi' re' dod' si |
la2. |
si4.~ si4 dod'8 |
re'2. |
re'8 re'' dod'' si' la' sol' |
fad'4 re''8 re'4 la'8 |
si'4. mi' |
fad'4 r8 r2*3/4 |
re''2.( |
dod''8) r r r2*3/4 |
re'2.( |
dod'8) r r r2*3/4 |
r mi''4 la'8~ |
la'4 sol'8 sol'4. |
fad'8 r r si'8 la' dod'' |
re''4. dod''\trill |
re'8\fort la fad' re' la' fad' |
re'' la' fad'' re'' la'' fad'' |
re'''4 r8 re'''4 r8 |
re'''4. r4 la''8 |
si'' sol'' mi'' si'' dod''' re''' |
re''' la'' fad'' re'' re''' la'' |
si'' sol'' re'' si'' dod''' re''' |
re''' la'' fad'' re'' re''' la'' |
si'' la'' sol'' fad'' mi'' re'' |
la' dod'' mi'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
la' dod'' mi'' la' re'' fad'' |
la' mi'' sol'' la' fad'' la'' |
dod'' mi'' re'' dod'' si' la' |
sol' si' la' sol' fad' mi' |
fad' la' re'' la' fad' re' |
la4 r8 mi'4.\trill |
re'8 fad' la' re'' fad'' la'' |
la4. mi'\trill |
re'8 fad' la' re'' fad'' la'' |
re'''4 r8 r2*3/4 |
