\clef "vdessus" r16 r4 |
r4 r r8 la' |
\appoggiatura la'16 si'4 si'8 dod''16 re'' dod''8.\trill re''16 |
\appoggiatura re''16 mi''1 |
r4 re''8 re''16 re'' la'4 fad'8\trill fad'16 la' |
re'4 sol'16 la' si' dod'' re''8 re''16 mi'' |
\appoggiatura mi''16 fad''4 fad''8 r16 fad' fad'4 fad'8 fad'16 fad' |
si'4 si'8. do''16 si'8.\trill([ la'16]) la'8 sold' |
sold'4\trill sold'16 r mi''8 mi''2~ |
mi''8 mi'' sold' la' mi'4. la'8 |
la'1 |
R2*5 |
r4 re''8 re'' la'4 la'8 si' |
\appoggiatura si'16 dod''2 r4 r8 la' |
mi''2 mi''4 fad''8 sol'' |
fad''2\trill fad''8 r r4 |
R2 |
R2.*20 |
r8 fad' re' la' la' re'' |
dod''4.\trill~ dod''4 re''8 |
mi''8[ re''] mi'' mi''[ la'] mi'' |
\appoggiatura mi''16 fad''2.\melisma |
r8 la''[ sol''] fad''[ mi'' re''] |
r mi''[ re''] dod''[ si' la'] |
r re''[ dod''] si'[ la' sold'] |
r8 la'4~ la'4.~ |
la'2.~ |
la'~ |
la'\melismaEnd |
r8 r mi'' dod''[ re''] mi'' |
la'4( sol'8) sol' sol' fad' |
fad'2.\trill |
r2*3/4 r4 re''8 |
re''[ si'] dod'' re''[ si'] dod'' |
re''[\melisma si' dod''] re''4.~ |
re''~ re''8\melismaEnd dod'' si' |
dod''4. sold'4 la'8 |
mi'4. r4 mi''8 |
mi'4 re''8 mi'4 dod''8 |
si'4.\melisma mi''~ |
mi''8[ re'' dod''] dod''[ si' la']( |
mi'4.)\melismaEnd r8 r mi'' |
fad''[ mi''] re'' re''[ dod''] si' |
sold''4.( la''8) r r |
r2*3/4 fad''8 mi'' re'' |
dod''4. si'\trill |
la'4.\melisma la''~ |
la''4 sold''8 fad''[ mi'' red''] |
r mi''[ red''] mi''[ red'' mi''] |
r la''[ sold''] fad''[ mi'' red''] |
r mi''[ red''] mi''[ red'' mi'']~ |
mi''[ fad'' mi''] re''[ dod'' re'']~ |
re''[ mi'' re''] re''[ dod'' si']( |
dod''4)\melismaEnd fad''8 mi''4 re''8 |
dod''4. si'\trill |
la'4 r8 fad'' mi'' la'' |
mi'4. si'\trill |
la' r2*3/4 |
R2.*7 |
r2*3/4 r8 r la' |
do''4 do''8 do''[ si'] do'' |
si'[\melisma mi'' red''] mi''4.~ |
mi''8[ fad'' mi''] re''[ dod'' re''] |
dod''[ fad'' mid''] fad''4.~ |
fad''8[ sol'' fad''] mi''[ re'' mi''] |
re''[ sol'' fad''] sol''4.~ |
sol''8\melismaEnd fad'' mi'' fad'' mi'' re'' |
dod''4.\trill r4 la''8 |
la'4 sol''8 la'4 fad''8 |
mi''4.\melisma la''4.~ |
la''8[ sol'' fad''] fad''[ mi'' re''] |
la'2.~ |
la'4.\melismaEnd r8 r re'' |
re''4. re'' |
re'' re'' |
re''8[\melisma fad'' mi''] re''4.~ |
re''8[ fad'' mi''] re''[ dod'' si'] |
la'[ re'' dod''] si'[ la' sol'] |
fad'[ si' la'] sol'[ fad' mi']( |
re'4)\melismaEnd si'8 la'4 re''8 |
mi''4. dod''\trill |
re''2\melisma r4 |
sold''2.( |
la''8) r r r2*3/4 |
sold'2.( |
la'8)\melismaEnd r r r2*3/4 |
r sol''8 fad'' fad'' |
si'4. dod''\trill |
re''8 r r si' la' sol' |
fad'4. mi'\trill |
re' r2*3/4 |
R2.*19 |
r8 si' dod'' re'' dod'' si' |
dod'' fad' fad' si' si' dod'' |
lad'\trill si' dod'' re'' dod'' si' |
fad''2.~ |
fad''~ |
fad''~ |
fad'' |
r8 dod'' dod'' re'' re'' si' |
sold'2\trill la'4. sold'8 |
sold'2~ sold'8\trill[ fad'] mid' fad' |
\appoggiatura fad'16 sold'1 |
r2 dod''4. mi''8 |
red''1 |
mid''2~ mid''8[\melisma sold'' mid'' dod''] |
si'4 sold''4. mid''8[ dod'' si'] |
la'4 r8 si'16[ la'] dod''8*2/3[ re'' dod''] fad''[ sold'' fad''] |
la''4. fad''8 sol''![ mid'' fad'' red''] |
mid''[ dod'' red'' dod''] sid'2~ |
sid'16*4/3[ la'' sold'' fad'' mid'' fad''32*4/3 mid''] red''4~ red''16[ mid'' red'' mid''] |
mid''2\trill~ mid''4.\melismaEnd fad''8 |
fad''1 |
R2.*2 |
r8 fad' re' la' la' re'' |
dod''4.\trill~ dod''4 re''8 |
mi''[ re''] mi'' mi''[ la'] mi'' |
\appoggiatura mi''16 fad''2.\melisma |
r8 la''[ sol''] fad''[ mi'' re''] |
r mi''[ re''] dod''[ si' la'] |
r re''[ dod''] si'[ la' sold'] |
r8 la'4~ la'4.~ |
la'2.~ |
la'~ |
la'\melismaEnd |
r2*3/4 r8 r la' |
do''4 do''8 do''[ si'] do'' |
si'[\melisma mi'' red''] mi''4.~ |
mi''8[ fad'' mi''] re''[ dod'' re''] |
dod''[ fad'' mid''] fad''4.~ |
fad''8[ sol'' fad''] mi''[ re'' mi''] |
re''[ sol'' fad''] sol''4.~ |
sol''8\melismaEnd fad'' mi'' fad'' mi'' re'' |
dod''4.\trill r4 la''8 |
la'4 sol''8 la'4 fad''8 |
mi''4.\melisma la''4.~ |
la''8[ sol'' fad''] fad''[ mi'' re''] |
la'2.~ |
la'4.\melismaEnd r8 r re'' |
re''4. re'' |
re'' re'' |
re''8[\melisma fad'' mi''] re''4.~ |
re''8[ fad'' mi''] re''[ dod'' si'] |
la'[ re'' dod''] si'[ la' sol'] |
fad'[ si' la'] sol'[ fad' mi']( |
re'4)\melismaEnd si'8 la'4 re''8 |
mi''4. dod''\trill |
re''2\melisma r4 |
sold''2.( |
la''8) r r r2*3/4 |
sold'2.( |
la'8)\melismaEnd r r r2*3/4 |
r sol''8 fad'' fad'' |
si'4. dod''\trill |
re''8 r r si' la' sol' |
fad'4. mi'\trill |
re' r2*3/4 |
R2.*20 |

