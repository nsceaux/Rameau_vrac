\clef "taille" r16 r4 |
R2.*2 R1*2 R2. R1*5 R1*7 |
fad'4 r8 fad'4 r8 |
fad'4 r8 fad'4 r8 |
la'4 r8 la'4 r8 |
la'4. r4 re''8 |
re''4. r4 re'8 |
re'4. r4 re'8 |
re'4. r4 re'8 |
re'4. r4 re'8 |
re'4. sol' |
mi'\trill la' |
sol' fad' |
mi' re' |
dod' la' |
sol' r4 fad'8 |
mi'4 r8 mi'4 r8 |
la4. la'4 si'8 |
mi'4. sol' |
fad' r4 fad'8 |
mi'4. sol' |
fad'2. |
R2.*5 |
r2*3/4 r4 mi'8 |
re'4. mi' |
mi' fad' |
mi' re' |
dod' re' |
mi' fad' |
sol' r2*3/4 |
r r4 mi'8 |
\appoggiatura mi'16 fad'4. \appoggiatura mi'16 re'4 r8 |
R2. |
re'4 dod'8 si4 mi'8 |
mi'2. |
mi' |
la4. re'4 dod'8 |
re'4 r8 dod'4 r8 |
re'4 r8 dod'4 r8 |
re'4 r8 dod'4 r8 |
si4 r8 mi'4 r8 |
r2*3/4 r8 r mi'' |
mi''4 re''8 re''4. |
si'4.\trill mi'8 r4 |
r2*3/4 r8 r si |
dod'4. re' |
dod' r2*3/4 |
red'4\tresdouxSug mi'8 fad' sold' fad' |
si2. |
dod'4.~ dod'4 si8 |
si4. r4 mi'8 |
\appoggiatura re'16 dod'4. fad' |
mi'2.~ |
mi'4 si8 mi'4 fad'8 |
mi'2. |
mi'4 r8 si' dod'' fad' |
mi'2. |
mi'4.\fortSug la'~ |
la' la'4 re''8 |
si'2.\trill |
mi'2. |
mi'4. r4 fad'8 |
si4. re' |
dod'8 mi' mi' la' dod'' mi'' |
mi'4. re' |
dod' r2*3/4 | \allowPageTurn
R2.*7 |
mi'4\douxSug r8 fad'4 r8 |
sol'4 r8 la'4 r8 |
dod'4 r8 re'4 r8 |
dod'4 r8 re'4 r8 |
sol' sol' fad' mi' re' dod' |
dod'4. mi' |
la4. r2*3/4 |
R2. |
r8 la' sol' fad'4. |
mi'2. |
fad'4. la |
re4 re'8 mi'( fad' mi') |
re'4. r4 re'8 |
\appoggiatura dod'16 si4. la |
la4 r8 r2*3/4 |
mi'2.~ |
mi'8 r r r2*3/4 |
mi2.~ |
mi8 r r r2*3/4 |
r r4 re'8 |
re'4 sol'8 mi'4\trill la'8 |
re' r r mi' fad' sol' |
fad'4. mi'\trill |
re'\fortSug fad'4 r8 |
fad'4 r8 fad'4 r8 |
la'4 r8 la'4 r8 |
la'4 r8 r4 re''8 |
re''4. r4 re'8 |
re'4. r4 re'8 |
re'4. r4 re'8 |
re'4. r4 re'8 |
re'4. sol' |
mi'\trill la' |
sol' fad' |
mi' re' |
dod' la' |
sol' r8 r fad' |
mi'4 r8 mi'4 r8 |
la4. la'4 si'8 |
mi'4. sol' |
fad' r8 r fad' |
mi'4. sol' |
fad' r4 r8 |
R2.*3 |
dod'4\douxSug r8 re'4 r8 |
mi'4 r8 fad'4. |
lad4 r8 si4 r8 |
mi'4 r8 re'4 r8 |
dod'4. fad4 r8 |
R1*13 R2.*7 |
r2*3/4 r4 mi'8 |
re'4. mi' |
mi' fad' |
mi' re' |
dod' re' |
mi' fad' |
mi' r2*3/4 |
R2.*7 |
mi'4\douxSug r8 fad'4 r8 |
sol'4 r8 la'4 r8 |
dod'4 r8 re'4 r8 |
dod'4 r8 re'4 r8 |
sol' sol' fad' mi' re' dod' |
dod'4. mi' |
la4. r2*3/4 |
R2. |
r8 la' sol' fad'4. |
mi'2. |
fad'4. la |
re4 re'8 mi'( fad' mi') |
re'4. r4 re'8 |
\appoggiatura dod'16 si4. la |
la4 r8 r2*3/4 |
mi'2.~ |
mi'8 r r r2*3/4 |
mi2.~ |
mi8 r r r2*3/4 |
r r4 re'8 |
re'4 sol'8 mi'4\trill la'8 |
re' r r mi' fad' sol' |
fad'4. mi'\trill |
re'\fortSug fad'4 r8 |
fad'4 r8 fad'4 r8 |
la'4 r8 la'4 r8 |
la'4 r8 r4 re''8 |
re''4. r4 re'8 |
re'4. r4 re'8 |
re'4. r4 re'8 |
re'4. r4 re'8 |
re'4. sol' |
mi'\trill la' |
sol' fad' |
mi' re' |
dod' la' |
sol' r8 r fad' |
mi'4 r8 mi'4 r8 |
la4. la'4 si'8 |
mi'4. sol' |
fad' r8 r fad' |
mi'4. sol' |
fad' r4 r8 |
R2. |
