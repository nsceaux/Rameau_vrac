\clef "basse" <>^"Tous" fad4 fad |
fad fad sol8\trill fad sol la |
fad4 sol8 la si dod' re'4 |
dod' re' la2 |
la, fad4 fad |
fad fad sol8\trill fad sol la |
fad4 sol8 la si dod' re'4 |
la re la,2 |
re re'8 dod' si re' |
dod' si la dod' re' dod' si re' |
dod' si la dod' re' dod' si re' |
dod'4 re'8 dod' si4 mi' |
la2 fad4 fad |
fad fad sol8\trill fad sol la |
fad4 sol8 la si dod' re'4 |
dod' re' la2 |
la, fad4 fad |
fad fad sol8\trill fad sol la |
fad4 sol8 la si dod' re'4 |
la re la,2 |
re4
