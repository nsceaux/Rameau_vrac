\clef "dessus" <>_"Tous" la'4 la' |
la' la' si'8\trill la' si' dod'' |
la'4 si'8 dod'' re'' mi'' fad''4 |
mi''\trill re'' la''2 |
la'' la'4 la' |
la' la' si'8\trill la' si' dod'' |
la'4 si'8 dod'' re'' mi'' fad''4 |
mi''\trill re'' dod''4.\trill( si'16 dod'') |
re''2 fad''8 mi'' re'' fad'' |
mi''4 la'' fad''8 mi'' re'' fad'' |
mi'' la' la'' sol'' fad'' mi'' re'' fad'' |
mi'' fad'' sol'' la'' fad''4 sold''\trill |
\appoggiatura sold''16 la''2 la'4 la' |
la' la' si'8\trill la' si' dod'' |
la'4 si'8 dod'' re'' mi'' fad''4 |
mi''\trill re'' la''2 |
la'' la'4 la' |
la' la' si'8\trill la' si' dod'' |
la'4 si'8 dod'' re'' mi'' fad''4 |
mi''\trill re'' dod''4.(\trill si'16 dod'') |
re''4
