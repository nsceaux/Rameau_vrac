\tag #'haute-contre \clef "haute-contre"
\tag #'(taille parties) \clef "taille"
re'4 re' |
re' re' re'2 |
re'4 re'' dod''8 si' la'4 |
la' re'' dod''2\trill |
mi'' re'4 re' |
re' re' re'2 |
re'4 re'' dod''8 si' la'4 |
dod'' re'' mi' sol' |
fad'2 la'4 la' |
la'2 la'4 la' |
la'2 la' |
la' re''4 si' |
\appoggiatura si'16 dod''2 re'4 re' |
re' re' re'2 |
re'4 re'' dod''8 si' la'4 |
la' re'' dod''2\trill |
mi'' re'4 re' |
re' re' re'2 |
re'4 re'' dod''8 si' la'4 |
dod'' re'' mi' sol' |
fad'4
