\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "parties" >>
    \new Staff <<
      \global \includeNotes "basse"
      \origLayout {
        s2 s1*6\break
        s1*6\break
      }
    >>
  >>
  \layout { indent = \smallindent }
  \midi { }
}
