\piecePartSpecs
#`((violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (haute-contre)
   (taille)
   (hautbois #:notes "dessus" #:instrument "Violons")
   (bassons #:notes "basse" #:tag-notes basson)
   (basses #:notes "basse" #:tag-notes basse)
   (percussions #:score "score-percussion"))
