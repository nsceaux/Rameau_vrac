\score {
  \new StaffGroup <<
    \new Staff \with { \tinyStaff } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \with { \tinyStaff } <<
      \global \keepWithTag #'basse \includeNotes "basse"
    >>
    \new Staff \with {
      \remove "Clef_engraver"
      \remove "Key_engraver"
    } << \keepWithTag #(*tag-global*) \global >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
    ragged-last = #(*score-ragged*)
  }
}
