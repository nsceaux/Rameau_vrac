\clef "dessus" r16 re' mi' fad' sol' la' si' do'' re'' sol' la' si' do'' re'' mi'' fad'' |
sol'' re'' mi'' fad'' sol'' la'' sol'' la'' si''4. do'''8 |
la''16\trill \ru#7 fad' \ru#7 re'' si'' |
sol'' \ru#7 mi' \ru#7 do'' la'' |
fad'' mi'' fad'' sol'' la'' sol'' fad'' mi'' re'' mi'' re'' do'' re'' do'' si' la' |
si' re' mi' fad' sol' la' si' do'' re'' mi'' fad'' re'' sol''8. si'16 |
dod''16\prall mi' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' mi'' la''8. dod''16 |
re'' re'' mi'' fad'' mi'' mi'' fad'' sol'' fad'' fad'' sol'' la'' sol'' sol'' la'' si'' |
la''2. r16 la'' si'' dod''' |
re''' la'' si'' dod''' re''' la'' si'' dod''' re''' la'' si'' dod''' re''' dod''' si'' la'' |
si'' dod''' re''' dod''' si'' la'' sol'' fad'' mi'' fad'' sol'' fad'' mi'' re'' dod'' si' |
la'4. fad''8 mi''4.\trill re''8 |
re''8 dod''16 si' la' sol' fad' mi' re'4 r |
re''8 dod''16 si' la' sol' fad' mi' re'4 r |
\ru#8 fad''16 fad'' sol'' fad'' mi'' red'' mi'' red'' dod'' |
si' dod'' si' dod'' red'' mi'' red'' mi'' fad''8. fad''16 si''8. si''16 |
sold''16 la'' sold'' la'' si'' la'' sold'' fad'' mi'' re'' mi'' fa'' mi'' re'' do'' si' |
do'' mi'' fad'' sold'' la'' mi'' fad'' sold'' la'' mi'' fa'' sol'' la'' sol'' fa'' mi'' |
fa'' sol'' la'' sol'' fa'' mi'' re'' dod'' re'' mi'' fa'' mi'' re'' do'' si' la' |
mi'4. do''8 si'4.\trill la'8 |
la'16 \ru#7 do''' do'''8 la'16 si' do'' si' la' sol' |
fad' \ru#7 do''' do'''8 fad'16 sol' la' sol' fad' mi' |
re' \ru#13 do''' si'' la'' |
si'' \ru#7 sol' \ru#7 mi'' do''' |
la'' \ru#7 fad' \ru#7 re'' si'' |
sol'' \ru#7 mi' \ru#7 do'' la'' |
fad'' re'' fad'' la'' fad'' re'' fad'' la'' fad'' re'' fad'' la'' fad'' re'' fad'' la'' |
fad'' re' mi' fad' sol' la' si' do'' re'' re'' mi'' fad'' sol'' la'' si'' do''' |
\ru#8 re''' re''' do''' si'' la'' si'' la'' sol'' fad'' |
sol''4. la''8 la''4.\trill sol''8 |
sol''16 \ru#7 sol' \ru#7 mi'' do''' |
la'' \ru#7 fad' \ru#7 re'' si'' |
sol'' \ru#7 mi' \ru#7 do'' la'' |
fad'' mi'' fad'' sol'' la'' sol'' fad'' mi'' re'' do'' re'' mi'' re'' do'' si' la' |
si' sol'' fad'' mi'' re'' do'' si' la' sol' re'' do'' si' la' sol' fad' mi' |
re'4. si'8 la'4.\trill sol'8 |
sol'8 fad'16 mi' re' do' si la sol4 r |
