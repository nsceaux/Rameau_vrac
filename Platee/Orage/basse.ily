\clef "basse" sol,2 r |
<<
  \tag #'basson {
    sol,8 sol, sol, sol, sol, sol, sol, sol, |
    fad, fad, fad, fad, fad, fad, fad, fad, |
    mi, mi, mi, mi, mi, mi, mi, mi, |
    re,4
  }
  \tag #'basse {
    <>^"à 2 c." \ru#16 <sol re'>16 |
    <fad re'>16 \ru#7 <fad la> \ru#8 <fad si> |
    <mi si>16 \ru#7 <mi sol> \ru#8 <mi la> |
    <re la>4
  }
>> r4 r r16 re re re |
sol4 r r r16 mi mi mi |
la4 r r r16 fad fad fad |
\clef "tenor" si4 dod' re' mi' |
fad'8. re'16 dod' si la sol fad4 r |
\clef "bass" r2 r4 fad, |
sol,2.~ sol,16 la, fad, sol, |
la,2 la, |
re, r8 re16 mi re do si, la, |
re,1 |
R1 |
\ru#8 si16 \ru#8 red |
\ru#8 mi \ru#8 sold, |
la,4 r r r16 la si dod' |
re' mi' fa' mi' re' do' si la si do' re' do' si la sold la |
mi2 mi, |
la, r8 la16 si do' si la sol |
fad4 r r8 fad16 sol la sol fad mi |
<<
  \tag #'basson {
    re8 re, re, re, re, re, re, re, |
    sol, sol, sol, sol, sol, sol, sol, sol, |
    fad, fad, fad, fad, fad, fad, fad, fad, |
    mi, mi, mi, mi, mi, mi, mi, mi, |
    re, re, re, re, re, re, re, re, |
    re, re, re, re, do, do, do, do, |
    si,, si,, si,, si,, si,, si,, si,, si,, |
    mi,4 do, re, re, |
    sol,8 sol, sol, sol, sol, sol, sol, sol, |
    fad, fad, fad, fad, fad, fad, fad, fad, |
    mi, mi, mi, mi, mi, mi, mi, mi, |
    re, re, re, re, fad, fad, fad, fad, |
  }
  \tag #'basse {
    re16 \ru#15 re,16 |
    <>^"à 2 c." \ru#8 <sol si>16 \ru#8 <sol do'> |
    <fad do'> \ru#7 <fad la> \ru#8 <fad si> |
    \ru#8 <mi si> \ru#8 <mi la> |
    \ru#24 <re la> \ru#8 do |
    \ru#16 si, |
    mi4 do re re |
    \ru#8 <sol si>16 \ru#8 <sol do'> |
    <fad do'>16 \ru#7 <fad la> \ru#8 <fad si> |
    \ru#8 <mi si> \ru#8 <mi la> |
    \ru#8 <re la> \ru#8 fad,16 |
  }
>>
sol,4 la, si, do |
re2 re, |
sol8 fad16 mi re do si, la, sol,4 r |
