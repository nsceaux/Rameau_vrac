\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Violons }
    } << \global \includeNotes "dessus" >>
    \new Staff \with {
      instrumentName = "Hautes-contre"
    } << \global \keepWithTag #'haute-contre \includeNotes "parties" >>
    \new Staff \with {
      instrumentName = "Tailles"
    } << \global \keepWithTag #'taille \includeNotes "parties" >>
    \new Staff \with {
      instrumentName = "Bassons"
    } <<
      \global \keepWithTag #'basson \includeNotes "basse"
    >>
    \new Staff \with {
      instrumentName = "Basses"
    } <<
      \global \keepWithTag #'basse \includeNotes "basse"
      \origLayout {
        s1*4\break s1*3\break
        s1*5\break s1*4\pageBreak
        s1*4\break s1*3\break
        s1*4\break s1*3\pageBreak
        s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
