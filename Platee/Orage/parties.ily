\tag #'haute-contre \clef "haute-contre"
\tag #'taille \clef "taille"
R1 |
\ru#8 si'16 <<
  \tag #'haute-contre { re''4. re''8 | re''16 }
  \tag #'taille { re'4. re'8 | re'16 }
>> \ru#7 fad'16 \ru#8 re'' |
re''16 \ru#7 mi' \ru#8 do'' |
do''4 r r r16 <<
  \tag #'haute-contre { re'16 re' re' | re'4 }
  \tag #'taille { fad'16 fad' fad' | sol'4 }
>> r4 r r16 <<
  \tag #'haute-contre { sol'16 sol' sol' | }
  \tag #'taille { si'16 si' si' | }
>>
mi'4 r r r16 <<
  \tag #'haute-contre { la'16 la' la' | fad'4 sol'8 mi'' }
  \tag #'taille { dod''16 dod'' dod'' | fad'4 la' }
>> la'8 re''4 dod''8\trill |
re''8. re''16 dod'' si' la' sol' fad'4 r |
r2 r4 <<
  \tag #'haute-contre {
    la'4 |
    si'2 re'' |
    re'' dod''4.\trill re''8 |
    re''1 |
    re'' |
  }
  \tag #'taille {
    re'4 |
    re'2. mi'4 |
    fad'2 sol' |
    fad'1 |
    fad' |
  }
>>
R1 |
<<
  \tag #'haute-contre {
    \ru#8 red''16 \ru#8 si' |
    \ru#16 si' |
    mi'4
  }
  \tag #'taille {
    \ru#8 fad'16 la' la' la' la' fad' fad' fad' fad' |
    \ru#8 si \ru#8 re' |
    do'4
  }
>> r4 r r16 la' la' la' |
<<
  \tag #'haute-contre {
    la'2~ la'4. si'16 la' |
    la'2. sold'4 |
    \ru#8 la'16 la'8
  }
  \tag #'taille {
    la'2 r4 r16 la' si' la' |
    do''4 mi'2 re'4 |
    do'16 \ru#7 mi' mi'8
  }
>> la'16 si' do'' si' la' sol' |
fad'16 <<
  \tag #'haute-contre { \ru#7 re''16 re''8 }
  \tag #'taille { \ru#7 la'16 la'8 }
>> fad'16 sol' la' sol' fad' mi' |
re' <<
  \tag #'haute-contre { \ru#14 fad'16 la' | }
  \tag #'taille { \ru#15 la'16 }
>>
re'16 \ru#7 sol' \ru#8 mi'' |
mi''16 \ru#7 fad' \ru#8 re'' |
re'' \ru#7 mi' \ru#8 do'' |
<<
  \tag #'haute-contre {
    \ru#24 do''16 \ru#7 la' fad' |
    \ru#8 sol' \ru#8 re'' |
    si'4.\trill la'16 sol' fad'4.\trill sol'8 |
    \ru#8 sol'16
  }
  \tag #'taille {
    \ru#24 la'16 \ru#6 fad'16 sol' la' |
    \ru#8 re' \ru#7 sol' la' |
    sol'4 do'' do''2 |
    si'16 \ru#7 sol'
  }
>> \ru#8 mi''16 |
mi'' \ru#7 fad' \ru#8 re'' |
re'' \ru#7 mi' \ru#8 do'' |
<<
  \tag #'haute-contre { \ru#8 do''16 }
  \tag #'taille { \ru#8 la'16 }
>> \ru#8 la' |
re'4 <<
  \tag #'haute-contre { re' re' mi' | si2 do' | si }
  \tag #'taille { do'4 si sol' | sol'2 fad'\trill | sol' }
>> r2 |

