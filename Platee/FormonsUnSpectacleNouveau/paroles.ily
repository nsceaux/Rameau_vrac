\tag #'(thespis basse) {
  For -- mons un spec -- ta -- cle nou -- veau,
  les fil -- les de mé -- moi -- re
  pu -- blie -- ront à ja -- mais la gloi -- re
  des au -- teurs d’un pro -- jet si beau.

  Les fil -- les de mé -- moi -- re
  pu -- blie -- ront à ja -- mais la gloi -- re
  des au -- teurs d’un pro -- jet si beau.

  Bac -- chus, c’est ta vic -- toi -- re,
  Bac -- chus, c’est ta vic -- toi -- re,
  c’est ta vic -- toi -- re,
  Li -- vrons- nous au plai -- sir de boi -- re.
  L’hip -- po -- crè -- ne est sur ce co -- teau,
  l’hip -- po -- crè -- ne est sur ce co -- teau.
}

\tag #'vdessus {
  For -- mons un spec -- ta -- cle nou -- veau,
  les fil -- les de mé -- moi -- re
  pu -- blie -- ront à ja -- mais la gloi -- re
  des au -- teurs d’un pro -- jet si beau,
  for -- mons, for -- mons __ un spec -- ta -- cle nou -- veau,
  For -- mons un spec -- ta -- cle nou -- veau.

  Li -- vrons- nous au plai -- sir de boi -- re.
  L’hip -- po -- crè -- ne est sur ce co -- teau.
  Li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crè -- ne est sur ce co -- teau.
  Li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crè -- ne est sur ce co -- teau.

  For -- mons __ un spec -- ta -- cle nou -- veau.
  Bac -- chus, c’est ta vic -- toi -- re,
  li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crè -- ne est sur ce co -- teau.
  Li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crè -- ne
  l’hip -- po -- crène est sur ce co -- teau,
  l’hip -- po -- crè -- ne est sur ce co -- teau.
}

\tag #'vhaute-contre {
  For -- mons un spec -- ta -- cle nou -- veau,
  les fil -- les de mé -- moi -- re
  pu -- blie -- ront à ja -- mais la gloi -- re
  des au -- teurs d’un pro -- jet si beau,
  For -- mons un spec -- ta -- cle nou -- veau.
  
  Bac -- chus, c’est ta vic -- toi -- re,
  c’est ta vic -- toi -- re.
  Li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crène est sur ce co -- teau,
  l’hip -- po -- crè -- ne,
  l’hip -- po -- crène est sur ce co -- teau.

  For -- mons un spec -- ta -- cle nou -- veau,
  Bac -- chus, c’est ta vic -- toi -- re.
  Bac -- chus, c’est ta vic -- toi -- re,
  Li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crène est sur ce co -- teau,
  l’hip -- po -- crè -- ne est sur ce co -- teau.
}
\tag #'vtaille {
  For -- mons un spec -- ta -- cle nou -- veau,
  for -- mons un spec -- ta -- cle nou -- veau,
  for -- mons un spec -- ta -- cle nou -- veau,
  for -- mons un spec -- ta -- cle nou -- veau.

  Bac -- chus, c’est ta vic -- toi -- re,
  c’est ta vic -- toi -- re.
  Li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crène est sur ce co -- teau,
  l’hip -- po -- crè -- ne,
  l’hip -- po -- crène est sur ce co -- teau.

  For -- mons un spec -- ta -- cle nou -- veau,
  Bac -- chus, c’est ta vic -- toi -- re.
  Bac -- chus, c’est ta vic -- toi -- re,
  Li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crène est sur ce co -- teau,
  l’hip -- po -- crè -- ne est sur ce co -- teau.
}
\tag #'vbasse {
  For -- mons un spec -- ta -- cle nou -- veau,
  for -- mons un spec -- ta -- cle nou -- veau,
  For -- mons, for -- mons __ un spec -- ta -- cle nou -- veau.

  Li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crè -- ne est sur ce co -- teau.
  Li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crène est sur ce co -- teau,
  l’hip -- po -- crè -- ne,
  l’hip -- po -- crène est sur ce co -- teau.

  For -- mons un spec -- ta -- cle nou -- veau,
  Bac -- chus, c’est ta vic -- toi -- re.
  Li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crène est sur ce co -- teau,
  Li -- vrons- nous au plai -- sir de boi -- re,
  l’hip -- po -- crè -- ne,
  l’hip -- po -- crène est sur ce co -- teau,
  l’hip -- po -- crè -- ne est sur ce co -- teau.
}

