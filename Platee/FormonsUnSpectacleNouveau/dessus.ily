\clef "dessus" <>^"Tous" re''1 |
dod''2 dod''4 dod'' |
si'2\trill si'4. mi''8 |
la'2. \cesureDown <>^"Viol. seul" la''4\doux |
si'' si'' sol'' mi'' |
la'' la'' fad'' re'' |
sol'' sol''8 sol'' sol''4. la''8 |
fad''4.\trill mi''8 re'' mi'' fad'' sol'' |
la''2~ la''8( si'') la''( si'') |
la''2~ la''8( si'') la''( si'') |
la''4 si''8( la'') sold''( la'') fad''( sold'') |
la''2 mi''~ |
mi''4( fad''8 mi'') re''2~ |
re''2.( dod''8\trill si') |
mi''( sold'') fad''( sold'') la''2~ |
la''4( sold''8 fad'') mi''4 re''8 mi'' |
dod''2\trill r |
sol''1 |
fad''2 fad''4 fad'' |
mi''2\trill mi''4. la''8 |
re''2 \cesure r4 <>^"avec H.b." mi''\fort |
fad'' fad'' re'' si' |
mi'' mi'' dod'' la' |
re'' dod''8 si' mi''4. fad''8 |
dod''8\trill re'' si' dod'' re''2~ |
re''1~ |
re''2 re''4 re'' |
mi'' mi''8 mi'' mi''4. mi''8 |
la'4 la' re'' re'' |
fad''1~ |
fad''2 re''4 re'' |
re''2 re''4. dod''8 |
si'2\trill r4 mi'' |
fad''4. sold''8 fad''( sold'') fad''( sold'') |
sold''2\trill si' |
mi''1~ |
mi''2 re''4 fad'' |
si'2.\trill si'8 dod'' |
\appoggiatura si'8 la'1 |
R1 |
r2 la'4 la' |
re'' re''8 re'' re''4 re'' |
re'' re'' re'' si' |
mi''1~ |
mi''4 mi'' mi'' mi''8 re'' |
dod''1\trill |
R1 |
r2 fad'4 fad' |
si' si'8 si' si'4 si' |
si' si' si' sold' |
dod'' dod''8 dod'' dod''4 dod'' |
dod''2 dod''4 lad' |
re''4 re''8 re'' re''4 re'' |
re'' re'' re'' si' |
mi''2 mi''4 re'' |
dod''2.\trill si'4 |
si'2. fad''4 |
sol''8 mi'' si' mi'' sold'' la'' fad'' sold'' |
la'' mi'' dod'' mi'' lad'' si'' sold'' lad'' |
si''4( la''!8)\trill sol'' fad'' sol'' mi'' fad'' |
re''4.\trill dod''8 dod''4.\trill si'8 |
si'2 \twoVoices #'(dessus1 dessus2 dessus) <<
  { \tag #'dessus <>^\markup\concat { P \super rs } si''2 |
    si''1~ |
    si''2 si''4 si'' |
    \appoggiatura si''8 la''2.\trill sol''8 fad'' |
    \appoggiatura fad''8 sol''1 | }
  { \tag #'dessus <>_\markup\concat { 2 \super es } re''2 |
    re''1~ |
    re''2 re''4 re'' |
    red''2.\trill red''8 mi'' |
    mi''1 | }
>>
<>^"Tous à 2 cordes" <mi' si'>2 <si' mi''> |
<si' sol''>2 r4 r8 \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''8 | sol''2 fad''4 fad''8 sol'' | mi''1\trill | }
  { si'8 | la'2 la'4 la'8 re'' | dod''1\trill | }
>>
r2 la'4 la' |
re'' re''8 re'' re''4 re'' |
re'' re'' re'' si' |
mi'' mi''8 mi'' mi''4 mi'' |
mi''2 mi''4 dod'' |
fad''4 fad''8 fad'' fad''4 fad'' |
fad'' fad'' fad'' re'' |
sol''1~ |
sol''2 sol''4 la'' |
fad''2.\trill \twoVoices #'(dessus1 dessus2 dessus) <<
  { \tag #'dessus <>^\markup\concat { P \super rs } sol''8 fad'' |
    mi''2.\trill re''4 | re''2. }
  { \tag #'dessus <>_\markup\concat { 2 \super es } mi''8 re'' |
    dod''2.\trill re''4 | re''2. }
>> \tag#'dessus <>^"Tous" la''4 |
si'' si'' sol'' mi'' |
la'' la'' fad'' re'' |
sol'' sol'' mi'' dod'' |
fad''4. mi''8 re'' mi'' fad'' sol'' |
la''1~ |
la''2. sol''8 fad'' |
fad''2( mi''4.)\trill re''8 |
re''1 |

