\clef "haute-contre" re''1 |
dod''2 dod''4 dod'' |
si'2\trill si'4. mi''8 |
la'2. r4 |
sol'1\doux |
fad'2 fad'4 fad' |
mi'2\trill mi'4. la'8 |
re'2 r |
R1*13 |
re'1 |
dod'2 dod'4 dod' |
si2\trill si4. mi'8 |
la2 r4 la' |
si' si' sol' mi' |
la'4 la' fad' re' |
sol' sol'8 sol' mi'4 dod' |
fad'4. mi'8 re' mi' fad' sol' |
la'1~ |
la'2 fad'4 fad' |
mi'2 si'4. la'8 |
sold'2 r4 mi'' |
mi''2. re''4 |
re''2 sold' |
la' fad' |
la'2 la'4 la' |
fad'2. fad'8 mi' |
mi'1 |
R1*2 |
r2 la'4 la' |
si' si'8 si' si'4 si' |
dod'' dod'' dod'' dod'' |
lad'\trill lad' lad' lad'8 si' |
fad'1 |
R1*2 |
r2 re''4 re'' |
re'' dod''8 si' mi''4 si' |
si'2 mi''4 mi'' |
mi'' re''8 dod'' lad'4 fad' |
si'2 si'4 fad' |
fad' fad' mi' mi' |
fad'2 fad'4 si' |
lad'2.\trill si'4 |
si'2. fad''4 |
fad''2. mi''4 |
mi''2. re''8 dod'' |
fad''2 dod'' |
fad'4. si'8 lad'4.\trill si'8 |
si'2 sol'' |
sol''1~ |
sol''2 sol''4 si' |
si'2. si'8 si' |
si'1 |
<>^"a 2 cordes" <mi' sol'>2 q |
<si mi'>2 r4 r8 mi'' |
mi''2 re''4 re''8 la' |
la'1 |
R1 |
r2 r4 r8 \twoVoices #'(haute-contre taille parties) <<
  { \tag#'parties <>^"H.c." la'8 |
    la'1 |
    sol'2.~ sol'8 sol' |
    sol'1*3/4( \afterGrace s4 fad'8) |
    fad'4\trill }
  { \tag#'parties <>_"Taille" fad'8 |
    fad'1 |
    si2.~ si8 si |
    la1 |
    la4 } 
>> \tag#'parties <>^"Tous" re''8 re'' re''4 re'' |
re'' re'' re'' re'' |
re''2 re''4 mi'' |
la'2 mi''4 dod'' |
re''2 la'4 si' |
la'2. la'4 |
fad'1\trill |
\twoVoices #'(haute-contre taille parties) <<
  { re''2. re''4 |
    re''2. re''4 |
    re''2 dod''\trill |
    re''2. }
  { re'2. re'4 |
    re'2. re'4 |
    si'2 la' |
    la'2. }
>> <>^\markup\whiteout "a 2. cordes" <re' re''>8 q |
q1~ |
<<
  \tag #'haute-contre <re'' re'>2.*2/3
  \tag #'taille <re'' re'>2
  \tag #'parties << re''2.*2/3 re'2 >>
>> \twoVoices #'(haute-contre taille parties) <<
  { \tag#'parties <>^"H.c." s4 mi''8 re'' |
    re''2( dod''4.)\trill re''8 |
    re''1 | }
  { \tag#'parties <>_"Taille" la'4. si'8 |
    la'2~ la'4. sol'8 |
    fad'1\trill | }
>>
