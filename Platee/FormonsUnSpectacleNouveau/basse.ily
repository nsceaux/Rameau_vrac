\clef "basse" R1*4 |
\clef "tenor" <>^"H.c. et Taille" sol'1\doux |
fad'2 fad'4 fad' |
mi'2\trill mi'4. la'8 |
re'2 r |
\clef "bass"
<>^"Basses" re1\doux |
dod2 dod4 dod |
si,2\trill mi |
la,2. la4 |
re'2. si4 |
mi'2. re'4 |
dod'2. dod4 |
re4~ re8.\trill dod32 re mi4 mi, |
la,2 la4\fort re' |
sol1 |
re~ |
re2 dod\trill |
re1 |
<>^"avec Bassons" re'1 |
dod'2 dod'4 dod' |
si2\trill si4. mi'8 |
la2 fad |
<>^"Tous" sol1\fort |
fad2 fad4 fad |
mi2\trill mi4. la8 |
re1 |
re' |
\appoggiatura dod'8 si2 si4 si |
sold2\trill sold4. la8 |
mi2 r4 dod' |
re'2. si4 |
mi'2 mi' |
dod' la |
fad2 fad4 fad |
re2.\trill re8 mi |
la,1 |
R1*2 |
r2 re4 re |
sol sol8 sol sol4 sol |
sol sol sol mi |
fad fad fad4. si8 |
lad1\trill |
R1*2 |
r2 si,4 si, |
mi mi8 mi mi4 mi |
mi mi mi dod |
fad fad8 fad fad4 fad |
fad2 fad4 re |
sol2 r4 sold8 sold |
lad2\trill lad4 si |
fad2 fad, |
si,2. si4 |
mi'2. re'4 |
dod'2 fad'4 mi' |
re'2 lad |
si4 mi fad fad, |
si,2 sol |
sol1~ |
sol2 sol4 sol |
fad2\trill fad4. si8 |
mi1 |
r2 mi, |
mi, r4 r8 mi' |
dod'2\trill re'4 re'8 re |
la1 |
R1 |
r2 re4 re |
sol sol8 sol sol4 sol |
sol sol sol mi |
la la8 la la4 la |
la2 la4 fad |
si si8 si si4 si |
si si si si |
dod'2 dod'4 la |
re'2 re'4 sol |
la2 la,4. re8 |
re1 |
\clef "tenor" sol'1 |
fad' mi' |
\clef "bass" re'2 r4 re'8 re' |
fad1~ |
fad2 fad4. sol8 |
la2 la, |
re,1 |
