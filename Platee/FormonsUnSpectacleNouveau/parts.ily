\piecePartSpecs
#`((violon1 #:notes "dessus" #:tag-notes dessus1)
   (violon2 #:notes "dessus" #:tag-notes dessus2)
   (haute-contre)
   (taille)
   (hautbois #:notes "dessus" #:tag-notes dessus
             #:instrument ,#{ \markup\center-column { Hautbois Violons } #})
   (bassons #:notes "basse")
   (basses #:notes "basse"))
