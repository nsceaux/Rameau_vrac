<<
  \tag #'(thespis basse) {
    \clef "vhaute-contre" R1*4 |
    sol'1 |
    fad'2 fad'4 fad' |
    mi'2\trill mi'4. la'8 |
    re'2 r4 mi' |
    fad' fad' re' si |
    mi' mi' dod' la |
    re' re'8 re' si4. mi'8 |
    dod'4.\trill\melisma re'8[ mi' fad' re' mi'] |
    fad'2~ fad'8[ sol' fad' la'] |
    sold'4. fad'8[ sold' la' fad' sold'] |
    la'4( sold'8)\trill[ fad']\melismaEnd mi'4 re'8 dod' |
    fad'4 si8 dod' si4.\trill la8 |
    la2 r4 la' |
    si'4 si' sol' mi' |
    la' la' fad' re' |
    sol' fad'8 mi' la'4. si'8 |
    fad'4.\trill\melisma mi'8[ re' mi' fad' sol']( |
    la'1)~ |
    la'4. si'8[ la' si' la' si']( |
    la'4) si'8([ la'] sold'[ la') fad' sold'(] |
    la'2.)( sol'8\trill[ fad']) |
    mi'4. re'8[ mi' fad' re' mi']( |
    fad'4)\melismaEnd fad' fad' re' |
    si' si'8 la' sol'4(\trill fad'8) sol' |
    fad'1\trill |
    R1*9 |
    r2 r4 mi' |
    la'1 |
    mi'2. fad'8 sol' |
    fad'1\trill |
    fad'4 r r2 |
    R1*2 |
    r2 r4 dod' |
    fad'1 |
    fad'2. fad'8 fad' |
    fad'2~ fad'8[\melisma sold' fad' la'] |
    sold'[ fad' mi' red'] mi'[ re' dod' si] |
    dod'4 sold'4. la'8[ sold' si'] |
    lad'[ sold' fad' mi'] fad'[ mi' re' dod'] |
    re'[ la' sold' lad'] si'2~ |
    si'4. la'!8[ sol' fad' mi' re']( |
    dod'4)\trill\melismaEnd dod' dod' re'8 si |
    fad'1 |
    fad'4 r r2 |
    R1*12 |
    r2 mi'4 mi' |
    la' la'8 la' la'4 la' |
    la'4.\melisma sol'8[ fad' mi' re' dod'] |
    si4. dod'8[ re' mi' fad' re'] |
    sol'4. fad'8[ mi' fad' re' mi'] |
    dod'2\trill~ dod'8[ re' si dod'] |
    re'2\melismaEnd la4 r |
    R1*2 |
    r4 mi'8 mi' la'2~ |
    la'2. sol'8 fad' |
    mi'2.\trill re'4 |
    re'1 |
    R1*2 |
    r2 r4 mi'8 mi' |
    la'1~ |
    la'~ |
    la'2. sol'8 fad' |
    fad'2( mi'4.)\trill re'8 |
    re'1 |
  }
  \tag #'vdessus {
    \clef "vdessus" R1*17 |
    sol''1 |
    fad''2 fad''4 fad'' |
    mi''2\trill mi''4. la''8 |
    re''2 r4 mi'' |
    fad'' fad'' re'' si' |
    mi'' mi'' dod'' la' |
    re'' dod''8 si' mi''4. fad''8 |
    dod''8[\trill\melisma re'' si' dod''] re''2~ |
    re''1~ |
    re''4\melismaEnd re'' re'' re'' |
    mi'' mi''8 mi'' mi''4. mi''8 |
    la'4 la' re'' re'' |
    fad''1~ |
    fad''2 re''4 re'' |
    re''2 re''4. dod''8 |
    si'1\trill |
    R1 |
    r2 si' |
    mi''1~ |
    mi''2 re''4 fad'' |
    si'2.\trill si'8 dod'' |
    \appoggiatura si'8 la'1 |
    R1 |
    r2 la'4 la' |
    re''4 re''8 re'' re''4 re'' |
    re'' re'' re'' si' |
    mi''1~ |
    mi''4 mi'' mi'' mi''8 re'' |
    dod''1\trill |
    R1 |
    r2 fad'4 fad' |
    si' si'8 si' si'4 si' |
    si' si' si' sold' |
    dod''4 dod''8 dod'' dod''4 dod'' |
    dod''2 dod''4 lad' |
    re''4 re''8 re'' re''4 re'' |
    re'' re'' re'' si' |
    mi''2 mi''4 re'' |
    dod''2.\trill dod''4 |
    si'1 |
    R1*4 |
    r2 re'' |
    re''1~ |
    re''2 re''4 re'' |
    red''2.\trill red''8 mi'' |
    mi''1 |
    R1 |
    r2 r4 r8 sol'' |
    sol''2 fad''4\trill fad''8 sol'' |
    mi''2\trill mi''4 r |
    r2 la'4 la' |
    re'' re''8 re'' re''4 re'' |
    re'' re'' re'' si' |
    mi'' mi''8 mi'' mi''4 mi'' |
    mi''2 mi''4 dod'' |
    fad'' fad''8 fad'' fad''4 fad'' |
    fad'' fad'' fad'' re'' |
    sol''1~ |
    sol''4 sol'' sol'' la'' |
    fad''2.\trill mi''8 re'' |
    dod''2.\trill re''4 |
    re''1 |
    R1*3 |
    r2 r4 fad''8 fad'' |
    fad''1~ |
    fad''2. mi''8 re'' |
    re''2( dod''4.)\trill re''8 |
    re''1 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R1*21 |
    re'1 |
    dod'2 dod'4 dod' |
    si2\trill si4. mi'8 |
    la2 r4 la' |
    si' si' sol' mi' |
    la' la' fad' re' |
    sol' sol'8 sol' sol'4 fad'8[ mi'] |
    fad'4.\melisma mi'8[ re' mi' fad' sol']( |
    la'4.) sol'8[ fad' sol' la' fad']( |
    si'4)\melismaEnd si' la' sold' |
    si' mi'8 mi' mi'4. la'8 |
    sold'1\trill |
    R1*2 |
    r2 mi' |
    la'2 la'4 la' |
    la'2. la'8 sold' |
    \appoggiatura sold'8 la'1 |
    R1*2 |
    r2 la' |
    la'1 |
    sol'2. sol'8 sol' |
    dod'4 dod' dod' dod'8 si |
    fad'1 |
    fad'4 r r2 |
    R1 |
    r2 fad'4 fad' |
    fad' mi'8 red' sold'4 sold' |
    sold' sold' mi' mi' |
    mi' re'8 dod' fad'4 fad' |
    fad'2 fad'4 fad' |
    fad' fad' mi' mi' |
    fad2 fad'4 si' |
    lad'2.\trill si'4 |
    si'1 |
    R1*4 |
    r2 si' |
    si'1~ |
    si'2 si'4 si' |
    la'2. sol'8 fad' |
    sol'1 |
    R1 |
    r2 r4 r8 mi' |
    la'2 la'4 la'8 la' |
    la'2 la'4 r |
    R1 |
    r2 r4 r8 la' |
    la'1 |
    sol'2. sol'8 sol' |
    sol'1*3/4( \afterGrace s4 fad'8) |
    fad'2\trill la'4 la' |
    la' la'8 la' la'4 la' |
    re'4 re' re' mi' |
    la1~ |
    la2 la'4 sol' |
    sol'2.( fad'8) sol' |
    fad'1\trill |
    R1*3 |
    r2 r4 re'8 re' |
    re'1~ |
    re'2. re'8 sol' |
    sol'2.( fad'8) sol' |
    fad'1\trill |
  }
  \tag #'vtaille {
    \clef "vtaille" R1*21 |
    re'1 |
    dod'2 dod'4 dod' |
    si2\trill si4. mi'8 |
    la1 |
    mi' |
    fad'2 re'4 re' |
    re'2 re'4. dod'8 |
    \appoggiatura dod'8 re'1 |
    la |
    fad'2 fad'4 fad' |
    si2 si4. la8 |
    mi'1 |
    R1*2 |
    r2 dod' |
    dod'2 dod'4 la |
    fad'2. fad'8 mi' |
    dod'1\trill |
    R1*2 |
    r2 fad' |
    fad'1 |
    mi'2. mi'8 mi' |
    lad4\trill lad lad lad8 si |
    fad1 |
    fad4 r r2 |
    R1 |
    r2 re'4 re' |
    re' dod'8 si mi'4 mi' |
    mi' mi' sold sold |
    lad4 lad8 lad lad4 si |
    si2 si4 si |
    si si si si |
    dod'2 dod'4 si |
    mi'2. re'8[ dod'] |
    \appoggiatura dod'8 re'1 |
    R1*4 |
    r2 sol' |
    sol'1~ |
    sol'2 sol'4 si |
    si2. si8 si |
    si1 |
    R1 |
    r2 r4 r8 mi' |
    mi'2 la4 la8 re' |
    dod'2\trill dod'4 r |
    R1 |
    r2 r4 r8 fad' |
    fad'1 |
    si2. si8 mi' |
    dod'1\trill |
    \appoggiatura si8 la2 re'4 re' |
    re' re'8 re' re'4 fad' |
    mi'4 mi' re' mi' |
    la1~ |
    la2 la4 si |
    la2. la4 |
    la1 |
    R1*3 |
    r2 r4 la8 la |
    la1~ |
    la2 la4. si8 |
    la2. la4 |
    la1 |
  }
  \tag #'vbasse {
    \clef "vbasse" R1*25 |
    sol1 |
    fad2 fad4 fad |
    mi2\trill mi4. la8 |
    re1 |
    re'1 |
    \appoggiatura dod'8 si2 si4 si |
    sold2\trill sold4. la8 |
    mi1 |
    R1 |
    r2 mi' |
    dod' la |
    fad2 fad4 fad |
    re2.\trill re8 mi |
    la,1 |
    R1*2 |
    r2 re4 re |
    sol sol8 sol sol4 sol |
    sol sol sol mi |
    fad fad fad fad8 si |
    lad1\trill |
    R1*2 |
    r2 si,4 si, |
    mi mi8 mi mi4 mi |
    mi mi mi dod |
    fad fad8 fad fad4 fad |
    fad2 fad4 re |
    sol sol r sold8 sold |
    lad2\trill lad4 si |
    fad2. fad4 |
    si,1 |
    R1*4 |
    r2 sol |
    sol1~ |
    sol2 sol4 sol |
    fad2\trill fad4. si8 |
    mi1 |
    R1 |
    r2 r4 r8 mi' |
    dod'2\trill re'4 re'8 re |
    la2 la4 r |
    R1 |
    r2 re4 re |
    sol sol8 sol sol4 sol |
    sol sol sol mi |
    la la8 la la4 la |
    la2 la4 fad |
    si4 si8 si si4 si |
    si4 si si si |
    dod' dod' dod' la |
    re'2 re'4 sol |
    la2. la4 |
    re1 |
    R1*3 |
    r2 r4 re'8 re' |
    fad1~ |
    fad2 fad4. sol8 |
    la2( la,4.) re8 |
    re1 |
  }
>>