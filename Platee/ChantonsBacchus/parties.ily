\clef "haute-contre" r4 r2 |
R1*3 |
r4 <>^"a 2. cordes" <fad' la'>4 q q |
q q q q |
q q q q |
<mi' la'>2 <re' la'>4. re''8 |
dod''4\trill r r2 |
R1*3 |
r4 re'' re'' re'' |
re' re'' re'' re'' |
re'' si' re'' si' |
re'' sol'8 fad' mi'2\trill |
re' r |
R1*7 |
r4 <fad' la'>4 q q |
q q q q |
q q q q |
<mi' la'>2 <re' la'>4. re''8 |
dod''4\trill re'' re'' re'' |
re' re'' re'' re'' |
re'' si' re'' si' |
re'' sol'8 fad' mi'2\trill |
re' r |
R1*11 |
r4 <fad' la'>4 q q |
q q q q |
q q q q |
<mi' la'>2 <re' la'>4. re''8 |
dod''4\trill re'' re'' re'' |
re' re'' re'' re'' |
re'' si' re'' si' |
re'' sol'8 fad' mi'2\trill |
re'1 |
