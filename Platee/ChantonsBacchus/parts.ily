\piecePartSpecs
#`((violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (haute-contre)
   (taille)
   (hautbois #:notes "dessus" #:tag-notes dessus
             #:instrument ,#{ \markup\center-column { Hautbois Violons } #})
   (bassons #:notes "basse")
   (basses #:notes "basse")
   (percussions #:notes "dessus"
                #:score-template "score-percussion"))
