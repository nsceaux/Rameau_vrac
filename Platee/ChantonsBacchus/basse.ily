\clef "basse" <>^"B.C." re'4 fad' re' |
la fad re la, |
re, re' fad' re' |
dod' dod'8 la re'2 |
la4 \cesure <>^"Tous" re'4 fad' re' |
la fad re la, |
re, re' fad' re' |
dod' dod'8 la re'2 |
la4 \cesure <>^"B.C." fad4 fad fad |
fad fad sol8( fad) sol( la) |
fad4 sol fad sol |
fad fad8 sol la2 |
re4 \cesure <>^"Tous" fad4 fad fad |
fad fad sol8( fad) sol( la) |
fad4 sol fad sol |
fad fad8 sol la2 |
re \clef "tenor" <>^"Bassons seul." re'4 fad' |
dod' mi' dod' la |
si2 mi' |
\appoggiatura re'8 dod'2 dod'4 si8\trill la |
mi2 dod'4 dod' |
dod' dod' re'2 |
la \clef "bass" dod |
re mi |
la,4 \cesure <>^"Tous" re'4 fad' re' |
la fad re la, |
re, re' fad' re' |
dod' dod'8 la re'2 |
la4 fad4 fad fad |
fad fad sol8( fad) sol( la) |
fad4 sol fad sol |
fad fad8 sol la2 |
re \clef "tenor" <>^"Bassons seul." r |
r re'4 re' |
re' re' mi'8\trill re' mi' fad' |
re'4 re' mi'8\trill re' mi' fad' |
lad4 fad si si, |
fad2 \clef "bass" lad, |
si, si,4.(\trill la,16 si,) |
dod2 dod'4 dod' |
dod' dod' si8\trill la si dod' |
si1 |
la4 si8 la sold4. fad8 |
dod'2 dod |
fad4 \cesure <>^"Tous" re'4 fad' re' |
la fad re la, |
re, re' fad' re' |
dod' dod'8 la re'2 |
la4 fad4 fad fad |
fad fad sol8( fad) sol( la) |
fad4 sol fad sol |
fad fad8 sol la2 |
re1 |

