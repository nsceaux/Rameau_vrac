\key re \major \midiTempo#160
\digitTime\time 2/2 \partial 2. s2. s1*15 s2
\beginMark\markup { \concat { 1 \super re } Reprise } s2 s1*15 s2
\beginMark\markup { \concat { 2 \super e } Reprise } s2 s1*20 \bar "|."
