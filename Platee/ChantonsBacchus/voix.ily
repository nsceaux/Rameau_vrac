<<
  \tag #'(thespis basse) {
    \clef "vhaute-contre" re'4 re' re' |
    re' re' re' re' |
    re' re' re' re' |
    mi' mi'8 fad' fad'2\trill |
    mi'4 r r2 |
    R1*3 |
    r4 la la la |
    la la si8[ la] si[ dod'] |
    la4 re' la re' |
    fad' mi'8 re' re'4.( dod'8)\trill |
    re'4 r r2 |
    R1*3 |
    r2 fad'4 \appoggiatura mi'8 re'4 |
    mi' mi' fad'8[ mi'] re'[ dod'] |
    re'4 re' re' si |
    mi' mi' la sold8 la |
    \appoggiatura la8 si2 mi'4 mi' |
    la'2 re'4 re'8 mi' |
    dod'4\trill dod' dod' mi' |
    re'4\trill dod' si\trill si8 dod' |
    \appoggiatura si8 la4 r r2 |
    R1*7 |
    r2 fad'4 fad' |
    fad'1~ |
    fad'~ |
    fad'2.\melisma sol'8[ fad'] |
    mi'[ re' mi' fad'] re'[ dod' re' mi']( |
    dod'4)\trill\melismaEnd dod' dod' si8 dod' |
    \appoggiatura dod'8 re'2 dod'4 si8 la |
    sold2. dod'4 |
    red'2 red'4 mid'8 fad' |
    mid'1\trill |
    dod'4 re'8 dod' \appoggiatura dod' si4.\trill la8 |
    la2( sold)\trill |
    fad4 r r2 |
    R1*8 |
  }
  \tag #'vdessus {
    \clef "vdessus" r4 r2 |
    R1*3 |
    r4 re'' re'' re'' |
    re'' re'' re'' re'' |
    re'' re'' re'' re'' |
    mi'' mi''8 fad'' fad''2\trill |
    mi''4 r r2 |
    R1*3 |
    r4 la' la' la' |
    la' la' si'8[ la'] si'[ dod''] |
    la'4 re'' la' re'' |
    fad'' mi''8 re'' re''4.( dod''8)\trill |
    re''2 r |
    R1*7 |
    r4 re'' re'' re'' |
    re'' re'' re'' re'' |
    re'' re'' re'' re'' |
    mi'' mi''8 fad'' fad''2\trill |
    mi''4 la' la' la' |
    la' la' si'8[ la'] si'[ dod''] |
    la'4 re'' la' re'' |
    fad'' mi''8 re'' re''4.( dod''8)\trill |
    re''2 r |
    R1*11 |
    r4 re'' re'' re'' |
    re'' re'' re'' re'' |
    re'' re'' re'' re'' |
    mi'' mi''8 fad'' fad''2\trill |
    mi''4 la' la' la' |
    la' la' si'8[ la'] si'[ dod''] |
    la'4 re'' la' re'' |
    fad'' mi''8 re'' re''4.( dod''8)\trill |
    re''1 |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r4 r2 |
    R1*3 |
    r4 la' la' la' |
    la' la' la' la' |
    la' la' la' la' |
    la' la'8 la' la'2 |
    la'4 r r2 |
    R1*3 |
    r4 fad' fad' fad' |
    fad' fad' mi' mi' |
    la' mi' fad' mi' |
    la'4 sol'8 fad' sol'2 |
    fad'2 r |
    R1*7 |
    r4 la' la' la' |
    la' la' la' la' |
    la' la' la' la' |
    la' la'8 la' la'2 |
    la'4 fad' fad' fad' |
    fad' fad' mi' mi' |
    la' mi' fad' mi' |
    la'4 sol'8 fad' sol'2 |
    fad'2 r |
    R1*11 |
    r4 la' la' la' |
    la' la' la' la' |
    la' la' la' la' |
    la' la'8 la' la'2 |
    la'4 fad' fad' fad' |
    fad' fad' mi' mi' |
    la' mi' fad' mi' |
    la'4 sol'8 fad' sol'2 |
    fad'1 |
  }
  \tag #'vtaille {
    \clef "vtaille" r4 r2 |
    R1*3 |
    r4 fad'4 fad' fad' |
    fad' fad' fad' fad' |
    fad' fad' fad' fad' |
    sol' sol'8 fad' re'2 |
    dod'4 r r2 |
    R1*3 |
    r4 re' re' re' |
    re' re' re' re' |
    re' re' re' re' |
    re' mi'8 fad' mi'2 |
    re' r |
    R1*7 |
    r4 fad'4 fad' fad' |
    fad' fad' fad' fad' |
    fad' fad' fad' fad' |
    sol' sol'8 fad' re'2 |
    dod'4 re' re' re' |
    re' re' re' re' |
    re' re' re' re' |
    re' mi'8 fad' mi'2 |
    re' r |
    R1*11 |
    r4 fad'4 fad' fad' |
    fad' fad' fad' fad' |
    fad' fad' fad' fad' |
    sol' sol'8 fad' re'2 |
    dod'4 re' re' re' |
    re' re' re' re' |
    re' re' re' re' |
    re' mi'8 fad' mi'2 |
    re'1 |
  }
  \tag #'vbasse {
    \clef "vbasse" r4 r2 |
    R1*3 |
    r4 re' re' re' |
    re' re' re' re' |
    re' re' re' re' |
    dod' dod'8 la re'2 |
    la4 r r2 |
    R1*3 |
    r4 fad fad fad |
    fad fad sol8[ fad] sol[ la] |
    fad4 sol fad sol |
    fad fad8 sol la2 |
    re2 r |
    R1*7 |
    r4 re' re' re' |
    re' re' re' re' |
    re' re' re' re' |
    dod' dod'8 la re'2 |
    la4 fad fad fad |
    fad fad sol8[ fad] sol[ la] |
    fad4 sol fad sol |
    fad fad8 sol la2 |
    re2 r |
    R1*11 |
    r4 re' re' re' |
    re' re' re' re' |
    re' re' re' re' |
    dod' dod'8 la re'2 |
    la4 fad fad fad |
    fad fad sol8[ fad] sol[ la] |
    fad4 sol fad sol |
    fad fad8 sol la2 |
    re1 |
  }
>>