\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = \markup\center-column { Hautbois Violons } } <<
        \global \keepWithTag #'dessus \includeNotes "dessus"
      >>
      \new Staff \with { instrumentName = \markup\center-column { Hautes-contre Tailles } } <<
        \global \keepWithTag #'parties \includeNotes "parties"
      >>
    >>
    \new ChoirStaff \with {
       instrumentName = \markup\character Chœur
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
       instrumentName = \markup\character Thespis
    } \withLyrics <<
      \global \keepWithTag #'thespis \includeNotes "voix"
    >> \keepWithTag #'thespis \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2. s1*7\break s1*8\pageBreak
        s1*7\pageBreak
        s1*7\break s1*7\break s1*7\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
