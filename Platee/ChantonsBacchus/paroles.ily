\tag #'(thespis basse) {
  Chan -- tons Bac -- chus,
  chan -- tons Mo -- mus,
  chan -- tons l’A -- mour et ses flam -- mes.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Chan -- tons Bac -- chus,
  chan -- tons Mo -- mus,
  chan -- tons l’A -- mour et ses flam -- mes.
}
\tag #'(thespis basse) {
  Que tour à tour,
  dans ce sé -- jour,
  ces Dieux rem -- plis -- sent nos â -- mes.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Que tour à tour,
  dans ce sé -- jour,
  ces Dieux rem -- plis -- sent nos â -- mes.
}
\tag #'(thespis basse) {
  Sans le vin,
  sans son i -- vres -- se,
  la ten -- dres -- se
  n’est que cha -- grin,
  sans le vin,
  sans son i -- vres -- se,
  la ten -- dres -- se
  n’est que cha -- grin.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Chan -- tons Bac -- chus,
  chan -- tons Mo -- mus,
  chan -- tons l’A -- mour et ses flam -- mes.
  Que tour à tour,
  dans ce sé -- jour,
  ces Dieux rem -- plis -- sent nos â -- mes.
}
\tag #'(thespis basse) {
  Veut- on ri -- re,
  c’est à Bac -- chus qu’on a re -- cours,
  Mo -- mus lui dût tou -- jours
  son plus char -- mant dé -- li -- re.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Chan -- tons Bac -- chus,
  chan -- tons Mo -- mus,
  chan -- tons l’A -- mour et ses flam -- mes.
  Que tour à tour,
  dans ce sé -- jour,
  ces Dieux rem -- plis -- sent nos â -- mes.
}
