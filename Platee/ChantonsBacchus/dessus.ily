\clef "dessus" r4 r2 |
R1*3 |
r4 fad'' la'' fad'' |
re'' la' fad' re' |
la fad'' la'' re'' |
mi'' mi''8 fad'' fad''2\trill |
mi''4 r r2 |
R1*3 |
r4 la' la' la' |
la' la' si'8 la' si' dod'' |
la'4 re'' la' re'' |
fad'' mi''8 re'' re''4.( dod''8)\trill |
re''2 r |
R1*3 |
r2 <>^"Hautbois seul" mi''4 mi'' |
mi'' mi'' fad''8\trill mi'' fad'' sold'' |
mi''4 fad''8 sold'' la'' si'' dod'''4 |
si''4\trill la'' sold''4.(\trill fad''16 sold'') |
la''4 <>^"Tous" fad'' la'' fad'' |
re'' la' fad' re' |
la fad'' la'' re'' |
mi'' mi''8 fad'' fad''2\trill |
mi''4 la' la' la' |
la' la' si'8 la' si' dod'' |
la'4 re'' la' re'' |
fad'' mi''8 re'' re''4.( dod''8)\trill |
re''2 r |
r2 <>^"Hautbois seul" si'4 si' |
si' si' dod''8\trill si' dod'' re'' |
si'4 si' dod''8\trill si' dod'' re'' |
dod''4 re''8 dod'' si' lad' si' dod'' |
lad'2\trill dod'' |
fad' sold' |
mid' r |
R1 |
r2 dod''4 dod'' |
dod'' dod'' re''8( dod'') la''( fad'') |
fad''2( mid''4.)\trill fad''8 |
fad''4 <>^"Tous" fad'' la'' fad'' |
re'' la' fad' re' |
la fad'' la'' re'' |
mi'' mi''8 fad'' fad''2\trill |
mi''4 la' la' la' |
la' la' si'8 la' si' dod'' |
la'4 re'' la' re'' |
fad'' mi''8 re'' re''4.( dod''8)\trill |
re''1 |
