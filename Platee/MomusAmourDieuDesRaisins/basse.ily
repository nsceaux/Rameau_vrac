\clef "basse" r8 re\doux fa la fa re |
r re fa la fa re |
r sib, re fa re sib, |
fa,2 sol,4 |
la,8 la dod' mi' dod' la |
dod2. |
r4 r la, |
re2 re,4 |
r8 fa la do' la fa |
r fa la do' la fa |
r mi sol do' sol mi |
r fa la do' la fa |
r la, do fa do la, |
sib, la, sol,4.\trill fa,8 |
do, do mi sol mi do |
r do mi sol mi do |
r sib, do mi do sib, |
r sib, do mi do sib, |
r la, do fa do la, |
sib,2 do4 |
r8 re fa la fa re |
r sib, re fa re sib, |
la,8. sib,16 do4 do, |
r8 fa la do' la fa |
fa,4 r r fad, |
sol,2 sib,4 |
la,2 r4 la |
dod1 |
re2 mi4 |
la,1 |
