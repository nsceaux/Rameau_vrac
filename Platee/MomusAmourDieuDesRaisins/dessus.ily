\clef "dessus" r8 re'\doux fa' la' fa' re' |
re'' re' fa' la' fa' re' |
fa'' re' fa' sib' fa' re' |
la''2 sol''8 fa'' |
mi''\trill la' dod'' mi'' dod'' la' |
la''4 r r |
R2.*2 |
r8 fa' la' do'' la' fa' |
fa'' fa' la' re'' la' fa' |
sol'' mi' sol' do'' sol' mi' |
do''' fa' la' do'' la' fa' |
la'' fa' la' do'' la' fa' |
fa''4 sib''4. la''8 |
sol''8\trill do' mi' sol' mi' do' |
do'' do' mi' sol' mi' do' |
do'' do' mi' sol' mi' do' |
do'' do' mi' sol' mi' do' |
do'' do' fa' la' fa' do' |
fa''4 fa' mi' |
fa'8 re' fa' la' fa' re' |
fa'' sib re' fa' sib re'' |
do'' fa'' sol'4.\trill fa'8 |
fa' fa' la' do'' la' fa' |
fa''4 r r2 |
R2. R1*2 R2. R1 |
