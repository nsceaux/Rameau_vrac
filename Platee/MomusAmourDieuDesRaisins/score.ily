\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = "Violons" \haraKiri } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = \markup\character Thespis } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basses" } <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*10\break s2.*9\break s2.*5 s1 s2.\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
