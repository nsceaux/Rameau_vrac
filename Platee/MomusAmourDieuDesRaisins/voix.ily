\clef "vhaute-contre" r4 r r8 la |
la2 r8 re' |
re'2. |
fa'2 mi'8 re' |
dod'2.\trill |
r4 r8 mi' fa' sol' |
sol'4.( fa'8)\trill mi'4 |
\appoggiatura mi'8 fa'2 \appoggiatura mi'8 re'4 |
R2. |
do'4 do' do' |
do'4. re'8 do' sib |
la2\trill \appoggiatura sol8 fa4 |
do'4. la8 sib do' |
re'4 mi'4. fa'8 |
mi'2.\trill |
sol'4 sib' la' |
la'( sol'8)\trill la' mi' fa' |
\appoggiatura fa' sol'2 do'4 |
fa'4. mi'8 re' do' |
do'4.( sib8)\trill la sib |
la2.\trill |
re'4. do'8 re' mi'16[ do'] |
fa'4 mi'4.\trill fa'8 |
fa'2. |
r4 r8 r16 la' la'4~ la'16 re' re' la |
\appoggiatura la8 sib4~ sib16 re' re' re' sol'8. mi'16 |
dod'2\trill dod'8 r mi'4 |
mi'2 mi'4 mi'8 fa' |
fa'4. re'16 do' si8 si16 do' |
\appoggiatura si8 la2 la4 r |
