\piecePartSpecs
#`((dessus)
   (violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (hautbois #:notes "dessus")
   (haute-contre #:notes "haute-contre")
   (taille #:notes "taille")
   (basses #:notes "basse")
   (percussions #:score-template "score-percussion"
                #:notes "dessus" #:tag-notes dessus))
