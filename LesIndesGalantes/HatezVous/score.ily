\score {
  \new StaffGroupNoBar <<
    \new Staff <<
      \instrumentName "Violons"
      \global \includeNotes "dessus"
    >>
    \new Staff \withLyrics <<
      \characterName "Valere"
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      <>_"Tous"
      \instrumentName "B.C."
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
