\version "2.19.80"
\include "common.ily"
%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = " " }
  \markup\null
}

\act "Dardanus"
\pieceToc\markup\wordwrap {
  Chaconne
}
\includeScore "Dardanus/Chaconne"
\pageBreak
\pieceToc\markup\wordwrap {
  Trois Songes, chœur : \italic { Par un sommeil agréable }
}
\includeScore "Dardanus/ParUnSommeilAgreable"
\markupCond #(and (member (*part*) '(haute-contre taille)) #t)
\markup\vspace#30
\pageBreak
\pieceToc\markup\wordwrap {
  Anténor : \italic { Voici les tristes lieux } – \italic { Monstre affreux }
}
\includeScore "Dardanus/VoiciLesTristesLieux"
\includeScore "Dardanus/MonstreAffreux"
\markupCond #(and (member (*part*) '(haute-contre taille)) #t)
\markup\vspace#30
\pageBreak
\pieceToc\markup\wordwrap {
  Prologue – Tambourins
}
\includeScore "Dardanus/PrologueTambourinI" \noPageTurn
\includeScore "Dardanus/PrologueTambourinII"
\markupCond #(and (member (*part*) '(haute-contre taille)) #t)
\markup\vspace#20
\pageBreak
\act "Les Fêtes d’Hébé"
\pieceToc\markup\wordwrap { Musette en Rondeau }
\includeScore "LesFetesdHebe/MusetteEnRondeau"

\pieceToc\markup\wordwrap { Tambourin en Rondeau }
\includeScore "LesFetesdHebe/TambourinEnRondeau"
\pageBreak
\pieceToc\markup\wordwrap {
  Chœur : \italic { Suivez les loix }
}
\includeScore "LesFetesdHebe/SuivezLesLoix"
\pageBreak
\act "Naïs"
\pieceToc "Ouverture"
\includeScore "Nais/ouverture"
\pageBreak
\act "Les Paladins"
\pieceToc "Entrée très gaye de Troubadours"
\includeScore "LesPaladins/EntreeTresGayeDeTroubadours"
\pageBreak
\act "Platée"
\pieceToc\markup\wordwrap {
  La Folie : \italic { Formons les plus brillants concerts }
  – \italic { Au langueurs d’Apollon }
}
\includeScore "Platee/FormonsLesPlusBrillantsConcerts"
\pageBreak
\act "Le Temple de la Gloire"
\pieceToc "Ouverture"
\includeScore "LeTempleDeLaGloire/ouverture"
\pageBreak
\pieceToc "Air tendre pour les Muses"
\includeScore "LeTempleDeLaGloire/AirTendrePourLesMuses"
\pageBreak
\act "Zaïs"
\pieceToc "Ouverture"
\includeScore "Zais/ouverture"
\pageBreak
\act "Zoroastre"
\pieceToc "Air tendre en rondeau"
\includeScore "Zoroastre/AirTendreEnRondeau"
\pageBreak
\pieceToc "Air grave pour les esprits infernaux"
\includeScore "Zoroastre/AirGravePourLesEspritsInfernaux"
