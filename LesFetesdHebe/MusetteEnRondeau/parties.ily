\clef "taille" si4\tresdoux |
si2.~ |
si~ |
si~ |
si2 si4 |
si2.~ |
si~ |
si~ |
si2\fermata
%%
mi'4 |
mi'2.~ |
mi'2 sold4 |
la2 si4 |
mi2 mi'4 |
mi'2.~ |
mi'2 sold4 |
la2 la8.\trill( sold32 la) |
si2
\origVersion { si4 | \custosNote si2 }
%%
mi'4 |
mi'2.~ |
mi'2 mi'4 |
la4 si dod'8.( red'16) |
\appoggiatura red'8 mi'4 mi mi' |
mi'2.~ |
mi'2 mi'4 |
la4 si dod'8. red'16 |
\appoggiatura red'8 mi'2
\origVersion { si4 | \custosNote si2 s4 }
