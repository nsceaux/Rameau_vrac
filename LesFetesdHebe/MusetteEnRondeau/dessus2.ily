\clef "dessus" si'4\tresdoux |
si'( la')\trill sold' |
\appoggiatura sold'8 fad'2\trill si'4 |
red'8.\trill( dod'16) red'8.( mi'16) fad'8.( red'16) |
mi'4 si8 r si'4 |
si'4( la')\trill sold' |
\appoggiatura sold'8 fad'2\trill si'4 |
red'8.\trill( dod'16) red'8.( mi'16) fad'8.( red'16) |
mi'2\fermata
%%
sold'4 |
\appoggiatura sold'8 la'4.( sold'8) fad'8.( la'16) |
sold'4\trill \appoggiatura fad'8 mi'4 sold' |
sold'4( fad'8.\trill mi'16) mi'8.( red'16) |
\appoggiatura red'8 mi'4 si sold' |
\appoggiatura sold'8 la'4.( sold'8) fad'8.( la'16) |
sold'4\trill \appoggiatura fad'8 mi'4 sold' |
sold'8.( fad'16) la'8.( sold'16) fad'8.( mi'16) |
red'2
\origVersion { si'4 | \custosNote si'2 }
%%
sold'4 |
si'4.( dod''8) \appoggiatura si'8 la'4\trill |
sold'4\trill \appoggiatura fad'8 mi'4 sold' |
la' sold' fad' |
si2 sold'4 |
si'4.( dod''8) \appoggiatura si'8 la'4\trill |
sold'4\trill \appoggiatura fad'8 mi'4 sold' |
la' sold' fad' |
si2
\origVersion { si'4 | \custosNote si'2 s4 }
