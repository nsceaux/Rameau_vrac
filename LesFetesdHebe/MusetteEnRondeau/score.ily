\score {
  <<
    \setMusic #'group <<
      \new Staff <<
        \modVersion\instrumentName\markup\center-column { Flutes Violons }
        \origVersion <>^"Flutes et Violons"
        \global \includeNotes "dessus1"
      >>
      \new Staff <<
        \modVersion\instrumentName\markup { \concat { 2 \super e } Violons }
        \origVersion <>_\markup { \concat { 2 \super e } Violons }
        \global \includeNotes "dessus2"
      >>
      \new Staff <<
        \modVersion\instrumentName\markup\center-column {
          H[autes]-contre Tailles
        }
        \origVersion <>_"H-Contre et Taille"
        \global \includeNotes "parties"
      >>
      \new Staff <<
        \modVersion\instrumentName "Bassons"
        \origVersion <>_"Bassons"
        \global \includeNotes "basson"
      >>
      \new Staff <<
        \modVersion\instrumentName "Basses"
        \origVersion <>_"Basses"
        \global \includeNotes "basse"
        \origLayout {
          s4 s2.*10\break
          s2.*11\break
          s2.*5\break
        }
        \modVersion {
          s4 s2.*7 s2\break
          s4 s2.*7 s2\break
        }
      >>
    >>
    \origVersion\new StaffGroupNoBar \group
    \modVersion\new StaffGroup \group
  >>
  \layout { short-indent = \noindent }
  \midi { }
  \header {
    source = "BNF A-143, VM2-342"
    editor = "Nicolas Sceaux"
  }
}
