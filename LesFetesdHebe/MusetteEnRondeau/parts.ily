\piecePartSpecs
#`((dessus #:score "score-dessus")
   (flutes #:notes "dessus1" #:instrument ,#{ \markup\center-column { Flûtes Violons } #}
           #:music ,#{ s4 s2.*7 s2\break s4 s2.*7 s2\break#})
   (hautbois #:notes "dessus1" #:instrument ,#{ \markup\center-column { Flûtes Violons } #}
             #:music ,#{ s4 s2.*7 s2\break s4 s2.*7 s2\break#})
   (violon1 #:notes "dessus1" #:music ,#{ s4 s2.*7 s2\break s4 s2.*7 s2\break#})
   (violon2 #:notes "dessus2" #:music ,#{ s4 s2.*7 s2\break s4 s2.*7 s2\break#})
   (parties #:music ,#{ s4 s2.*7 s2\break s4 s2.*7 s2\break#})
   (haute-contre #:music ,#{ s4 s2.*7 s2\break s4 s2.*7 s2\break#})
   (taille #:music ,#{ s4 s2.*7 s2\break s4 s2.*7 s2\break#})
   (bassons #:notes "basson")
   (basses #:notes "basse"))
