\clef "dessus"
sold'4\tresdoux |
sold'4( fad')\trill si' |
\appoggiatura mi'8 red'2\trill mi'4 |
fad'8.\trill( mi'16) fad'8.( sold'16) la'8.( fad'16) |
\appoggiatura fad'8 sold'4 \appoggiatura fad'8 mi'4 sold' |
sold'4( fad')\trill si' |
\appoggiatura mi'8 red'2\trill mi'4 |
fad'8.\trill( mi'16) fad'8.( sold'16) la'8.( fad'16) |
mi'2\fermata
%%
si'4 |
\appoggiatura si'8 dod''4. dod''8 red''4\trill |
\appoggiatura red''8 mi''4 si' mi'' |
mi''( red''8.\trill dod''16) si'8. la'16 |
sold'4\trill \appoggiatura fad'8 mi'4 si' |
\appoggiatura si'8 dod''4. dod''8 red''4\trill |
\appoggiatura red''8 mi''4 si' mi'' |
mi''8.( red''16) dod''8.( si'16) la'8.( sold'16) |
fad'4.\trill( mi'16 fad') 
\origVersion { sold'4 | \custosNote sold'2 }
%%
mi''4 |
sold''4.( la''8) \appoggiatura sold''8 fad''4\trill |
mi'' si'8 r si'4 |
dod'' si' la' |
sold'\trill \appoggiatura fad'8 mi'4 mi'' |
sold''4. la''8 \appoggiatura sold''8 fad''4\trill |
mi'' si'8 r si'4 |
dod'' si' la' |
sold'2
\origVersion { sold'4 | \custosNote sold'2 s4 }
