\clef "basse" mi,4\tresdoux |
mi,2.~ |
mi,~ |
mi,~ |
mi,2 mi,4 |
mi,2.~ |
mi,~ |
mi,~ |
mi,2
%%
r4 | R2.*7 | r4 r
\origVersion { mi,4 | \custosNote mi,2 }
%%
r4 | R2.*7 | r4 r
\origVersion { mi,4 | \custosNote mi,2 }
