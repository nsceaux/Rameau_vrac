\key mi \major
\tempo "Tendrement" \midiTempo #120
\digitTime\time 3/4 \partial 4
\modVersion {
  \segnoMark
  s4 s2.*7 s4 \fineMark s \bar ":|."
  \beginMark\markup { \concat { P \super re } reprise }
  s4 s2.*7 s4... \endMark "le rondeau" s32 \bar "|."
  \beginMark\markup { \concat { 2 \super e } reprise }
  s4 s2.*7 s2 \endMark "le rondeau" \bar "|."
}
\origVersion {
  s4 <>^\markup\musicglyph #"scripts.segno"
  s2.*7 <>^\markup\large\italic fin
  s2 \bar "|:|"
  \beginMark\markup { \concat { P \super re } reprise }
  s4 s2.*8 s4.
  \once\override TextScript #'extra-offset = #'(0 . -3)
  <>^\markup\musicglyph #"scripts.segno"
  s16 <>_\markup\right-align "le rondeau" s \bar "|:|"
  \beginMark\markup { \concat { 2 \super e } reprise }
  s4 s2.*8 s4.
  \once\override TextScript #'extra-offset = #'(0 . -3)
  <>^\markup\musicglyph #"scripts.segno"
  s4 <>_\markup\right-align "le rondeau" s8 \bar "|:|"
}
