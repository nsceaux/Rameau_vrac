\clef "tenor/tenor" mi4\tresdoux |
mi4( red)\trill mi |
la2 sold4 |
la8.( sold16) fad8.( mi16) red8.( fad16) |
si,2 mi4 |
mi4( red)\trill mi |
la2 sold4 |
la8.( sold16) fad8.( mi16) si8.( si,16) |
mi2
%%
r4 | R2.*7 | r4 r
\origVersion { mi4 | \custosNote mi2 }
%%
r4 | R2.*7 | r4 r
\origVersion { mi4 | \custosNote mi2 }
