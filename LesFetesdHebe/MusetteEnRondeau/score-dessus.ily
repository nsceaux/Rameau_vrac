\score {
  \new GrandStaff <<
    \new Staff <<
      \instrumentName\markup\center-column { Flutes Violons }
      \global \includeNotes "dessus1"
    >>
    \new Staff <<
      \instrumentName\markup { \concat { 2 \super e } Violons }
      \global \includeNotes "dessus2"
    >>
  >>
  \layout { }
}