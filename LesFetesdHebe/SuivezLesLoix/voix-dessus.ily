\clef "vdessus"
  \setMusic #'rondeau {
    sold'4 |
    sold'4.( fad'8)\trill si'4 |
    red'2\trill mi'4 |
    fad'8\trill mi' fad' sold' la' fad' |
    \appoggiatura fad'8 sold'4 \appoggiatura fad'8 mi'4 sold' |
    sold'4.( fad'8)\trill si'4 |
    red'2\trill mi'4 |
    fad'8\trill mi' fad' sold' la' fad' |
    mi'2-\origVersion\fermata
  }
  %% rondeau
  \keepWithTag #'() \rondeau
  \clef "vbas-dessus" <>^\markup\character "Une Bergere"
  si'4 |
  si'4.( la'8)\trill sold'4 |
  fad'2\trill r8 si' |
  si' dod'' red'' mi'' fad'' red'' |
  \appoggiatura red''8 mi''4 si'8 r
  \clef "vdessus" <>^\markup\character Chœur
  sold'4 |
  sold'4.( fad'8)\trill si'4 |
  red'2\trill mi'4 |
  fad'8\trill mi' fad' sold' la' fad' |
  mi'2
  %% 1re reprise
  \clef "vbas-dessus" <>^\markup\character "Une Bergere"
  r8 si'8 |
  dod''2 dod''8 red'' |
  \appoggiatura red''8 mi''4 si'8 r mi''4 |
  mi''4( red''8)\trill dod'' si' la' |
  sold'4\trill \appoggiatura fad'8 mi'4 r8 si' |
  dod''2 dod''8 red'' |
  \appoggiatura red''8 mi''4 si'8 r mi''4 |
  mi''8 red'' dod'' si' la' sold' |
  fad'2\trill <>^\markup\character Duo si'4 |
  si'4.( la'8)\trill sold'4 |
  fad'2\trill r8 si' |
  si'8 dod'' red'' mi'' fad'' red'' |
  mi''4 si'8 r si'4 |
  si'4.( la'8)\trill sold'4 |
  fad'2\trill si'4 |
  si'8 dod'' red'' mi'' fad'' red'' |
  mi''2
  \origVersion { \clef "vdessus" \custosNote sold'4 }
  %% rondeau
  \modVersion {
    <>^\markup\character Chœur
    \keepWithTag #'() \rondeau
  }
  %% 2e reprise
  \clef "vbas-dessus" <>^\markup\character "[Une Bergere]"
  mi''4 |
  sold''4. la''8 fad''4\trill |
  mi''4 si' si' |
  dod'' si' la' |
  sold'\trill \appoggiatura fad'8 mi'4 sold' |
  si'4. dod''8 la'4 |
  sold'\trill sold' sold' |
  la' sold'\trill fad' |
  mi'2 si'4 |
  si'4.( la'8)\trill sold'4 |
  fad'2\trill r8 si' |
  si' dod'' red'' mi'' fad'' red'' |
  mi''4 si'8 r si'4 |
  si'4.( la'8)\trill sold'4 |
  fad'2\trill si'4 |
  si'8 dod'' red'' mi'' fad'' red'' mi''2
  \origVersion { \custosNote sold'4 }
  %% rondeau
  \modVersion {
    <>^\markup\character Chœur
    \keepWithTag #'() \rondeau
  }
