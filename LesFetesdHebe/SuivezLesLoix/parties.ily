\clef "haute-contre"
  \setMusic #'rondeau {
    mi'4 |
    mi'4.( red'8)\trill mi'4 |
    la'2 sold'4 |
    red'8\trill dod' red' mi' fad' red' |
    \appoggiatura red'8 mi'4 si mi' |
    mi'4.( red'8)\trill mi'4 |
    la'2 sold'4 |
    red'8\trill dod' red' mi' fad' red' |
    mi'2-\origVersion\fermata
  }
  %% rondeau
  \keepWithTag #'() \rondeau
  si4 | si2.~ | si~ | si~ | si2
  mi'4 |
  mi'4.( red'8)\trill mi'4 |
  la'2 sold'4 |
  red'8\trill dod' red' mi' fad' red' |
  mi'2
  %% 1re rerise
  mi'4\doux |
  la2~ la8. fad16 |
  si2 mi'4 |
  mi' si2 |
  mi'4 mi8 r mi'4 |
  la2~ la8. fad16 |
  << si2 \\ mi >> si4 |
  si fad'8. mi'16 si4 |
  si2 si4~ | si2.~ | si~ | si~ | si~ | si~ | si~ | si~ | si2
  \origVersion\custosNote mi'4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
  %% 2e rerise
  si4\doux |
  si2.~ |
  si2 si'4 |
  mi'4 mi'4. red'8 |
  \appoggiatura red'8 mi'2 mi'4 |
  mi'4. mi'8 fad'4\trill |
  sold'2 si4 |
  << { dod'8.( red'16) mi'4 fad' | si2 }
    \\ { dod'8. dod'16 si4 la | sold2 } >>
  si4 |
  si2.~ |
  si~ |
  << \origVersion { si2~ si4~ } \modVersion si2.~ >> |
  si2.~ |
  si~ |
  si~ |
  si~ |
  si2
  \origVersion\custosNote mi'4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
