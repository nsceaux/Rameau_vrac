\setMusic #'rondeau {
  s4 s2.*7 s2
}
%% rondeau
\keepWithTag #'() \rondeau
s4 s2.*7 s2
%% 1re rerise
s4 <6 4>2 s8 <7> <8>2. <7> <8> <6 4>2 s8 <7>
<8>2. s4 <2> <7>8. <8>16 <7>2 <8>4 s <7> <8> <7>2 <8>4
<7>2. <8> s4 <7> <8> <7>2 <8>4 s2. s2
\origVersion s4
%% rondeau
\modVersion\keepWithTag #'() \rondeau
%% 2e rerise
s4 s2 <7>4 <8>2. <6 4>4 <3>4. <7>8 <8>2.
s2 <7>4 <8>2. <6 4>4 <3> <7> s2. s4 <7> <8> <7>2 <8>4 <7>2.
<8>2. s4 <7> <8> <7>2 <8>4 <7>2. <8>2
\origVersion s4
%% rondeau
\modVersion\keepWithTag #'() \rondeau
