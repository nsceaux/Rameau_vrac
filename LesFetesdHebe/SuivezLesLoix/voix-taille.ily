\clef "vtaille"
  \setMusic #'rondeau {
    si4 |
    si2 si4 |
    si2 si4 |
    red'8\trill mi' red' mi' fad' si |
    si4 si8 r si4 |
    si2 si4 |
    si2 si4 |
    red'8 mi' red' mi' mi' red' |
    \appoggiatura red' mi'2-\origVersion\fermata
  }
  %% rondeau
  \keepWithTag #'() \rondeau
  r4 R2.*3 r4 r
  si4 |
  si2 si4 |
  si2 si4 |
  red'8 mi' red' mi' mi' red' |
  \appoggiatura red'16 mi'2
  %% 1re rerise
  r4 R2.*15 r4 r
  \origVersion s4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
  %% 2e rerise
  r4 R2.*15 r4 r
  \origVersion s4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
