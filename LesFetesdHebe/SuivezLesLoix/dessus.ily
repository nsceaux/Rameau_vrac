\clef "dessus"
  \setMusic #'rondeau {
    <>^"Violons et hautbois"
    sold'4 |
    sold'4.( fad'8)\trill si'4 |
    red'2\trillSug mi'4 |
    fad'8\trill mi' fad' sold' la' fad' |
    \appoggiatura fad'8 sold'4 \appoggiatura fad'8 mi'4 sold' |
    sold'4.( fad'8)\trill si'4 |
    red'2\trill mi'4 |
    fad'8\trill mi' fad' sold' la' fad' |
    mi'2-\origVersion\fermata
  }
  %% rondeau
  \keepWithTag #'() \rondeau
  <>^"Violons"
  sold'4 |
  sold'4.( fad'8)\trill si'4 |
  red'2 mi'4 |
  fad'8\trill mi' fad' sold' la' fad' |
  \appoggiatura fad'8 sold'4 \appoggiatura fad'8 mi'4
  <>^"[Violons et hautbois]"
  sold'4 |
  sold'4.( fad'8)\trill si'4 |
  red'2\trill mi'4 |
  fad'8\trill mi' fad' sold' la' fad' |
  mi'2
  %% 1re reprise
  <>^"Violons"
  r8\doux sold' |
  \appoggiatura sold'8 la'4.( sold'8) fad'8. la'16 |
  sold'4\trill \appoggiatura fad'8 mi'4 sold' |
  sold'4( fad'8.)\trill mi'16 red'8. fad'16 |
  si2 r8 sold' |
  \appoggiatura sold'8 la'4.( sold'8) fad'8. la'16 |
  sold'4\trill \appoggiatura fad'8 mi'4 sold' |
  sold'8.( fad'16) la'8.( sold'16) fad'8. mi'16 |
  red'2\trill mi''4 |
  mi''4( red'')\trill mi'' |
  la''2 sold''4 |
  la''8.( sold''16) fad''8.( mi''16) red''8.( fad''16) |
  si'2 mi''4 |
  mi''4( red'')\trill mi'' |
  la''2 sold''4 |
  red''8.\trill mi''16 si'2 |
  sold'\trill
  \origVersion\custosNote sold'4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
  %% 2e reprise
  sold'4\doux |
  si'4.( dod''8) si'8.( la'16)\trill |
  sold'4\trill \appoggiatura fad'8 mi'4 sold' |
  la' sold' fad' |
  mi' si8 r r4 |
  R2. |
  r4 r mi' |
  la' si'4.( dod''16 red'') |
  \appoggiatura red''8 mi''2 mi''4 |
  mi''( red'')\trill mi'' |
  la''2 sold''4 |
  la''8.( sold''16) fad''8.( mi''16) red''8.( fad''16) |
  si'2 mi''4 |
  mi''4( red'')\trill mi''4 |
  la''2 sold''4 |
  red''8.\trill mi''16 si'2 |
  sold'2\trill
  \origVersion\custosNote sold'4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
