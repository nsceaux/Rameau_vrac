\setMusic #'rondeau {
  Sui -- vez les loix
  Qu’A -- mour vient nous dic -- ter luy mê -- me,
  Sui -- vez les loix,
  Que nous ché -- ris -- sons dans nos bois.
}

\tag #'choeur \rondeau
\tag #'bergere {
  On fait un choix,
  On aime, et pour tou -- jours on ai -- me.
}
\tag #'choeur {
  Sui -- vez les loix,
  Que nous ché -- ris -- sons dans nos bois.
}

\tag #'bergere {
  L’A -- mour vous ap -- pel -- le,
  Ai -- mez, soy -- ez fi -- de -- le,
  L’A -- mour vous ap -- pel -- le,
  Qu’il est doux d’en -- ten -- dre sa voix.

  On fait un choix,
  On aime, et pour tou -- jours on ai -- me.
  Sui -- vez les loix,
  Que nous ché -- ris -- sons dans nos bois.
  \origVersion { Suivez_&c }
}
\tag #'mercure {
  J’ay fait un choix,
  J’aime, et c’est pour tou -- jours que j’ai -- me.
  Sui -- vons les loix,
  Que vous ché -- ris -- sez dans vos bois.
  \origVersion _
}

\modVersion\tag #'choeur \rondeau

\tag #'bergere {
  Notre ar -- deur cons -- tan -- te
  Sans ces -- se s’aug -- men -- te,
  Qu’i -- ci cha -- cun chan -- te mille et mil -- le fois,
  On fait un choix,
  On aime, et pour tou -- jours on ai -- me.
  Sui -- vez les loix,
  Que nous ché -- ris -- sons dans nos bois.
  \origVersion _
}
\tag #'mercure {
  Qu’i -- ci cha -- cun chan -- te mille et mil -- le fois,
  Sui -- vons les loix,
  Qu’A -- mour vient nous dic -- ter luy mê -- me,
  Sui -- vons les loix,
  Que vous ché -- ris -- sez dans vos bois.
  \origVersion _
}

\modVersion\tag #'choeur \rondeau
