  \setMusic #'rondeau {
    \clef "basse"
    si4 |
    si4.( la8)\trill sold4 |
    fad2\trill mi4 |
    la8 si la sold fad si |
    mi4 mi8 r si4 |
    si4.( la8)\trill sold4 |
    fad2\trill mi4 |
    la8 si la sold fad si |
    mi2-\origVersion\fermata
  }
  %% rondeau
  \keepWithTag #'() \rondeau
  mi4 |
  mi4.( red8)\trill mi4 |
  la2 sold4 |
  la8 si la sold fad si |
  mi2 si4 |
  si4.( la8)\trill sold4 |
  fad2\trill mi4 |
  la8 si la sold fad si |
  mi2
  %% 1re rerise
  \clef "tenor/bass"
  mi'4\doux |
  mi'2 si4 |
  si2 si4 |
  si8.( dod'16) red'8.( mi'16) fad'8.( red'16) |
  mi'4 si8 r mi'4 |
  mi'2 si4 |
  si2.~ |
  si4. mi'8 red'8.\trill mi'16 |
  si2 mi'4 |
  si2. |
  si2 mi'4 |
  red'8.\trill mi'16 si4 si, |
  \clef "basse" mi4 mi,8 r \clef "tenor/tenor" mi'4 |
  si2. |
  si2~ si8 mi'16( sold) |
  \appoggiatura sold16 la4. sold8 fad8. si16 |
  mi2
  \origVersion\custosNote si4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
  %% 2e rerise
  \clef "tenor/bass"
  mi'4 |
  mi'2 red'4\trill |
  \appoggiatura red'8 mi'2 mi4 |
  la si4.( dod'16 red') |
  \appoggiatura red'8 mi'2 mi'4 |
  mi'2 red'4\trill |
  \appoggiatura red'8 mi'2.~ |
  mi'2 si4 |
  si2 mi'4 |
  si2. |
  si2 mi'4 |
  red'8.\trill mi'16 si4 \clef "basse" si, |
  mi4 mi,8 r mi'4 |
  si2. |
  si2~ si8 mi'16 sold |
  \appoggiatura sold8 la4. sold8 fad8. si16 |
  mi2
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
  \origVersion\custosNote si4
