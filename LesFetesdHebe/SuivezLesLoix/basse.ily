  \clef "basse"
  \setMusic #'rondeau {
    mi,4~ | mi,2.~ | mi,~ | mi,~ | mi,2
    mi,4  | mi,2.~ | mi,~ | mi,~ | mi,2-\origVersion\fermata
  }
  %% rondeau
  \keepWithTag #'() \rondeau
  mi,4 | mi,2.~ | mi,~ | mi,~ | mi,2
  mi,4 | mi,2.~ | mi,~ | mi,~ | mi,2
  %% 1re rerise
  <>^\markup\whiteout B.C.
  mi,4\doux~ | mi,2.~ | mi,~ | mi,~ | mi,~ | mi,2.~ | mi,~ | mi,~ | mi,2
  mi,4  | mi,2.~ | mi,~ | mi,~ | mi,2
  mi,4~ | \once\tieDashed mi,2.~ | mi,~ | mi,~ | mi,2
  \origVersion\custosNote mi,4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
  %% 2e rerise
  mi,4 | mi,2.~ | mi,~ | mi,~ | mi,2
  mi,4 | mi,2.~ | mi,~ | mi,~ | mi,2
  mi,4 | mi,2.~ | mi,~ |
  << \origVersion { mi,2~ mi,4 } \modVersion mi,2.~ >> | mi,2
  mi,4 | mi,2.~ | mi,~ | mi,~ | mi,2
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
  \origVersion\custosNote mi,4
