\score {
  <<
    \setMusic #'choeur <<
      \new Staff \withLyrics <<
        \global \includeNotes "voix-dessus"
      >> \keepWithTag #'(choeur bergere) \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-haute-contre"
      >> \keepWithTag #'(choeur mercure) \includeLyrics "paroles"
      \new Staff \with { \haraKiri } \withLyrics <<
        \global \includeNotes "voix-taille"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \haraKiri } \withLyrics <<
        \global \includeNotes "voix-basse"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>

    \setMusic #'orchestre <<
      \new Staff <<
        \modVersion\instrumentName\markup\center-column { Violons Hautbois }
        \origVersion <>_"Violons et hautbois"
        \global \includeNotes "dessus"
      >>
      \new Staff <<
        \modVersion\instrumentName\markup\center-column {
          H[autes]-contre Tailles
        }
        \origVersion {
          <>_"H-contre et Taille" s4 s2.*7 s2
          <>_"H-contre et Taille" s4 s2.*7 s2
          <>^"H-contre et Taille" s4 s2.*16
          <>^"H-contre et Taille"
        }
        \global \includeNotes "parties"
      >>
      \new Staff <<
        \modVersion\instrumentName "Basson"
        \origVersion {
          <>_"Bassons" s4 s2.*7 s2
          <>_"Bassons" s4 s2.*7 s2
          <>^"Bassons" s4 s2.*16
          <>^"Bassons"
        }
        \global \includeNotes "basson"
      >>
    >>

    \origVersion\new StaffGroupNoBar <<
      \choeur
      \orchestre
      \new Staff <<
        { <>_"Basse" s4 s2.*7 s2
          <>_"Basse" s4 s2.*7 s2
           s4 s2.*16
          <>_"Basse"
        }
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s4 s2.*4\pageBreak
          s2.*3 s2 \bar "" \break s4 s2.*3 s2 \bar "" \pageBreak
          s4 s2.*3 s2 \bar "" \pageBreak
          s4 s2.*5\break \grace s8 s2.*5\pageBreak
          s2.*4\break \grace s16 s2.*2\pageBreak
          s4 s2.*5\break \grace s8 s2.*5 s2 \bar "" \pageBreak
          s4 s2.*5\break
        }
      >>
    >>

    \modVersion\new StaffGroupNoBar <<
      \new StaffGroupNoBracket \orchestre
      \new ChoirStaff \with {
        instrumentName = \markup { \character Chœur \hspace #3 }
      } \choeur
      \new Staff <<
        \modVersion\instrumentName "Basses"
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \modVersion {
          s4 s2.*15 s2\break
          s4 s2.*23 s2\break
          s4 s2.*23 s2\break
        }
      >>
    >>
  >>
  \layout { ragged-last = ##f}
  \midi { }
}
