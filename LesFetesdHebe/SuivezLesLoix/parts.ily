\piecePartSpecs
#`((dessus #:instrument ,#{ \markup\center-column { Violons Hautbois } #}
           #:music ,#{ s4 s2.*31 s2\break s4 s2.*23 s2\break #})
   (violon1 #:notes "dessus")
   (violon2 #:notes "dessus")
   (hautbois #:notes "dessus")
   (flutes #:notes "dessus")
   (parties)
   (haute-contre)
   (taille)
   (bassons)
   (basses #:notes "basse"))
