\clef "vbasse"
  \setMusic #'rondeau {
    si4 |
    si4.( la8)\trill sold4 |
    fad2\trill mi4 |
    la8 si la sold fad si |
    mi4 mi8 r si4 |
    si4.( la8)\trill sold4 |
    fad2\trill mi4 |
    la8 si la sold fad si |
    mi2-\origVersion\fermata
  }
  %% rondeau
  \keepWithTag #'() \rondeau
  r4 R2.*3 r4 r
  si4 |
  si4.( la8)\trill sold4 |
  fad2\trill mi4 |
  la8 si la sold fad si |
  mi2
  %% 1re rerise
  r4 R2.*15 r4 r
  \origVersion s4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
  %% 2e rerise
  r4 R2.*15 r4 r
  \origVersion s4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
