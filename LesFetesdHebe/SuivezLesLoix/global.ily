\key mi \major
\midiTempo #120
\digitTime\time 3/4 \partial 4
\modVersion {
  %% rondeau
  \beginMark "Chœur"
  s4 s2.*15 s2 \bar "||"
  %% 1e reprise
  \beginMark\markup {
    \concat { P \super re } Reprise
  }
  s4 s2.*15 s2 \bar "||"
  %% rondeau
  \beginMark "Chœur"
  s4 s2.*7 s2 \bar "||"
  %% 2e reprise
  \beginMark\markup {
    \concat { 2 \super e } Reprise
  }
  s4 s2.*15 s2 \bar "||"
  %% rondeau
  \beginMark "Chœur"
  s4 s2.*7 s2
  \bar "|."
}
\origVersion {
  <>^\markup\musicglyph #"scripts.segno"
  s4 s2.*7
  s4 s^\markup\large\italic fin
  s4 s2.*7 s2
  %% 1e reprise
  \beginMark\markup {
    \concat { P \super re } Reprise
  }
  \set Score.measureLength = #(ly:make-moment 1 4)
  s4
  \set Score.measureLength = #(ly:make-moment 3 4)
  s2.*15 s2 \once\override TextScript #'extra-offset = #'(0 . -2)
  s8.^\markup\musicglyph #"scripts.segno"
  \endMark\markup {
    On reprend le chœur \italic { Suivez les Loix &c. }
  } s16
  \bar "|:|"
  %% 2e reprise
  \beginMark\markup {
    \concat { 2 \super e } Reprise
  }
  \set Score.measureLength = #(ly:make-moment 1 4)
  s4
  \set Score.measureLength = #(ly:make-moment 3 4)
  s2.*15 s2 \once\override TextScript #'extra-offset = #'(0 . -1)
  s4^\markup\musicglyph #"scripts.segno"
  \endMark\markup {
    On reprend le chœur \italic { Suivez les Loix &c. }
  }
  \bar "|:|"
}
