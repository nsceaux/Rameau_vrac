\clef "vhaute-contre"
  \setMusic #'rondeau {
    mi'4 |
    mi'4.( red'8)\trill mi'4 |
    la'2 sold'4 |
    fad'8 sold' fad' mi' mi' red' |
    \appoggiatura red'8 mi'4 si8 r mi'4 |
    mi'4.( red'8)\trill mi'4 |
    la'2 sold'4 |
    fad'8 sold' fad' mi' fad' la' |
    sold'2-\origVersion\fermata
  }
  %% rondeau
  \keepWithTag #'() \rondeau
  r4 R2.*3 r4 r
  mi'4 |
  mi'4.( red'8)\trill mi'4 |
  la'2 sold'4 |
  fad'8 sold' fad' mi' fad' la' |
  sold'2
  %% 1re rerise
  <>^\markup\character Mercure
  r4 R2.*7 r4 r
  sold'4 |
  sold'4.( fad'8)\trill si'4 |
  red'2\trill mi'4 |
  fad'8\trill mi' fad' sold' la' fad' |
  \appoggiatura fad'8 sold'4 \appoggiatura fad'8 mi'4 sold' |
  sold'4.( fad'8)\trill si'4 |
  red'2\trill mi'4 |
  fad'8\trill mi' fad' sold' la' fad' |
  mi'2
  \origVersion\custosNote mi'4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
  %% 2e rerise
  <>^\markup\character [Mercure]
  r4 R2.*3 r4 r
  mi' |
  sold'4. la'8 fad'4\trill |
  mi'4 si8 r si4 |
  dod' si la |
  sold2\trill sold'4 |
  sold'4.( fad'8)\trill si'4 |
  red'2\trill mi'4 |
  fad'8\trill mi' fad' sold' la' fad' |
  \appoggiatura fad'8 sold'4 \appoggiatura fad'8 mi'4 sold' |
  sold'4.( fad'8)\trill si'4 |
  red'2\trill mi'4 |
  fad'8\trill mi' fad' sold' la' fad' |
  mi'2
  \origVersion\custosNote mi'4
  %% rondeau
  \modVersion\keepWithTag #'() \rondeau
