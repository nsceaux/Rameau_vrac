\score {
  <<
    \setMusic #'group <<
      \new Staff <<
        \modVersion\instrumentName "[Dessus]"
        \global \includeNotes "dessus"
      >>
      \new Staff <<
        \modVersion\instrumentName "H[autes]-contre"
        \origVersion <>_"H-Contre"
        \global \includeNotes "haute-contre"
      >>
      \new Staff <<
        \modVersion\instrumentName "Tailles"
        \origVersion <>_"Taille"
        \global \includeNotes "taille"
      >>
      \new Staff <<
        \modVersion\instrumentName "Bassons"
        \origVersion <>_"Bassons"
        \global \includeNotes "basson"
      >>
      \new Staff <<
        \modVersion\instrumentName "Basses"
        \origVersion <>_"Basses"
        \global \includeNotes "basse"
        \origLayout {
          s2 s1*8\pageBreak
          s1*9\break s1*9\pageBreak
          s1*10\break s1*9 s2 \bar "" \pageBreak
          s2 s1*7 s2\break
        }
      >>
    >>
    \origVersion\new StaffGroupNoBar \group
    \modVersion\new StaffGroup \group
  >>
  \layout { short-indent = \noindent }
  \midi { }
}
