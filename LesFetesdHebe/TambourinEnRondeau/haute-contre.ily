\clef "haute-contre"
\setMusic #'rondeau {
  sol'8 la' si' la' |
  sol' la' si' la' sol'4 red' |
  mi' red' mi'8 red' mi' fad' |
  mi'4 si' si' mi'' |
  red''2\trill sol'8 la' si' la' |
  sol' la' si' la' sol'4 red' |
  mi' red' mi'8 red' mi' fad' |
  mi'4 sol' fad' la' |
}
%% rondeau
\keepWithTag #'() \rondeau
sol'2-\origVersion\fermata sol'8 la' si' la' |
sol'4 sol' sol'8 la' si' la' |
sol'4 sol' sol'8 la' si' la' |
sol'4 sol' sol'8 la' si' la' |
sol' la' si' la' sol' la' si' la' |
sol'4 la' sol'8 la' si' la' |
sol' la' si' la' sol' la' si' la' |
sol'4 mi' red' fad' |
mi' red'
\origVersion { sol'8 la' si' la' | \custosNote sol'2 s }
\modVersion\rondeau
\origVersion <>_"finale" |
sol'-\origVersion\fermata \cesureInstr r4 sol' |
sol'2 fad' |
mi' mi' |
mi' red' |
sol'4 si si si |
si si sol'8 la' si' la' |
sol'4 si si si |
si si sol'8 la' si' la' |
sol' la' si' la' sol' fad' mi' red' |
mi'2 sol' |
sol' mi' |
mi' mi' |
red'
\origVersion { sol'8 la' si' la' | \custosNote sol'2 s }
\modVersion\rondeau
\origVersion <>_"finale"
sol'2-\origVersion\fermata r4 si' |
la'2 r4 la' |
sol'2 r4 si' |
la'2 r4 la' |
sol' sol' sol' sol' |
si' sol' sol' sol' |
sol' sol' sol' sol' |
si' sol' sol' sol' |
sol'2 la'4 sol' |
fad'2. red'4 |
mi' si si2~ |
si1~ |
si2. mi'8 fad' |
sol'4 si' si' mi'' |
red''2\trill si2 |
si sol'4. fad'8 |
si4 la sol8 la si4 |
mi sol' la'2 |
sol' \cesureInstr si\doux |
si sol'4. fad'8 |
si4 la sol8 la si4 |
mi sol' la'2 |
sol'
