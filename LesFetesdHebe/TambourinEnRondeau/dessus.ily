\clef "dessus"
\setMusic #'rondeau {
  mi'8 fad' sol' fad' |
  mi'4 si' si' la' |
  sol'\trill fad' mi'8 fad' sol' la' |
  si'4 mi'' red''\trill mi'' |
  fad''2 mi'8 fad' sol' fad' |
  mi'4 si' si' la' |
  sol'\trill fad'4 mi'8 fad' sol' la' |
  si'4 mi'' red''8 mi'' fad'' red'' |
}
%% rondeau
\keepWithTag #'() \rondeau
mi''2-\origVersion\fermata mi''8 fad'' sol'' fad'' |
mi''4 si' do''\trill si' |
do''\trill si' mi''8 fad'' sol'' fad'' |
mi''4 si' do''\trill si' |
do''\trill si' sol''4\trill mi'' |
do''\trill si' sol''\trill mi'' |
do''\trill si' sol''\trill mi'' |
do''\trill si' la'\trill si' |
sol'\trill fad'
\origVersion { mi'8 fad' sol' fad' | \custosNote mi'2 s }
\modVersion\rondeau
\origVersion <>_"finale"
mi''4-\origVersion\fermata \cesureInstr si' do''\trill si' |
lad' si' si'\trill la' |
sold' la'4 la'\trill sol' |
fad' sol' sol'\trill fad' |
mi' mi'' mi'8 fad' sol' fad' |
mi'4 si' si si |
si mi'' mi'8 fad' sol' fad' |
mi'4 si' si si |
si mi'' mi'8 fad' sol' fad' |
mi' fad' sol' la' si' la' sol' fad' |
mi' fad' sol' la' si' la' sol' la' |
si' la' sol' la' si'4 la'8 sol' |
fad'2\trill
\origVersion { mi'8 fad' sol' fad' | \custosNote mi'2 s }
\modVersion\rondeau
\origVersion <>_"finale"
mi''4-\origVersion\fermata \cesureInstr si' sol''\trill fad''~ |
fad'' si' fad''\trill mi''~ |
mi'' si' sol''\trill fad''~ |
fad'' si' fad''\trill mi''~ |
mi'' si' mi''\trill si' |
sol''\trill si' mi''\trill si' |
si'' si' mi''\trill si' |
sol''\trill si' mi''\trill si' |
si'' la''8 sol'' fad'' sol'' mi'' fad'' |
red'' mi'' dod'' red'' si' do'' la' si' |
sol' la' fad' sol' mi' fad' sol' fad' |
mi' fad' sol' fad' mi' fad' sol' fad' |
mi' fad' sol' fad' mi' fad' sol' la' |
si'4 mi'' red''\trill mi'' |
fad''2\trill mi'8 fad' sol' fad' |
mi' fad' sol' la' si' do'' la' si' |
sol' la' fad' sol' mi' fad' sol' la' |
si'4 mi'' red''8 mi'' fad'' red'' |
mi''2 \cesureInstr mi'8\doux fad' sol' fad' |
mi' fad' sol' la' si' do'' la' si' |
sol' la' fad' sol' mi' fad' sol' la' |
si'4 mi'' red''8 mi'' fad'' red'' |
mi''2

