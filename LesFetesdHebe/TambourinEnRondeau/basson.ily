\clef "tenor/tenor"
\setMusic #'rondeau {
  r2 |
  r mi8 fad sol fad |
  mi4 si si la |
  sol\trill sol' fad'\trill mi' |
  si2 r |
  r mi8 fad sol fad |
  mi4 si si la |
  sol4\trill fad8 mi si4 si, |
}
%% rondeau
\keepWithTag #'() \rondeau
mi2-\origVersion\fermata r |
r mi'8 fad' sol' fad' |
mi'4 si do'\trill si |
do'\trill si mi'8 fad' sol' fad' |
mi' fad' sol' fad' mi' fad' sol' fad' |
mi'4 si mi'8 fad' sol' fad' |
mi' fad' sol' fad' mi' fad' sol' fad' |
mi'4 sol' fad' red'\trill |
mi' si
\origVersion { r2 | \custosNote s1 }
\modVersion\rondeau
mi2-\origVersion\fermata \cesureInstr r4 mi' |
mi'2 red' |
re'! dod' |
do'! si |
si4 sol' sol' sol' |
sol' mi' mi8 fad sol fad |
mi4 sol' sol' sol' |
sol' mi' mi8 fad sol fad |
mi fad sol la si la sol fad |
sol la si la sol fad mi red |
mi red mi fad sol fad mi fad |
sol fad mi fad sol4 fad8 mi |
si2 
\origVersion { r2 | \custosNote s1 }
\modVersion\rondeau
mi2-\origVersion\fermata r4 si |
red'2 r4 si |
mi'2 r4 si |
fad'2 r4 si |
sol' mi mi mi |
mi mi mi mi |
mi mi mi mi |
mi mi mi mi |
mi1~ |
mi2 mi'4 fad' |
si8 do' la si sol la si la |
sol la si la sol la si la |
sol la si la sol la si la |
sol4 sol' fad'\trill mi' |
si2 r |
r mi8 fad sol fad |
mi fad sol la si do' la si |
sol si la sol fad4 si |
mi2 \cesureInstr r |
r mi8\doux fad sol fad |
mi fad sol la si do' la si |
sol si la sol fad4 si |
mi2

