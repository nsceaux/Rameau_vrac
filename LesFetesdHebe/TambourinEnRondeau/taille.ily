\clef "taille"
\setMusic #'rondeau {
  si2 |
  si si |
  si4 la sol8 fad' mi' red' |
  mi'4 si' la' sol' |
  fad'2\trill si |
  si si |
  si4 la sol8 fad' mi' red' |
  mi'4 si si si |
}
%% rondeau
\keepWithTag #'() \rondeau
si2-\origVersion\fermata si |
si4 mi' mi'2 |
mi' mi' |
mi' sol'8 la' si' la' |
sol' la' si' do'' si' la' si' la' |
sol'4 red' mi' si |
sol'8 la' si' do'' si' la' si' la' |
sol'4 si si si |
si2
\origVersion { si2 | \custosNote si2 s }
\modVersion\rondeau
si2-\origVersion\fermata \cesureInstr r4 mi' |
\appoggiatura re'8 dod'2 do' |
si la |
la red' |
mi'4 si si si |
si si sol'8 la' si' la' |
sol'4 si si si |
si si sol'8 la' si' la' |
sol' la' si' la' sol' fad' mi' red' |
mi'2 si |
si si |
si si |
si
\origVersion { si2 | \custosNote si2 s }
\modVersion\rondeau
\origVersion <>_"finale"
si2-\origVersion\fermata r |
si r |
si r |
si r |
si4 si si si |
si si si si |
si si si si |
si si si si |
si1~ |
si~ |
si~ |
si |
si2. mi'8 red' |
mi'4 si' la' sol' |
fad'2\trill sol8 la si la |
sol la sol fad mi fad sol la |
si4 la sol8 la si do' |
si2. si4 |
si2 \cesureInstr sol8\doux la si la |
sol la sol fad mi fad sol la |
si4 la sol8 la si do' |
si2. si4 |
si2

