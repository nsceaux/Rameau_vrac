\clef "basse"
\setMusic #'rondeau {
  mi,2 | \ru#7 { mi, mi, | }
}
%% rondeau
\keepWithTag #'() \rondeau
mi,2-\origVersion\fermata mi, | \ru#7 { mi, mi, | } mi,
\origVersion { mi,2 | \custosNote mi,2 s }
\modVersion\rondeau
mi,2-\origVersion\fermata \cesureInstr mi, |
mi, mi, | mi, mi, | mi, mi, |
mi,4 mi mi mi |
mi mi, mi8 fad sol fad |
mi4 mi mi mi |
mi mi, mi8 fad sol fad |
mi2 mi, | mi, mi, | mi, mi, | mi, mi, | mi,
\origVersion { mi,2 | \custosNote mi,2 s }
\modVersion\rondeau
mi,2-\origVersion\fermata r |
mi, r |
mi, r |
mi, r |
mi,4 mi, mi, mi, |
mi, mi, mi, mi, |
mi, mi, mi, mi, |
mi, mi, mi, mi, |
mi,1~ |
mi,~ |
mi,~ |
mi,~ |
mi, |
mi,4 mi, mi, mi, |
mi,2 mi, | mi, mi, | mi, mi, | mi, mi, | mi, \cesureInstr
mi,\doux | mi, mi, | mi, mi, | mi, mi, | mi,
