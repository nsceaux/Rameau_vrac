\key mi \minor
\digitTime\time 2/2 \midiTempo #180 \partial 2
s2 s1*7 s2 \bar ":|."
\beginMarkSmall\markup\vcenter {
  \musicglyph#"scripts.segno" \line { \concat { P \super re } Reprise }
}
s2 s1*7 s2
\bar "||" \beginMarkSmall\markup [rondeau]
s2 s1*7 s4
\beginMarkSmall\markup { \concat { 2 \super e } Reprise }
s2. s1*11 s2
\bar "||" \beginMarkSmall\markup [rondeau]
s2 s1*7 s4
\beginMarkSmall\markup { \concat { 3 \super e } Reprise }
s2. s1*21 s2 \bar "|."
\dalSegnoMark